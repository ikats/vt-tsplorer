(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/css/d3-context-menu.css":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".d3-context-menu{position:absolute;min-width:150px;z-index:1200}.d3-context-menu ul,.d3-context-menu ul li{margin:0;padding:0}.d3-context-menu ul{list-style-type:none;cursor:default}.d3-context-menu ul li{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.d3-context-menu ul li.is-disabled,.d3-context-menu ul li.is-disabled:hover{cursor:not-allowed}.d3-context-menu ul li.is-divider{padding:0}.d3-context-menu-theme{background-color:#f2f2f2;border-radius:4px;font-family:Arial,sans-serif;font-size:14px;border:1px solid #d4d4d4}.d3-context-menu-theme ul{margin:4px 0}.d3-context-menu-theme ul li{padding:4px 16px}.d3-context-menu-theme ul li:hover{background-color:#4677f8;color:#fefefe}.d3-context-menu-theme ul li.is-header,.d3-context-menu-theme ul li.is-header:hover{background-color:#f2f2f2;color:#444;font-weight:bold;font-style:italic}.d3-context-menu-theme ul li.is-disabled,.d3-context-menu-theme ul li.is-disabled:hover{background-color:#f2f2f2;color:#888}.d3-context-menu-theme ul li.is-divider:hover{background-color:#f2f2f2}.d3-context-menu-theme ul hr{border:0;height:0;border-top:1px solid rgba(0,0,0,0.1);border-bottom:1px solid rgba(255,255,255,0.3)}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js":[function(require,module,exports){
(function (global){
(function(root, factory) {
	if (typeof module === 'object' && module.exports) {
		var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);
		module.exports = factory(d3);
	} else if(typeof define === 'function' && define.amd) {
		try {
			var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);
		} catch (e) {
			d3 = root.d3;
		}

		d3.contextMenu = factory(d3);
		define([], function() {
			return d3.contextMenu;
		});
	} else if(root.d3) {
		root.d3.contextMenu = factory(root.d3);
	}
}(this,
	function (d3) {
		var utils = {
			noop: function () {},
			
			/**
			 * @param {*} value
			 * @returns {Boolean}
			 */
			isFn: function (value) {
				return typeof value === 'function';
			},

			/**
			 * @param {*} value
			 * @returns {Function}
			 */
			const: function (value) {
				return function () { return value; };
			},

			/**
			 * @param {Function|*} value
			 * @param {*} [fallback]
			 * @returns {Function}
			 */
			toFactory: function (value, fallback) {
				value = (value === undefined) ? fallback : value;
				return utils.isFn(value) ? value : utils.const(value);
			}
		};

		// global state for d3-context-menu
		var d3ContextMenu = null;

		var closeMenu = function () {
			// global state is populated if a menu is currently opened
			if (d3ContextMenu) {
				d3.select('.d3-context-menu').remove();
				d3.select('body').on('mousedown.d3-context-menu', null);
				d3ContextMenu.boundCloseCallback();
				d3ContextMenu = null;
			}
		};

		/**
		 * Calls API method (e.g. `close`) or
		 * returns handler function for the `contextmenu` event
		 * @param {Function|Array|String} menuItems
		 * @param {Function|Object} config
		 * @returns {?Function}
		 */
		return function (menuItems, config) {
			// allow for `d3.contextMenu('close');` calls
			// to programatically close the menu
			if (menuItems === 'close') {
				return closeMenu();
			}

			// for convenience, make `menuItems` a factory
			// and `config` an object
			menuItems = utils.toFactory(menuItems);

			if (utils.isFn(config)) {
				config = { onOpen: config };
			}
			else {
				config = config || {};
			}

			// resolve config
			var openCallback = config.onOpen || utils.noop;
			var closeCallback = config.onClose || utils.noop;
			var positionFactory = utils.toFactory(config.position);
			var themeFactory = utils.toFactory(config.theme, 'd3-context-menu-theme');

			/**
			 * Context menu event handler
			 * @param {*} data
			 * @param {Number} index
			 */
			return function (data, index) {
				var element = this;

				// close any menu that's already opened
				closeMenu();

				// store close callback already bound to the correct args and scope
				d3ContextMenu = {
					boundCloseCallback: closeCallback.bind(element, data, index)
				};

				// create the div element that will hold the context menu
				d3.selectAll('.d3-context-menu').data([1])
					.enter()
					.append('div')
					.attr('class', 'd3-context-menu ' + themeFactory.bind(element)(data, index));

				// close menu on mousedown outside
				d3.select('body').on('mousedown.d3-context-menu', closeMenu);

				var list = d3.selectAll('.d3-context-menu')
					.on('contextmenu', function() {
						closeMenu();
						d3.event.preventDefault();
						d3.event.stopPropagation();
					})
					.append('ul');
				
				list.selectAll('li').data(menuItems.bind(element)(data, index)).enter()
					.append('li')
					.attr('class', function(d) {
						var ret = '';
						if (utils.toFactory(d.divider).bind(element)(data, index)) {
							ret += ' is-divider';
						}
						if (utils.toFactory(d.disabled).bind(element)(data, index)) {
							ret += ' is-disabled';
						}
						if (!d.action) {
							ret += ' is-header';
						}
						return ret;
					})
					.html(function(d) {
						if (utils.toFactory(d.divider).bind(element)(data, index)) {
							return '<hr>';
						}
						if (!d.title) {
							console.error('No title attribute set. Check the spelling of your options.');
						}
						return utils.toFactory(d.title).bind(element)(data, index);
					})
					.on('click', function(d, i) {
						if (utils.toFactory(d.disabled).bind(element)(data, index)) return; // do nothing if disabled
						if (!d.action) return; // headers have no "action"
						d.action.bind(element)(data, index);
						closeMenu();
					});

				// the openCallback allows an action to fire before the menu is displayed
				// an example usage would be closing a tooltip
				if (openCallback.bind(element)(data, index) === false) {
					return;
				}

				// get position
				var position = positionFactory.bind(element)(data, index);

				// display context menu
				d3.select('.d3-context-menu')
					.style('left', (position ? position.left : d3.event.pageX - 2) + 'px')
					.style('top', (position ? position.top : d3.event.pageY - 2) + 'px')
					.style('display', 'block');

				d3.event.preventDefault();
				d3.event.stopPropagation();
			};
		};
	}
));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/node_modules/keymaster/keymaster.js":[function(require,module,exports){
//     keymaster.js
//     (c) 2011-2013 Thomas Fuchs
//     keymaster.js may be freely distributed under the MIT license.

;(function(global){
  var k,
    _handlers = {},
    _mods = { 16: false, 18: false, 17: false, 91: false },
    _scope = 'all',
    // modifier keys
    _MODIFIERS = {
      '⇧': 16, shift: 16,
      '⌥': 18, alt: 18, option: 18,
      '⌃': 17, ctrl: 17, control: 17,
      '⌘': 91, command: 91
    },
    // special keys
    _MAP = {
      backspace: 8, tab: 9, clear: 12,
      enter: 13, 'return': 13,
      esc: 27, escape: 27, space: 32,
      left: 37, up: 38,
      right: 39, down: 40,
      del: 46, 'delete': 46,
      home: 36, end: 35,
      pageup: 33, pagedown: 34,
      ',': 188, '.': 190, '/': 191,
      '`': 192, '-': 189, '=': 187,
      ';': 186, '\'': 222,
      '[': 219, ']': 221, '\\': 220
    },
    code = function(x){
      return _MAP[x] || x.toUpperCase().charCodeAt(0);
    },
    _downKeys = [];

  for(k=1;k<20;k++) _MAP['f'+k] = 111+k;

  // IE doesn't support Array#indexOf, so have a simple replacement
  function index(array, item){
    var i = array.length;
    while(i--) if(array[i]===item) return i;
    return -1;
  }

  // for comparing mods before unassignment
  function compareArray(a1, a2) {
    if (a1.length != a2.length) return false;
    for (var i = 0; i < a1.length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
  }

  var modifierMap = {
      16:'shiftKey',
      18:'altKey',
      17:'ctrlKey',
      91:'metaKey'
  };
  function updateModifierKey(event) {
      for(k in _mods) _mods[k] = event[modifierMap[k]];
  };

  // handle keydown event
  function dispatch(event) {
    var key, handler, k, i, modifiersMatch, scope;
    key = event.keyCode;

    if (index(_downKeys, key) == -1) {
        _downKeys.push(key);
    }

    // if a modifier key, set the key.<modifierkeyname> property to true and return
    if(key == 93 || key == 224) key = 91; // right command on webkit, command on Gecko
    if(key in _mods) {
      _mods[key] = true;
      // 'assignKey' from inside this closure is exported to window.key
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = true;
      return;
    }
    updateModifierKey(event);

    // see if we need to ignore the keypress (filter() can can be overridden)
    // by default ignore key presses if a select, textarea, or input is focused
    if(!assignKey.filter.call(this, event)) return;

    // abort if no potentially matching shortcuts found
    if (!(key in _handlers)) return;

    scope = getScope();

    // for each potential shortcut
    for (i = 0; i < _handlers[key].length; i++) {
      handler = _handlers[key][i];

      // see if it's in the current scope
      if(handler.scope == scope || handler.scope == 'all'){
        // check if modifiers match if any
        modifiersMatch = handler.mods.length > 0;
        for(k in _mods)
          if((!_mods[k] && index(handler.mods, +k) > -1) ||
            (_mods[k] && index(handler.mods, +k) == -1)) modifiersMatch = false;
        // call the handler and stop the event if neccessary
        if((handler.mods.length == 0 && !_mods[16] && !_mods[18] && !_mods[17] && !_mods[91]) || modifiersMatch){
          if(handler.method(event, handler)===false){
            if(event.preventDefault) event.preventDefault();
              else event.returnValue = false;
            if(event.stopPropagation) event.stopPropagation();
            if(event.cancelBubble) event.cancelBubble = true;
          }
        }
      }
    }
  };

  // unset modifier keys on keyup
  function clearModifier(event){
    var key = event.keyCode, k,
        i = index(_downKeys, key);

    // remove key from _downKeys
    if (i >= 0) {
        _downKeys.splice(i, 1);
    }

    if(key == 93 || key == 224) key = 91;
    if(key in _mods) {
      _mods[key] = false;
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = false;
    }
  };

  function resetModifiers() {
    for(k in _mods) _mods[k] = false;
    for(k in _MODIFIERS) assignKey[k] = false;
  };

  // parse and assign shortcut
  function assignKey(key, scope, method){
    var keys, mods;
    keys = getKeys(key);
    if (method === undefined) {
      method = scope;
      scope = 'all';
    }

    // for each shortcut
    for (var i = 0; i < keys.length; i++) {
      // set modifier keys if any
      mods = [];
      key = keys[i].split('+');
      if (key.length > 1){
        mods = getMods(key);
        key = [key[key.length-1]];
      }
      // convert to keycode and...
      key = key[0]
      key = code(key);
      // ...store handler
      if (!(key in _handlers)) _handlers[key] = [];
      _handlers[key].push({ shortcut: keys[i], scope: scope, method: method, key: keys[i], mods: mods });
    }
  };

  // unbind all handlers for given key in current scope
  function unbindKey(key, scope) {
    var multipleKeys, keys,
      mods = [],
      i, j, obj;

    multipleKeys = getKeys(key);

    for (j = 0; j < multipleKeys.length; j++) {
      keys = multipleKeys[j].split('+');

      if (keys.length > 1) {
        mods = getMods(keys);
        key = keys[keys.length - 1];
      }

      key = code(key);

      if (scope === undefined) {
        scope = getScope();
      }
      if (!_handlers[key]) {
        return;
      }
      for (i = 0; i < _handlers[key].length; i++) {
        obj = _handlers[key][i];
        // only clear handlers if correct scope and mods match
        if (obj.scope === scope && compareArray(obj.mods, mods)) {
          _handlers[key][i] = {};
        }
      }
    }
  };

  // Returns true if the key with code 'keyCode' is currently down
  // Converts strings into key codes.
  function isPressed(keyCode) {
      if (typeof(keyCode)=='string') {
        keyCode = code(keyCode);
      }
      return index(_downKeys, keyCode) != -1;
  }

  function getPressedKeyCodes() {
      return _downKeys.slice(0);
  }

  function filter(event){
    var tagName = (event.target || event.srcElement).tagName;
    // ignore keypressed in any elements that support keyboard data input
    return !(tagName == 'INPUT' || tagName == 'SELECT' || tagName == 'TEXTAREA');
  }

  // initialize key.<modifier> to false
  for(k in _MODIFIERS) assignKey[k] = false;

  // set current scope (default 'all')
  function setScope(scope){ _scope = scope || 'all' };
  function getScope(){ return _scope || 'all' };

  // delete all handlers for a given scope
  function deleteScope(scope){
    var key, handlers, i;

    for (key in _handlers) {
      handlers = _handlers[key];
      for (i = 0; i < handlers.length; ) {
        if (handlers[i].scope === scope) handlers.splice(i, 1);
        else i++;
      }
    }
  };

  // abstract key logic for assign and unassign
  function getKeys(key) {
    var keys;
    key = key.replace(/\s/g, '');
    keys = key.split(',');
    if ((keys[keys.length - 1]) == '') {
      keys[keys.length - 2] += ',';
    }
    return keys;
  }

  // abstract mods logic for assign and unassign
  function getMods(key) {
    var mods = key.slice(0, key.length - 1);
    for (var mi = 0; mi < mods.length; mi++)
    mods[mi] = _MODIFIERS[mods[mi]];
    return mods;
  }

  // cross-browser events
  function addEvent(object, event, method) {
    if (object.addEventListener)
      object.addEventListener(event, method, false);
    else if(object.attachEvent)
      object.attachEvent('on'+event, function(){ method(window.event) });
  };

  // set the handlers globally on document
  addEvent(document, 'keydown', function(event) { dispatch(event) }); // Passing _scope to a callback to ensure it remains the same by execution. Fixes #48
  addEvent(document, 'keyup', clearModifier);

  // reset modifiers to false whenever the window is (re)focused.
  addEvent(window, 'focus', resetModifiers);

  // store previously defined key
  var previousKey = global.key;

  // restore previously defined key and return reference to our key object
  function noConflict() {
    var k = global.key;
    global.key = previousKey;
    return k;
  }

  // set window.key and window.key.set/get/deleteScope, and the default filter
  global.key = assignKey;
  global.key.setScope = setScope;
  global.key.getScope = getScope;
  global.key.deleteScope = deleteScope;
  global.key.filter = filter;
  global.key.isPressed = isPressed;
  global.key.getPressedKeyCodes = getPressedKeyCodes;
  global.key.noConflict = noConflict;
  global.key.unbind = unbindKey;

  if(typeof module !== 'undefined') module.exports = assignKey;

})(this);

},{}],"/home/davisp/projects/ikats_lig/hmi/node_modules/resize-observer-polyfill/dist/ResizeObserver.global.js":[function(require,module,exports){
(function (global){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (global, factory) {
    (typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : global.ResizeObserver = factory();
})(undefined, function () {
    'use strict';

    /**
     * A collection of shims that provide minimal functionality of the ES6 collections.
     *
     * These implementations are not meant to be used outside of the ResizeObserver
     * modules as they cover only a limited range of use cases.
     */
    /* eslint-disable require-jsdoc, valid-jsdoc */

    var MapShim = function () {
        if (typeof Map !== 'undefined') {
            return Map;
        }

        /**
         * Returns index in provided array that matches the specified key.
         *
         * @param {Array<Array>} arr
         * @param {*} key
         * @returns {number}
         */
        function getIndex(arr, key) {
            var result = -1;

            arr.some(function (entry, index) {
                if (entry[0] === key) {
                    result = index;

                    return true;
                }

                return false;
            });

            return result;
        }

        return function () {
            function anonymous() {
                this.__entries__ = [];
            }

            var prototypeAccessors = { size: { configurable: true } };

            /**
             * @returns {boolean}
             */
            prototypeAccessors.size.get = function () {
                return this.__entries__.length;
            };

            /**
             * @param {*} key
             * @returns {*}
             */
            anonymous.prototype.get = function (key) {
                var index = getIndex(this.__entries__, key);
                var entry = this.__entries__[index];

                return entry && entry[1];
            };

            /**
             * @param {*} key
             * @param {*} value
             * @returns {void}
             */
            anonymous.prototype.set = function (key, value) {
                var index = getIndex(this.__entries__, key);

                if (~index) {
                    this.__entries__[index][1] = value;
                } else {
                    this.__entries__.push([key, value]);
                }
            };

            /**
             * @param {*} key
             * @returns {void}
             */
            anonymous.prototype.delete = function (key) {
                var entries = this.__entries__;
                var index = getIndex(entries, key);

                if (~index) {
                    entries.splice(index, 1);
                }
            };

            /**
             * @param {*} key
             * @returns {void}
             */
            anonymous.prototype.has = function (key) {
                return !!~getIndex(this.__entries__, key);
            };

            /**
             * @returns {void}
             */
            anonymous.prototype.clear = function () {
                this.__entries__.splice(0);
            };

            /**
             * @param {Function} callback
             * @param {*} [ctx=null]
             * @returns {void}
             */
            anonymous.prototype.forEach = function (callback, ctx) {
                var this$1 = this;
                if (ctx === void 0) ctx = null;

                for (var i = 0, list = this$1.__entries__; i < list.length; i += 1) {
                    var entry = list[i];

                    callback.call(ctx, entry[1], entry[0]);
                }
            };

            Object.defineProperties(anonymous.prototype, prototypeAccessors);

            return anonymous;
        }();
    }();

    /**
     * Detects whether window and document objects are available in current environment.
     */
    var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined' && window.document === document;

    // Returns global object of a current environment.
    var global$1 = function () {
        if (typeof global !== 'undefined' && global.Math === Math) {
            return global;
        }

        if (typeof self !== 'undefined' && self.Math === Math) {
            return self;
        }

        if (typeof window !== 'undefined' && window.Math === Math) {
            return window;
        }

        // eslint-disable-next-line no-new-func
        return Function('return this')();
    }();

    /**
     * A shim for the requestAnimationFrame which falls back to the setTimeout if
     * first one is not supported.
     *
     * @returns {number} Requests' identifier.
     */
    var requestAnimationFrame$1 = function () {
        if (typeof requestAnimationFrame === 'function') {
            // It's required to use a bounded function because IE sometimes throws
            // an "Invalid calling object" error if rAF is invoked without the global
            // object on the left hand side.
            return requestAnimationFrame.bind(global$1);
        }

        return function (callback) {
            return setTimeout(function () {
                return callback(Date.now());
            }, 1000 / 60);
        };
    }();

    // Defines minimum timeout before adding a trailing call.
    var trailingTimeout = 2;

    /**
     * Creates a wrapper function which ensures that provided callback will be
     * invoked only once during the specified delay period.
     *
     * @param {Function} callback - Function to be invoked after the delay period.
     * @param {number} delay - Delay after which to invoke callback.
     * @returns {Function}
     */
    var throttle = function throttle(callback, delay) {
        var leadingCall = false,
            trailingCall = false,
            lastCallTime = 0;

        /**
         * Invokes the original callback function and schedules new invocation if
         * the "proxy" was called during current request.
         *
         * @returns {void}
         */
        function resolvePending() {
            if (leadingCall) {
                leadingCall = false;

                callback();
            }

            if (trailingCall) {
                proxy();
            }
        }

        /**
         * Callback invoked after the specified delay. It will further postpone
         * invocation of the original function delegating it to the
         * requestAnimationFrame.
         *
         * @returns {void}
         */
        function timeoutCallback() {
            requestAnimationFrame$1(resolvePending);
        }

        /**
         * Schedules invocation of the original function.
         *
         * @returns {void}
         */
        function proxy() {
            var timeStamp = Date.now();

            if (leadingCall) {
                // Reject immediately following calls.
                if (timeStamp - lastCallTime < trailingTimeout) {
                    return;
                }

                // Schedule new call to be in invoked when the pending one is resolved.
                // This is important for "transitions" which never actually start
                // immediately so there is a chance that we might miss one if change
                // happens amids the pending invocation.
                trailingCall = true;
            } else {
                leadingCall = true;
                trailingCall = false;

                setTimeout(timeoutCallback, delay);
            }

            lastCallTime = timeStamp;
        }

        return proxy;
    };

    // Minimum delay before invoking the update of observers.
    var REFRESH_DELAY = 20;

    // A list of substrings of CSS properties used to find transition events that
    // might affect dimensions of observed elements.
    var transitionKeys = ['top', 'right', 'bottom', 'left', 'width', 'height', 'size', 'weight'];

    // Check if MutationObserver is available.
    var mutationObserverSupported = typeof MutationObserver !== 'undefined';

    /**
     * Singleton controller class which handles updates of ResizeObserver instances.
     */
    var ResizeObserverController = function ResizeObserverController() {
        this.connected_ = false;
        this.mutationEventsAdded_ = false;
        this.mutationsObserver_ = null;
        this.observers_ = [];

        this.onTransitionEnd_ = this.onTransitionEnd_.bind(this);
        this.refresh = throttle(this.refresh.bind(this), REFRESH_DELAY);
    };

    /**
     * Adds observer to observers list.
     *
     * @param {ResizeObserverSPI} observer - Observer to be added.
     * @returns {void}
     */

    /**
     * Holds reference to the controller's instance.
     *
     * @private {ResizeObserverController}
     */

    /**
     * Keeps reference to the instance of MutationObserver.
     *
     * @private {MutationObserver}
     */

    /**
     * Indicates whether DOM listeners have been added.
     *
     * @private {boolean}
     */
    ResizeObserverController.prototype.addObserver = function (observer) {
        if (!~this.observers_.indexOf(observer)) {
            this.observers_.push(observer);
        }

        // Add listeners if they haven't been added yet.
        if (!this.connected_) {
            this.connect_();
        }
    };

    /**
     * Removes observer from observers list.
     *
     * @param {ResizeObserverSPI} observer - Observer to be removed.
     * @returns {void}
     */
    ResizeObserverController.prototype.removeObserver = function (observer) {
        var observers = this.observers_;
        var index = observers.indexOf(observer);

        // Remove observer if it's present in registry.
        if (~index) {
            observers.splice(index, 1);
        }

        // Remove listeners if controller has no connected observers.
        if (!observers.length && this.connected_) {
            this.disconnect_();
        }
    };

    /**
     * Invokes the update of observers. It will continue running updates insofar
     * it detects changes.
     *
     * @returns {void}
     */
    ResizeObserverController.prototype.refresh = function () {
        var changesDetected = this.updateObservers_();

        // Continue running updates if changes have been detected as there might
        // be future ones caused by CSS transitions.
        if (changesDetected) {
            this.refresh();
        }
    };

    /**
     * Updates every observer from observers list and notifies them of queued
     * entries.
     *
     * @private
     * @returns {boolean} Returns "true" if any observer has detected changes in
     *  dimensions of it's elements.
     */
    ResizeObserverController.prototype.updateObservers_ = function () {
        // Collect observers that have active observations.
        var activeObservers = this.observers_.filter(function (observer) {
            return observer.gatherActive(), observer.hasActive();
        });

        // Deliver notifications in a separate cycle in order to avoid any
        // collisions between observers, e.g. when multiple instances of
        // ResizeObserver are tracking the same element and the callback of one
        // of them changes content dimensions of the observed target. Sometimes
        // this may result in notifications being blocked for the rest of observers.
        activeObservers.forEach(function (observer) {
            return observer.broadcastActive();
        });

        return activeObservers.length > 0;
    };

    /**
     * Initializes DOM listeners.
     *
     * @private
     * @returns {void}
     */
    ResizeObserverController.prototype.connect_ = function () {
        // Do nothing if running in a non-browser environment or if listeners
        // have been already added.
        if (!isBrowser || this.connected_) {
            return;
        }

        // Subscription to the "Transitionend" event is used as a workaround for
        // delayed transitions. This way it's possible to capture at least the
        // final state of an element.
        document.addEventListener('transitionend', this.onTransitionEnd_);

        window.addEventListener('resize', this.refresh);

        if (mutationObserverSupported) {
            this.mutationsObserver_ = new MutationObserver(this.refresh);

            this.mutationsObserver_.observe(document, {
                attributes: true,
                childList: true,
                characterData: true,
                subtree: true
            });
        } else {
            document.addEventListener('DOMSubtreeModified', this.refresh);

            this.mutationEventsAdded_ = true;
        }

        this.connected_ = true;
    };

    /**
     * Removes DOM listeners.
     *
     * @private
     * @returns {void}
     */
    ResizeObserverController.prototype.disconnect_ = function () {
        // Do nothing if running in a non-browser environment or if listeners
        // have been already removed.
        if (!isBrowser || !this.connected_) {
            return;
        }

        document.removeEventListener('transitionend', this.onTransitionEnd_);
        window.removeEventListener('resize', this.refresh);

        if (this.mutationsObserver_) {
            this.mutationsObserver_.disconnect();
        }

        if (this.mutationEventsAdded_) {
            document.removeEventListener('DOMSubtreeModified', this.refresh);
        }

        this.mutationsObserver_ = null;
        this.mutationEventsAdded_ = false;
        this.connected_ = false;
    };

    /**
     * "Transitionend" event handler.
     *
     * @private
     * @param {TransitionEvent} event
     * @returns {void}
     */
    ResizeObserverController.prototype.onTransitionEnd_ = function (ref) {
        var propertyName = ref.propertyName;if (propertyName === void 0) propertyName = '';

        // Detect whether transition may affect dimensions of an element.
        var isReflowProperty = transitionKeys.some(function (key) {
            return !!~propertyName.indexOf(key);
        });

        if (isReflowProperty) {
            this.refresh();
        }
    };

    /**
     * Returns instance of the ResizeObserverController.
     *
     * @returns {ResizeObserverController}
     */
    ResizeObserverController.getInstance = function () {
        if (!this.instance_) {
            this.instance_ = new ResizeObserverController();
        }

        return this.instance_;
    };

    ResizeObserverController.instance_ = null;

    /**
     * Defines non-writable/enumerable properties of the provided target object.
     *
     * @param {Object} target - Object for which to define properties.
     * @param {Object} props - Properties to be defined.
     * @returns {Object} Target object.
     */
    var defineConfigurable = function defineConfigurable(target, props) {
        for (var i = 0, list = Object.keys(props); i < list.length; i += 1) {
            var key = list[i];

            Object.defineProperty(target, key, {
                value: props[key],
                enumerable: false,
                writable: false,
                configurable: true
            });
        }

        return target;
    };

    /**
     * Returns the global object associated with provided element.
     *
     * @param {Object} target
     * @returns {Object}
     */
    var getWindowOf = function getWindowOf(target) {
        // Assume that the element is an instance of Node, which means that it
        // has the "ownerDocument" property from which we can retrieve a
        // corresponding global object.
        var ownerGlobal = target && target.ownerDocument && target.ownerDocument.defaultView;

        // Return the local global object if it's not possible extract one from
        // provided element.
        return ownerGlobal || global$1;
    };

    // Placeholder of an empty content rectangle.
    var emptyRect = createRectInit(0, 0, 0, 0);

    /**
     * Converts provided string to a number.
     *
     * @param {number|string} value
     * @returns {number}
     */
    function toFloat(value) {
        return parseFloat(value) || 0;
    }

    /**
     * Extracts borders size from provided styles.
     *
     * @param {CSSStyleDeclaration} styles
     * @param {...string} positions - Borders positions (top, right, ...)
     * @returns {number}
     */
    function getBordersSize(styles) {
        var positions = [],
            len = arguments.length - 1;
        while (len-- > 0) {
            positions[len] = arguments[len + 1];
        }return positions.reduce(function (size, position) {
            var value = styles['border-' + position + '-width'];

            return size + toFloat(value);
        }, 0);
    }

    /**
     * Extracts paddings sizes from provided styles.
     *
     * @param {CSSStyleDeclaration} styles
     * @returns {Object} Paddings box.
     */
    function getPaddings(styles) {
        var positions = ['top', 'right', 'bottom', 'left'];
        var paddings = {};

        for (var i = 0, list = positions; i < list.length; i += 1) {
            var position = list[i];

            var value = styles['padding-' + position];

            paddings[position] = toFloat(value);
        }

        return paddings;
    }

    /**
     * Calculates content rectangle of provided SVG element.
     *
     * @param {SVGGraphicsElement} target - Element content rectangle of which needs
     *      to be calculated.
     * @returns {DOMRectInit}
     */
    function getSVGContentRect(target) {
        var bbox = target.getBBox();

        return createRectInit(0, 0, bbox.width, bbox.height);
    }

    /**
     * Calculates content rectangle of provided HTMLElement.
     *
     * @param {HTMLElement} target - Element for which to calculate the content rectangle.
     * @returns {DOMRectInit}
     */
    function getHTMLElementContentRect(target) {
        // Client width & height properties can't be
        // used exclusively as they provide rounded values.
        var clientWidth = target.clientWidth;
        var clientHeight = target.clientHeight;

        // By this condition we can catch all non-replaced inline, hidden and
        // detached elements. Though elements with width & height properties less
        // than 0.5 will be discarded as well.
        //
        // Without it we would need to implement separate methods for each of
        // those cases and it's not possible to perform a precise and performance
        // effective test for hidden elements. E.g. even jQuery's ':visible' filter
        // gives wrong results for elements with width & height less than 0.5.
        if (!clientWidth && !clientHeight) {
            return emptyRect;
        }

        var styles = getWindowOf(target).getComputedStyle(target);
        var paddings = getPaddings(styles);
        var horizPad = paddings.left + paddings.right;
        var vertPad = paddings.top + paddings.bottom;

        // Computed styles of width & height are being used because they are the
        // only dimensions available to JS that contain non-rounded values. It could
        // be possible to utilize the getBoundingClientRect if only it's data wasn't
        // affected by CSS transformations let alone paddings, borders and scroll bars.
        var width = toFloat(styles.width),
            height = toFloat(styles.height);

        // Width & height include paddings and borders when the 'border-box' box
        // model is applied (except for IE).
        if (styles.boxSizing === 'border-box') {
            // Following conditions are required to handle Internet Explorer which
            // doesn't include paddings and borders to computed CSS dimensions.
            //
            // We can say that if CSS dimensions + paddings are equal to the "client"
            // properties then it's either IE, and thus we don't need to subtract
            // anything, or an element merely doesn't have paddings/borders styles.
            if (Math.round(width + horizPad) !== clientWidth) {
                width -= getBordersSize(styles, 'left', 'right') + horizPad;
            }

            if (Math.round(height + vertPad) !== clientHeight) {
                height -= getBordersSize(styles, 'top', 'bottom') + vertPad;
            }
        }

        // Following steps can't be applied to the document's root element as its
        // client[Width/Height] properties represent viewport area of the window.
        // Besides, it's as well not necessary as the <html> itself neither has
        // rendered scroll bars nor it can be clipped.
        if (!isDocumentElement(target)) {
            // In some browsers (only in Firefox, actually) CSS width & height
            // include scroll bars size which can be removed at this step as scroll
            // bars are the only difference between rounded dimensions + paddings
            // and "client" properties, though that is not always true in Chrome.
            var vertScrollbar = Math.round(width + horizPad) - clientWidth;
            var horizScrollbar = Math.round(height + vertPad) - clientHeight;

            // Chrome has a rather weird rounding of "client" properties.
            // E.g. for an element with content width of 314.2px it sometimes gives
            // the client width of 315px and for the width of 314.7px it may give
            // 314px. And it doesn't happen all the time. So just ignore this delta
            // as a non-relevant.
            if (Math.abs(vertScrollbar) !== 1) {
                width -= vertScrollbar;
            }

            if (Math.abs(horizScrollbar) !== 1) {
                height -= horizScrollbar;
            }
        }

        return createRectInit(paddings.left, paddings.top, width, height);
    }

    /**
     * Checks whether provided element is an instance of the SVGGraphicsElement.
     *
     * @param {Element} target - Element to be checked.
     * @returns {boolean}
     */
    var isSVGGraphicsElement = function () {
        // Some browsers, namely IE and Edge, don't have the SVGGraphicsElement
        // interface.
        if (typeof SVGGraphicsElement !== 'undefined') {
            return function (target) {
                return target instanceof getWindowOf(target).SVGGraphicsElement;
            };
        }

        // If it's so, then check that element is at least an instance of the
        // SVGElement and that it has the "getBBox" method.
        // eslint-disable-next-line no-extra-parens
        return function (target) {
            return target instanceof getWindowOf(target).SVGElement && typeof target.getBBox === 'function';
        };
    }();

    /**
     * Checks whether provided element is a document element (<html>).
     *
     * @param {Element} target - Element to be checked.
     * @returns {boolean}
     */
    function isDocumentElement(target) {
        return target === getWindowOf(target).document.documentElement;
    }

    /**
     * Calculates an appropriate content rectangle for provided html or svg element.
     *
     * @param {Element} target - Element content rectangle of which needs to be calculated.
     * @returns {DOMRectInit}
     */
    function getContentRect(target) {
        if (!isBrowser) {
            return emptyRect;
        }

        if (isSVGGraphicsElement(target)) {
            return getSVGContentRect(target);
        }

        return getHTMLElementContentRect(target);
    }

    /**
     * Creates rectangle with an interface of the DOMRectReadOnly.
     * Spec: https://drafts.fxtf.org/geometry/#domrectreadonly
     *
     * @param {DOMRectInit} rectInit - Object with rectangle's x/y coordinates and dimensions.
     * @returns {DOMRectReadOnly}
     */
    function createReadOnlyRect(ref) {
        var x = ref.x;
        var y = ref.y;
        var width = ref.width;
        var height = ref.height;

        // If DOMRectReadOnly is available use it as a prototype for the rectangle.
        var Constr = typeof DOMRectReadOnly !== 'undefined' ? DOMRectReadOnly : Object;
        var rect = Object.create(Constr.prototype);

        // Rectangle's properties are not writable and non-enumerable.
        defineConfigurable(rect, {
            x: x, y: y, width: width, height: height,
            top: y,
            right: x + width,
            bottom: height + y,
            left: x
        });

        return rect;
    }

    /**
     * Creates DOMRectInit object based on the provided dimensions and the x/y coordinates.
     * Spec: https://drafts.fxtf.org/geometry/#dictdef-domrectinit
     *
     * @param {number} x - X coordinate.
     * @param {number} y - Y coordinate.
     * @param {number} width - Rectangle's width.
     * @param {number} height - Rectangle's height.
     * @returns {DOMRectInit}
     */
    function createRectInit(x, y, width, height) {
        return { x: x, y: y, width: width, height: height };
    }

    /**
     * Class that is responsible for computations of the content rectangle of
     * provided DOM element and for keeping track of it's changes.
     */
    var ResizeObservation = function ResizeObservation(target) {
        this.broadcastWidth = 0;
        this.broadcastHeight = 0;
        this.contentRect_ = createRectInit(0, 0, 0, 0);

        this.target = target;
    };

    /**
     * Updates content rectangle and tells whether it's width or height properties
     * have changed since the last broadcast.
     *
     * @returns {boolean}
     */

    /**
     * Reference to the last observed content rectangle.
     *
     * @private {DOMRectInit}
     */

    /**
     * Broadcasted width of content rectangle.
     *
     * @type {number}
     */
    ResizeObservation.prototype.isActive = function () {
        var rect = getContentRect(this.target);

        this.contentRect_ = rect;

        return rect.width !== this.broadcastWidth || rect.height !== this.broadcastHeight;
    };

    /**
     * Updates 'broadcastWidth' and 'broadcastHeight' properties with a data
     * from the corresponding properties of the last observed content rectangle.
     *
     * @returns {DOMRectInit} Last observed content rectangle.
     */
    ResizeObservation.prototype.broadcastRect = function () {
        var rect = this.contentRect_;

        this.broadcastWidth = rect.width;
        this.broadcastHeight = rect.height;

        return rect;
    };

    var ResizeObserverEntry = function ResizeObserverEntry(target, rectInit) {
        var contentRect = createReadOnlyRect(rectInit);

        // According to the specification following properties are not writable
        // and are also not enumerable in the native implementation.
        //
        // Property accessors are not being used as they'd require to define a
        // private WeakMap storage which may cause memory leaks in browsers that
        // don't support this type of collections.
        defineConfigurable(this, { target: target, contentRect: contentRect });
    };

    var ResizeObserverSPI = function ResizeObserverSPI(callback, controller, callbackCtx) {
        this.activeObservations_ = [];
        this.observations_ = new MapShim();

        if (typeof callback !== 'function') {
            throw new TypeError('The callback provided as parameter 1 is not a function.');
        }

        this.callback_ = callback;
        this.controller_ = controller;
        this.callbackCtx_ = callbackCtx;
    };

    /**
     * Starts observing provided element.
     *
     * @param {Element} target - Element to be observed.
     * @returns {void}
     */

    /**
     * Registry of the ResizeObservation instances.
     *
     * @private {Map<Element, ResizeObservation>}
     */

    /**
     * Public ResizeObserver instance which will be passed to the callback
     * function and used as a value of it's "this" binding.
     *
     * @private {ResizeObserver}
     */

    /**
     * Collection of resize observations that have detected changes in dimensions
     * of elements.
     *
     * @private {Array<ResizeObservation>}
     */
    ResizeObserverSPI.prototype.observe = function (target) {
        if (!arguments.length) {
            throw new TypeError('1 argument required, but only 0 present.');
        }

        // Do nothing if current environment doesn't have the Element interface.
        if (typeof Element === 'undefined' || !(Element instanceof Object)) {
            return;
        }

        if (!(target instanceof getWindowOf(target).Element)) {
            throw new TypeError('parameter 1 is not of type "Element".');
        }

        var observations = this.observations_;

        // Do nothing if element is already being observed.
        if (observations.has(target)) {
            return;
        }

        observations.set(target, new ResizeObservation(target));

        this.controller_.addObserver(this);

        // Force the update of observations.
        this.controller_.refresh();
    };

    /**
     * Stops observing provided element.
     *
     * @param {Element} target - Element to stop observing.
     * @returns {void}
     */
    ResizeObserverSPI.prototype.unobserve = function (target) {
        if (!arguments.length) {
            throw new TypeError('1 argument required, but only 0 present.');
        }

        // Do nothing if current environment doesn't have the Element interface.
        if (typeof Element === 'undefined' || !(Element instanceof Object)) {
            return;
        }

        if (!(target instanceof getWindowOf(target).Element)) {
            throw new TypeError('parameter 1 is not of type "Element".');
        }

        var observations = this.observations_;

        // Do nothing if element is not being observed.
        if (!observations.has(target)) {
            return;
        }

        observations.delete(target);

        if (!observations.size) {
            this.controller_.removeObserver(this);
        }
    };

    /**
     * Stops observing all elements.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.disconnect = function () {
        this.clearActive();
        this.observations_.clear();
        this.controller_.removeObserver(this);
    };

    /**
     * Collects observation instances the associated element of which has changed
     * it's content rectangle.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.gatherActive = function () {
        var this$1 = this;

        this.clearActive();

        this.observations_.forEach(function (observation) {
            if (observation.isActive()) {
                this$1.activeObservations_.push(observation);
            }
        });
    };

    /**
     * Invokes initial callback function with a list of ResizeObserverEntry
     * instances collected from active resize observations.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.broadcastActive = function () {
        // Do nothing if observer doesn't have active observations.
        if (!this.hasActive()) {
            return;
        }

        var ctx = this.callbackCtx_;

        // Create ResizeObserverEntry instance for every active observation.
        var entries = this.activeObservations_.map(function (observation) {
            return new ResizeObserverEntry(observation.target, observation.broadcastRect());
        });

        this.callback_.call(ctx, entries, ctx);
        this.clearActive();
    };

    /**
     * Clears the collection of active observations.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.clearActive = function () {
        this.activeObservations_.splice(0);
    };

    /**
     * Tells whether observer has active observations.
     *
     * @returns {boolean}
     */
    ResizeObserverSPI.prototype.hasActive = function () {
        return this.activeObservations_.length > 0;
    };

    // Registry of internal observers. If WeakMap is not available use current shim
    // for the Map collection as it has all required methods and because WeakMap
    // can't be fully polyfilled anyway.
    var observers = typeof WeakMap !== 'undefined' ? new WeakMap() : new MapShim();

    /**
     * ResizeObserver API. Encapsulates the ResizeObserver SPI implementation
     * exposing only those methods and properties that are defined in the spec.
     */
    var ResizeObserver = function ResizeObserver(callback) {
        if (!(this instanceof ResizeObserver)) {
            throw new TypeError('Cannot call a class as a function.');
        }
        if (!arguments.length) {
            throw new TypeError('1 argument required, but only 0 present.');
        }

        var controller = ResizeObserverController.getInstance();
        var observer = new ResizeObserverSPI(callback, controller, this);

        observers.set(this, observer);
    };

    // Expose public methods of ResizeObserver.
    ['observe', 'unobserve', 'disconnect'].forEach(function (method) {
        ResizeObserver.prototype[method] = function () {
            return (ref = observers.get(this))[method].apply(ref, arguments);
            var ref;
        };
    });

    var index = function () {
        // Export existing implementation if available.
        if (typeof global$1.ResizeObserver !== 'undefined') {
            return global$1.ResizeObserver;
        }

        global$1.ResizeObserver = ResizeObserver;

        return ResizeObserver;
    }();

    return index;
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/node_modules/select2/dist/js/select2.js":[function(require,module,exports){
(function (global){
/*!
 * Select2 4.0.6-rc.1
 * https://select2.github.io
 *
 * Released under the MIT license
 * https://github.com/select2/select2/blob/master/LICENSE.md
 */
;(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = function (root, jQuery) {
      if (jQuery === undefined) {
        // require('jQuery') returns a factory that requires window to
        // build a jQuery instance, we normalize how we use modules
        // that require this pattern but the window provided is a noop
        // if it's defined (how jquery works)
        if (typeof window !== 'undefined') {
          jQuery = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null);
        }
        else {
          jQuery = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null)(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
} (function (jQuery) {
  // This is needed so we can catch the AMD loader configuration and use it
  // The inner file should be wrapped (by `banner.start.js`) in a function that
  // returns the AMD loader references.
  var S2 =(function () {
  // Restore the Select2 AMD loader so it can be used
  // Needed mostly in the language files, where the loader is not inserted
  if (jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd) {
    var S2 = jQuery.fn.select2.amd;
  }
var S2;(function () { if (!S2 || !S2.requirejs) {
if (!S2) { S2 = {}; } else { require = S2; }
/**
 * @license almond 0.3.3 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    //Creates a parts array for a relName where first part is plugin ID,
    //second part is resource ID. Assumes relName has already been normalized.
    function makeRelParts(relName) {
        return relName ? splitPrefix(relName) : [];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relParts) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0],
            relResourceName = relParts[1];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relResourceName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relResourceName));
            } else {
                name = normalize(name, relResourceName);
            }
        } else {
            name = normalize(name, relResourceName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, relParts,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;
        relParts = makeRelParts(relName);

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relParts);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, makeRelParts(callback)).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

S2.requirejs = requirejs;S2.require = require;S2.define = define;
}
}());
S2.define("almond", function(){});

/* global jQuery:false, $:false */
S2.define('jquery',[],function () {
  var _$ = jQuery || $;

  if (_$ == null && console && console.error) {
    console.error(
      'Select2: An instance of jQuery or a jQuery-compatible library was not ' +
      'found. Make sure that you are including jQuery before Select2 on your ' +
      'web page.'
    );
  }

  return _$;
});

S2.define('select2/utils',[
  'jquery'
], function ($) {
  var Utils = {};

  Utils.Extend = function (ChildClass, SuperClass) {
    var __hasProp = {}.hasOwnProperty;

    function BaseConstructor () {
      this.constructor = ChildClass;
    }

    for (var key in SuperClass) {
      if (__hasProp.call(SuperClass, key)) {
        ChildClass[key] = SuperClass[key];
      }
    }

    BaseConstructor.prototype = SuperClass.prototype;
    ChildClass.prototype = new BaseConstructor();
    ChildClass.__super__ = SuperClass.prototype;

    return ChildClass;
  };

  function getMethods (theClass) {
    var proto = theClass.prototype;

    var methods = [];

    for (var methodName in proto) {
      var m = proto[methodName];

      if (typeof m !== 'function') {
        continue;
      }

      if (methodName === 'constructor') {
        continue;
      }

      methods.push(methodName);
    }

    return methods;
  }

  Utils.Decorate = function (SuperClass, DecoratorClass) {
    var decoratedMethods = getMethods(DecoratorClass);
    var superMethods = getMethods(SuperClass);

    function DecoratedClass () {
      var unshift = Array.prototype.unshift;

      var argCount = DecoratorClass.prototype.constructor.length;

      var calledConstructor = SuperClass.prototype.constructor;

      if (argCount > 0) {
        unshift.call(arguments, SuperClass.prototype.constructor);

        calledConstructor = DecoratorClass.prototype.constructor;
      }

      calledConstructor.apply(this, arguments);
    }

    DecoratorClass.displayName = SuperClass.displayName;

    function ctr () {
      this.constructor = DecoratedClass;
    }

    DecoratedClass.prototype = new ctr();

    for (var m = 0; m < superMethods.length; m++) {
      var superMethod = superMethods[m];

      DecoratedClass.prototype[superMethod] =
        SuperClass.prototype[superMethod];
    }

    var calledMethod = function (methodName) {
      // Stub out the original method if it's not decorating an actual method
      var originalMethod = function () {};

      if (methodName in DecoratedClass.prototype) {
        originalMethod = DecoratedClass.prototype[methodName];
      }

      var decoratedMethod = DecoratorClass.prototype[methodName];

      return function () {
        var unshift = Array.prototype.unshift;

        unshift.call(arguments, originalMethod);

        return decoratedMethod.apply(this, arguments);
      };
    };

    for (var d = 0; d < decoratedMethods.length; d++) {
      var decoratedMethod = decoratedMethods[d];

      DecoratedClass.prototype[decoratedMethod] = calledMethod(decoratedMethod);
    }

    return DecoratedClass;
  };

  var Observable = function () {
    this.listeners = {};
  };

  Observable.prototype.on = function (event, callback) {
    this.listeners = this.listeners || {};

    if (event in this.listeners) {
      this.listeners[event].push(callback);
    } else {
      this.listeners[event] = [callback];
    }
  };

  Observable.prototype.trigger = function (event) {
    var slice = Array.prototype.slice;
    var params = slice.call(arguments, 1);

    this.listeners = this.listeners || {};

    // Params should always come in as an array
    if (params == null) {
      params = [];
    }

    // If there are no arguments to the event, use a temporary object
    if (params.length === 0) {
      params.push({});
    }

    // Set the `_type` of the first object to the event
    params[0]._type = event;

    if (event in this.listeners) {
      this.invoke(this.listeners[event], slice.call(arguments, 1));
    }

    if ('*' in this.listeners) {
      this.invoke(this.listeners['*'], arguments);
    }
  };

  Observable.prototype.invoke = function (listeners, params) {
    for (var i = 0, len = listeners.length; i < len; i++) {
      listeners[i].apply(this, params);
    }
  };

  Utils.Observable = Observable;

  Utils.generateChars = function (length) {
    var chars = '';

    for (var i = 0; i < length; i++) {
      var randomChar = Math.floor(Math.random() * 36);
      chars += randomChar.toString(36);
    }

    return chars;
  };

  Utils.bind = function (func, context) {
    return function () {
      func.apply(context, arguments);
    };
  };

  Utils._convertData = function (data) {
    for (var originalKey in data) {
      var keys = originalKey.split('-');

      var dataLevel = data;

      if (keys.length === 1) {
        continue;
      }

      for (var k = 0; k < keys.length; k++) {
        var key = keys[k];

        // Lowercase the first letter
        // By default, dash-separated becomes camelCase
        key = key.substring(0, 1).toLowerCase() + key.substring(1);

        if (!(key in dataLevel)) {
          dataLevel[key] = {};
        }

        if (k == keys.length - 1) {
          dataLevel[key] = data[originalKey];
        }

        dataLevel = dataLevel[key];
      }

      delete data[originalKey];
    }

    return data;
  };

  Utils.hasScroll = function (index, el) {
    // Adapted from the function created by @ShadowScripter
    // and adapted by @BillBarry on the Stack Exchange Code Review website.
    // The original code can be found at
    // http://codereview.stackexchange.com/q/13338
    // and was designed to be used with the Sizzle selector engine.

    var $el = $(el);
    var overflowX = el.style.overflowX;
    var overflowY = el.style.overflowY;

    //Check both x and y declarations
    if (overflowX === overflowY &&
        (overflowY === 'hidden' || overflowY === 'visible')) {
      return false;
    }

    if (overflowX === 'scroll' || overflowY === 'scroll') {
      return true;
    }

    return ($el.innerHeight() < el.scrollHeight ||
      $el.innerWidth() < el.scrollWidth);
  };

  Utils.escapeMarkup = function (markup) {
    var replaceMap = {
      '\\': '&#92;',
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      '\'': '&#39;',
      '/': '&#47;'
    };

    // Do not try to escape the markup if it's not a string
    if (typeof markup !== 'string') {
      return markup;
    }

    return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
      return replaceMap[match];
    });
  };

  // Append an array of jQuery nodes to a given element.
  Utils.appendMany = function ($element, $nodes) {
    // jQuery 1.7.x does not support $.fn.append() with an array
    // Fall back to a jQuery object collection using $.fn.add()
    if ($.fn.jquery.substr(0, 3) === '1.7') {
      var $jqNodes = $();

      $.map($nodes, function (node) {
        $jqNodes = $jqNodes.add(node);
      });

      $nodes = $jqNodes;
    }

    $element.append($nodes);
  };

  // Cache objects in Utils.__cache instead of $.data (see #4346)
  Utils.__cache = {};

  var id = 0;
  Utils.GetUniqueElementId = function (element) {
    // Get a unique element Id. If element has no id, 
    // creates a new unique number, stores it in the id 
    // attribute and returns the new id. 
    // If an id already exists, it simply returns it.

    var select2Id = element.getAttribute('data-select2-id');
    if (select2Id == null) {
      // If element has id, use it.
      if (element.id) {
        select2Id = element.id;
        element.setAttribute('data-select2-id', select2Id);
      } else {
        element.setAttribute('data-select2-id', ++id);
        select2Id = id.toString();
      }
    }
    return select2Id;
  };

  Utils.StoreData = function (element, name, value) {
    // Stores an item in the cache for a specified element.
    // name is the cache key.    
    var id = Utils.GetUniqueElementId(element);
    if (!Utils.__cache[id]) {
      Utils.__cache[id] = {};
    }

    Utils.__cache[id][name] = value;
  };

  Utils.GetData = function (element, name) {
    // Retrieves a value from the cache by its key (name)
    // name is optional. If no name specified, return 
    // all cache items for the specified element.
    // and for a specified element.
    var id = Utils.GetUniqueElementId(element);
    if (name) {
      if (Utils.__cache[id]) {
        return Utils.__cache[id][name] != null ? 
	      Utils.__cache[id][name]:
	      $(element).data(name); // Fallback to HTML5 data attribs.
      }
      return $(element).data(name); // Fallback to HTML5 data attribs.
    } else {
      return Utils.__cache[id];			   
    }
  };

  Utils.RemoveData = function (element) {
    // Removes all cached items for a specified element.
    var id = Utils.GetUniqueElementId(element);
    if (Utils.__cache[id] != null) {
      delete Utils.__cache[id];
    }
  };

  return Utils;
});

S2.define('select2/results',[
  'jquery',
  './utils'
], function ($, Utils) {
  function Results ($element, options, dataAdapter) {
    this.$element = $element;
    this.data = dataAdapter;
    this.options = options;

    Results.__super__.constructor.call(this);
  }

  Utils.Extend(Results, Utils.Observable);

  Results.prototype.render = function () {
    var $results = $(
      '<ul class="select2-results__options" role="tree"></ul>'
    );

    if (this.options.get('multiple')) {
      $results.attr('aria-multiselectable', 'true');
    }

    this.$results = $results;

    return $results;
  };

  Results.prototype.clear = function () {
    this.$results.empty();
  };

  Results.prototype.displayMessage = function (params) {
    var escapeMarkup = this.options.get('escapeMarkup');

    this.clear();
    this.hideLoading();

    var $message = $(
      '<li role="treeitem" aria-live="assertive"' +
      ' class="select2-results__option"></li>'
    );

    var message = this.options.get('translations').get(params.message);

    $message.append(
      escapeMarkup(
        message(params.args)
      )
    );

    $message[0].className += ' select2-results__message';

    this.$results.append($message);
  };

  Results.prototype.hideMessages = function () {
    this.$results.find('.select2-results__message').remove();
  };

  Results.prototype.append = function (data) {
    this.hideLoading();

    var $options = [];

    if (data.results == null || data.results.length === 0) {
      if (this.$results.children().length === 0) {
        this.trigger('results:message', {
          message: 'noResults'
        });
      }

      return;
    }

    data.results = this.sort(data.results);

    for (var d = 0; d < data.results.length; d++) {
      var item = data.results[d];

      var $option = this.option(item);

      $options.push($option);
    }

    this.$results.append($options);
  };

  Results.prototype.position = function ($results, $dropdown) {
    var $resultsContainer = $dropdown.find('.select2-results');
    $resultsContainer.append($results);
  };

  Results.prototype.sort = function (data) {
    var sorter = this.options.get('sorter');

    return sorter(data);
  };

  Results.prototype.highlightFirstItem = function () {
    var $options = this.$results
      .find('.select2-results__option[aria-selected]');

    var $selected = $options.filter('[aria-selected=true]');

    // Check if there are any selected options
    if ($selected.length > 0) {
      // If there are selected options, highlight the first
      $selected.first().trigger('mouseenter');
    } else {
      // If there are no selected options, highlight the first option
      // in the dropdown
      $options.first().trigger('mouseenter');
    }

    this.ensureHighlightVisible();
  };

  Results.prototype.setClasses = function () {
    var self = this;

    this.data.current(function (selected) {
      var selectedIds = $.map(selected, function (s) {
        return s.id.toString();
      });

      var $options = self.$results
        .find('.select2-results__option[aria-selected]');

      $options.each(function () {
        var $option = $(this);

        var item = Utils.GetData(this, 'data');

        // id needs to be converted to a string when comparing
        var id = '' + item.id;

        if ((item.element != null && item.element.selected) ||
            (item.element == null && $.inArray(id, selectedIds) > -1)) {
          $option.attr('aria-selected', 'true');
        } else {
          $option.attr('aria-selected', 'false');
        }
      });

    });
  };

  Results.prototype.showLoading = function (params) {
    this.hideLoading();

    var loadingMore = this.options.get('translations').get('searching');

    var loading = {
      disabled: true,
      loading: true,
      text: loadingMore(params)
    };
    var $loading = this.option(loading);
    $loading.className += ' loading-results';

    this.$results.prepend($loading);
  };

  Results.prototype.hideLoading = function () {
    this.$results.find('.loading-results').remove();
  };

  Results.prototype.option = function (data) {
    var option = document.createElement('li');
    option.className = 'select2-results__option';

    var attrs = {
      'role': 'treeitem',
      'aria-selected': 'false'
    };

    if (data.disabled) {
      delete attrs['aria-selected'];
      attrs['aria-disabled'] = 'true';
    }

    if (data.id == null) {
      delete attrs['aria-selected'];
    }

    if (data._resultId != null) {
      option.id = data._resultId;
    }

    if (data.title) {
      option.title = data.title;
    }

    if (data.children) {
      attrs.role = 'group';
      attrs['aria-label'] = data.text;
      delete attrs['aria-selected'];
    }

    for (var attr in attrs) {
      var val = attrs[attr];

      option.setAttribute(attr, val);
    }

    if (data.children) {
      var $option = $(option);

      var label = document.createElement('strong');
      label.className = 'select2-results__group';

      var $label = $(label);
      this.template(data, label);

      var $children = [];

      for (var c = 0; c < data.children.length; c++) {
        var child = data.children[c];

        var $child = this.option(child);

        $children.push($child);
      }

      var $childrenContainer = $('<ul></ul>', {
        'class': 'select2-results__options select2-results__options--nested'
      });

      $childrenContainer.append($children);

      $option.append(label);
      $option.append($childrenContainer);
    } else {
      this.template(data, option);
    }

    Utils.StoreData(option, 'data', data);

    return option;
  };

  Results.prototype.bind = function (container, $container) {
    var self = this;

    var id = container.id + '-results';

    this.$results.attr('id', id);

    container.on('results:all', function (params) {
      self.clear();
      self.append(params.data);

      if (container.isOpen()) {
        self.setClasses();
        self.highlightFirstItem();
      }
    });

    container.on('results:append', function (params) {
      self.append(params.data);

      if (container.isOpen()) {
        self.setClasses();
      }
    });

    container.on('query', function (params) {
      self.hideMessages();
      self.showLoading(params);
    });

    container.on('select', function () {
      if (!container.isOpen()) {
        return;
      }

      self.setClasses();
      self.highlightFirstItem();
    });

    container.on('unselect', function () {
      if (!container.isOpen()) {
        return;
      }

      self.setClasses();
      self.highlightFirstItem();
    });

    container.on('open', function () {
      // When the dropdown is open, aria-expended="true"
      self.$results.attr('aria-expanded', 'true');
      self.$results.attr('aria-hidden', 'false');

      self.setClasses();
      self.ensureHighlightVisible();
    });

    container.on('close', function () {
      // When the dropdown is closed, aria-expended="false"
      self.$results.attr('aria-expanded', 'false');
      self.$results.attr('aria-hidden', 'true');
      self.$results.removeAttr('aria-activedescendant');
    });

    container.on('results:toggle', function () {
      var $highlighted = self.getHighlightedResults();

      if ($highlighted.length === 0) {
        return;
      }

      $highlighted.trigger('mouseup');
    });

    container.on('results:select', function () {
      var $highlighted = self.getHighlightedResults();

      if ($highlighted.length === 0) {
        return;
      }

      var data = Utils.GetData($highlighted[0], 'data');

      if ($highlighted.attr('aria-selected') == 'true') {
        self.trigger('close', {});
      } else {
        self.trigger('select', {
          data: data
        });
      }
    });

    container.on('results:previous', function () {
      var $highlighted = self.getHighlightedResults();

      var $options = self.$results.find('[aria-selected]');

      var currentIndex = $options.index($highlighted);

      // If we are already at te top, don't move further
      // If no options, currentIndex will be -1
      if (currentIndex <= 0) {
        return;
      }

      var nextIndex = currentIndex - 1;

      // If none are highlighted, highlight the first
      if ($highlighted.length === 0) {
        nextIndex = 0;
      }

      var $next = $options.eq(nextIndex);

      $next.trigger('mouseenter');

      var currentOffset = self.$results.offset().top;
      var nextTop = $next.offset().top;
      var nextOffset = self.$results.scrollTop() + (nextTop - currentOffset);

      if (nextIndex === 0) {
        self.$results.scrollTop(0);
      } else if (nextTop - currentOffset < 0) {
        self.$results.scrollTop(nextOffset);
      }
    });

    container.on('results:next', function () {
      var $highlighted = self.getHighlightedResults();

      var $options = self.$results.find('[aria-selected]');

      var currentIndex = $options.index($highlighted);

      var nextIndex = currentIndex + 1;

      // If we are at the last option, stay there
      if (nextIndex >= $options.length) {
        return;
      }

      var $next = $options.eq(nextIndex);

      $next.trigger('mouseenter');

      var currentOffset = self.$results.offset().top +
        self.$results.outerHeight(false);
      var nextBottom = $next.offset().top + $next.outerHeight(false);
      var nextOffset = self.$results.scrollTop() + nextBottom - currentOffset;

      if (nextIndex === 0) {
        self.$results.scrollTop(0);
      } else if (nextBottom > currentOffset) {
        self.$results.scrollTop(nextOffset);
      }
    });

    container.on('results:focus', function (params) {
      params.element.addClass('select2-results__option--highlighted');
    });

    container.on('results:message', function (params) {
      self.displayMessage(params);
    });

    if ($.fn.mousewheel) {
      this.$results.on('mousewheel', function (e) {
        var top = self.$results.scrollTop();

        var bottom = self.$results.get(0).scrollHeight - top + e.deltaY;

        var isAtTop = e.deltaY > 0 && top - e.deltaY <= 0;
        var isAtBottom = e.deltaY < 0 && bottom <= self.$results.height();

        if (isAtTop) {
          self.$results.scrollTop(0);

          e.preventDefault();
          e.stopPropagation();
        } else if (isAtBottom) {
          self.$results.scrollTop(
            self.$results.get(0).scrollHeight - self.$results.height()
          );

          e.preventDefault();
          e.stopPropagation();
        }
      });
    }

    this.$results.on('mouseup', '.select2-results__option[aria-selected]',
      function (evt) {
      var $this = $(this);

      var data = Utils.GetData(this, 'data');

      if ($this.attr('aria-selected') === 'true') {
        if (self.options.get('multiple')) {
          self.trigger('unselect', {
            originalEvent: evt,
            data: data
          });
        } else {
          self.trigger('close', {});
        }

        return;
      }

      self.trigger('select', {
        originalEvent: evt,
        data: data
      });
    });

    this.$results.on('mouseenter', '.select2-results__option[aria-selected]',
      function (evt) {
      var data = Utils.GetData(this, 'data');

      self.getHighlightedResults()
          .removeClass('select2-results__option--highlighted');

      self.trigger('results:focus', {
        data: data,
        element: $(this)
      });
    });
  };

  Results.prototype.getHighlightedResults = function () {
    var $highlighted = this.$results
    .find('.select2-results__option--highlighted');

    return $highlighted;
  };

  Results.prototype.destroy = function () {
    this.$results.remove();
  };

  Results.prototype.ensureHighlightVisible = function () {
    var $highlighted = this.getHighlightedResults();

    if ($highlighted.length === 0) {
      return;
    }

    var $options = this.$results.find('[aria-selected]');

    var currentIndex = $options.index($highlighted);

    var currentOffset = this.$results.offset().top;
    var nextTop = $highlighted.offset().top;
    var nextOffset = this.$results.scrollTop() + (nextTop - currentOffset);

    var offsetDelta = nextTop - currentOffset;
    nextOffset -= $highlighted.outerHeight(false) * 2;

    if (currentIndex <= 2) {
      this.$results.scrollTop(0);
    } else if (offsetDelta > this.$results.outerHeight() || offsetDelta < 0) {
      this.$results.scrollTop(nextOffset);
    }
  };

  Results.prototype.template = function (result, container) {
    var template = this.options.get('templateResult');
    var escapeMarkup = this.options.get('escapeMarkup');

    var content = template(result, container);

    if (content == null) {
      container.style.display = 'none';
    } else if (typeof content === 'string') {
      container.innerHTML = escapeMarkup(content);
    } else {
      $(container).append(content);
    }
  };

  return Results;
});

S2.define('select2/keys',[

], function () {
  var KEYS = {
    BACKSPACE: 8,
    TAB: 9,
    ENTER: 13,
    SHIFT: 16,
    CTRL: 17,
    ALT: 18,
    ESC: 27,
    SPACE: 32,
    PAGE_UP: 33,
    PAGE_DOWN: 34,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DELETE: 46
  };

  return KEYS;
});

S2.define('select2/selection/base',[
  'jquery',
  '../utils',
  '../keys'
], function ($, Utils, KEYS) {
  function BaseSelection ($element, options) {
    this.$element = $element;
    this.options = options;

    BaseSelection.__super__.constructor.call(this);
  }

  Utils.Extend(BaseSelection, Utils.Observable);

  BaseSelection.prototype.render = function () {
    var $selection = $(
      '<span class="select2-selection" role="combobox" ' +
      ' aria-haspopup="true" aria-expanded="false">' +
      '</span>'
    );

    this._tabindex = 0;

    if (Utils.GetData(this.$element[0], 'old-tabindex') != null) {
      this._tabindex = Utils.GetData(this.$element[0], 'old-tabindex');
    } else if (this.$element.attr('tabindex') != null) {
      this._tabindex = this.$element.attr('tabindex');
    }

    $selection.attr('title', this.$element.attr('title'));
    $selection.attr('tabindex', this._tabindex);

    this.$selection = $selection;

    return $selection;
  };

  BaseSelection.prototype.bind = function (container, $container) {
    var self = this;

    var id = container.id + '-container';
    var resultsId = container.id + '-results';

    this.container = container;

    this.$selection.on('focus', function (evt) {
      self.trigger('focus', evt);
    });

    this.$selection.on('blur', function (evt) {
      self._handleBlur(evt);
    });

    this.$selection.on('keydown', function (evt) {
      self.trigger('keypress', evt);

      if (evt.which === KEYS.SPACE) {
        evt.preventDefault();
      }
    });

    container.on('results:focus', function (params) {
      self.$selection.attr('aria-activedescendant', params.data._resultId);
    });

    container.on('selection:update', function (params) {
      self.update(params.data);
    });

    container.on('open', function () {
      // When the dropdown is open, aria-expanded="true"
      self.$selection.attr('aria-expanded', 'true');
      self.$selection.attr('aria-owns', resultsId);

      self._attachCloseHandler(container);
    });

    container.on('close', function () {
      // When the dropdown is closed, aria-expanded="false"
      self.$selection.attr('aria-expanded', 'false');
      self.$selection.removeAttr('aria-activedescendant');
      self.$selection.removeAttr('aria-owns');

      self.$selection.focus();
      window.setTimeout(function () {
        self.$selection.focus();
      }, 0);

      self._detachCloseHandler(container);
    });

    container.on('enable', function () {
      self.$selection.attr('tabindex', self._tabindex);
    });

    container.on('disable', function () {
      self.$selection.attr('tabindex', '-1');
    });
  };

  BaseSelection.prototype._handleBlur = function (evt) {
    var self = this;

    // This needs to be delayed as the active element is the body when the tab
    // key is pressed, possibly along with others.
    window.setTimeout(function () {
      // Don't trigger `blur` if the focus is still in the selection
      if (
        (document.activeElement == self.$selection[0]) ||
        ($.contains(self.$selection[0], document.activeElement))
      ) {
        return;
      }

      self.trigger('blur', evt);
    }, 1);
  };

  BaseSelection.prototype._attachCloseHandler = function (container) {
    var self = this;

    $(document.body).on('mousedown.select2.' + container.id, function (e) {
      var $target = $(e.target);

      var $select = $target.closest('.select2');

      var $all = $('.select2.select2-container--open');

      $all.each(function () {
        var $this = $(this);

        if (this == $select[0]) {
          return;
        }

        var $element = Utils.GetData(this, 'element');

        $element.select2('close');
      });
    });
  };

  BaseSelection.prototype._detachCloseHandler = function (container) {
    $(document.body).off('mousedown.select2.' + container.id);
  };

  BaseSelection.prototype.position = function ($selection, $container) {
    var $selectionContainer = $container.find('.selection');
    $selectionContainer.append($selection);
  };

  BaseSelection.prototype.destroy = function () {
    this._detachCloseHandler(this.container);
  };

  BaseSelection.prototype.update = function (data) {
    throw new Error('The `update` method must be defined in child classes.');
  };

  return BaseSelection;
});

S2.define('select2/selection/single',[
  'jquery',
  './base',
  '../utils',
  '../keys'
], function ($, BaseSelection, Utils, KEYS) {
  function SingleSelection () {
    SingleSelection.__super__.constructor.apply(this, arguments);
  }

  Utils.Extend(SingleSelection, BaseSelection);

  SingleSelection.prototype.render = function () {
    var $selection = SingleSelection.__super__.render.call(this);

    $selection.addClass('select2-selection--single');

    $selection.html(
      '<span class="select2-selection__rendered"></span>' +
      '<span class="select2-selection__arrow" role="presentation">' +
        '<b role="presentation"></b>' +
      '</span>'
    );

    return $selection;
  };

  SingleSelection.prototype.bind = function (container, $container) {
    var self = this;

    SingleSelection.__super__.bind.apply(this, arguments);

    var id = container.id + '-container';

    this.$selection.find('.select2-selection__rendered')
      .attr('id', id)
      .attr('role', 'textbox')
      .attr('aria-readonly', 'true');
    this.$selection.attr('aria-labelledby', id);

    this.$selection.on('mousedown', function (evt) {
      // Only respond to left clicks
      if (evt.which !== 1) {
        return;
      }

      self.trigger('toggle', {
        originalEvent: evt
      });
    });

    this.$selection.on('focus', function (evt) {
      // User focuses on the container
    });

    this.$selection.on('blur', function (evt) {
      // User exits the container
    });

    container.on('focus', function (evt) {
      if (!container.isOpen()) {
        self.$selection.focus();
      }
    });
  };

  SingleSelection.prototype.clear = function () {
    var $rendered = this.$selection.find('.select2-selection__rendered');
    $rendered.empty();
    $rendered.removeAttr('title'); // clear tooltip on empty
  };

  SingleSelection.prototype.display = function (data, container) {
    var template = this.options.get('templateSelection');
    var escapeMarkup = this.options.get('escapeMarkup');

    return escapeMarkup(template(data, container));
  };

  SingleSelection.prototype.selectionContainer = function () {
    return $('<span></span>');
  };

  SingleSelection.prototype.update = function (data) {
    if (data.length === 0) {
      this.clear();
      return;
    }

    var selection = data[0];

    var $rendered = this.$selection.find('.select2-selection__rendered');
    var formatted = this.display(selection, $rendered);

    $rendered.empty().append(formatted);
    $rendered.attr('title', selection.title || selection.text);
  };

  return SingleSelection;
});

S2.define('select2/selection/multiple',[
  'jquery',
  './base',
  '../utils'
], function ($, BaseSelection, Utils) {
  function MultipleSelection ($element, options) {
    MultipleSelection.__super__.constructor.apply(this, arguments);
  }

  Utils.Extend(MultipleSelection, BaseSelection);

  MultipleSelection.prototype.render = function () {
    var $selection = MultipleSelection.__super__.render.call(this);

    $selection.addClass('select2-selection--multiple');

    $selection.html(
      '<ul class="select2-selection__rendered"></ul>'
    );

    return $selection;
  };

  MultipleSelection.prototype.bind = function (container, $container) {
    var self = this;

    MultipleSelection.__super__.bind.apply(this, arguments);

    this.$selection.on('click', function (evt) {
      self.trigger('toggle', {
        originalEvent: evt
      });
    });

    this.$selection.on(
      'click',
      '.select2-selection__choice__remove',
      function (evt) {
        // Ignore the event if it is disabled
        if (self.options.get('disabled')) {
          return;
        }

        var $remove = $(this);
        var $selection = $remove.parent();

        var data = Utils.GetData($selection[0], 'data');

        self.trigger('unselect', {
          originalEvent: evt,
          data: data
        });
      }
    );
  };

  MultipleSelection.prototype.clear = function () {
    var $rendered = this.$selection.find('.select2-selection__rendered');
    $rendered.empty();
    $rendered.removeAttr('title');
  };

  MultipleSelection.prototype.display = function (data, container) {
    var template = this.options.get('templateSelection');
    var escapeMarkup = this.options.get('escapeMarkup');

    return escapeMarkup(template(data, container));
  };

  MultipleSelection.prototype.selectionContainer = function () {
    var $container = $(
      '<li class="select2-selection__choice">' +
        '<span class="select2-selection__choice__remove" role="presentation">' +
          '&times;' +
        '</span>' +
      '</li>'
    );

    return $container;
  };

  MultipleSelection.prototype.update = function (data) {
    this.clear();

    if (data.length === 0) {
      return;
    }

    var $selections = [];

    for (var d = 0; d < data.length; d++) {
      var selection = data[d];

      var $selection = this.selectionContainer();
      var formatted = this.display(selection, $selection);

      $selection.append(formatted);
      $selection.attr('title', selection.title || selection.text);

      Utils.StoreData($selection[0], 'data', selection);

      $selections.push($selection);
    }

    var $rendered = this.$selection.find('.select2-selection__rendered');

    Utils.appendMany($rendered, $selections);
  };

  return MultipleSelection;
});

S2.define('select2/selection/placeholder',[
  '../utils'
], function (Utils) {
  function Placeholder (decorated, $element, options) {
    this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

    decorated.call(this, $element, options);
  }

  Placeholder.prototype.normalizePlaceholder = function (_, placeholder) {
    if (typeof placeholder === 'string') {
      placeholder = {
        id: '',
        text: placeholder
      };
    }

    return placeholder;
  };

  Placeholder.prototype.createPlaceholder = function (decorated, placeholder) {
    var $placeholder = this.selectionContainer();

    $placeholder.html(this.display(placeholder));
    $placeholder.addClass('select2-selection__placeholder')
                .removeClass('select2-selection__choice');

    return $placeholder;
  };

  Placeholder.prototype.update = function (decorated, data) {
    var singlePlaceholder = (
      data.length == 1 && data[0].id != this.placeholder.id
    );
    var multipleSelections = data.length > 1;

    if (multipleSelections || singlePlaceholder) {
      return decorated.call(this, data);
    }

    this.clear();

    var $placeholder = this.createPlaceholder(this.placeholder);

    this.$selection.find('.select2-selection__rendered').append($placeholder);
  };

  return Placeholder;
});

S2.define('select2/selection/allowClear',[
  'jquery',
  '../keys',
  '../utils'
], function ($, KEYS, Utils) {
  function AllowClear () { }

  AllowClear.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    if (this.placeholder == null) {
      if (this.options.get('debug') && window.console && console.error) {
        console.error(
          'Select2: The `allowClear` option should be used in combination ' +
          'with the `placeholder` option.'
        );
      }
    }

    this.$selection.on('mousedown', '.select2-selection__clear',
      function (evt) {
        self._handleClear(evt);
    });

    container.on('keypress', function (evt) {
      self._handleKeyboardClear(evt, container);
    });
  };

  AllowClear.prototype._handleClear = function (_, evt) {
    // Ignore the event if it is disabled
    if (this.options.get('disabled')) {
      return;
    }

    var $clear = this.$selection.find('.select2-selection__clear');

    // Ignore the event if nothing has been selected
    if ($clear.length === 0) {
      return;
    }

    evt.stopPropagation();

    var data = Utils.GetData($clear[0], 'data');

    var previousVal = this.$element.val();
    this.$element.val(this.placeholder.id);

    var unselectData = {
      data: data
    };
    this.trigger('clear', unselectData);
    if (unselectData.prevented) {
      this.$element.val(previousVal);
      return;
    }

    for (var d = 0; d < data.length; d++) {
      unselectData = {
        data: data[d]
      };

      // Trigger the `unselect` event, so people can prevent it from being
      // cleared.
      this.trigger('unselect', unselectData);

      // If the event was prevented, don't clear it out.
      if (unselectData.prevented) {
        this.$element.val(previousVal);
        return;
      }
    }

    this.$element.trigger('change');

    this.trigger('toggle', {});
  };

  AllowClear.prototype._handleKeyboardClear = function (_, evt, container) {
    if (container.isOpen()) {
      return;
    }

    if (evt.which == KEYS.DELETE || evt.which == KEYS.BACKSPACE) {
      this._handleClear(evt);
    }
  };

  AllowClear.prototype.update = function (decorated, data) {
    decorated.call(this, data);

    if (this.$selection.find('.select2-selection__placeholder').length > 0 ||
        data.length === 0) {
      return;
    }

    var $remove = $(
      '<span class="select2-selection__clear">' +
        '&times;' +
      '</span>'
    );
    Utils.StoreData($remove[0], 'data', data);

    this.$selection.find('.select2-selection__rendered').prepend($remove);
  };

  return AllowClear;
});

S2.define('select2/selection/search',[
  'jquery',
  '../utils',
  '../keys'
], function ($, Utils, KEYS) {
  function Search (decorated, $element, options) {
    decorated.call(this, $element, options);
  }

  Search.prototype.render = function (decorated) {
    var $search = $(
      '<li class="select2-search select2-search--inline">' +
        '<input class="select2-search__field" type="search" tabindex="-1"' +
        ' autocomplete="off" autocorrect="off" autocapitalize="none"' +
        ' spellcheck="false" role="textbox" aria-autocomplete="list" />' +
      '</li>'
    );

    this.$searchContainer = $search;
    this.$search = $search.find('input');

    var $rendered = decorated.call(this);

    this._transferTabIndex();

    return $rendered;
  };

  Search.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('open', function () {
      self.$search.trigger('focus');
    });

    container.on('close', function () {
      self.$search.val('');
      self.$search.removeAttr('aria-activedescendant');
      self.$search.trigger('focus');
    });

    container.on('enable', function () {
      self.$search.prop('disabled', false);

      self._transferTabIndex();
    });

    container.on('disable', function () {
      self.$search.prop('disabled', true);
    });

    container.on('focus', function (evt) {
      self.$search.trigger('focus');
    });

    container.on('results:focus', function (params) {
      self.$search.attr('aria-activedescendant', params.id);
    });

    this.$selection.on('focusin', '.select2-search--inline', function (evt) {
      self.trigger('focus', evt);
    });

    this.$selection.on('focusout', '.select2-search--inline', function (evt) {
      self._handleBlur(evt);
    });

    this.$selection.on('keydown', '.select2-search--inline', function (evt) {
      evt.stopPropagation();

      self.trigger('keypress', evt);

      self._keyUpPrevented = evt.isDefaultPrevented();

      var key = evt.which;

      if (key === KEYS.BACKSPACE && self.$search.val() === '') {
        var $previousChoice = self.$searchContainer
          .prev('.select2-selection__choice');

        if ($previousChoice.length > 0) {
          var item = Utils.GetData($previousChoice[0], 'data');

          self.searchRemoveChoice(item);

          evt.preventDefault();
        }
      }
    });

    // Try to detect the IE version should the `documentMode` property that
    // is stored on the document. This is only implemented in IE and is
    // slightly cleaner than doing a user agent check.
    // This property is not available in Edge, but Edge also doesn't have
    // this bug.
    var msie = document.documentMode;
    var disableInputEvents = msie && msie <= 11;

    // Workaround for browsers which do not support the `input` event
    // This will prevent double-triggering of events for browsers which support
    // both the `keyup` and `input` events.
    this.$selection.on(
      'input.searchcheck',
      '.select2-search--inline',
      function (evt) {
        // IE will trigger the `input` event when a placeholder is used on a
        // search box. To get around this issue, we are forced to ignore all
        // `input` events in IE and keep using `keyup`.
        if (disableInputEvents) {
          self.$selection.off('input.search input.searchcheck');
          return;
        }

        // Unbind the duplicated `keyup` event
        self.$selection.off('keyup.search');
      }
    );

    this.$selection.on(
      'keyup.search input.search',
      '.select2-search--inline',
      function (evt) {
        // IE will trigger the `input` event when a placeholder is used on a
        // search box. To get around this issue, we are forced to ignore all
        // `input` events in IE and keep using `keyup`.
        if (disableInputEvents && evt.type === 'input') {
          self.$selection.off('input.search input.searchcheck');
          return;
        }

        var key = evt.which;

        // We can freely ignore events from modifier keys
        if (key == KEYS.SHIFT || key == KEYS.CTRL || key == KEYS.ALT) {
          return;
        }

        // Tabbing will be handled during the `keydown` phase
        if (key == KEYS.TAB) {
          return;
        }

        self.handleSearch(evt);
      }
    );
  };

  /**
   * This method will transfer the tabindex attribute from the rendered
   * selection to the search box. This allows for the search box to be used as
   * the primary focus instead of the selection container.
   *
   * @private
   */
  Search.prototype._transferTabIndex = function (decorated) {
    this.$search.attr('tabindex', this.$selection.attr('tabindex'));
    this.$selection.attr('tabindex', '-1');
  };

  Search.prototype.createPlaceholder = function (decorated, placeholder) {
    this.$search.attr('placeholder', placeholder.text);
  };

  Search.prototype.update = function (decorated, data) {
    var searchHadFocus = this.$search[0] == document.activeElement;

    this.$search.attr('placeholder', '');

    decorated.call(this, data);

    this.$selection.find('.select2-selection__rendered')
                   .append(this.$searchContainer);

    this.resizeSearch();
    if (searchHadFocus) {
      var isTagInput = this.$element.find('[data-select2-tag]').length;
      if (isTagInput) {
        // fix IE11 bug where tag input lost focus
        this.$element.focus();
      } else {
        this.$search.focus();
      }
    }
  };

  Search.prototype.handleSearch = function () {
    this.resizeSearch();

    if (!this._keyUpPrevented) {
      var input = this.$search.val();

      this.trigger('query', {
        term: input
      });
    }

    this._keyUpPrevented = false;
  };

  Search.prototype.searchRemoveChoice = function (decorated, item) {
    this.trigger('unselect', {
      data: item
    });

    this.$search.val(item.text);
    this.handleSearch();
  };

  Search.prototype.resizeSearch = function () {
    this.$search.css('width', '25px');

    var width = '';

    if (this.$search.attr('placeholder') !== '') {
      width = this.$selection.find('.select2-selection__rendered').innerWidth();
    } else {
      var minimumWidth = this.$search.val().length + 1;

      width = (minimumWidth * 0.75) + 'em';
    }

    this.$search.css('width', width);
  };

  return Search;
});

S2.define('select2/selection/eventRelay',[
  'jquery'
], function ($) {
  function EventRelay () { }

  EventRelay.prototype.bind = function (decorated, container, $container) {
    var self = this;
    var relayEvents = [
      'open', 'opening',
      'close', 'closing',
      'select', 'selecting',
      'unselect', 'unselecting',
      'clear', 'clearing'
    ];

    var preventableEvents = [
      'opening', 'closing', 'selecting', 'unselecting', 'clearing'
    ];

    decorated.call(this, container, $container);

    container.on('*', function (name, params) {
      // Ignore events that should not be relayed
      if ($.inArray(name, relayEvents) === -1) {
        return;
      }

      // The parameters should always be an object
      params = params || {};

      // Generate the jQuery event for the Select2 event
      var evt = $.Event('select2:' + name, {
        params: params
      });

      self.$element.trigger(evt);

      // Only handle preventable events if it was one
      if ($.inArray(name, preventableEvents) === -1) {
        return;
      }

      params.prevented = evt.isDefaultPrevented();
    });
  };

  return EventRelay;
});

S2.define('select2/translation',[
  'jquery',
  'require'
], function ($, require) {
  function Translation (dict) {
    this.dict = dict || {};
  }

  Translation.prototype.all = function () {
    return this.dict;
  };

  Translation.prototype.get = function (key) {
    return this.dict[key];
  };

  Translation.prototype.extend = function (translation) {
    this.dict = $.extend({}, translation.all(), this.dict);
  };

  // Static functions

  Translation._cache = {};

  Translation.loadPath = function (path) {
    if (!(path in Translation._cache)) {
      var translations = require(path);

      Translation._cache[path] = translations;
    }

    return new Translation(Translation._cache[path]);
  };

  return Translation;
});

S2.define('select2/diacritics',[

], function () {
  var diacritics = {
    '\u24B6': 'A',
    '\uFF21': 'A',
    '\u00C0': 'A',
    '\u00C1': 'A',
    '\u00C2': 'A',
    '\u1EA6': 'A',
    '\u1EA4': 'A',
    '\u1EAA': 'A',
    '\u1EA8': 'A',
    '\u00C3': 'A',
    '\u0100': 'A',
    '\u0102': 'A',
    '\u1EB0': 'A',
    '\u1EAE': 'A',
    '\u1EB4': 'A',
    '\u1EB2': 'A',
    '\u0226': 'A',
    '\u01E0': 'A',
    '\u00C4': 'A',
    '\u01DE': 'A',
    '\u1EA2': 'A',
    '\u00C5': 'A',
    '\u01FA': 'A',
    '\u01CD': 'A',
    '\u0200': 'A',
    '\u0202': 'A',
    '\u1EA0': 'A',
    '\u1EAC': 'A',
    '\u1EB6': 'A',
    '\u1E00': 'A',
    '\u0104': 'A',
    '\u023A': 'A',
    '\u2C6F': 'A',
    '\uA732': 'AA',
    '\u00C6': 'AE',
    '\u01FC': 'AE',
    '\u01E2': 'AE',
    '\uA734': 'AO',
    '\uA736': 'AU',
    '\uA738': 'AV',
    '\uA73A': 'AV',
    '\uA73C': 'AY',
    '\u24B7': 'B',
    '\uFF22': 'B',
    '\u1E02': 'B',
    '\u1E04': 'B',
    '\u1E06': 'B',
    '\u0243': 'B',
    '\u0182': 'B',
    '\u0181': 'B',
    '\u24B8': 'C',
    '\uFF23': 'C',
    '\u0106': 'C',
    '\u0108': 'C',
    '\u010A': 'C',
    '\u010C': 'C',
    '\u00C7': 'C',
    '\u1E08': 'C',
    '\u0187': 'C',
    '\u023B': 'C',
    '\uA73E': 'C',
    '\u24B9': 'D',
    '\uFF24': 'D',
    '\u1E0A': 'D',
    '\u010E': 'D',
    '\u1E0C': 'D',
    '\u1E10': 'D',
    '\u1E12': 'D',
    '\u1E0E': 'D',
    '\u0110': 'D',
    '\u018B': 'D',
    '\u018A': 'D',
    '\u0189': 'D',
    '\uA779': 'D',
    '\u01F1': 'DZ',
    '\u01C4': 'DZ',
    '\u01F2': 'Dz',
    '\u01C5': 'Dz',
    '\u24BA': 'E',
    '\uFF25': 'E',
    '\u00C8': 'E',
    '\u00C9': 'E',
    '\u00CA': 'E',
    '\u1EC0': 'E',
    '\u1EBE': 'E',
    '\u1EC4': 'E',
    '\u1EC2': 'E',
    '\u1EBC': 'E',
    '\u0112': 'E',
    '\u1E14': 'E',
    '\u1E16': 'E',
    '\u0114': 'E',
    '\u0116': 'E',
    '\u00CB': 'E',
    '\u1EBA': 'E',
    '\u011A': 'E',
    '\u0204': 'E',
    '\u0206': 'E',
    '\u1EB8': 'E',
    '\u1EC6': 'E',
    '\u0228': 'E',
    '\u1E1C': 'E',
    '\u0118': 'E',
    '\u1E18': 'E',
    '\u1E1A': 'E',
    '\u0190': 'E',
    '\u018E': 'E',
    '\u24BB': 'F',
    '\uFF26': 'F',
    '\u1E1E': 'F',
    '\u0191': 'F',
    '\uA77B': 'F',
    '\u24BC': 'G',
    '\uFF27': 'G',
    '\u01F4': 'G',
    '\u011C': 'G',
    '\u1E20': 'G',
    '\u011E': 'G',
    '\u0120': 'G',
    '\u01E6': 'G',
    '\u0122': 'G',
    '\u01E4': 'G',
    '\u0193': 'G',
    '\uA7A0': 'G',
    '\uA77D': 'G',
    '\uA77E': 'G',
    '\u24BD': 'H',
    '\uFF28': 'H',
    '\u0124': 'H',
    '\u1E22': 'H',
    '\u1E26': 'H',
    '\u021E': 'H',
    '\u1E24': 'H',
    '\u1E28': 'H',
    '\u1E2A': 'H',
    '\u0126': 'H',
    '\u2C67': 'H',
    '\u2C75': 'H',
    '\uA78D': 'H',
    '\u24BE': 'I',
    '\uFF29': 'I',
    '\u00CC': 'I',
    '\u00CD': 'I',
    '\u00CE': 'I',
    '\u0128': 'I',
    '\u012A': 'I',
    '\u012C': 'I',
    '\u0130': 'I',
    '\u00CF': 'I',
    '\u1E2E': 'I',
    '\u1EC8': 'I',
    '\u01CF': 'I',
    '\u0208': 'I',
    '\u020A': 'I',
    '\u1ECA': 'I',
    '\u012E': 'I',
    '\u1E2C': 'I',
    '\u0197': 'I',
    '\u24BF': 'J',
    '\uFF2A': 'J',
    '\u0134': 'J',
    '\u0248': 'J',
    '\u24C0': 'K',
    '\uFF2B': 'K',
    '\u1E30': 'K',
    '\u01E8': 'K',
    '\u1E32': 'K',
    '\u0136': 'K',
    '\u1E34': 'K',
    '\u0198': 'K',
    '\u2C69': 'K',
    '\uA740': 'K',
    '\uA742': 'K',
    '\uA744': 'K',
    '\uA7A2': 'K',
    '\u24C1': 'L',
    '\uFF2C': 'L',
    '\u013F': 'L',
    '\u0139': 'L',
    '\u013D': 'L',
    '\u1E36': 'L',
    '\u1E38': 'L',
    '\u013B': 'L',
    '\u1E3C': 'L',
    '\u1E3A': 'L',
    '\u0141': 'L',
    '\u023D': 'L',
    '\u2C62': 'L',
    '\u2C60': 'L',
    '\uA748': 'L',
    '\uA746': 'L',
    '\uA780': 'L',
    '\u01C7': 'LJ',
    '\u01C8': 'Lj',
    '\u24C2': 'M',
    '\uFF2D': 'M',
    '\u1E3E': 'M',
    '\u1E40': 'M',
    '\u1E42': 'M',
    '\u2C6E': 'M',
    '\u019C': 'M',
    '\u24C3': 'N',
    '\uFF2E': 'N',
    '\u01F8': 'N',
    '\u0143': 'N',
    '\u00D1': 'N',
    '\u1E44': 'N',
    '\u0147': 'N',
    '\u1E46': 'N',
    '\u0145': 'N',
    '\u1E4A': 'N',
    '\u1E48': 'N',
    '\u0220': 'N',
    '\u019D': 'N',
    '\uA790': 'N',
    '\uA7A4': 'N',
    '\u01CA': 'NJ',
    '\u01CB': 'Nj',
    '\u24C4': 'O',
    '\uFF2F': 'O',
    '\u00D2': 'O',
    '\u00D3': 'O',
    '\u00D4': 'O',
    '\u1ED2': 'O',
    '\u1ED0': 'O',
    '\u1ED6': 'O',
    '\u1ED4': 'O',
    '\u00D5': 'O',
    '\u1E4C': 'O',
    '\u022C': 'O',
    '\u1E4E': 'O',
    '\u014C': 'O',
    '\u1E50': 'O',
    '\u1E52': 'O',
    '\u014E': 'O',
    '\u022E': 'O',
    '\u0230': 'O',
    '\u00D6': 'O',
    '\u022A': 'O',
    '\u1ECE': 'O',
    '\u0150': 'O',
    '\u01D1': 'O',
    '\u020C': 'O',
    '\u020E': 'O',
    '\u01A0': 'O',
    '\u1EDC': 'O',
    '\u1EDA': 'O',
    '\u1EE0': 'O',
    '\u1EDE': 'O',
    '\u1EE2': 'O',
    '\u1ECC': 'O',
    '\u1ED8': 'O',
    '\u01EA': 'O',
    '\u01EC': 'O',
    '\u00D8': 'O',
    '\u01FE': 'O',
    '\u0186': 'O',
    '\u019F': 'O',
    '\uA74A': 'O',
    '\uA74C': 'O',
    '\u01A2': 'OI',
    '\uA74E': 'OO',
    '\u0222': 'OU',
    '\u24C5': 'P',
    '\uFF30': 'P',
    '\u1E54': 'P',
    '\u1E56': 'P',
    '\u01A4': 'P',
    '\u2C63': 'P',
    '\uA750': 'P',
    '\uA752': 'P',
    '\uA754': 'P',
    '\u24C6': 'Q',
    '\uFF31': 'Q',
    '\uA756': 'Q',
    '\uA758': 'Q',
    '\u024A': 'Q',
    '\u24C7': 'R',
    '\uFF32': 'R',
    '\u0154': 'R',
    '\u1E58': 'R',
    '\u0158': 'R',
    '\u0210': 'R',
    '\u0212': 'R',
    '\u1E5A': 'R',
    '\u1E5C': 'R',
    '\u0156': 'R',
    '\u1E5E': 'R',
    '\u024C': 'R',
    '\u2C64': 'R',
    '\uA75A': 'R',
    '\uA7A6': 'R',
    '\uA782': 'R',
    '\u24C8': 'S',
    '\uFF33': 'S',
    '\u1E9E': 'S',
    '\u015A': 'S',
    '\u1E64': 'S',
    '\u015C': 'S',
    '\u1E60': 'S',
    '\u0160': 'S',
    '\u1E66': 'S',
    '\u1E62': 'S',
    '\u1E68': 'S',
    '\u0218': 'S',
    '\u015E': 'S',
    '\u2C7E': 'S',
    '\uA7A8': 'S',
    '\uA784': 'S',
    '\u24C9': 'T',
    '\uFF34': 'T',
    '\u1E6A': 'T',
    '\u0164': 'T',
    '\u1E6C': 'T',
    '\u021A': 'T',
    '\u0162': 'T',
    '\u1E70': 'T',
    '\u1E6E': 'T',
    '\u0166': 'T',
    '\u01AC': 'T',
    '\u01AE': 'T',
    '\u023E': 'T',
    '\uA786': 'T',
    '\uA728': 'TZ',
    '\u24CA': 'U',
    '\uFF35': 'U',
    '\u00D9': 'U',
    '\u00DA': 'U',
    '\u00DB': 'U',
    '\u0168': 'U',
    '\u1E78': 'U',
    '\u016A': 'U',
    '\u1E7A': 'U',
    '\u016C': 'U',
    '\u00DC': 'U',
    '\u01DB': 'U',
    '\u01D7': 'U',
    '\u01D5': 'U',
    '\u01D9': 'U',
    '\u1EE6': 'U',
    '\u016E': 'U',
    '\u0170': 'U',
    '\u01D3': 'U',
    '\u0214': 'U',
    '\u0216': 'U',
    '\u01AF': 'U',
    '\u1EEA': 'U',
    '\u1EE8': 'U',
    '\u1EEE': 'U',
    '\u1EEC': 'U',
    '\u1EF0': 'U',
    '\u1EE4': 'U',
    '\u1E72': 'U',
    '\u0172': 'U',
    '\u1E76': 'U',
    '\u1E74': 'U',
    '\u0244': 'U',
    '\u24CB': 'V',
    '\uFF36': 'V',
    '\u1E7C': 'V',
    '\u1E7E': 'V',
    '\u01B2': 'V',
    '\uA75E': 'V',
    '\u0245': 'V',
    '\uA760': 'VY',
    '\u24CC': 'W',
    '\uFF37': 'W',
    '\u1E80': 'W',
    '\u1E82': 'W',
    '\u0174': 'W',
    '\u1E86': 'W',
    '\u1E84': 'W',
    '\u1E88': 'W',
    '\u2C72': 'W',
    '\u24CD': 'X',
    '\uFF38': 'X',
    '\u1E8A': 'X',
    '\u1E8C': 'X',
    '\u24CE': 'Y',
    '\uFF39': 'Y',
    '\u1EF2': 'Y',
    '\u00DD': 'Y',
    '\u0176': 'Y',
    '\u1EF8': 'Y',
    '\u0232': 'Y',
    '\u1E8E': 'Y',
    '\u0178': 'Y',
    '\u1EF6': 'Y',
    '\u1EF4': 'Y',
    '\u01B3': 'Y',
    '\u024E': 'Y',
    '\u1EFE': 'Y',
    '\u24CF': 'Z',
    '\uFF3A': 'Z',
    '\u0179': 'Z',
    '\u1E90': 'Z',
    '\u017B': 'Z',
    '\u017D': 'Z',
    '\u1E92': 'Z',
    '\u1E94': 'Z',
    '\u01B5': 'Z',
    '\u0224': 'Z',
    '\u2C7F': 'Z',
    '\u2C6B': 'Z',
    '\uA762': 'Z',
    '\u24D0': 'a',
    '\uFF41': 'a',
    '\u1E9A': 'a',
    '\u00E0': 'a',
    '\u00E1': 'a',
    '\u00E2': 'a',
    '\u1EA7': 'a',
    '\u1EA5': 'a',
    '\u1EAB': 'a',
    '\u1EA9': 'a',
    '\u00E3': 'a',
    '\u0101': 'a',
    '\u0103': 'a',
    '\u1EB1': 'a',
    '\u1EAF': 'a',
    '\u1EB5': 'a',
    '\u1EB3': 'a',
    '\u0227': 'a',
    '\u01E1': 'a',
    '\u00E4': 'a',
    '\u01DF': 'a',
    '\u1EA3': 'a',
    '\u00E5': 'a',
    '\u01FB': 'a',
    '\u01CE': 'a',
    '\u0201': 'a',
    '\u0203': 'a',
    '\u1EA1': 'a',
    '\u1EAD': 'a',
    '\u1EB7': 'a',
    '\u1E01': 'a',
    '\u0105': 'a',
    '\u2C65': 'a',
    '\u0250': 'a',
    '\uA733': 'aa',
    '\u00E6': 'ae',
    '\u01FD': 'ae',
    '\u01E3': 'ae',
    '\uA735': 'ao',
    '\uA737': 'au',
    '\uA739': 'av',
    '\uA73B': 'av',
    '\uA73D': 'ay',
    '\u24D1': 'b',
    '\uFF42': 'b',
    '\u1E03': 'b',
    '\u1E05': 'b',
    '\u1E07': 'b',
    '\u0180': 'b',
    '\u0183': 'b',
    '\u0253': 'b',
    '\u24D2': 'c',
    '\uFF43': 'c',
    '\u0107': 'c',
    '\u0109': 'c',
    '\u010B': 'c',
    '\u010D': 'c',
    '\u00E7': 'c',
    '\u1E09': 'c',
    '\u0188': 'c',
    '\u023C': 'c',
    '\uA73F': 'c',
    '\u2184': 'c',
    '\u24D3': 'd',
    '\uFF44': 'd',
    '\u1E0B': 'd',
    '\u010F': 'd',
    '\u1E0D': 'd',
    '\u1E11': 'd',
    '\u1E13': 'd',
    '\u1E0F': 'd',
    '\u0111': 'd',
    '\u018C': 'd',
    '\u0256': 'd',
    '\u0257': 'd',
    '\uA77A': 'd',
    '\u01F3': 'dz',
    '\u01C6': 'dz',
    '\u24D4': 'e',
    '\uFF45': 'e',
    '\u00E8': 'e',
    '\u00E9': 'e',
    '\u00EA': 'e',
    '\u1EC1': 'e',
    '\u1EBF': 'e',
    '\u1EC5': 'e',
    '\u1EC3': 'e',
    '\u1EBD': 'e',
    '\u0113': 'e',
    '\u1E15': 'e',
    '\u1E17': 'e',
    '\u0115': 'e',
    '\u0117': 'e',
    '\u00EB': 'e',
    '\u1EBB': 'e',
    '\u011B': 'e',
    '\u0205': 'e',
    '\u0207': 'e',
    '\u1EB9': 'e',
    '\u1EC7': 'e',
    '\u0229': 'e',
    '\u1E1D': 'e',
    '\u0119': 'e',
    '\u1E19': 'e',
    '\u1E1B': 'e',
    '\u0247': 'e',
    '\u025B': 'e',
    '\u01DD': 'e',
    '\u24D5': 'f',
    '\uFF46': 'f',
    '\u1E1F': 'f',
    '\u0192': 'f',
    '\uA77C': 'f',
    '\u24D6': 'g',
    '\uFF47': 'g',
    '\u01F5': 'g',
    '\u011D': 'g',
    '\u1E21': 'g',
    '\u011F': 'g',
    '\u0121': 'g',
    '\u01E7': 'g',
    '\u0123': 'g',
    '\u01E5': 'g',
    '\u0260': 'g',
    '\uA7A1': 'g',
    '\u1D79': 'g',
    '\uA77F': 'g',
    '\u24D7': 'h',
    '\uFF48': 'h',
    '\u0125': 'h',
    '\u1E23': 'h',
    '\u1E27': 'h',
    '\u021F': 'h',
    '\u1E25': 'h',
    '\u1E29': 'h',
    '\u1E2B': 'h',
    '\u1E96': 'h',
    '\u0127': 'h',
    '\u2C68': 'h',
    '\u2C76': 'h',
    '\u0265': 'h',
    '\u0195': 'hv',
    '\u24D8': 'i',
    '\uFF49': 'i',
    '\u00EC': 'i',
    '\u00ED': 'i',
    '\u00EE': 'i',
    '\u0129': 'i',
    '\u012B': 'i',
    '\u012D': 'i',
    '\u00EF': 'i',
    '\u1E2F': 'i',
    '\u1EC9': 'i',
    '\u01D0': 'i',
    '\u0209': 'i',
    '\u020B': 'i',
    '\u1ECB': 'i',
    '\u012F': 'i',
    '\u1E2D': 'i',
    '\u0268': 'i',
    '\u0131': 'i',
    '\u24D9': 'j',
    '\uFF4A': 'j',
    '\u0135': 'j',
    '\u01F0': 'j',
    '\u0249': 'j',
    '\u24DA': 'k',
    '\uFF4B': 'k',
    '\u1E31': 'k',
    '\u01E9': 'k',
    '\u1E33': 'k',
    '\u0137': 'k',
    '\u1E35': 'k',
    '\u0199': 'k',
    '\u2C6A': 'k',
    '\uA741': 'k',
    '\uA743': 'k',
    '\uA745': 'k',
    '\uA7A3': 'k',
    '\u24DB': 'l',
    '\uFF4C': 'l',
    '\u0140': 'l',
    '\u013A': 'l',
    '\u013E': 'l',
    '\u1E37': 'l',
    '\u1E39': 'l',
    '\u013C': 'l',
    '\u1E3D': 'l',
    '\u1E3B': 'l',
    '\u017F': 'l',
    '\u0142': 'l',
    '\u019A': 'l',
    '\u026B': 'l',
    '\u2C61': 'l',
    '\uA749': 'l',
    '\uA781': 'l',
    '\uA747': 'l',
    '\u01C9': 'lj',
    '\u24DC': 'm',
    '\uFF4D': 'm',
    '\u1E3F': 'm',
    '\u1E41': 'm',
    '\u1E43': 'm',
    '\u0271': 'm',
    '\u026F': 'm',
    '\u24DD': 'n',
    '\uFF4E': 'n',
    '\u01F9': 'n',
    '\u0144': 'n',
    '\u00F1': 'n',
    '\u1E45': 'n',
    '\u0148': 'n',
    '\u1E47': 'n',
    '\u0146': 'n',
    '\u1E4B': 'n',
    '\u1E49': 'n',
    '\u019E': 'n',
    '\u0272': 'n',
    '\u0149': 'n',
    '\uA791': 'n',
    '\uA7A5': 'n',
    '\u01CC': 'nj',
    '\u24DE': 'o',
    '\uFF4F': 'o',
    '\u00F2': 'o',
    '\u00F3': 'o',
    '\u00F4': 'o',
    '\u1ED3': 'o',
    '\u1ED1': 'o',
    '\u1ED7': 'o',
    '\u1ED5': 'o',
    '\u00F5': 'o',
    '\u1E4D': 'o',
    '\u022D': 'o',
    '\u1E4F': 'o',
    '\u014D': 'o',
    '\u1E51': 'o',
    '\u1E53': 'o',
    '\u014F': 'o',
    '\u022F': 'o',
    '\u0231': 'o',
    '\u00F6': 'o',
    '\u022B': 'o',
    '\u1ECF': 'o',
    '\u0151': 'o',
    '\u01D2': 'o',
    '\u020D': 'o',
    '\u020F': 'o',
    '\u01A1': 'o',
    '\u1EDD': 'o',
    '\u1EDB': 'o',
    '\u1EE1': 'o',
    '\u1EDF': 'o',
    '\u1EE3': 'o',
    '\u1ECD': 'o',
    '\u1ED9': 'o',
    '\u01EB': 'o',
    '\u01ED': 'o',
    '\u00F8': 'o',
    '\u01FF': 'o',
    '\u0254': 'o',
    '\uA74B': 'o',
    '\uA74D': 'o',
    '\u0275': 'o',
    '\u01A3': 'oi',
    '\u0223': 'ou',
    '\uA74F': 'oo',
    '\u24DF': 'p',
    '\uFF50': 'p',
    '\u1E55': 'p',
    '\u1E57': 'p',
    '\u01A5': 'p',
    '\u1D7D': 'p',
    '\uA751': 'p',
    '\uA753': 'p',
    '\uA755': 'p',
    '\u24E0': 'q',
    '\uFF51': 'q',
    '\u024B': 'q',
    '\uA757': 'q',
    '\uA759': 'q',
    '\u24E1': 'r',
    '\uFF52': 'r',
    '\u0155': 'r',
    '\u1E59': 'r',
    '\u0159': 'r',
    '\u0211': 'r',
    '\u0213': 'r',
    '\u1E5B': 'r',
    '\u1E5D': 'r',
    '\u0157': 'r',
    '\u1E5F': 'r',
    '\u024D': 'r',
    '\u027D': 'r',
    '\uA75B': 'r',
    '\uA7A7': 'r',
    '\uA783': 'r',
    '\u24E2': 's',
    '\uFF53': 's',
    '\u00DF': 's',
    '\u015B': 's',
    '\u1E65': 's',
    '\u015D': 's',
    '\u1E61': 's',
    '\u0161': 's',
    '\u1E67': 's',
    '\u1E63': 's',
    '\u1E69': 's',
    '\u0219': 's',
    '\u015F': 's',
    '\u023F': 's',
    '\uA7A9': 's',
    '\uA785': 's',
    '\u1E9B': 's',
    '\u24E3': 't',
    '\uFF54': 't',
    '\u1E6B': 't',
    '\u1E97': 't',
    '\u0165': 't',
    '\u1E6D': 't',
    '\u021B': 't',
    '\u0163': 't',
    '\u1E71': 't',
    '\u1E6F': 't',
    '\u0167': 't',
    '\u01AD': 't',
    '\u0288': 't',
    '\u2C66': 't',
    '\uA787': 't',
    '\uA729': 'tz',
    '\u24E4': 'u',
    '\uFF55': 'u',
    '\u00F9': 'u',
    '\u00FA': 'u',
    '\u00FB': 'u',
    '\u0169': 'u',
    '\u1E79': 'u',
    '\u016B': 'u',
    '\u1E7B': 'u',
    '\u016D': 'u',
    '\u00FC': 'u',
    '\u01DC': 'u',
    '\u01D8': 'u',
    '\u01D6': 'u',
    '\u01DA': 'u',
    '\u1EE7': 'u',
    '\u016F': 'u',
    '\u0171': 'u',
    '\u01D4': 'u',
    '\u0215': 'u',
    '\u0217': 'u',
    '\u01B0': 'u',
    '\u1EEB': 'u',
    '\u1EE9': 'u',
    '\u1EEF': 'u',
    '\u1EED': 'u',
    '\u1EF1': 'u',
    '\u1EE5': 'u',
    '\u1E73': 'u',
    '\u0173': 'u',
    '\u1E77': 'u',
    '\u1E75': 'u',
    '\u0289': 'u',
    '\u24E5': 'v',
    '\uFF56': 'v',
    '\u1E7D': 'v',
    '\u1E7F': 'v',
    '\u028B': 'v',
    '\uA75F': 'v',
    '\u028C': 'v',
    '\uA761': 'vy',
    '\u24E6': 'w',
    '\uFF57': 'w',
    '\u1E81': 'w',
    '\u1E83': 'w',
    '\u0175': 'w',
    '\u1E87': 'w',
    '\u1E85': 'w',
    '\u1E98': 'w',
    '\u1E89': 'w',
    '\u2C73': 'w',
    '\u24E7': 'x',
    '\uFF58': 'x',
    '\u1E8B': 'x',
    '\u1E8D': 'x',
    '\u24E8': 'y',
    '\uFF59': 'y',
    '\u1EF3': 'y',
    '\u00FD': 'y',
    '\u0177': 'y',
    '\u1EF9': 'y',
    '\u0233': 'y',
    '\u1E8F': 'y',
    '\u00FF': 'y',
    '\u1EF7': 'y',
    '\u1E99': 'y',
    '\u1EF5': 'y',
    '\u01B4': 'y',
    '\u024F': 'y',
    '\u1EFF': 'y',
    '\u24E9': 'z',
    '\uFF5A': 'z',
    '\u017A': 'z',
    '\u1E91': 'z',
    '\u017C': 'z',
    '\u017E': 'z',
    '\u1E93': 'z',
    '\u1E95': 'z',
    '\u01B6': 'z',
    '\u0225': 'z',
    '\u0240': 'z',
    '\u2C6C': 'z',
    '\uA763': 'z',
    '\u0386': '\u0391',
    '\u0388': '\u0395',
    '\u0389': '\u0397',
    '\u038A': '\u0399',
    '\u03AA': '\u0399',
    '\u038C': '\u039F',
    '\u038E': '\u03A5',
    '\u03AB': '\u03A5',
    '\u038F': '\u03A9',
    '\u03AC': '\u03B1',
    '\u03AD': '\u03B5',
    '\u03AE': '\u03B7',
    '\u03AF': '\u03B9',
    '\u03CA': '\u03B9',
    '\u0390': '\u03B9',
    '\u03CC': '\u03BF',
    '\u03CD': '\u03C5',
    '\u03CB': '\u03C5',
    '\u03B0': '\u03C5',
    '\u03C9': '\u03C9',
    '\u03C2': '\u03C3'
  };

  return diacritics;
});

S2.define('select2/data/base',[
  '../utils'
], function (Utils) {
  function BaseAdapter ($element, options) {
    BaseAdapter.__super__.constructor.call(this);
  }

  Utils.Extend(BaseAdapter, Utils.Observable);

  BaseAdapter.prototype.current = function (callback) {
    throw new Error('The `current` method must be defined in child classes.');
  };

  BaseAdapter.prototype.query = function (params, callback) {
    throw new Error('The `query` method must be defined in child classes.');
  };

  BaseAdapter.prototype.bind = function (container, $container) {
    // Can be implemented in subclasses
  };

  BaseAdapter.prototype.destroy = function () {
    // Can be implemented in subclasses
  };

  BaseAdapter.prototype.generateResultId = function (container, data) {
    var id = container.id + '-result-';

    id += Utils.generateChars(4);

    if (data.id != null) {
      id += '-' + data.id.toString();
    } else {
      id += '-' + Utils.generateChars(4);
    }
    return id;
  };

  return BaseAdapter;
});

S2.define('select2/data/select',[
  './base',
  '../utils',
  'jquery'
], function (BaseAdapter, Utils, $) {
  function SelectAdapter ($element, options) {
    this.$element = $element;
    this.options = options;

    SelectAdapter.__super__.constructor.call(this);
  }

  Utils.Extend(SelectAdapter, BaseAdapter);

  SelectAdapter.prototype.current = function (callback) {
    var data = [];
    var self = this;

    this.$element.find(':selected').each(function () {
      var $option = $(this);

      var option = self.item($option);

      data.push(option);
    });

    callback(data);
  };

  SelectAdapter.prototype.select = function (data) {
    var self = this;

    data.selected = true;

    // If data.element is a DOM node, use it instead
    if ($(data.element).is('option')) {
      data.element.selected = true;

      this.$element.trigger('change');

      return;
    }

    if (this.$element.prop('multiple')) {
      this.current(function (currentData) {
        var val = [];

        data = [data];
        data.push.apply(data, currentData);

        for (var d = 0; d < data.length; d++) {
          var id = data[d].id;

          if ($.inArray(id, val) === -1) {
            val.push(id);
          }
        }

        self.$element.val(val);
        self.$element.trigger('change');
      });
    } else {
      var val = data.id;

      this.$element.val(val);
      this.$element.trigger('change');
    }
  };

  SelectAdapter.prototype.unselect = function (data) {
    var self = this;

    if (!this.$element.prop('multiple')) {
      return;
    }

    data.selected = false;

    if ($(data.element).is('option')) {
      data.element.selected = false;

      this.$element.trigger('change');

      return;
    }

    this.current(function (currentData) {
      var val = [];

      for (var d = 0; d < currentData.length; d++) {
        var id = currentData[d].id;

        if (id !== data.id && $.inArray(id, val) === -1) {
          val.push(id);
        }
      }

      self.$element.val(val);

      self.$element.trigger('change');
    });
  };

  SelectAdapter.prototype.bind = function (container, $container) {
    var self = this;

    this.container = container;

    container.on('select', function (params) {
      self.select(params.data);
    });

    container.on('unselect', function (params) {
      self.unselect(params.data);
    });
  };

  SelectAdapter.prototype.destroy = function () {
    // Remove anything added to child elements
    this.$element.find('*').each(function () {
      // Remove any custom data set by Select2
      Utils.RemoveData(this);
    });
  };

  SelectAdapter.prototype.query = function (params, callback) {
    var data = [];
    var self = this;

    var $options = this.$element.children();

    $options.each(function () {
      var $option = $(this);

      if (!$option.is('option') && !$option.is('optgroup')) {
        return;
      }

      var option = self.item($option);

      var matches = self.matches(params, option);

      if (matches !== null) {
        data.push(matches);
      }
    });

    callback({
      results: data
    });
  };

  SelectAdapter.prototype.addOptions = function ($options) {
    Utils.appendMany(this.$element, $options);
  };

  SelectAdapter.prototype.option = function (data) {
    var option;

    if (data.children) {
      option = document.createElement('optgroup');
      option.label = data.text;
    } else {
      option = document.createElement('option');

      if (option.textContent !== undefined) {
        option.textContent = data.text;
      } else {
        option.innerText = data.text;
      }
    }

    if (data.id !== undefined) {
      option.value = data.id;
    }

    if (data.disabled) {
      option.disabled = true;
    }

    if (data.selected) {
      option.selected = true;
    }

    if (data.title) {
      option.title = data.title;
    }

    var $option = $(option);

    var normalizedData = this._normalizeItem(data);
    normalizedData.element = option;

    // Override the option's data with the combined data
    Utils.StoreData(option, 'data', normalizedData);

    return $option;
  };

  SelectAdapter.prototype.item = function ($option) {
    var data = {};

    data = Utils.GetData($option[0], 'data');

    if (data != null) {
      return data;
    }

    if ($option.is('option')) {
      data = {
        id: $option.val(),
        text: $option.text(),
        disabled: $option.prop('disabled'),
        selected: $option.prop('selected'),
        title: $option.prop('title')
      };
    } else if ($option.is('optgroup')) {
      data = {
        text: $option.prop('label'),
        children: [],
        title: $option.prop('title')
      };

      var $children = $option.children('option');
      var children = [];

      for (var c = 0; c < $children.length; c++) {
        var $child = $($children[c]);

        var child = this.item($child);

        children.push(child);
      }

      data.children = children;
    }

    data = this._normalizeItem(data);
    data.element = $option[0];

    Utils.StoreData($option[0], 'data', data);

    return data;
  };

  SelectAdapter.prototype._normalizeItem = function (item) {
    if (item !== Object(item)) {
      item = {
        id: item,
        text: item
      };
    }

    item = $.extend({}, {
      text: ''
    }, item);

    var defaults = {
      selected: false,
      disabled: false
    };

    if (item.id != null) {
      item.id = item.id.toString();
    }

    if (item.text != null) {
      item.text = item.text.toString();
    }

    if (item._resultId == null && item.id && this.container != null) {
      item._resultId = this.generateResultId(this.container, item);
    }

    return $.extend({}, defaults, item);
  };

  SelectAdapter.prototype.matches = function (params, data) {
    var matcher = this.options.get('matcher');

    return matcher(params, data);
  };

  return SelectAdapter;
});

S2.define('select2/data/array',[
  './select',
  '../utils',
  'jquery'
], function (SelectAdapter, Utils, $) {
  function ArrayAdapter ($element, options) {
    var data = options.get('data') || [];

    ArrayAdapter.__super__.constructor.call(this, $element, options);

    this.addOptions(this.convertToOptions(data));
  }

  Utils.Extend(ArrayAdapter, SelectAdapter);

  ArrayAdapter.prototype.select = function (data) {
    var $option = this.$element.find('option').filter(function (i, elm) {
      return elm.value == data.id.toString();
    });

    if ($option.length === 0) {
      $option = this.option(data);

      this.addOptions($option);
    }

    ArrayAdapter.__super__.select.call(this, data);
  };

  ArrayAdapter.prototype.convertToOptions = function (data) {
    var self = this;

    var $existing = this.$element.find('option');
    var existingIds = $existing.map(function () {
      return self.item($(this)).id;
    }).get();

    var $options = [];

    // Filter out all items except for the one passed in the argument
    function onlyItem (item) {
      return function () {
        return $(this).val() == item.id;
      };
    }

    for (var d = 0; d < data.length; d++) {
      var item = this._normalizeItem(data[d]);

      // Skip items which were pre-loaded, only merge the data
      if ($.inArray(item.id, existingIds) >= 0) {
        var $existingOption = $existing.filter(onlyItem(item));

        var existingData = this.item($existingOption);
        var newData = $.extend(true, {}, item, existingData);

        var $newOption = this.option(newData);

        $existingOption.replaceWith($newOption);

        continue;
      }

      var $option = this.option(item);

      if (item.children) {
        var $children = this.convertToOptions(item.children);

        Utils.appendMany($option, $children);
      }

      $options.push($option);
    }

    return $options;
  };

  return ArrayAdapter;
});

S2.define('select2/data/ajax',[
  './array',
  '../utils',
  'jquery'
], function (ArrayAdapter, Utils, $) {
  function AjaxAdapter ($element, options) {
    this.ajaxOptions = this._applyDefaults(options.get('ajax'));

    if (this.ajaxOptions.processResults != null) {
      this.processResults = this.ajaxOptions.processResults;
    }

    AjaxAdapter.__super__.constructor.call(this, $element, options);
  }

  Utils.Extend(AjaxAdapter, ArrayAdapter);

  AjaxAdapter.prototype._applyDefaults = function (options) {
    var defaults = {
      data: function (params) {
        return $.extend({}, params, {
          q: params.term
        });
      },
      transport: function (params, success, failure) {
        var $request = $.ajax(params);

        $request.then(success);
        $request.fail(failure);

        return $request;
      }
    };

    return $.extend({}, defaults, options, true);
  };

  AjaxAdapter.prototype.processResults = function (results) {
    return results;
  };

  AjaxAdapter.prototype.query = function (params, callback) {
    var matches = [];
    var self = this;

    if (this._request != null) {
      // JSONP requests cannot always be aborted
      if ($.isFunction(this._request.abort)) {
        this._request.abort();
      }

      this._request = null;
    }

    var options = $.extend({
      type: 'GET'
    }, this.ajaxOptions);

    if (typeof options.url === 'function') {
      options.url = options.url.call(this.$element, params);
    }

    if (typeof options.data === 'function') {
      options.data = options.data.call(this.$element, params);
    }

    function request () {
      var $request = options.transport(options, function (data) {
        var results = self.processResults(data, params);

        if (self.options.get('debug') && window.console && console.error) {
          // Check to make sure that the response included a `results` key.
          if (!results || !results.results || !$.isArray(results.results)) {
            console.error(
              'Select2: The AJAX results did not return an array in the ' +
              '`results` key of the response.'
            );
          }
        }

        callback(results);
      }, function () {
        // Attempt to detect if a request was aborted
        // Only works if the transport exposes a status property
        if ('status' in $request &&
            ($request.status === 0 || $request.status === '0')) {
          return;
        }

        self.trigger('results:message', {
          message: 'errorLoading'
        });
      });

      self._request = $request;
    }

    if (this.ajaxOptions.delay && params.term != null) {
      if (this._queryTimeout) {
        window.clearTimeout(this._queryTimeout);
      }

      this._queryTimeout = window.setTimeout(request, this.ajaxOptions.delay);
    } else {
      request();
    }
  };

  return AjaxAdapter;
});

S2.define('select2/data/tags',[
  'jquery'
], function ($) {
  function Tags (decorated, $element, options) {
    var tags = options.get('tags');

    var createTag = options.get('createTag');

    if (createTag !== undefined) {
      this.createTag = createTag;
    }

    var insertTag = options.get('insertTag');

    if (insertTag !== undefined) {
        this.insertTag = insertTag;
    }

    decorated.call(this, $element, options);

    if ($.isArray(tags)) {
      for (var t = 0; t < tags.length; t++) {
        var tag = tags[t];
        var item = this._normalizeItem(tag);

        var $option = this.option(item);

        this.$element.append($option);
      }
    }
  }

  Tags.prototype.query = function (decorated, params, callback) {
    var self = this;

    this._removeOldTags();

    if (params.term == null || params.page != null) {
      decorated.call(this, params, callback);
      return;
    }

    function wrapper (obj, child) {
      var data = obj.results;

      for (var i = 0; i < data.length; i++) {
        var option = data[i];

        var checkChildren = (
          option.children != null &&
          !wrapper({
            results: option.children
          }, true)
        );

        var optionText = (option.text || '').toUpperCase();
        var paramsTerm = (params.term || '').toUpperCase();

        var checkText = optionText === paramsTerm;

        if (checkText || checkChildren) {
          if (child) {
            return false;
          }

          obj.data = data;
          callback(obj);

          return;
        }
      }

      if (child) {
        return true;
      }

      var tag = self.createTag(params);

      if (tag != null) {
        var $option = self.option(tag);
        $option.attr('data-select2-tag', true);

        self.addOptions([$option]);

        self.insertTag(data, tag);
      }

      obj.results = data;

      callback(obj);
    }

    decorated.call(this, params, wrapper);
  };

  Tags.prototype.createTag = function (decorated, params) {
    var term = $.trim(params.term);

    if (term === '') {
      return null;
    }

    return {
      id: term,
      text: term
    };
  };

  Tags.prototype.insertTag = function (_, data, tag) {
    data.unshift(tag);
  };

  Tags.prototype._removeOldTags = function (_) {
    var tag = this._lastTag;

    var $options = this.$element.find('option[data-select2-tag]');

    $options.each(function () {
      if (this.selected) {
        return;
      }

      $(this).remove();
    });
  };

  return Tags;
});

S2.define('select2/data/tokenizer',[
  'jquery'
], function ($) {
  function Tokenizer (decorated, $element, options) {
    var tokenizer = options.get('tokenizer');

    if (tokenizer !== undefined) {
      this.tokenizer = tokenizer;
    }

    decorated.call(this, $element, options);
  }

  Tokenizer.prototype.bind = function (decorated, container, $container) {
    decorated.call(this, container, $container);

    this.$search =  container.dropdown.$search || container.selection.$search ||
      $container.find('.select2-search__field');
  };

  Tokenizer.prototype.query = function (decorated, params, callback) {
    var self = this;

    function createAndSelect (data) {
      // Normalize the data object so we can use it for checks
      var item = self._normalizeItem(data);

      // Check if the data object already exists as a tag
      // Select it if it doesn't
      var $existingOptions = self.$element.find('option').filter(function () {
        return $(this).val() === item.id;
      });

      // If an existing option wasn't found for it, create the option
      if (!$existingOptions.length) {
        var $option = self.option(item);
        $option.attr('data-select2-tag', true);

        self._removeOldTags();
        self.addOptions([$option]);
      }

      // Select the item, now that we know there is an option for it
      select(item);
    }

    function select (data) {
      self.trigger('select', {
        data: data
      });
    }

    params.term = params.term || '';

    var tokenData = this.tokenizer(params, this.options, createAndSelect);

    if (tokenData.term !== params.term) {
      // Replace the search term if we have the search box
      if (this.$search.length) {
        this.$search.val(tokenData.term);
        this.$search.focus();
      }

      params.term = tokenData.term;
    }

    decorated.call(this, params, callback);
  };

  Tokenizer.prototype.tokenizer = function (_, params, options, callback) {
    var separators = options.get('tokenSeparators') || [];
    var term = params.term;
    var i = 0;

    var createTag = this.createTag || function (params) {
      return {
        id: params.term,
        text: params.term
      };
    };

    while (i < term.length) {
      var termChar = term[i];

      if ($.inArray(termChar, separators) === -1) {
        i++;

        continue;
      }

      var part = term.substr(0, i);
      var partParams = $.extend({}, params, {
        term: part
      });

      var data = createTag(partParams);

      if (data == null) {
        i++;
        continue;
      }

      callback(data);

      // Reset the term to not include the tokenized portion
      term = term.substr(i + 1) || '';
      i = 0;
    }

    return {
      term: term
    };
  };

  return Tokenizer;
});

S2.define('select2/data/minimumInputLength',[

], function () {
  function MinimumInputLength (decorated, $e, options) {
    this.minimumInputLength = options.get('minimumInputLength');

    decorated.call(this, $e, options);
  }

  MinimumInputLength.prototype.query = function (decorated, params, callback) {
    params.term = params.term || '';

    if (params.term.length < this.minimumInputLength) {
      this.trigger('results:message', {
        message: 'inputTooShort',
        args: {
          minimum: this.minimumInputLength,
          input: params.term,
          params: params
        }
      });

      return;
    }

    decorated.call(this, params, callback);
  };

  return MinimumInputLength;
});

S2.define('select2/data/maximumInputLength',[

], function () {
  function MaximumInputLength (decorated, $e, options) {
    this.maximumInputLength = options.get('maximumInputLength');

    decorated.call(this, $e, options);
  }

  MaximumInputLength.prototype.query = function (decorated, params, callback) {
    params.term = params.term || '';

    if (this.maximumInputLength > 0 &&
        params.term.length > this.maximumInputLength) {
      this.trigger('results:message', {
        message: 'inputTooLong',
        args: {
          maximum: this.maximumInputLength,
          input: params.term,
          params: params
        }
      });

      return;
    }

    decorated.call(this, params, callback);
  };

  return MaximumInputLength;
});

S2.define('select2/data/maximumSelectionLength',[

], function (){
  function MaximumSelectionLength (decorated, $e, options) {
    this.maximumSelectionLength = options.get('maximumSelectionLength');

    decorated.call(this, $e, options);
  }

  MaximumSelectionLength.prototype.query =
    function (decorated, params, callback) {
      var self = this;

      this.current(function (currentData) {
        var count = currentData != null ? currentData.length : 0;
        if (self.maximumSelectionLength > 0 &&
          count >= self.maximumSelectionLength) {
          self.trigger('results:message', {
            message: 'maximumSelected',
            args: {
              maximum: self.maximumSelectionLength
            }
          });
          return;
        }
        decorated.call(self, params, callback);
      });
  };

  return MaximumSelectionLength;
});

S2.define('select2/dropdown',[
  'jquery',
  './utils'
], function ($, Utils) {
  function Dropdown ($element, options) {
    this.$element = $element;
    this.options = options;

    Dropdown.__super__.constructor.call(this);
  }

  Utils.Extend(Dropdown, Utils.Observable);

  Dropdown.prototype.render = function () {
    var $dropdown = $(
      '<span class="select2-dropdown">' +
        '<span class="select2-results"></span>' +
      '</span>'
    );

    $dropdown.attr('dir', this.options.get('dir'));

    this.$dropdown = $dropdown;

    return $dropdown;
  };

  Dropdown.prototype.bind = function () {
    // Should be implemented in subclasses
  };

  Dropdown.prototype.position = function ($dropdown, $container) {
    // Should be implmented in subclasses
  };

  Dropdown.prototype.destroy = function () {
    // Remove the dropdown from the DOM
    this.$dropdown.remove();
  };

  return Dropdown;
});

S2.define('select2/dropdown/search',[
  'jquery',
  '../utils'
], function ($, Utils) {
  function Search () { }

  Search.prototype.render = function (decorated) {
    var $rendered = decorated.call(this);

    var $search = $(
      '<span class="select2-search select2-search--dropdown">' +
        '<input class="select2-search__field" type="search" tabindex="-1"' +
        ' autocomplete="off" autocorrect="off" autocapitalize="none"' +
        ' spellcheck="false" role="textbox" />' +
      '</span>'
    );

    this.$searchContainer = $search;
    this.$search = $search.find('input');

    $rendered.prepend($search);

    return $rendered;
  };

  Search.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    this.$search.on('keydown', function (evt) {
      self.trigger('keypress', evt);

      self._keyUpPrevented = evt.isDefaultPrevented();
    });

    // Workaround for browsers which do not support the `input` event
    // This will prevent double-triggering of events for browsers which support
    // both the `keyup` and `input` events.
    this.$search.on('input', function (evt) {
      // Unbind the duplicated `keyup` event
      $(this).off('keyup');
    });

    this.$search.on('keyup input', function (evt) {
      self.handleSearch(evt);
    });

    container.on('open', function () {
      self.$search.attr('tabindex', 0);

      self.$search.focus();

      window.setTimeout(function () {
        self.$search.focus();
      }, 0);
    });

    container.on('close', function () {
      self.$search.attr('tabindex', -1);

      self.$search.val('');
      self.$search.blur();
    });

    container.on('focus', function () {
      if (!container.isOpen()) {
        self.$search.focus();
      }
    });

    container.on('results:all', function (params) {
      if (params.query.term == null || params.query.term === '') {
        var showSearch = self.showSearch(params);

        if (showSearch) {
          self.$searchContainer.removeClass('select2-search--hide');
        } else {
          self.$searchContainer.addClass('select2-search--hide');
        }
      }
    });
  };

  Search.prototype.handleSearch = function (evt) {
    if (!this._keyUpPrevented) {
      var input = this.$search.val();

      this.trigger('query', {
        term: input
      });
    }

    this._keyUpPrevented = false;
  };

  Search.prototype.showSearch = function (_, params) {
    return true;
  };

  return Search;
});

S2.define('select2/dropdown/hidePlaceholder',[

], function () {
  function HidePlaceholder (decorated, $element, options, dataAdapter) {
    this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

    decorated.call(this, $element, options, dataAdapter);
  }

  HidePlaceholder.prototype.append = function (decorated, data) {
    data.results = this.removePlaceholder(data.results);

    decorated.call(this, data);
  };

  HidePlaceholder.prototype.normalizePlaceholder = function (_, placeholder) {
    if (typeof placeholder === 'string') {
      placeholder = {
        id: '',
        text: placeholder
      };
    }

    return placeholder;
  };

  HidePlaceholder.prototype.removePlaceholder = function (_, data) {
    var modifiedData = data.slice(0);

    for (var d = data.length - 1; d >= 0; d--) {
      var item = data[d];

      if (this.placeholder.id === item.id) {
        modifiedData.splice(d, 1);
      }
    }

    return modifiedData;
  };

  return HidePlaceholder;
});

S2.define('select2/dropdown/infiniteScroll',[
  'jquery'
], function ($) {
  function InfiniteScroll (decorated, $element, options, dataAdapter) {
    this.lastParams = {};

    decorated.call(this, $element, options, dataAdapter);

    this.$loadingMore = this.createLoadingMore();
    this.loading = false;
  }

  InfiniteScroll.prototype.append = function (decorated, data) {
    this.$loadingMore.remove();
    this.loading = false;

    decorated.call(this, data);

    if (this.showLoadingMore(data)) {
      this.$results.append(this.$loadingMore);
    }
  };

  InfiniteScroll.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('query', function (params) {
      self.lastParams = params;
      self.loading = true;
    });

    container.on('query:append', function (params) {
      self.lastParams = params;
      self.loading = true;
    });

    this.$results.on('scroll', function () {
      var isLoadMoreVisible = $.contains(
        document.documentElement,
        self.$loadingMore[0]
      );

      if (self.loading || !isLoadMoreVisible) {
        return;
      }

      var currentOffset = self.$results.offset().top +
        self.$results.outerHeight(false);
      var loadingMoreOffset = self.$loadingMore.offset().top +
        self.$loadingMore.outerHeight(false);

      if (currentOffset + 50 >= loadingMoreOffset) {
        self.loadMore();
      }
    });
  };

  InfiniteScroll.prototype.loadMore = function () {
    this.loading = true;

    var params = $.extend({}, {page: 1}, this.lastParams);

    params.page++;

    this.trigger('query:append', params);
  };

  InfiniteScroll.prototype.showLoadingMore = function (_, data) {
    return data.pagination && data.pagination.more;
  };

  InfiniteScroll.prototype.createLoadingMore = function () {
    var $option = $(
      '<li ' +
      'class="select2-results__option select2-results__option--load-more"' +
      'role="treeitem" aria-disabled="true"></li>'
    );

    var message = this.options.get('translations').get('loadingMore');

    $option.html(message(this.lastParams));

    return $option;
  };

  return InfiniteScroll;
});

S2.define('select2/dropdown/attachBody',[
  'jquery',
  '../utils'
], function ($, Utils) {
  function AttachBody (decorated, $element, options) {
    this.$dropdownParent = options.get('dropdownParent') || $(document.body);

    decorated.call(this, $element, options);
  }

  AttachBody.prototype.bind = function (decorated, container, $container) {
    var self = this;

    var setupResultsEvents = false;

    decorated.call(this, container, $container);

    container.on('open', function () {
      self._showDropdown();
      self._attachPositioningHandler(container);

      if (!setupResultsEvents) {
        setupResultsEvents = true;

        container.on('results:all', function () {
          self._positionDropdown();
          self._resizeDropdown();
        });

        container.on('results:append', function () {
          self._positionDropdown();
          self._resizeDropdown();
        });
      }
    });

    container.on('close', function () {
      self._hideDropdown();
      self._detachPositioningHandler(container);
    });

    this.$dropdownContainer.on('mousedown', function (evt) {
      evt.stopPropagation();
    });
  };

  AttachBody.prototype.destroy = function (decorated) {
    decorated.call(this);

    this.$dropdownContainer.remove();
  };

  AttachBody.prototype.position = function (decorated, $dropdown, $container) {
    // Clone all of the container classes
    $dropdown.attr('class', $container.attr('class'));

    $dropdown.removeClass('select2');
    $dropdown.addClass('select2-container--open');

    $dropdown.css({
      position: 'absolute',
      top: -999999
    });

    this.$container = $container;
  };

  AttachBody.prototype.render = function (decorated) {
    var $container = $('<span></span>');

    var $dropdown = decorated.call(this);
    $container.append($dropdown);

    this.$dropdownContainer = $container;

    return $container;
  };

  AttachBody.prototype._hideDropdown = function (decorated) {
    this.$dropdownContainer.detach();
  };

  AttachBody.prototype._attachPositioningHandler =
      function (decorated, container) {
    var self = this;

    var scrollEvent = 'scroll.select2.' + container.id;
    var resizeEvent = 'resize.select2.' + container.id;
    var orientationEvent = 'orientationchange.select2.' + container.id;

    var $watchers = this.$container.parents().filter(Utils.hasScroll);
    $watchers.each(function () {
      Utils.StoreData(this, 'select2-scroll-position', {
        x: $(this).scrollLeft(),
        y: $(this).scrollTop()
      });
    });

    $watchers.on(scrollEvent, function (ev) {
      var position = Utils.GetData(this, 'select2-scroll-position');
      $(this).scrollTop(position.y);
    });

    $(window).on(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent,
      function (e) {
      self._positionDropdown();
      self._resizeDropdown();
    });
  };

  AttachBody.prototype._detachPositioningHandler =
      function (decorated, container) {
    var scrollEvent = 'scroll.select2.' + container.id;
    var resizeEvent = 'resize.select2.' + container.id;
    var orientationEvent = 'orientationchange.select2.' + container.id;

    var $watchers = this.$container.parents().filter(Utils.hasScroll);
    $watchers.off(scrollEvent);

    $(window).off(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent);
  };

  AttachBody.prototype._positionDropdown = function () {
    var $window = $(window);

    var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
    var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

    var newDirection = null;

    var offset = this.$container.offset();

    offset.bottom = offset.top + this.$container.outerHeight(false);

    var container = {
      height: this.$container.outerHeight(false)
    };

    container.top = offset.top;
    container.bottom = offset.top + container.height;

    var dropdown = {
      height: this.$dropdown.outerHeight(false)
    };

    var viewport = {
      top: $window.scrollTop(),
      bottom: $window.scrollTop() + $window.height()
    };

    var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
    var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

    var css = {
      left: offset.left,
      top: container.bottom
    };

    // Determine what the parent element is to use for calciulating the offset
    var $offsetParent = this.$dropdownParent;

    // For statically positoned elements, we need to get the element
    // that is determining the offset
    if ($offsetParent.css('position') === 'static') {
      $offsetParent = $offsetParent.offsetParent();
    }

    var parentOffset = $offsetParent.offset();

    css.top -= parentOffset.top;
    css.left -= parentOffset.left;

    if (!isCurrentlyAbove && !isCurrentlyBelow) {
      newDirection = 'below';
    }

    if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
      newDirection = 'above';
    } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
      newDirection = 'below';
    }

    if (newDirection == 'above' ||
      (isCurrentlyAbove && newDirection !== 'below')) {
      css.top = container.top - parentOffset.top - dropdown.height;
    }

    if (newDirection != null) {
      this.$dropdown
        .removeClass('select2-dropdown--below select2-dropdown--above')
        .addClass('select2-dropdown--' + newDirection);
      this.$container
        .removeClass('select2-container--below select2-container--above')
        .addClass('select2-container--' + newDirection);
    }

    this.$dropdownContainer.css(css);
  };

  AttachBody.prototype._resizeDropdown = function () {
    var css = {
      width: this.$container.outerWidth(false) + 'px'
    };

    if (this.options.get('dropdownAutoWidth')) {
      css.minWidth = css.width;
      css.position = 'relative';
      css.width = 'auto';
    }

    this.$dropdown.css(css);
  };

  AttachBody.prototype._showDropdown = function (decorated) {
    this.$dropdownContainer.appendTo(this.$dropdownParent);

    this._positionDropdown();
    this._resizeDropdown();
  };

  return AttachBody;
});

S2.define('select2/dropdown/minimumResultsForSearch',[

], function () {
  function countResults (data) {
    var count = 0;

    for (var d = 0; d < data.length; d++) {
      var item = data[d];

      if (item.children) {
        count += countResults(item.children);
      } else {
        count++;
      }
    }

    return count;
  }

  function MinimumResultsForSearch (decorated, $element, options, dataAdapter) {
    this.minimumResultsForSearch = options.get('minimumResultsForSearch');

    if (this.minimumResultsForSearch < 0) {
      this.minimumResultsForSearch = Infinity;
    }

    decorated.call(this, $element, options, dataAdapter);
  }

  MinimumResultsForSearch.prototype.showSearch = function (decorated, params) {
    if (countResults(params.data.results) < this.minimumResultsForSearch) {
      return false;
    }

    return decorated.call(this, params);
  };

  return MinimumResultsForSearch;
});

S2.define('select2/dropdown/selectOnClose',[
  '../utils'
], function (Utils) {
  function SelectOnClose () { }

  SelectOnClose.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('close', function (params) {
      self._handleSelectOnClose(params);
    });
  };

  SelectOnClose.prototype._handleSelectOnClose = function (_, params) {
    if (params && params.originalSelect2Event != null) {
      var event = params.originalSelect2Event;

      // Don't select an item if the close event was triggered from a select or
      // unselect event
      if (event._type === 'select' || event._type === 'unselect') {
        return;
      }
    }

    var $highlightedResults = this.getHighlightedResults();

    // Only select highlighted results
    if ($highlightedResults.length < 1) {
      return;
    }

    var data = Utils.GetData($highlightedResults[0], 'data');

    // Don't re-select already selected resulte
    if (
      (data.element != null && data.element.selected) ||
      (data.element == null && data.selected)
    ) {
      return;
    }

    this.trigger('select', {
        data: data
    });
  };

  return SelectOnClose;
});

S2.define('select2/dropdown/closeOnSelect',[

], function () {
  function CloseOnSelect () { }

  CloseOnSelect.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('select', function (evt) {
      self._selectTriggered(evt);
    });

    container.on('unselect', function (evt) {
      self._selectTriggered(evt);
    });
  };

  CloseOnSelect.prototype._selectTriggered = function (_, evt) {
    var originalEvent = evt.originalEvent;

    // Don't close if the control key is being held
    if (originalEvent && originalEvent.ctrlKey) {
      return;
    }

    this.trigger('close', {
      originalEvent: originalEvent,
      originalSelect2Event: evt
    });
  };

  return CloseOnSelect;
});

S2.define('select2/i18n/en',[],function () {
  // English
  return {
    errorLoading: function () {
      return 'The results could not be loaded.';
    },
    inputTooLong: function (args) {
      var overChars = args.input.length - args.maximum;

      var message = 'Please delete ' + overChars + ' character';

      if (overChars != 1) {
        message += 's';
      }

      return message;
    },
    inputTooShort: function (args) {
      var remainingChars = args.minimum - args.input.length;

      var message = 'Please enter ' + remainingChars + ' or more characters';

      return message;
    },
    loadingMore: function () {
      return 'Loading more results…';
    },
    maximumSelected: function (args) {
      var message = 'You can only select ' + args.maximum + ' item';

      if (args.maximum != 1) {
        message += 's';
      }

      return message;
    },
    noResults: function () {
      return 'No results found';
    },
    searching: function () {
      return 'Searching…';
    }
  };
});

S2.define('select2/defaults',[
  'jquery',
  'require',

  './results',

  './selection/single',
  './selection/multiple',
  './selection/placeholder',
  './selection/allowClear',
  './selection/search',
  './selection/eventRelay',

  './utils',
  './translation',
  './diacritics',

  './data/select',
  './data/array',
  './data/ajax',
  './data/tags',
  './data/tokenizer',
  './data/minimumInputLength',
  './data/maximumInputLength',
  './data/maximumSelectionLength',

  './dropdown',
  './dropdown/search',
  './dropdown/hidePlaceholder',
  './dropdown/infiniteScroll',
  './dropdown/attachBody',
  './dropdown/minimumResultsForSearch',
  './dropdown/selectOnClose',
  './dropdown/closeOnSelect',

  './i18n/en'
], function ($, require,

             ResultsList,

             SingleSelection, MultipleSelection, Placeholder, AllowClear,
             SelectionSearch, EventRelay,

             Utils, Translation, DIACRITICS,

             SelectData, ArrayData, AjaxData, Tags, Tokenizer,
             MinimumInputLength, MaximumInputLength, MaximumSelectionLength,

             Dropdown, DropdownSearch, HidePlaceholder, InfiniteScroll,
             AttachBody, MinimumResultsForSearch, SelectOnClose, CloseOnSelect,

             EnglishTranslation) {
  function Defaults () {
    this.reset();
  }

  Defaults.prototype.apply = function (options) {
    options = $.extend(true, {}, this.defaults, options);

    if (options.dataAdapter == null) {
      if (options.ajax != null) {
        options.dataAdapter = AjaxData;
      } else if (options.data != null) {
        options.dataAdapter = ArrayData;
      } else {
        options.dataAdapter = SelectData;
      }

      if (options.minimumInputLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MinimumInputLength
        );
      }

      if (options.maximumInputLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MaximumInputLength
        );
      }

      if (options.maximumSelectionLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MaximumSelectionLength
        );
      }

      if (options.tags) {
        options.dataAdapter = Utils.Decorate(options.dataAdapter, Tags);
      }

      if (options.tokenSeparators != null || options.tokenizer != null) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          Tokenizer
        );
      }

      if (options.query != null) {
        var Query = require(options.amdBase + 'compat/query');

        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          Query
        );
      }

      if (options.initSelection != null) {
        var InitSelection = require(options.amdBase + 'compat/initSelection');

        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          InitSelection
        );
      }
    }

    if (options.resultsAdapter == null) {
      options.resultsAdapter = ResultsList;

      if (options.ajax != null) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          InfiniteScroll
        );
      }

      if (options.placeholder != null) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          HidePlaceholder
        );
      }

      if (options.selectOnClose) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          SelectOnClose
        );
      }
    }

    if (options.dropdownAdapter == null) {
      if (options.multiple) {
        options.dropdownAdapter = Dropdown;
      } else {
        var SearchableDropdown = Utils.Decorate(Dropdown, DropdownSearch);

        options.dropdownAdapter = SearchableDropdown;
      }

      if (options.minimumResultsForSearch !== 0) {
        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          MinimumResultsForSearch
        );
      }

      if (options.closeOnSelect) {
        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          CloseOnSelect
        );
      }

      if (
        options.dropdownCssClass != null ||
        options.dropdownCss != null ||
        options.adaptDropdownCssClass != null
      ) {
        var DropdownCSS = require(options.amdBase + 'compat/dropdownCss');

        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          DropdownCSS
        );
      }

      options.dropdownAdapter = Utils.Decorate(
        options.dropdownAdapter,
        AttachBody
      );
    }

    if (options.selectionAdapter == null) {
      if (options.multiple) {
        options.selectionAdapter = MultipleSelection;
      } else {
        options.selectionAdapter = SingleSelection;
      }

      // Add the placeholder mixin if a placeholder was specified
      if (options.placeholder != null) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          Placeholder
        );
      }

      if (options.allowClear) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          AllowClear
        );
      }

      if (options.multiple) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          SelectionSearch
        );
      }

      if (
        options.containerCssClass != null ||
        options.containerCss != null ||
        options.adaptContainerCssClass != null
      ) {
        var ContainerCSS = require(options.amdBase + 'compat/containerCss');

        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          ContainerCSS
        );
      }

      options.selectionAdapter = Utils.Decorate(
        options.selectionAdapter,
        EventRelay
      );
    }

    if (typeof options.language === 'string') {
      // Check if the language is specified with a region
      if (options.language.indexOf('-') > 0) {
        // Extract the region information if it is included
        var languageParts = options.language.split('-');
        var baseLanguage = languageParts[0];

        options.language = [options.language, baseLanguage];
      } else {
        options.language = [options.language];
      }
    }

    if ($.isArray(options.language)) {
      var languages = new Translation();
      options.language.push('en');

      var languageNames = options.language;

      for (var l = 0; l < languageNames.length; l++) {
        var name = languageNames[l];
        var language = {};

        try {
          // Try to load it with the original name
          language = Translation.loadPath(name);
        } catch (e) {
          try {
            // If we couldn't load it, check if it wasn't the full path
            name = this.defaults.amdLanguageBase + name;
            language = Translation.loadPath(name);
          } catch (ex) {
            // The translation could not be loaded at all. Sometimes this is
            // because of a configuration problem, other times this can be
            // because of how Select2 helps load all possible translation files.
            if (options.debug && window.console && console.warn) {
              console.warn(
                'Select2: The language file for "' + name + '" could not be ' +
                'automatically loaded. A fallback will be used instead.'
              );
            }

            continue;
          }
        }

        languages.extend(language);
      }

      options.translations = languages;
    } else {
      var baseTranslation = Translation.loadPath(
        this.defaults.amdLanguageBase + 'en'
      );
      var customTranslation = new Translation(options.language);

      customTranslation.extend(baseTranslation);

      options.translations = customTranslation;
    }

    return options;
  };

  Defaults.prototype.reset = function () {
    function stripDiacritics (text) {
      // Used 'uni range + named function' from http://jsperf.com/diacritics/18
      function match(a) {
        return DIACRITICS[a] || a;
      }

      return text.replace(/[^\u0000-\u007E]/g, match);
    }

    function matcher (params, data) {
      // Always return the object if there is nothing to compare
      if ($.trim(params.term) === '') {
        return data;
      }

      // Do a recursive check for options with children
      if (data.children && data.children.length > 0) {
        // Clone the data object if there are children
        // This is required as we modify the object to remove any non-matches
        var match = $.extend(true, {}, data);

        // Check each child of the option
        for (var c = data.children.length - 1; c >= 0; c--) {
          var child = data.children[c];

          var matches = matcher(params, child);

          // If there wasn't a match, remove the object in the array
          if (matches == null) {
            match.children.splice(c, 1);
          }
        }

        // If any children matched, return the new object
        if (match.children.length > 0) {
          return match;
        }

        // If there were no matching children, check just the plain object
        return matcher(params, match);
      }

      var original = stripDiacritics(data.text).toUpperCase();
      var term = stripDiacritics(params.term).toUpperCase();

      // Check if the text contains the term
      if (original.indexOf(term) > -1) {
        return data;
      }

      // If it doesn't contain the term, don't return anything
      return null;
    }

    this.defaults = {
      amdBase: './',
      amdLanguageBase: './i18n/',
      closeOnSelect: true,
      debug: false,
      dropdownAutoWidth: false,
      escapeMarkup: Utils.escapeMarkup,
      language: EnglishTranslation,
      matcher: matcher,
      minimumInputLength: 0,
      maximumInputLength: 0,
      maximumSelectionLength: 0,
      minimumResultsForSearch: 0,
      selectOnClose: false,
      sorter: function (data) {
        return data;
      },
      templateResult: function (result) {
        return result.text;
      },
      templateSelection: function (selection) {
        return selection.text;
      },
      theme: 'default',
      width: 'resolve'
    };
  };

  Defaults.prototype.set = function (key, value) {
    var camelKey = $.camelCase(key);

    var data = {};
    data[camelKey] = value;

    var convertedData = Utils._convertData(data);

    $.extend(true, this.defaults, convertedData);
  };

  var defaults = new Defaults();

  return defaults;
});

S2.define('select2/options',[
  'require',
  'jquery',
  './defaults',
  './utils'
], function (require, $, Defaults, Utils) {
  function Options (options, $element) {
    this.options = options;

    if ($element != null) {
      this.fromElement($element);
    }

    this.options = Defaults.apply(this.options);

    if ($element && $element.is('input')) {
      var InputCompat = require(this.get('amdBase') + 'compat/inputData');

      this.options.dataAdapter = Utils.Decorate(
        this.options.dataAdapter,
        InputCompat
      );
    }
  }

  Options.prototype.fromElement = function ($e) {
    var excludedData = ['select2'];

    if (this.options.multiple == null) {
      this.options.multiple = $e.prop('multiple');
    }

    if (this.options.disabled == null) {
      this.options.disabled = $e.prop('disabled');
    }

    if (this.options.language == null) {
      if ($e.prop('lang')) {
        this.options.language = $e.prop('lang').toLowerCase();
      } else if ($e.closest('[lang]').prop('lang')) {
        this.options.language = $e.closest('[lang]').prop('lang');
      }
    }

    if (this.options.dir == null) {
      if ($e.prop('dir')) {
        this.options.dir = $e.prop('dir');
      } else if ($e.closest('[dir]').prop('dir')) {
        this.options.dir = $e.closest('[dir]').prop('dir');
      } else {
        this.options.dir = 'ltr';
      }
    }

    $e.prop('disabled', this.options.disabled);
    $e.prop('multiple', this.options.multiple);

    if (Utils.GetData($e[0], 'select2Tags')) {
      if (this.options.debug && window.console && console.warn) {
        console.warn(
          'Select2: The `data-select2-tags` attribute has been changed to ' +
          'use the `data-data` and `data-tags="true"` attributes and will be ' +
          'removed in future versions of Select2.'
        );
      }

      Utils.StoreData($e[0], 'data', Utils.GetData($e[0], 'select2Tags'));
      Utils.StoreData($e[0], 'tags', true);
    }

    if (Utils.GetData($e[0], 'ajaxUrl')) {
      if (this.options.debug && window.console && console.warn) {
        console.warn(
          'Select2: The `data-ajax-url` attribute has been changed to ' +
          '`data-ajax--url` and support for the old attribute will be removed' +
          ' in future versions of Select2.'
        );
      }

      $e.attr('ajax--url', Utils.GetData($e[0], 'ajaxUrl'));
      Utils.StoreData($e[0], 'ajax-Url', Utils.GetData($e[0], 'ajaxUrl'));
	  
    }

    var dataset = {};

    // Prefer the element's `dataset` attribute if it exists
    // jQuery 1.x does not correctly handle data attributes with multiple dashes
    if ($.fn.jquery && $.fn.jquery.substr(0, 2) == '1.' && $e[0].dataset) {
      dataset = $.extend(true, {}, $e[0].dataset, Utils.GetData($e[0]));
    } else {
      dataset = Utils.GetData($e[0]);
    }

    var data = $.extend(true, {}, dataset);

    data = Utils._convertData(data);

    for (var key in data) {
      if ($.inArray(key, excludedData) > -1) {
        continue;
      }

      if ($.isPlainObject(this.options[key])) {
        $.extend(this.options[key], data[key]);
      } else {
        this.options[key] = data[key];
      }
    }

    return this;
  };

  Options.prototype.get = function (key) {
    return this.options[key];
  };

  Options.prototype.set = function (key, val) {
    this.options[key] = val;
  };

  return Options;
});

S2.define('select2/core',[
  'jquery',
  './options',
  './utils',
  './keys'
], function ($, Options, Utils, KEYS) {
  var Select2 = function ($element, options) {
    if (Utils.GetData($element[0], 'select2') != null) {
      Utils.GetData($element[0], 'select2').destroy();
    }

    this.$element = $element;

    this.id = this._generateId($element);

    options = options || {};

    this.options = new Options(options, $element);

    Select2.__super__.constructor.call(this);

    // Set up the tabindex

    var tabindex = $element.attr('tabindex') || 0;
    Utils.StoreData($element[0], 'old-tabindex', tabindex);
    $element.attr('tabindex', '-1');

    // Set up containers and adapters

    var DataAdapter = this.options.get('dataAdapter');
    this.dataAdapter = new DataAdapter($element, this.options);

    var $container = this.render();

    this._placeContainer($container);

    var SelectionAdapter = this.options.get('selectionAdapter');
    this.selection = new SelectionAdapter($element, this.options);
    this.$selection = this.selection.render();

    this.selection.position(this.$selection, $container);

    var DropdownAdapter = this.options.get('dropdownAdapter');
    this.dropdown = new DropdownAdapter($element, this.options);
    this.$dropdown = this.dropdown.render();

    this.dropdown.position(this.$dropdown, $container);

    var ResultsAdapter = this.options.get('resultsAdapter');
    this.results = new ResultsAdapter($element, this.options, this.dataAdapter);
    this.$results = this.results.render();

    this.results.position(this.$results, this.$dropdown);

    // Bind events

    var self = this;

    // Bind the container to all of the adapters
    this._bindAdapters();

    // Register any DOM event handlers
    this._registerDomEvents();

    // Register any internal event handlers
    this._registerDataEvents();
    this._registerSelectionEvents();
    this._registerDropdownEvents();
    this._registerResultsEvents();
    this._registerEvents();

    // Set the initial state
    this.dataAdapter.current(function (initialData) {
      self.trigger('selection:update', {
        data: initialData
      });
    });

    // Hide the original select
    $element.addClass('select2-hidden-accessible');
    $element.attr('aria-hidden', 'true');

    // Synchronize any monitored attributes
    this._syncAttributes();

    Utils.StoreData($element[0], 'select2', this);

    // Ensure backwards compatibility with $element.data('select2').
    $element.data('select2', this);
  };

  Utils.Extend(Select2, Utils.Observable);

  Select2.prototype._generateId = function ($element) {
    var id = '';

    if ($element.attr('id') != null) {
      id = $element.attr('id');
    } else if ($element.attr('name') != null) {
      id = $element.attr('name') + '-' + Utils.generateChars(2);
    } else {
      id = Utils.generateChars(4);
    }

    id = id.replace(/(:|\.|\[|\]|,)/g, '');
    id = 'select2-' + id;

    return id;
  };

  Select2.prototype._placeContainer = function ($container) {
    $container.insertAfter(this.$element);

    var width = this._resolveWidth(this.$element, this.options.get('width'));

    if (width != null) {
      $container.css('width', width);
    }
  };

  Select2.prototype._resolveWidth = function ($element, method) {
    var WIDTH = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;

    if (method == 'resolve') {
      var styleWidth = this._resolveWidth($element, 'style');

      if (styleWidth != null) {
        return styleWidth;
      }

      return this._resolveWidth($element, 'element');
    }

    if (method == 'element') {
      var elementWidth = $element.outerWidth(false);

      if (elementWidth <= 0) {
        return 'auto';
      }

      return elementWidth + 'px';
    }

    if (method == 'style') {
      var style = $element.attr('style');

      if (typeof(style) !== 'string') {
        return null;
      }

      var attrs = style.split(';');

      for (var i = 0, l = attrs.length; i < l; i = i + 1) {
        var attr = attrs[i].replace(/\s/g, '');
        var matches = attr.match(WIDTH);

        if (matches !== null && matches.length >= 1) {
          return matches[1];
        }
      }

      return null;
    }

    return method;
  };

  Select2.prototype._bindAdapters = function () {
    this.dataAdapter.bind(this, this.$container);
    this.selection.bind(this, this.$container);

    this.dropdown.bind(this, this.$container);
    this.results.bind(this, this.$container);
  };

  Select2.prototype._registerDomEvents = function () {
    var self = this;

    this.$element.on('change.select2', function () {
      self.dataAdapter.current(function (data) {
        self.trigger('selection:update', {
          data: data
        });
      });
    });

    this.$element.on('focus.select2', function (evt) {
      self.trigger('focus', evt);
    });

    this._syncA = Utils.bind(this._syncAttributes, this);
    this._syncS = Utils.bind(this._syncSubtree, this);

    if (this.$element[0].attachEvent) {
      this.$element[0].attachEvent('onpropertychange', this._syncA);
    }

    var observer = window.MutationObserver ||
      window.WebKitMutationObserver ||
      window.MozMutationObserver
    ;

    if (observer != null) {
      this._observer = new observer(function (mutations) {
        $.each(mutations, self._syncA);
        $.each(mutations, self._syncS);
      });
      this._observer.observe(this.$element[0], {
        attributes: true,
        childList: true,
        subtree: false
      });
    } else if (this.$element[0].addEventListener) {
      this.$element[0].addEventListener(
        'DOMAttrModified',
        self._syncA,
        false
      );
      this.$element[0].addEventListener(
        'DOMNodeInserted',
        self._syncS,
        false
      );
      this.$element[0].addEventListener(
        'DOMNodeRemoved',
        self._syncS,
        false
      );
    }
  };

  Select2.prototype._registerDataEvents = function () {
    var self = this;

    this.dataAdapter.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerSelectionEvents = function () {
    var self = this;
    var nonRelayEvents = ['toggle', 'focus'];

    this.selection.on('toggle', function () {
      self.toggleDropdown();
    });

    this.selection.on('focus', function (params) {
      self.focus(params);
    });

    this.selection.on('*', function (name, params) {
      if ($.inArray(name, nonRelayEvents) !== -1) {
        return;
      }

      self.trigger(name, params);
    });
  };

  Select2.prototype._registerDropdownEvents = function () {
    var self = this;

    this.dropdown.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerResultsEvents = function () {
    var self = this;

    this.results.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerEvents = function () {
    var self = this;

    this.on('open', function () {
      self.$container.addClass('select2-container--open');
    });

    this.on('close', function () {
      self.$container.removeClass('select2-container--open');
    });

    this.on('enable', function () {
      self.$container.removeClass('select2-container--disabled');
    });

    this.on('disable', function () {
      self.$container.addClass('select2-container--disabled');
    });

    this.on('blur', function () {
      self.$container.removeClass('select2-container--focus');
    });

    this.on('query', function (params) {
      if (!self.isOpen()) {
        self.trigger('open', {});
      }

      this.dataAdapter.query(params, function (data) {
        self.trigger('results:all', {
          data: data,
          query: params
        });
      });
    });

    this.on('query:append', function (params) {
      this.dataAdapter.query(params, function (data) {
        self.trigger('results:append', {
          data: data,
          query: params
        });
      });
    });

    this.on('keypress', function (evt) {
      var key = evt.which;

      if (self.isOpen()) {
        if (key === KEYS.ESC || key === KEYS.TAB ||
            (key === KEYS.UP && evt.altKey)) {
          self.close();

          evt.preventDefault();
        } else if (key === KEYS.ENTER) {
          self.trigger('results:select', {});

          evt.preventDefault();
        } else if ((key === KEYS.SPACE && evt.ctrlKey)) {
          self.trigger('results:toggle', {});

          evt.preventDefault();
        } else if (key === KEYS.UP) {
          self.trigger('results:previous', {});

          evt.preventDefault();
        } else if (key === KEYS.DOWN) {
          self.trigger('results:next', {});

          evt.preventDefault();
        }
      } else {
        if (key === KEYS.ENTER || key === KEYS.SPACE ||
            (key === KEYS.DOWN && evt.altKey)) {
          self.open();

          evt.preventDefault();
        }
      }
    });
  };

  Select2.prototype._syncAttributes = function () {
    this.options.set('disabled', this.$element.prop('disabled'));

    if (this.options.get('disabled')) {
      if (this.isOpen()) {
        this.close();
      }

      this.trigger('disable', {});
    } else {
      this.trigger('enable', {});
    }
  };

  Select2.prototype._syncSubtree = function (evt, mutations) {
    var changed = false;
    var self = this;

    // Ignore any mutation events raised for elements that aren't options or
    // optgroups. This handles the case when the select element is destroyed
    if (
      evt && evt.target && (
        evt.target.nodeName !== 'OPTION' && evt.target.nodeName !== 'OPTGROUP'
      )
    ) {
      return;
    }

    if (!mutations) {
      // If mutation events aren't supported, then we can only assume that the
      // change affected the selections
      changed = true;
    } else if (mutations.addedNodes && mutations.addedNodes.length > 0) {
      for (var n = 0; n < mutations.addedNodes.length; n++) {
        var node = mutations.addedNodes[n];

        if (node.selected) {
          changed = true;
        }
      }
    } else if (mutations.removedNodes && mutations.removedNodes.length > 0) {
      changed = true;
    }

    // Only re-pull the data if we think there is a change
    if (changed) {
      this.dataAdapter.current(function (currentData) {
        self.trigger('selection:update', {
          data: currentData
        });
      });
    }
  };

  /**
   * Override the trigger method to automatically trigger pre-events when
   * there are events that can be prevented.
   */
  Select2.prototype.trigger = function (name, args) {
    var actualTrigger = Select2.__super__.trigger;
    var preTriggerMap = {
      'open': 'opening',
      'close': 'closing',
      'select': 'selecting',
      'unselect': 'unselecting',
      'clear': 'clearing'
    };

    if (args === undefined) {
      args = {};
    }

    if (name in preTriggerMap) {
      var preTriggerName = preTriggerMap[name];
      var preTriggerArgs = {
        prevented: false,
        name: name,
        args: args
      };

      actualTrigger.call(this, preTriggerName, preTriggerArgs);

      if (preTriggerArgs.prevented) {
        args.prevented = true;

        return;
      }
    }

    actualTrigger.call(this, name, args);
  };

  Select2.prototype.toggleDropdown = function () {
    if (this.options.get('disabled')) {
      return;
    }

    if (this.isOpen()) {
      this.close();
    } else {
      this.open();
    }
  };

  Select2.prototype.open = function () {
    if (this.isOpen()) {
      return;
    }

    this.trigger('query', {});
  };

  Select2.prototype.close = function () {
    if (!this.isOpen()) {
      return;
    }

    this.trigger('close', {});
  };

  Select2.prototype.isOpen = function () {
    return this.$container.hasClass('select2-container--open');
  };

  Select2.prototype.hasFocus = function () {
    return this.$container.hasClass('select2-container--focus');
  };

  Select2.prototype.focus = function (data) {
    // No need to re-trigger focus events if we are already focused
    if (this.hasFocus()) {
      return;
    }

    this.$container.addClass('select2-container--focus');
    this.trigger('focus', {});
  };

  Select2.prototype.enable = function (args) {
    if (this.options.get('debug') && window.console && console.warn) {
      console.warn(
        'Select2: The `select2("enable")` method has been deprecated and will' +
        ' be removed in later Select2 versions. Use $element.prop("disabled")' +
        ' instead.'
      );
    }

    if (args == null || args.length === 0) {
      args = [true];
    }

    var disabled = !args[0];

    this.$element.prop('disabled', disabled);
  };

  Select2.prototype.data = function () {
    if (this.options.get('debug') &&
        arguments.length > 0 && window.console && console.warn) {
      console.warn(
        'Select2: Data can no longer be set using `select2("data")`. You ' +
        'should consider setting the value instead using `$element.val()`.'
      );
    }

    var data = [];

    this.dataAdapter.current(function (currentData) {
      data = currentData;
    });

    return data;
  };

  Select2.prototype.val = function (args) {
    if (this.options.get('debug') && window.console && console.warn) {
      console.warn(
        'Select2: The `select2("val")` method has been deprecated and will be' +
        ' removed in later Select2 versions. Use $element.val() instead.'
      );
    }

    if (args == null || args.length === 0) {
      return this.$element.val();
    }

    var newVal = args[0];

    if ($.isArray(newVal)) {
      newVal = $.map(newVal, function (obj) {
        return obj.toString();
      });
    }

    this.$element.val(newVal).trigger('change');
  };

  Select2.prototype.destroy = function () {
    this.$container.remove();

    if (this.$element[0].detachEvent) {
      this.$element[0].detachEvent('onpropertychange', this._syncA);
    }

    if (this._observer != null) {
      this._observer.disconnect();
      this._observer = null;
    } else if (this.$element[0].removeEventListener) {
      this.$element[0]
        .removeEventListener('DOMAttrModified', this._syncA, false);
      this.$element[0]
        .removeEventListener('DOMNodeInserted', this._syncS, false);
      this.$element[0]
        .removeEventListener('DOMNodeRemoved', this._syncS, false);
    }

    this._syncA = null;
    this._syncS = null;

    this.$element.off('.select2');
    this.$element.attr('tabindex',
    Utils.GetData(this.$element[0], 'old-tabindex'));

    this.$element.removeClass('select2-hidden-accessible');
    this.$element.attr('aria-hidden', 'false');
    Utils.RemoveData(this.$element[0]);
    this.$element.removeData('select2');

    this.dataAdapter.destroy();
    this.selection.destroy();
    this.dropdown.destroy();
    this.results.destroy();

    this.dataAdapter = null;
    this.selection = null;
    this.dropdown = null;
    this.results = null;
  };

  Select2.prototype.render = function () {
    var $container = $(
      '<span class="select2 select2-container">' +
        '<span class="selection"></span>' +
        '<span class="dropdown-wrapper" aria-hidden="true"></span>' +
      '</span>'
    );

    $container.attr('dir', this.options.get('dir'));

    this.$container = $container;

    this.$container.addClass('select2-container--' + this.options.get('theme'));

    Utils.StoreData($container[0], 'element', this.$element);

    return $container;
  };

  return Select2;
});

S2.define('jquery-mousewheel',[
  'jquery'
], function ($) {
  // Used to shim jQuery.mousewheel for non-full builds.
  return $;
});

S2.define('jquery.select2',[
  'jquery',
  'jquery-mousewheel',

  './select2/core',
  './select2/defaults',
  './select2/utils'
], function ($, _, Select2, Defaults, Utils) {
  if ($.fn.select2 == null) {
    // All methods that should return the element
    var thisMethods = ['open', 'close', 'destroy'];

    $.fn.select2 = function (options) {
      options = options || {};

      if (typeof options === 'object') {
        this.each(function () {
          var instanceOptions = $.extend(true, {}, options);

          var instance = new Select2($(this), instanceOptions);
        });

        return this;
      } else if (typeof options === 'string') {
        var ret;
        var args = Array.prototype.slice.call(arguments, 1);

        this.each(function () {
          var instance = Utils.GetData(this, 'select2');

          if (instance == null && window.console && console.error) {
            console.error(
              'The select2(\'' + options + '\') method was called on an ' +
              'element that is not using Select2.'
            );
          }

          ret = instance[options].apply(instance, args);
        });

        // Check if we should be returning `this`
        if ($.inArray(options, thisMethods) > -1) {
          return this;
        }

        return ret;
      } else {
        throw new Error('Invalid arguments for Select2: ' + options);
      }
    };
  }

  if ($.fn.select2.defaults == null) {
    $.fn.select2.defaults = Defaults;
  }

  return Select2;
});

  // Return the AMD loader configuration so it can be used outside of this file
  return {
    define: S2.define,
    require: S2.require
  };
}());

  // Autoload the jQuery bindings
  // We know that all of the modules exist above this, so we're safe
  var select2 = S2.require('jquery.select2');

  // Hold the AMD module references on the jQuery function that was just loaded
  // This allows Select2 to use the internal loader outside of this file, such
  // as in the language files.
  jQuery.fn.select2.amd = S2;

  // Return the Select2 instance for anyone who is importing it.
  return select2;
}));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/node_modules/spectrum-colorpicker/spectrum.js":[function(require,module,exports){
(function (global){
// Spectrum Colorpicker v1.8.0
// https://github.com/bgrins/spectrum
// Author: Brian Grinstead
// License: MIT

(function (factory) {
    "use strict";

    if (typeof define === 'function' && define.amd) { // AMD
        define(['jquery'], factory);
    }
    else if (typeof exports == "object" && typeof module == "object") { // CommonJS
        module.exports = factory((typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null));
    }
    else { // Browser
        factory(jQuery);
    }
})(function($, undefined) {
    "use strict";

    var defaultOpts = {

        // Callbacks
        beforeShow: noop,
        move: noop,
        change: noop,
        show: noop,
        hide: noop,

        // Options
        color: false,
        flat: false,
        showInput: false,
        allowEmpty: false,
        showButtons: true,
        clickoutFiresChange: true,
        showInitial: false,
        showPalette: false,
        showPaletteOnly: false,
        hideAfterPaletteSelect: false,
        togglePaletteOnly: false,
        showSelectionPalette: true,
        localStorageKey: false,
        appendTo: "body",
        maxSelectionSize: 7,
        cancelText: "cancel",
        chooseText: "choose",
        togglePaletteMoreText: "more",
        togglePaletteLessText: "less",
        clearText: "Clear Color Selection",
        noColorSelectedText: "No Color Selected",
        preferredFormat: false,
        className: "", // Deprecated - use containerClassName and replacerClassName instead.
        containerClassName: "",
        replacerClassName: "",
        showAlpha: false,
        theme: "sp-light",
        palette: [["#ffffff", "#000000", "#ff0000", "#ff8000", "#ffff00", "#008000", "#0000ff", "#4b0082", "#9400d3"]],
        selectionPalette: [],
        disabled: false,
        offset: null
    },
    spectrums = [],
    IE = !!/msie/i.exec( window.navigator.userAgent ),
    rgbaSupport = (function() {
        function contains( str, substr ) {
            return !!~('' + str).indexOf(substr);
        }

        var elem = document.createElement('div');
        var style = elem.style;
        style.cssText = 'background-color:rgba(0,0,0,.5)';
        return contains(style.backgroundColor, 'rgba') || contains(style.backgroundColor, 'hsla');
    })(),
    replaceInput = [
        "<div class='sp-replacer'>",
            "<div class='sp-preview'><div class='sp-preview-inner'></div></div>",
            "<div class='sp-dd'>&#9660;</div>",
        "</div>"
    ].join(''),
    markup = (function () {

        // IE does not support gradients with multiple stops, so we need to simulate
        //  that for the rainbow slider with 8 divs that each have a single gradient
        var gradientFix = "";
        if (IE) {
            for (var i = 1; i <= 6; i++) {
                gradientFix += "<div class='sp-" + i + "'></div>";
            }
        }

        return [
            "<div class='sp-container sp-hidden'>",
                "<div class='sp-palette-container'>",
                    "<div class='sp-palette sp-thumb sp-cf'></div>",
                    "<div class='sp-palette-button-container sp-cf'>",
                        "<button type='button' class='sp-palette-toggle'></button>",
                    "</div>",
                "</div>",
                "<div class='sp-picker-container'>",
                    "<div class='sp-top sp-cf'>",
                        "<div class='sp-fill'></div>",
                        "<div class='sp-top-inner'>",
                            "<div class='sp-color'>",
                                "<div class='sp-sat'>",
                                    "<div class='sp-val'>",
                                        "<div class='sp-dragger'></div>",
                                    "</div>",
                                "</div>",
                            "</div>",
                            "<div class='sp-clear sp-clear-display'>",
                            "</div>",
                            "<div class='sp-hue'>",
                                "<div class='sp-slider'></div>",
                                gradientFix,
                            "</div>",
                        "</div>",
                        "<div class='sp-alpha'><div class='sp-alpha-inner'><div class='sp-alpha-handle'></div></div></div>",
                    "</div>",
                    "<div class='sp-input-container sp-cf'>",
                        "<input class='sp-input' type='text' spellcheck='false'  />",
                    "</div>",
                    "<div class='sp-initial sp-thumb sp-cf'></div>",
                    "<div class='sp-button-container sp-cf'>",
                        "<a class='sp-cancel' href='#'></a>",
                        "<button type='button' class='sp-choose'></button>",
                    "</div>",
                "</div>",
            "</div>"
        ].join("");
    })();

    function paletteTemplate (p, color, className, opts) {
        var html = [];
        for (var i = 0; i < p.length; i++) {
            var current = p[i];
            if(current) {
                var tiny = tinycolor(current);
                var c = tiny.toHsl().l < 0.5 ? "sp-thumb-el sp-thumb-dark" : "sp-thumb-el sp-thumb-light";
                c += (tinycolor.equals(color, current)) ? " sp-thumb-active" : "";
                var formattedString = tiny.toString(opts.preferredFormat || "rgb");
                var swatchStyle = rgbaSupport ? ("background-color:" + tiny.toRgbString()) : "filter:" + tiny.toFilter();
                html.push('<span title="' + formattedString + '" data-color="' + tiny.toRgbString() + '" class="' + c + '"><span class="sp-thumb-inner" style="' + swatchStyle + ';" /></span>');
            } else {
                var cls = 'sp-clear-display';
                html.push($('<div />')
                    .append($('<span data-color="" style="background-color:transparent;" class="' + cls + '"></span>')
                        .attr('title', opts.noColorSelectedText)
                    )
                    .html()
                );
            }
        }
        return "<div class='sp-cf " + className + "'>" + html.join('') + "</div>";
    }

    function hideAll() {
        for (var i = 0; i < spectrums.length; i++) {
            if (spectrums[i]) {
                spectrums[i].hide();
            }
        }
    }

    function instanceOptions(o, callbackContext) {
        var opts = $.extend({}, defaultOpts, o);
        opts.callbacks = {
            'move': bind(opts.move, callbackContext),
            'change': bind(opts.change, callbackContext),
            'show': bind(opts.show, callbackContext),
            'hide': bind(opts.hide, callbackContext),
            'beforeShow': bind(opts.beforeShow, callbackContext)
        };

        return opts;
    }

    function spectrum(element, o) {

        var opts = instanceOptions(o, element),
            flat = opts.flat,
            showSelectionPalette = opts.showSelectionPalette,
            localStorageKey = opts.localStorageKey,
            theme = opts.theme,
            callbacks = opts.callbacks,
            resize = throttle(reflow, 10),
            visible = false,
            isDragging = false,
            dragWidth = 0,
            dragHeight = 0,
            dragHelperHeight = 0,
            slideHeight = 0,
            slideWidth = 0,
            alphaWidth = 0,
            alphaSlideHelperWidth = 0,
            slideHelperHeight = 0,
            currentHue = 0,
            currentSaturation = 0,
            currentValue = 0,
            currentAlpha = 1,
            palette = [],
            paletteArray = [],
            paletteLookup = {},
            selectionPalette = opts.selectionPalette.slice(0),
            maxSelectionSize = opts.maxSelectionSize,
            draggingClass = "sp-dragging",
            shiftMovementDirection = null;

        var doc = element.ownerDocument,
            body = doc.body,
            boundElement = $(element),
            disabled = false,
            container = $(markup, doc).addClass(theme),
            pickerContainer = container.find(".sp-picker-container"),
            dragger = container.find(".sp-color"),
            dragHelper = container.find(".sp-dragger"),
            slider = container.find(".sp-hue"),
            slideHelper = container.find(".sp-slider"),
            alphaSliderInner = container.find(".sp-alpha-inner"),
            alphaSlider = container.find(".sp-alpha"),
            alphaSlideHelper = container.find(".sp-alpha-handle"),
            textInput = container.find(".sp-input"),
            paletteContainer = container.find(".sp-palette"),
            initialColorContainer = container.find(".sp-initial"),
            cancelButton = container.find(".sp-cancel"),
            clearButton = container.find(".sp-clear"),
            chooseButton = container.find(".sp-choose"),
            toggleButton = container.find(".sp-palette-toggle"),
            isInput = boundElement.is("input"),
            isInputTypeColor = isInput && boundElement.attr("type") === "color" && inputTypeColorSupport(),
            shouldReplace = isInput && !flat,
            replacer = (shouldReplace) ? $(replaceInput).addClass(theme).addClass(opts.className).addClass(opts.replacerClassName) : $([]),
            offsetElement = (shouldReplace) ? replacer : boundElement,
            previewElement = replacer.find(".sp-preview-inner"),
            initialColor = opts.color || (isInput && boundElement.val()),
            colorOnShow = false,
            currentPreferredFormat = opts.preferredFormat,
            clickoutFiresChange = !opts.showButtons || opts.clickoutFiresChange,
            isEmpty = !initialColor,
            allowEmpty = opts.allowEmpty && !isInputTypeColor;

        function applyOptions() {

            if (opts.showPaletteOnly) {
                opts.showPalette = true;
            }

            toggleButton.text(opts.showPaletteOnly ? opts.togglePaletteMoreText : opts.togglePaletteLessText);

            if (opts.palette) {
                palette = opts.palette.slice(0);
                paletteArray = $.isArray(palette[0]) ? palette : [palette];
                paletteLookup = {};
                for (var i = 0; i < paletteArray.length; i++) {
                    for (var j = 0; j < paletteArray[i].length; j++) {
                        var rgb = tinycolor(paletteArray[i][j]).toRgbString();
                        paletteLookup[rgb] = true;
                    }
                }
            }

            container.toggleClass("sp-flat", flat);
            container.toggleClass("sp-input-disabled", !opts.showInput);
            container.toggleClass("sp-alpha-enabled", opts.showAlpha);
            container.toggleClass("sp-clear-enabled", allowEmpty);
            container.toggleClass("sp-buttons-disabled", !opts.showButtons);
            container.toggleClass("sp-palette-buttons-disabled", !opts.togglePaletteOnly);
            container.toggleClass("sp-palette-disabled", !opts.showPalette);
            container.toggleClass("sp-palette-only", opts.showPaletteOnly);
            container.toggleClass("sp-initial-disabled", !opts.showInitial);
            container.addClass(opts.className).addClass(opts.containerClassName);

            reflow();
        }

        function initialize() {

            if (IE) {
                container.find("*:not(input)").attr("unselectable", "on");
            }

            applyOptions();

            if (shouldReplace) {
                boundElement.after(replacer).hide();
            }

            if (!allowEmpty) {
                clearButton.hide();
            }

            if (flat) {
                boundElement.after(container).hide();
            }
            else {

                var appendTo = opts.appendTo === "parent" ? boundElement.parent() : $(opts.appendTo);
                if (appendTo.length !== 1) {
                    appendTo = $("body");
                }

                appendTo.append(container);
            }

            updateSelectionPaletteFromStorage();

            offsetElement.bind("click.spectrum touchstart.spectrum", function (e) {
                if (!disabled) {
                    toggle();
                }

                e.stopPropagation();

                if (!$(e.target).is("input")) {
                    e.preventDefault();
                }
            });

            if(boundElement.is(":disabled") || (opts.disabled === true)) {
                disable();
            }

            // Prevent clicks from bubbling up to document.  This would cause it to be hidden.
            container.click(stopPropagation);

            // Handle user typed input
            textInput.change(setFromTextInput);
            textInput.bind("paste", function () {
                setTimeout(setFromTextInput, 1);
            });
            textInput.keydown(function (e) { if (e.keyCode == 13) { setFromTextInput(); } });

            cancelButton.text(opts.cancelText);
            cancelButton.bind("click.spectrum", function (e) {
                e.stopPropagation();
                e.preventDefault();
                revert();
                hide();
            });

            clearButton.attr("title", opts.clearText);
            clearButton.bind("click.spectrum", function (e) {
                e.stopPropagation();
                e.preventDefault();
                isEmpty = true;
                move();

                if(flat) {
                    //for the flat style, this is a change event
                    updateOriginalInput(true);
                }
            });

            chooseButton.text(opts.chooseText);
            chooseButton.bind("click.spectrum", function (e) {
                e.stopPropagation();
                e.preventDefault();

                if (IE && textInput.is(":focus")) {
                    textInput.trigger('change');
                }

                if (isValid()) {
                    updateOriginalInput(true);
                    hide();
                }
            });

            toggleButton.text(opts.showPaletteOnly ? opts.togglePaletteMoreText : opts.togglePaletteLessText);
            toggleButton.bind("click.spectrum", function (e) {
                e.stopPropagation();
                e.preventDefault();

                opts.showPaletteOnly = !opts.showPaletteOnly;

                // To make sure the Picker area is drawn on the right, next to the
                // Palette area (and not below the palette), first move the Palette
                // to the left to make space for the picker, plus 5px extra.
                // The 'applyOptions' function puts the whole container back into place
                // and takes care of the button-text and the sp-palette-only CSS class.
                if (!opts.showPaletteOnly && !flat) {
                    container.css('left', '-=' + (pickerContainer.outerWidth(true) + 5));
                }
                applyOptions();
            });

            draggable(alphaSlider, function (dragX, dragY, e) {
                currentAlpha = (dragX / alphaWidth);
                isEmpty = false;
                if (e.shiftKey) {
                    currentAlpha = Math.round(currentAlpha * 10) / 10;
                }

                move();
            }, dragStart, dragStop);

            draggable(slider, function (dragX, dragY) {
                currentHue = parseFloat(dragY / slideHeight);
                isEmpty = false;
                if (!opts.showAlpha) {
                    currentAlpha = 1;
                }
                move();
            }, dragStart, dragStop);

            draggable(dragger, function (dragX, dragY, e) {

                // shift+drag should snap the movement to either the x or y axis.
                if (!e.shiftKey) {
                    shiftMovementDirection = null;
                }
                else if (!shiftMovementDirection) {
                    var oldDragX = currentSaturation * dragWidth;
                    var oldDragY = dragHeight - (currentValue * dragHeight);
                    var furtherFromX = Math.abs(dragX - oldDragX) > Math.abs(dragY - oldDragY);

                    shiftMovementDirection = furtherFromX ? "x" : "y";
                }

                var setSaturation = !shiftMovementDirection || shiftMovementDirection === "x";
                var setValue = !shiftMovementDirection || shiftMovementDirection === "y";

                if (setSaturation) {
                    currentSaturation = parseFloat(dragX / dragWidth);
                }
                if (setValue) {
                    currentValue = parseFloat((dragHeight - dragY) / dragHeight);
                }

                isEmpty = false;
                if (!opts.showAlpha) {
                    currentAlpha = 1;
                }

                move();

            }, dragStart, dragStop);

            if (!!initialColor) {
                set(initialColor);

                // In case color was black - update the preview UI and set the format
                // since the set function will not run (default color is black).
                updateUI();
                currentPreferredFormat = opts.preferredFormat || tinycolor(initialColor).format;

                addColorToSelectionPalette(initialColor);
            }
            else {
                updateUI();
            }

            if (flat) {
                show();
            }

            function paletteElementClick(e) {
                if (e.data && e.data.ignore) {
                    set($(e.target).closest(".sp-thumb-el").data("color"));
                    move();
                }
                else {
                    set($(e.target).closest(".sp-thumb-el").data("color"));
                    move();
                    updateOriginalInput(true);
                    if (opts.hideAfterPaletteSelect) {
                      hide();
                    }
                }

                return false;
            }

            var paletteEvent = IE ? "mousedown.spectrum" : "click.spectrum touchstart.spectrum";
            paletteContainer.delegate(".sp-thumb-el", paletteEvent, paletteElementClick);
            initialColorContainer.delegate(".sp-thumb-el:nth-child(1)", paletteEvent, { ignore: true }, paletteElementClick);
        }

        function updateSelectionPaletteFromStorage() {

            if (localStorageKey && window.localStorage) {

                // Migrate old palettes over to new format.  May want to remove this eventually.
                try {
                    var oldPalette = window.localStorage[localStorageKey].split(",#");
                    if (oldPalette.length > 1) {
                        delete window.localStorage[localStorageKey];
                        $.each(oldPalette, function(i, c) {
                             addColorToSelectionPalette(c);
                        });
                    }
                }
                catch(e) { }

                try {
                    selectionPalette = window.localStorage[localStorageKey].split(";");
                }
                catch (e) { }
            }
        }

        function addColorToSelectionPalette(color) {
            if (showSelectionPalette) {
                var rgb = tinycolor(color).toRgbString();
                if (!paletteLookup[rgb] && $.inArray(rgb, selectionPalette) === -1) {
                    selectionPalette.push(rgb);
                    while(selectionPalette.length > maxSelectionSize) {
                        selectionPalette.shift();
                    }
                }

                if (localStorageKey && window.localStorage) {
                    try {
                        window.localStorage[localStorageKey] = selectionPalette.join(";");
                    }
                    catch(e) { }
                }
            }
        }

        function getUniqueSelectionPalette() {
            var unique = [];
            if (opts.showPalette) {
                for (var i = 0; i < selectionPalette.length; i++) {
                    var rgb = tinycolor(selectionPalette[i]).toRgbString();

                    if (!paletteLookup[rgb]) {
                        unique.push(selectionPalette[i]);
                    }
                }
            }

            return unique.reverse().slice(0, opts.maxSelectionSize);
        }

        function drawPalette() {

            var currentColor = get();

            var html = $.map(paletteArray, function (palette, i) {
                return paletteTemplate(palette, currentColor, "sp-palette-row sp-palette-row-" + i, opts);
            });

            updateSelectionPaletteFromStorage();

            if (selectionPalette) {
                html.push(paletteTemplate(getUniqueSelectionPalette(), currentColor, "sp-palette-row sp-palette-row-selection", opts));
            }

            paletteContainer.html(html.join(""));
        }

        function drawInitial() {
            if (opts.showInitial) {
                var initial = colorOnShow;
                var current = get();
                initialColorContainer.html(paletteTemplate([initial, current], current, "sp-palette-row-initial", opts));
            }
        }

        function dragStart() {
            if (dragHeight <= 0 || dragWidth <= 0 || slideHeight <= 0) {
                reflow();
            }
            isDragging = true;
            container.addClass(draggingClass);
            shiftMovementDirection = null;
            boundElement.trigger('dragstart.spectrum', [ get() ]);
        }

        function dragStop() {
            isDragging = false;
            container.removeClass(draggingClass);
            boundElement.trigger('dragstop.spectrum', [ get() ]);
        }

        function setFromTextInput() {

            var value = textInput.val();

            if ((value === null || value === "") && allowEmpty) {
                set(null);
                updateOriginalInput(true);
            }
            else {
                var tiny = tinycolor(value);
                if (tiny.isValid()) {
                    set(tiny);
                    updateOriginalInput(true);
                }
                else {
                    textInput.addClass("sp-validation-error");
                }
            }
        }

        function toggle() {
            if (visible) {
                hide();
            }
            else {
                show();
            }
        }

        function show() {
            var event = $.Event('beforeShow.spectrum');

            if (visible) {
                reflow();
                return;
            }

            boundElement.trigger(event, [ get() ]);

            if (callbacks.beforeShow(get()) === false || event.isDefaultPrevented()) {
                return;
            }

            hideAll();
            visible = true;

            $(doc).bind("keydown.spectrum", onkeydown);
            $(doc).bind("click.spectrum", clickout);
            $(window).bind("resize.spectrum", resize);
            replacer.addClass("sp-active");
            container.removeClass("sp-hidden");

            reflow();
            updateUI();

            colorOnShow = get();

            drawInitial();
            callbacks.show(colorOnShow);
            boundElement.trigger('show.spectrum', [ colorOnShow ]);
        }

        function onkeydown(e) {
            // Close on ESC
            if (e.keyCode === 27) {
                hide();
            }
        }

        function clickout(e) {
            // Return on right click.
            if (e.button == 2) { return; }

            // If a drag event was happening during the mouseup, don't hide
            // on click.
            if (isDragging) { return; }

            if (clickoutFiresChange) {
                updateOriginalInput(true);
            }
            else {
                revert();
            }
            hide();
        }

        function hide() {
            // Return if hiding is unnecessary
            if (!visible || flat) { return; }
            visible = false;

            $(doc).unbind("keydown.spectrum", onkeydown);
            $(doc).unbind("click.spectrum", clickout);
            $(window).unbind("resize.spectrum", resize);

            replacer.removeClass("sp-active");
            container.addClass("sp-hidden");

            callbacks.hide(get());
            boundElement.trigger('hide.spectrum', [ get() ]);
        }

        function revert() {
            set(colorOnShow, true);
        }

        function set(color, ignoreFormatChange) {
            if (tinycolor.equals(color, get())) {
                // Update UI just in case a validation error needs
                // to be cleared.
                updateUI();
                return;
            }

            var newColor, newHsv;
            if (!color && allowEmpty) {
                isEmpty = true;
            } else {
                isEmpty = false;
                newColor = tinycolor(color);
                newHsv = newColor.toHsv();

                currentHue = (newHsv.h % 360) / 360;
                currentSaturation = newHsv.s;
                currentValue = newHsv.v;
                currentAlpha = newHsv.a;
            }
            updateUI();

            if (newColor && newColor.isValid() && !ignoreFormatChange) {
                currentPreferredFormat = opts.preferredFormat || newColor.getFormat();
            }
        }

        function get(opts) {
            opts = opts || { };

            if (allowEmpty && isEmpty) {
                return null;
            }

            return tinycolor.fromRatio({
                h: currentHue,
                s: currentSaturation,
                v: currentValue,
                a: Math.round(currentAlpha * 100) / 100
            }, { format: opts.format || currentPreferredFormat });
        }

        function isValid() {
            return !textInput.hasClass("sp-validation-error");
        }

        function move() {
            updateUI();

            callbacks.move(get());
            boundElement.trigger('move.spectrum', [ get() ]);
        }

        function updateUI() {

            textInput.removeClass("sp-validation-error");

            updateHelperLocations();

            // Update dragger background color (gradients take care of saturation and value).
            var flatColor = tinycolor.fromRatio({ h: currentHue, s: 1, v: 1 });
            dragger.css("background-color", flatColor.toHexString());

            // Get a format that alpha will be included in (hex and names ignore alpha)
            var format = currentPreferredFormat;
            if (currentAlpha < 1 && !(currentAlpha === 0 && format === "name")) {
                if (format === "hex" || format === "hex3" || format === "hex6" || format === "name") {
                    format = "rgb";
                }
            }

            var realColor = get({ format: format }),
                displayColor = '';

             //reset background info for preview element
            previewElement.removeClass("sp-clear-display");
            previewElement.css('background-color', 'transparent');

            if (!realColor && allowEmpty) {
                // Update the replaced elements background with icon indicating no color selection
                previewElement.addClass("sp-clear-display");
            }
            else {
                var realHex = realColor.toHexString(),
                    realRgb = realColor.toRgbString();

                // Update the replaced elements background color (with actual selected color)
                if (rgbaSupport || realColor.alpha === 1) {
                    previewElement.css("background-color", realRgb);
                }
                else {
                    previewElement.css("background-color", "transparent");
                    previewElement.css("filter", realColor.toFilter());
                }

                if (opts.showAlpha) {
                    var rgb = realColor.toRgb();
                    rgb.a = 0;
                    var realAlpha = tinycolor(rgb).toRgbString();
                    var gradient = "linear-gradient(left, " + realAlpha + ", " + realHex + ")";

                    if (IE) {
                        alphaSliderInner.css("filter", tinycolor(realAlpha).toFilter({ gradientType: 1 }, realHex));
                    }
                    else {
                        alphaSliderInner.css("background", "-webkit-" + gradient);
                        alphaSliderInner.css("background", "-moz-" + gradient);
                        alphaSliderInner.css("background", "-ms-" + gradient);
                        // Use current syntax gradient on unprefixed property.
                        alphaSliderInner.css("background",
                            "linear-gradient(to right, " + realAlpha + ", " + realHex + ")");
                    }
                }

                displayColor = realColor.toString(format);
            }

            // Update the text entry input as it changes happen
            if (opts.showInput) {
                textInput.val(displayColor);
            }

            if (opts.showPalette) {
                drawPalette();
            }

            drawInitial();
        }

        function updateHelperLocations() {
            var s = currentSaturation;
            var v = currentValue;

            if(allowEmpty && isEmpty) {
                //if selected color is empty, hide the helpers
                alphaSlideHelper.hide();
                slideHelper.hide();
                dragHelper.hide();
            }
            else {
                //make sure helpers are visible
                alphaSlideHelper.show();
                slideHelper.show();
                dragHelper.show();

                // Where to show the little circle in that displays your current selected color
                var dragX = s * dragWidth;
                var dragY = dragHeight - (v * dragHeight);
                dragX = Math.max(
                    -dragHelperHeight,
                    Math.min(dragWidth - dragHelperHeight, dragX - dragHelperHeight)
                );
                dragY = Math.max(
                    -dragHelperHeight,
                    Math.min(dragHeight - dragHelperHeight, dragY - dragHelperHeight)
                );
                dragHelper.css({
                    "top": dragY + "px",
                    "left": dragX + "px"
                });

                var alphaX = currentAlpha * alphaWidth;
                alphaSlideHelper.css({
                    "left": (alphaX - (alphaSlideHelperWidth / 2)) + "px"
                });

                // Where to show the bar that displays your current selected hue
                var slideY = (currentHue) * slideHeight;
                slideHelper.css({
                    "top": (slideY - slideHelperHeight) + "px"
                });
            }
        }

        function updateOriginalInput(fireCallback) {
            var color = get(),
                displayColor = '',
                hasChanged = !tinycolor.equals(color, colorOnShow);

            if (color) {
                displayColor = color.toString(currentPreferredFormat);
                // Update the selection palette with the current color
                addColorToSelectionPalette(color);
            }

            if (isInput) {
                boundElement.val(displayColor);
            }

            if (fireCallback && hasChanged) {
                callbacks.change(color);
                boundElement.trigger('change', [ color ]);
            }
        }

        function reflow() {
            if (!visible) {
                return; // Calculations would be useless and wouldn't be reliable anyways
            }
            dragWidth = dragger.width();
            dragHeight = dragger.height();
            dragHelperHeight = dragHelper.height();
            slideWidth = slider.width();
            slideHeight = slider.height();
            slideHelperHeight = slideHelper.height();
            alphaWidth = alphaSlider.width();
            alphaSlideHelperWidth = alphaSlideHelper.width();

            if (!flat) {
                container.css("position", "absolute");
                if (opts.offset) {
                    container.offset(opts.offset);
                } else {
                    container.offset(getOffset(container, offsetElement));
                }
            }

            updateHelperLocations();

            if (opts.showPalette) {
                drawPalette();
            }

            boundElement.trigger('reflow.spectrum');
        }

        function destroy() {
            boundElement.show();
            offsetElement.unbind("click.spectrum touchstart.spectrum");
            container.remove();
            replacer.remove();
            spectrums[spect.id] = null;
        }

        function option(optionName, optionValue) {
            if (optionName === undefined) {
                return $.extend({}, opts);
            }
            if (optionValue === undefined) {
                return opts[optionName];
            }

            opts[optionName] = optionValue;

            if (optionName === "preferredFormat") {
                currentPreferredFormat = opts.preferredFormat;
            }
            applyOptions();
        }

        function enable() {
            disabled = false;
            boundElement.attr("disabled", false);
            offsetElement.removeClass("sp-disabled");
        }

        function disable() {
            hide();
            disabled = true;
            boundElement.attr("disabled", true);
            offsetElement.addClass("sp-disabled");
        }

        function setOffset(coord) {
            opts.offset = coord;
            reflow();
        }

        initialize();

        var spect = {
            show: show,
            hide: hide,
            toggle: toggle,
            reflow: reflow,
            option: option,
            enable: enable,
            disable: disable,
            offset: setOffset,
            set: function (c) {
                set(c);
                updateOriginalInput();
            },
            get: get,
            destroy: destroy,
            container: container
        };

        spect.id = spectrums.push(spect) - 1;

        return spect;
    }

    /**
    * checkOffset - get the offset below/above and left/right element depending on screen position
    * Thanks https://github.com/jquery/jquery-ui/blob/master/ui/jquery.ui.datepicker.js
    */
    function getOffset(picker, input) {
        var extraY = 0;
        var dpWidth = picker.outerWidth();
        var dpHeight = picker.outerHeight();
        var inputHeight = input.outerHeight();
        var doc = picker[0].ownerDocument;
        var docElem = doc.documentElement;
        var viewWidth = docElem.clientWidth + $(doc).scrollLeft();
        var viewHeight = docElem.clientHeight + $(doc).scrollTop();
        var offset = input.offset();
        offset.top += inputHeight;

        offset.left -=
            Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
            Math.abs(offset.left + dpWidth - viewWidth) : 0);

        offset.top -=
            Math.min(offset.top, ((offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
            Math.abs(dpHeight + inputHeight - extraY) : extraY));

        return offset;
    }

    /**
    * noop - do nothing
    */
    function noop() {

    }

    /**
    * stopPropagation - makes the code only doing this a little easier to read in line
    */
    function stopPropagation(e) {
        e.stopPropagation();
    }

    /**
    * Create a function bound to a given object
    * Thanks to underscore.js
    */
    function bind(func, obj) {
        var slice = Array.prototype.slice;
        var args = slice.call(arguments, 2);
        return function () {
            return func.apply(obj, args.concat(slice.call(arguments)));
        };
    }

    /**
    * Lightweight drag helper.  Handles containment within the element, so that
    * when dragging, the x is within [0,element.width] and y is within [0,element.height]
    */
    function draggable(element, onmove, onstart, onstop) {
        onmove = onmove || function () { };
        onstart = onstart || function () { };
        onstop = onstop || function () { };
        var doc = document;
        var dragging = false;
        var offset = {};
        var maxHeight = 0;
        var maxWidth = 0;
        var hasTouch = ('ontouchstart' in window);

        var duringDragEvents = {};
        duringDragEvents["selectstart"] = prevent;
        duringDragEvents["dragstart"] = prevent;
        duringDragEvents["touchmove mousemove"] = move;
        duringDragEvents["touchend mouseup"] = stop;

        function prevent(e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            if (e.preventDefault) {
                e.preventDefault();
            }
            e.returnValue = false;
        }

        function move(e) {
            if (dragging) {
                // Mouseup happened outside of window
                if (IE && doc.documentMode < 9 && !e.button) {
                    return stop();
                }

                var t0 = e.originalEvent && e.originalEvent.touches && e.originalEvent.touches[0];
                var pageX = t0 && t0.pageX || e.pageX;
                var pageY = t0 && t0.pageY || e.pageY;

                var dragX = Math.max(0, Math.min(pageX - offset.left, maxWidth));
                var dragY = Math.max(0, Math.min(pageY - offset.top, maxHeight));

                if (hasTouch) {
                    // Stop scrolling in iOS
                    prevent(e);
                }

                onmove.apply(element, [dragX, dragY, e]);
            }
        }

        function start(e) {
            var rightclick = (e.which) ? (e.which == 3) : (e.button == 2);

            if (!rightclick && !dragging) {
                if (onstart.apply(element, arguments) !== false) {
                    dragging = true;
                    maxHeight = $(element).height();
                    maxWidth = $(element).width();
                    offset = $(element).offset();

                    $(doc).bind(duringDragEvents);
                    $(doc.body).addClass("sp-dragging");

                    move(e);

                    prevent(e);
                }
            }
        }

        function stop() {
            if (dragging) {
                $(doc).unbind(duringDragEvents);
                $(doc.body).removeClass("sp-dragging");

                // Wait a tick before notifying observers to allow the click event
                // to fire in Chrome.
                setTimeout(function() {
                    onstop.apply(element, arguments);
                }, 0);
            }
            dragging = false;
        }

        $(element).bind("touchstart mousedown", start);
    }

    function throttle(func, wait, debounce) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var throttler = function () {
                timeout = null;
                func.apply(context, args);
            };
            if (debounce) clearTimeout(timeout);
            if (debounce || !timeout) timeout = setTimeout(throttler, wait);
        };
    }

    function inputTypeColorSupport() {
        return $.fn.spectrum.inputTypeColorSupport();
    }

    /**
    * Define a jQuery plugin
    */
    var dataID = "spectrum.id";
    $.fn.spectrum = function (opts, extra) {

        if (typeof opts == "string") {

            var returnValue = this;
            var args = Array.prototype.slice.call( arguments, 1 );

            this.each(function () {
                var spect = spectrums[$(this).data(dataID)];
                if (spect) {
                    var method = spect[opts];
                    if (!method) {
                        throw new Error( "Spectrum: no such method: '" + opts + "'" );
                    }

                    if (opts == "get") {
                        returnValue = spect.get();
                    }
                    else if (opts == "container") {
                        returnValue = spect.container;
                    }
                    else if (opts == "option") {
                        returnValue = spect.option.apply(spect, args);
                    }
                    else if (opts == "destroy") {
                        spect.destroy();
                        $(this).removeData(dataID);
                    }
                    else {
                        method.apply(spect, args);
                    }
                }
            });

            return returnValue;
        }

        // Initializing a new instance of spectrum
        return this.spectrum("destroy").each(function () {
            var options = $.extend({}, opts, $(this).data());
            var spect = spectrum(this, options);
            $(this).data(dataID, spect.id);
        });
    };

    $.fn.spectrum.load = true;
    $.fn.spectrum.loadOpts = {};
    $.fn.spectrum.draggable = draggable;
    $.fn.spectrum.defaults = defaultOpts;
    $.fn.spectrum.inputTypeColorSupport = function inputTypeColorSupport() {
        if (typeof inputTypeColorSupport._cachedResult === "undefined") {
            var colorInput = $("<input type='color'/>")[0]; // if color element is supported, value will default to not null
            inputTypeColorSupport._cachedResult = colorInput.type === "color" && colorInput.value !== "";
        }
        return inputTypeColorSupport._cachedResult;
    };

    $.spectrum = { };
    $.spectrum.localization = { };
    $.spectrum.palettes = { };

    $.fn.spectrum.processNativeColorInputs = function () {
        var colorInputs = $("input[type=color]");
        if (colorInputs.length && !inputTypeColorSupport()) {
            colorInputs.spectrum({
                preferredFormat: "hex6"
            });
        }
    };

    // TinyColor v1.1.2
    // https://github.com/bgrins/TinyColor
    // Brian Grinstead, MIT License

    (function() {

    var trimLeft = /^[\s,#]+/,
        trimRight = /\s+$/,
        tinyCounter = 0,
        math = Math,
        mathRound = math.round,
        mathMin = math.min,
        mathMax = math.max,
        mathRandom = math.random;

    var tinycolor = function(color, opts) {

        color = (color) ? color : '';
        opts = opts || { };

        // If input is already a tinycolor, return itself
        if (color instanceof tinycolor) {
           return color;
        }
        // If we are called as a function, call using new instead
        if (!(this instanceof tinycolor)) {
            return new tinycolor(color, opts);
        }

        var rgb = inputToRGB(color);
        this._originalInput = color,
        this._r = rgb.r,
        this._g = rgb.g,
        this._b = rgb.b,
        this._a = rgb.a,
        this._roundA = mathRound(100*this._a) / 100,
        this._format = opts.format || rgb.format;
        this._gradientType = opts.gradientType;

        // Don't let the range of [0,255] come back in [0,1].
        // Potentially lose a little bit of precision here, but will fix issues where
        // .5 gets interpreted as half of the total, instead of half of 1
        // If it was supposed to be 128, this was already taken care of by `inputToRgb`
        if (this._r < 1) { this._r = mathRound(this._r); }
        if (this._g < 1) { this._g = mathRound(this._g); }
        if (this._b < 1) { this._b = mathRound(this._b); }

        this._ok = rgb.ok;
        this._tc_id = tinyCounter++;
    };

    tinycolor.prototype = {
        isDark: function() {
            return this.getBrightness() < 128;
        },
        isLight: function() {
            return !this.isDark();
        },
        isValid: function() {
            return this._ok;
        },
        getOriginalInput: function() {
          return this._originalInput;
        },
        getFormat: function() {
            return this._format;
        },
        getAlpha: function() {
            return this._a;
        },
        getBrightness: function() {
            var rgb = this.toRgb();
            return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
        },
        setAlpha: function(value) {
            this._a = boundAlpha(value);
            this._roundA = mathRound(100*this._a) / 100;
            return this;
        },
        toHsv: function() {
            var hsv = rgbToHsv(this._r, this._g, this._b);
            return { h: hsv.h * 360, s: hsv.s, v: hsv.v, a: this._a };
        },
        toHsvString: function() {
            var hsv = rgbToHsv(this._r, this._g, this._b);
            var h = mathRound(hsv.h * 360), s = mathRound(hsv.s * 100), v = mathRound(hsv.v * 100);
            return (this._a == 1) ?
              "hsv("  + h + ", " + s + "%, " + v + "%)" :
              "hsva(" + h + ", " + s + "%, " + v + "%, "+ this._roundA + ")";
        },
        toHsl: function() {
            var hsl = rgbToHsl(this._r, this._g, this._b);
            return { h: hsl.h * 360, s: hsl.s, l: hsl.l, a: this._a };
        },
        toHslString: function() {
            var hsl = rgbToHsl(this._r, this._g, this._b);
            var h = mathRound(hsl.h * 360), s = mathRound(hsl.s * 100), l = mathRound(hsl.l * 100);
            return (this._a == 1) ?
              "hsl("  + h + ", " + s + "%, " + l + "%)" :
              "hsla(" + h + ", " + s + "%, " + l + "%, "+ this._roundA + ")";
        },
        toHex: function(allow3Char) {
            return rgbToHex(this._r, this._g, this._b, allow3Char);
        },
        toHexString: function(allow3Char) {
            return '#' + this.toHex(allow3Char);
        },
        toHex8: function() {
            return rgbaToHex(this._r, this._g, this._b, this._a);
        },
        toHex8String: function() {
            return '#' + this.toHex8();
        },
        toRgb: function() {
            return { r: mathRound(this._r), g: mathRound(this._g), b: mathRound(this._b), a: this._a };
        },
        toRgbString: function() {
            return (this._a == 1) ?
              "rgb("  + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ")" :
              "rgba(" + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ", " + this._roundA + ")";
        },
        toPercentageRgb: function() {
            return { r: mathRound(bound01(this._r, 255) * 100) + "%", g: mathRound(bound01(this._g, 255) * 100) + "%", b: mathRound(bound01(this._b, 255) * 100) + "%", a: this._a };
        },
        toPercentageRgbString: function() {
            return (this._a == 1) ?
              "rgb("  + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%)" :
              "rgba(" + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%, " + this._roundA + ")";
        },
        toName: function() {
            if (this._a === 0) {
                return "transparent";
            }

            if (this._a < 1) {
                return false;
            }

            return hexNames[rgbToHex(this._r, this._g, this._b, true)] || false;
        },
        toFilter: function(secondColor) {
            var hex8String = '#' + rgbaToHex(this._r, this._g, this._b, this._a);
            var secondHex8String = hex8String;
            var gradientType = this._gradientType ? "GradientType = 1, " : "";

            if (secondColor) {
                var s = tinycolor(secondColor);
                secondHex8String = s.toHex8String();
            }

            return "progid:DXImageTransform.Microsoft.gradient("+gradientType+"startColorstr="+hex8String+",endColorstr="+secondHex8String+")";
        },
        toString: function(format) {
            var formatSet = !!format;
            format = format || this._format;

            var formattedString = false;
            var hasAlpha = this._a < 1 && this._a >= 0;
            var needsAlphaFormat = !formatSet && hasAlpha && (format === "hex" || format === "hex6" || format === "hex3" || format === "name");

            if (needsAlphaFormat) {
                // Special case for "transparent", all other non-alpha formats
                // will return rgba when there is transparency.
                if (format === "name" && this._a === 0) {
                    return this.toName();
                }
                return this.toRgbString();
            }
            if (format === "rgb") {
                formattedString = this.toRgbString();
            }
            if (format === "prgb") {
                formattedString = this.toPercentageRgbString();
            }
            if (format === "hex" || format === "hex6") {
                formattedString = this.toHexString();
            }
            if (format === "hex3") {
                formattedString = this.toHexString(true);
            }
            if (format === "hex8") {
                formattedString = this.toHex8String();
            }
            if (format === "name") {
                formattedString = this.toName();
            }
            if (format === "hsl") {
                formattedString = this.toHslString();
            }
            if (format === "hsv") {
                formattedString = this.toHsvString();
            }

            return formattedString || this.toHexString();
        },

        _applyModification: function(fn, args) {
            var color = fn.apply(null, [this].concat([].slice.call(args)));
            this._r = color._r;
            this._g = color._g;
            this._b = color._b;
            this.setAlpha(color._a);
            return this;
        },
        lighten: function() {
            return this._applyModification(lighten, arguments);
        },
        brighten: function() {
            return this._applyModification(brighten, arguments);
        },
        darken: function() {
            return this._applyModification(darken, arguments);
        },
        desaturate: function() {
            return this._applyModification(desaturate, arguments);
        },
        saturate: function() {
            return this._applyModification(saturate, arguments);
        },
        greyscale: function() {
            return this._applyModification(greyscale, arguments);
        },
        spin: function() {
            return this._applyModification(spin, arguments);
        },

        _applyCombination: function(fn, args) {
            return fn.apply(null, [this].concat([].slice.call(args)));
        },
        analogous: function() {
            return this._applyCombination(analogous, arguments);
        },
        complement: function() {
            return this._applyCombination(complement, arguments);
        },
        monochromatic: function() {
            return this._applyCombination(monochromatic, arguments);
        },
        splitcomplement: function() {
            return this._applyCombination(splitcomplement, arguments);
        },
        triad: function() {
            return this._applyCombination(triad, arguments);
        },
        tetrad: function() {
            return this._applyCombination(tetrad, arguments);
        }
    };

    // If input is an object, force 1 into "1.0" to handle ratios properly
    // String input requires "1.0" as input, so 1 will be treated as 1
    tinycolor.fromRatio = function(color, opts) {
        if (typeof color == "object") {
            var newColor = {};
            for (var i in color) {
                if (color.hasOwnProperty(i)) {
                    if (i === "a") {
                        newColor[i] = color[i];
                    }
                    else {
                        newColor[i] = convertToPercentage(color[i]);
                    }
                }
            }
            color = newColor;
        }

        return tinycolor(color, opts);
    };

    // Given a string or object, convert that input to RGB
    // Possible string inputs:
    //
    //     "red"
    //     "#f00" or "f00"
    //     "#ff0000" or "ff0000"
    //     "#ff000000" or "ff000000"
    //     "rgb 255 0 0" or "rgb (255, 0, 0)"
    //     "rgb 1.0 0 0" or "rgb (1, 0, 0)"
    //     "rgba (255, 0, 0, 1)" or "rgba 255, 0, 0, 1"
    //     "rgba (1.0, 0, 0, 1)" or "rgba 1.0, 0, 0, 1"
    //     "hsl(0, 100%, 50%)" or "hsl 0 100% 50%"
    //     "hsla(0, 100%, 50%, 1)" or "hsla 0 100% 50%, 1"
    //     "hsv(0, 100%, 100%)" or "hsv 0 100% 100%"
    //
    function inputToRGB(color) {

        var rgb = { r: 0, g: 0, b: 0 };
        var a = 1;
        var ok = false;
        var format = false;

        if (typeof color == "string") {
            color = stringInputToObject(color);
        }

        if (typeof color == "object") {
            if (color.hasOwnProperty("r") && color.hasOwnProperty("g") && color.hasOwnProperty("b")) {
                rgb = rgbToRgb(color.r, color.g, color.b);
                ok = true;
                format = String(color.r).substr(-1) === "%" ? "prgb" : "rgb";
            }
            else if (color.hasOwnProperty("h") && color.hasOwnProperty("s") && color.hasOwnProperty("v")) {
                color.s = convertToPercentage(color.s);
                color.v = convertToPercentage(color.v);
                rgb = hsvToRgb(color.h, color.s, color.v);
                ok = true;
                format = "hsv";
            }
            else if (color.hasOwnProperty("h") && color.hasOwnProperty("s") && color.hasOwnProperty("l")) {
                color.s = convertToPercentage(color.s);
                color.l = convertToPercentage(color.l);
                rgb = hslToRgb(color.h, color.s, color.l);
                ok = true;
                format = "hsl";
            }

            if (color.hasOwnProperty("a")) {
                a = color.a;
            }
        }

        a = boundAlpha(a);

        return {
            ok: ok,
            format: color.format || format,
            r: mathMin(255, mathMax(rgb.r, 0)),
            g: mathMin(255, mathMax(rgb.g, 0)),
            b: mathMin(255, mathMax(rgb.b, 0)),
            a: a
        };
    }


    // Conversion Functions
    // --------------------

    // `rgbToHsl`, `rgbToHsv`, `hslToRgb`, `hsvToRgb` modified from:
    // <http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript>

    // `rgbToRgb`
    // Handle bounds / percentage checking to conform to CSS color spec
    // <http://www.w3.org/TR/css3-color/>
    // *Assumes:* r, g, b in [0, 255] or [0, 1]
    // *Returns:* { r, g, b } in [0, 255]
    function rgbToRgb(r, g, b){
        return {
            r: bound01(r, 255) * 255,
            g: bound01(g, 255) * 255,
            b: bound01(b, 255) * 255
        };
    }

    // `rgbToHsl`
    // Converts an RGB color value to HSL.
    // *Assumes:* r, g, and b are contained in [0, 255] or [0, 1]
    // *Returns:* { h, s, l } in [0,1]
    function rgbToHsl(r, g, b) {

        r = bound01(r, 255);
        g = bound01(g, 255);
        b = bound01(b, 255);

        var max = mathMax(r, g, b), min = mathMin(r, g, b);
        var h, s, l = (max + min) / 2;

        if(max == min) {
            h = s = 0; // achromatic
        }
        else {
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch(max) {
                case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                case g: h = (b - r) / d + 2; break;
                case b: h = (r - g) / d + 4; break;
            }

            h /= 6;
        }

        return { h: h, s: s, l: l };
    }

    // `hslToRgb`
    // Converts an HSL color value to RGB.
    // *Assumes:* h is contained in [0, 1] or [0, 360] and s and l are contained [0, 1] or [0, 100]
    // *Returns:* { r, g, b } in the set [0, 255]
    function hslToRgb(h, s, l) {
        var r, g, b;

        h = bound01(h, 360);
        s = bound01(s, 100);
        l = bound01(l, 100);

        function hue2rgb(p, q, t) {
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        if(s === 0) {
            r = g = b = l; // achromatic
        }
        else {
            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1/3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1/3);
        }

        return { r: r * 255, g: g * 255, b: b * 255 };
    }

    // `rgbToHsv`
    // Converts an RGB color value to HSV
    // *Assumes:* r, g, and b are contained in the set [0, 255] or [0, 1]
    // *Returns:* { h, s, v } in [0,1]
    function rgbToHsv(r, g, b) {

        r = bound01(r, 255);
        g = bound01(g, 255);
        b = bound01(b, 255);

        var max = mathMax(r, g, b), min = mathMin(r, g, b);
        var h, s, v = max;

        var d = max - min;
        s = max === 0 ? 0 : d / max;

        if(max == min) {
            h = 0; // achromatic
        }
        else {
            switch(max) {
                case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                case g: h = (b - r) / d + 2; break;
                case b: h = (r - g) / d + 4; break;
            }
            h /= 6;
        }
        return { h: h, s: s, v: v };
    }

    // `hsvToRgb`
    // Converts an HSV color value to RGB.
    // *Assumes:* h is contained in [0, 1] or [0, 360] and s and v are contained in [0, 1] or [0, 100]
    // *Returns:* { r, g, b } in the set [0, 255]
     function hsvToRgb(h, s, v) {

        h = bound01(h, 360) * 6;
        s = bound01(s, 100);
        v = bound01(v, 100);

        var i = math.floor(h),
            f = h - i,
            p = v * (1 - s),
            q = v * (1 - f * s),
            t = v * (1 - (1 - f) * s),
            mod = i % 6,
            r = [v, q, p, p, t, v][mod],
            g = [t, v, v, q, p, p][mod],
            b = [p, p, t, v, v, q][mod];

        return { r: r * 255, g: g * 255, b: b * 255 };
    }

    // `rgbToHex`
    // Converts an RGB color to hex
    // Assumes r, g, and b are contained in the set [0, 255]
    // Returns a 3 or 6 character hex
    function rgbToHex(r, g, b, allow3Char) {

        var hex = [
            pad2(mathRound(r).toString(16)),
            pad2(mathRound(g).toString(16)),
            pad2(mathRound(b).toString(16))
        ];

        // Return a 3 character hex if possible
        if (allow3Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1)) {
            return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0);
        }

        return hex.join("");
    }
        // `rgbaToHex`
        // Converts an RGBA color plus alpha transparency to hex
        // Assumes r, g, b and a are contained in the set [0, 255]
        // Returns an 8 character hex
        function rgbaToHex(r, g, b, a) {

            var hex = [
                pad2(convertDecimalToHex(a)),
                pad2(mathRound(r).toString(16)),
                pad2(mathRound(g).toString(16)),
                pad2(mathRound(b).toString(16))
            ];

            return hex.join("");
        }

    // `equals`
    // Can be called with any tinycolor input
    tinycolor.equals = function (color1, color2) {
        if (!color1 || !color2) { return false; }
        return tinycolor(color1).toRgbString() == tinycolor(color2).toRgbString();
    };
    tinycolor.random = function() {
        return tinycolor.fromRatio({
            r: mathRandom(),
            g: mathRandom(),
            b: mathRandom()
        });
    };


    // Modification Functions
    // ----------------------
    // Thanks to less.js for some of the basics here
    // <https://github.com/cloudhead/less.js/blob/master/lib/less/functions.js>

    function desaturate(color, amount) {
        amount = (amount === 0) ? 0 : (amount || 10);
        var hsl = tinycolor(color).toHsl();
        hsl.s -= amount / 100;
        hsl.s = clamp01(hsl.s);
        return tinycolor(hsl);
    }

    function saturate(color, amount) {
        amount = (amount === 0) ? 0 : (amount || 10);
        var hsl = tinycolor(color).toHsl();
        hsl.s += amount / 100;
        hsl.s = clamp01(hsl.s);
        return tinycolor(hsl);
    }

    function greyscale(color) {
        return tinycolor(color).desaturate(100);
    }

    function lighten (color, amount) {
        amount = (amount === 0) ? 0 : (amount || 10);
        var hsl = tinycolor(color).toHsl();
        hsl.l += amount / 100;
        hsl.l = clamp01(hsl.l);
        return tinycolor(hsl);
    }

    function brighten(color, amount) {
        amount = (amount === 0) ? 0 : (amount || 10);
        var rgb = tinycolor(color).toRgb();
        rgb.r = mathMax(0, mathMin(255, rgb.r - mathRound(255 * - (amount / 100))));
        rgb.g = mathMax(0, mathMin(255, rgb.g - mathRound(255 * - (amount / 100))));
        rgb.b = mathMax(0, mathMin(255, rgb.b - mathRound(255 * - (amount / 100))));
        return tinycolor(rgb);
    }

    function darken (color, amount) {
        amount = (amount === 0) ? 0 : (amount || 10);
        var hsl = tinycolor(color).toHsl();
        hsl.l -= amount / 100;
        hsl.l = clamp01(hsl.l);
        return tinycolor(hsl);
    }

    // Spin takes a positive or negative amount within [-360, 360] indicating the change of hue.
    // Values outside of this range will be wrapped into this range.
    function spin(color, amount) {
        var hsl = tinycolor(color).toHsl();
        var hue = (mathRound(hsl.h) + amount) % 360;
        hsl.h = hue < 0 ? 360 + hue : hue;
        return tinycolor(hsl);
    }

    // Combination Functions
    // ---------------------
    // Thanks to jQuery xColor for some of the ideas behind these
    // <https://github.com/infusion/jQuery-xcolor/blob/master/jquery.xcolor.js>

    function complement(color) {
        var hsl = tinycolor(color).toHsl();
        hsl.h = (hsl.h + 180) % 360;
        return tinycolor(hsl);
    }

    function triad(color) {
        var hsl = tinycolor(color).toHsl();
        var h = hsl.h;
        return [
            tinycolor(color),
            tinycolor({ h: (h + 120) % 360, s: hsl.s, l: hsl.l }),
            tinycolor({ h: (h + 240) % 360, s: hsl.s, l: hsl.l })
        ];
    }

    function tetrad(color) {
        var hsl = tinycolor(color).toHsl();
        var h = hsl.h;
        return [
            tinycolor(color),
            tinycolor({ h: (h + 90) % 360, s: hsl.s, l: hsl.l }),
            tinycolor({ h: (h + 180) % 360, s: hsl.s, l: hsl.l }),
            tinycolor({ h: (h + 270) % 360, s: hsl.s, l: hsl.l })
        ];
    }

    function splitcomplement(color) {
        var hsl = tinycolor(color).toHsl();
        var h = hsl.h;
        return [
            tinycolor(color),
            tinycolor({ h: (h + 72) % 360, s: hsl.s, l: hsl.l}),
            tinycolor({ h: (h + 216) % 360, s: hsl.s, l: hsl.l})
        ];
    }

    function analogous(color, results, slices) {
        results = results || 6;
        slices = slices || 30;

        var hsl = tinycolor(color).toHsl();
        var part = 360 / slices;
        var ret = [tinycolor(color)];

        for (hsl.h = ((hsl.h - (part * results >> 1)) + 720) % 360; --results; ) {
            hsl.h = (hsl.h + part) % 360;
            ret.push(tinycolor(hsl));
        }
        return ret;
    }

    function monochromatic(color, results) {
        results = results || 6;
        var hsv = tinycolor(color).toHsv();
        var h = hsv.h, s = hsv.s, v = hsv.v;
        var ret = [];
        var modification = 1 / results;

        while (results--) {
            ret.push(tinycolor({ h: h, s: s, v: v}));
            v = (v + modification) % 1;
        }

        return ret;
    }

    // Utility Functions
    // ---------------------

    tinycolor.mix = function(color1, color2, amount) {
        amount = (amount === 0) ? 0 : (amount || 50);

        var rgb1 = tinycolor(color1).toRgb();
        var rgb2 = tinycolor(color2).toRgb();

        var p = amount / 100;
        var w = p * 2 - 1;
        var a = rgb2.a - rgb1.a;

        var w1;

        if (w * a == -1) {
            w1 = w;
        } else {
            w1 = (w + a) / (1 + w * a);
        }

        w1 = (w1 + 1) / 2;

        var w2 = 1 - w1;

        var rgba = {
            r: rgb2.r * w1 + rgb1.r * w2,
            g: rgb2.g * w1 + rgb1.g * w2,
            b: rgb2.b * w1 + rgb1.b * w2,
            a: rgb2.a * p  + rgb1.a * (1 - p)
        };

        return tinycolor(rgba);
    };


    // Readability Functions
    // ---------------------
    // <http://www.w3.org/TR/AERT#color-contrast>

    // `readability`
    // Analyze the 2 colors and returns an object with the following properties:
    //    `brightness`: difference in brightness between the two colors
    //    `color`: difference in color/hue between the two colors
    tinycolor.readability = function(color1, color2) {
        var c1 = tinycolor(color1);
        var c2 = tinycolor(color2);
        var rgb1 = c1.toRgb();
        var rgb2 = c2.toRgb();
        var brightnessA = c1.getBrightness();
        var brightnessB = c2.getBrightness();
        var colorDiff = (
            Math.max(rgb1.r, rgb2.r) - Math.min(rgb1.r, rgb2.r) +
            Math.max(rgb1.g, rgb2.g) - Math.min(rgb1.g, rgb2.g) +
            Math.max(rgb1.b, rgb2.b) - Math.min(rgb1.b, rgb2.b)
        );

        return {
            brightness: Math.abs(brightnessA - brightnessB),
            color: colorDiff
        };
    };

    // `readable`
    // http://www.w3.org/TR/AERT#color-contrast
    // Ensure that foreground and background color combinations provide sufficient contrast.
    // *Example*
    //    tinycolor.isReadable("#000", "#111") => false
    tinycolor.isReadable = function(color1, color2) {
        var readability = tinycolor.readability(color1, color2);
        return readability.brightness > 125 && readability.color > 500;
    };

    // `mostReadable`
    // Given a base color and a list of possible foreground or background
    // colors for that base, returns the most readable color.
    // *Example*
    //    tinycolor.mostReadable("#123", ["#fff", "#000"]) => "#000"
    tinycolor.mostReadable = function(baseColor, colorList) {
        var bestColor = null;
        var bestScore = 0;
        var bestIsReadable = false;
        for (var i=0; i < colorList.length; i++) {

            // We normalize both around the "acceptable" breaking point,
            // but rank brightness constrast higher than hue.

            var readability = tinycolor.readability(baseColor, colorList[i]);
            var readable = readability.brightness > 125 && readability.color > 500;
            var score = 3 * (readability.brightness / 125) + (readability.color / 500);

            if ((readable && ! bestIsReadable) ||
                (readable && bestIsReadable && score > bestScore) ||
                ((! readable) && (! bestIsReadable) && score > bestScore)) {
                bestIsReadable = readable;
                bestScore = score;
                bestColor = tinycolor(colorList[i]);
            }
        }
        return bestColor;
    };


    // Big List of Colors
    // ------------------
    // <http://www.w3.org/TR/css3-color/#svg-color>
    var names = tinycolor.names = {
        aliceblue: "f0f8ff",
        antiquewhite: "faebd7",
        aqua: "0ff",
        aquamarine: "7fffd4",
        azure: "f0ffff",
        beige: "f5f5dc",
        bisque: "ffe4c4",
        black: "000",
        blanchedalmond: "ffebcd",
        blue: "00f",
        blueviolet: "8a2be2",
        brown: "a52a2a",
        burlywood: "deb887",
        burntsienna: "ea7e5d",
        cadetblue: "5f9ea0",
        chartreuse: "7fff00",
        chocolate: "d2691e",
        coral: "ff7f50",
        cornflowerblue: "6495ed",
        cornsilk: "fff8dc",
        crimson: "dc143c",
        cyan: "0ff",
        darkblue: "00008b",
        darkcyan: "008b8b",
        darkgoldenrod: "b8860b",
        darkgray: "a9a9a9",
        darkgreen: "006400",
        darkgrey: "a9a9a9",
        darkkhaki: "bdb76b",
        darkmagenta: "8b008b",
        darkolivegreen: "556b2f",
        darkorange: "ff8c00",
        darkorchid: "9932cc",
        darkred: "8b0000",
        darksalmon: "e9967a",
        darkseagreen: "8fbc8f",
        darkslateblue: "483d8b",
        darkslategray: "2f4f4f",
        darkslategrey: "2f4f4f",
        darkturquoise: "00ced1",
        darkviolet: "9400d3",
        deeppink: "ff1493",
        deepskyblue: "00bfff",
        dimgray: "696969",
        dimgrey: "696969",
        dodgerblue: "1e90ff",
        firebrick: "b22222",
        floralwhite: "fffaf0",
        forestgreen: "228b22",
        fuchsia: "f0f",
        gainsboro: "dcdcdc",
        ghostwhite: "f8f8ff",
        gold: "ffd700",
        goldenrod: "daa520",
        gray: "808080",
        green: "008000",
        greenyellow: "adff2f",
        grey: "808080",
        honeydew: "f0fff0",
        hotpink: "ff69b4",
        indianred: "cd5c5c",
        indigo: "4b0082",
        ivory: "fffff0",
        khaki: "f0e68c",
        lavender: "e6e6fa",
        lavenderblush: "fff0f5",
        lawngreen: "7cfc00",
        lemonchiffon: "fffacd",
        lightblue: "add8e6",
        lightcoral: "f08080",
        lightcyan: "e0ffff",
        lightgoldenrodyellow: "fafad2",
        lightgray: "d3d3d3",
        lightgreen: "90ee90",
        lightgrey: "d3d3d3",
        lightpink: "ffb6c1",
        lightsalmon: "ffa07a",
        lightseagreen: "20b2aa",
        lightskyblue: "87cefa",
        lightslategray: "789",
        lightslategrey: "789",
        lightsteelblue: "b0c4de",
        lightyellow: "ffffe0",
        lime: "0f0",
        limegreen: "32cd32",
        linen: "faf0e6",
        magenta: "f0f",
        maroon: "800000",
        mediumaquamarine: "66cdaa",
        mediumblue: "0000cd",
        mediumorchid: "ba55d3",
        mediumpurple: "9370db",
        mediumseagreen: "3cb371",
        mediumslateblue: "7b68ee",
        mediumspringgreen: "00fa9a",
        mediumturquoise: "48d1cc",
        mediumvioletred: "c71585",
        midnightblue: "191970",
        mintcream: "f5fffa",
        mistyrose: "ffe4e1",
        moccasin: "ffe4b5",
        navajowhite: "ffdead",
        navy: "000080",
        oldlace: "fdf5e6",
        olive: "808000",
        olivedrab: "6b8e23",
        orange: "ffa500",
        orangered: "ff4500",
        orchid: "da70d6",
        palegoldenrod: "eee8aa",
        palegreen: "98fb98",
        paleturquoise: "afeeee",
        palevioletred: "db7093",
        papayawhip: "ffefd5",
        peachpuff: "ffdab9",
        peru: "cd853f",
        pink: "ffc0cb",
        plum: "dda0dd",
        powderblue: "b0e0e6",
        purple: "800080",
        rebeccapurple: "663399",
        red: "f00",
        rosybrown: "bc8f8f",
        royalblue: "4169e1",
        saddlebrown: "8b4513",
        salmon: "fa8072",
        sandybrown: "f4a460",
        seagreen: "2e8b57",
        seashell: "fff5ee",
        sienna: "a0522d",
        silver: "c0c0c0",
        skyblue: "87ceeb",
        slateblue: "6a5acd",
        slategray: "708090",
        slategrey: "708090",
        snow: "fffafa",
        springgreen: "00ff7f",
        steelblue: "4682b4",
        tan: "d2b48c",
        teal: "008080",
        thistle: "d8bfd8",
        tomato: "ff6347",
        turquoise: "40e0d0",
        violet: "ee82ee",
        wheat: "f5deb3",
        white: "fff",
        whitesmoke: "f5f5f5",
        yellow: "ff0",
        yellowgreen: "9acd32"
    };

    // Make it easy to access colors via `hexNames[hex]`
    var hexNames = tinycolor.hexNames = flip(names);


    // Utilities
    // ---------

    // `{ 'name1': 'val1' }` becomes `{ 'val1': 'name1' }`
    function flip(o) {
        var flipped = { };
        for (var i in o) {
            if (o.hasOwnProperty(i)) {
                flipped[o[i]] = i;
            }
        }
        return flipped;
    }

    // Return a valid alpha value [0,1] with all invalid values being set to 1
    function boundAlpha(a) {
        a = parseFloat(a);

        if (isNaN(a) || a < 0 || a > 1) {
            a = 1;
        }

        return a;
    }

    // Take input from [0, n] and return it as [0, 1]
    function bound01(n, max) {
        if (isOnePointZero(n)) { n = "100%"; }

        var processPercent = isPercentage(n);
        n = mathMin(max, mathMax(0, parseFloat(n)));

        // Automatically convert percentage into number
        if (processPercent) {
            n = parseInt(n * max, 10) / 100;
        }

        // Handle floating point rounding errors
        if ((math.abs(n - max) < 0.000001)) {
            return 1;
        }

        // Convert into [0, 1] range if it isn't already
        return (n % max) / parseFloat(max);
    }

    // Force a number between 0 and 1
    function clamp01(val) {
        return mathMin(1, mathMax(0, val));
    }

    // Parse a base-16 hex value into a base-10 integer
    function parseIntFromHex(val) {
        return parseInt(val, 16);
    }

    // Need to handle 1.0 as 100%, since once it is a number, there is no difference between it and 1
    // <http://stackoverflow.com/questions/7422072/javascript-how-to-detect-number-as-a-decimal-including-1-0>
    function isOnePointZero(n) {
        return typeof n == "string" && n.indexOf('.') != -1 && parseFloat(n) === 1;
    }

    // Check to see if string passed in is a percentage
    function isPercentage(n) {
        return typeof n === "string" && n.indexOf('%') != -1;
    }

    // Force a hex value to have 2 characters
    function pad2(c) {
        return c.length == 1 ? '0' + c : '' + c;
    }

    // Replace a decimal with it's percentage value
    function convertToPercentage(n) {
        if (n <= 1) {
            n = (n * 100) + "%";
        }

        return n;
    }

    // Converts a decimal to a hex value
    function convertDecimalToHex(d) {
        return Math.round(parseFloat(d) * 255).toString(16);
    }
    // Converts a hex value to a decimal
    function convertHexToDecimal(h) {
        return (parseIntFromHex(h) / 255);
    }

    var matchers = (function() {

        // <http://www.w3.org/TR/css3-values/#integers>
        var CSS_INTEGER = "[-\\+]?\\d+%?";

        // <http://www.w3.org/TR/css3-values/#number-value>
        var CSS_NUMBER = "[-\\+]?\\d*\\.\\d+%?";

        // Allow positive/negative integer/number.  Don't capture the either/or, just the entire outcome.
        var CSS_UNIT = "(?:" + CSS_NUMBER + ")|(?:" + CSS_INTEGER + ")";

        // Actual matching.
        // Parentheses and commas are optional, but not required.
        // Whitespace can take the place of commas or opening paren
        var PERMISSIVE_MATCH3 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
        var PERMISSIVE_MATCH4 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";

        return {
            rgb: new RegExp("rgb" + PERMISSIVE_MATCH3),
            rgba: new RegExp("rgba" + PERMISSIVE_MATCH4),
            hsl: new RegExp("hsl" + PERMISSIVE_MATCH3),
            hsla: new RegExp("hsla" + PERMISSIVE_MATCH4),
            hsv: new RegExp("hsv" + PERMISSIVE_MATCH3),
            hsva: new RegExp("hsva" + PERMISSIVE_MATCH4),
            hex3: /^([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
            hex6: /^([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/,
            hex8: /^([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/
        };
    })();

    // `stringInputToObject`
    // Permissive string parsing.  Take in a number of formats, and output an object
    // based on detected format.  Returns `{ r, g, b }` or `{ h, s, l }` or `{ h, s, v}`
    function stringInputToObject(color) {

        color = color.replace(trimLeft,'').replace(trimRight, '').toLowerCase();
        var named = false;
        if (names[color]) {
            color = names[color];
            named = true;
        }
        else if (color == 'transparent') {
            return { r: 0, g: 0, b: 0, a: 0, format: "name" };
        }

        // Try to match string input using regular expressions.
        // Keep most of the number bounding out of this function - don't worry about [0,1] or [0,100] or [0,360]
        // Just return an object and let the conversion functions handle that.
        // This way the result will be the same whether the tinycolor is initialized with string or object.
        var match;
        if ((match = matchers.rgb.exec(color))) {
            return { r: match[1], g: match[2], b: match[3] };
        }
        if ((match = matchers.rgba.exec(color))) {
            return { r: match[1], g: match[2], b: match[3], a: match[4] };
        }
        if ((match = matchers.hsl.exec(color))) {
            return { h: match[1], s: match[2], l: match[3] };
        }
        if ((match = matchers.hsla.exec(color))) {
            return { h: match[1], s: match[2], l: match[3], a: match[4] };
        }
        if ((match = matchers.hsv.exec(color))) {
            return { h: match[1], s: match[2], v: match[3] };
        }
        if ((match = matchers.hsva.exec(color))) {
            return { h: match[1], s: match[2], v: match[3], a: match[4] };
        }
        if ((match = matchers.hex8.exec(color))) {
            return {
                a: convertHexToDecimal(match[1]),
                r: parseIntFromHex(match[2]),
                g: parseIntFromHex(match[3]),
                b: parseIntFromHex(match[4]),
                format: named ? "name" : "hex8"
            };
        }
        if ((match = matchers.hex6.exec(color))) {
            return {
                r: parseIntFromHex(match[1]),
                g: parseIntFromHex(match[2]),
                b: parseIntFromHex(match[3]),
                format: named ? "name" : "hex"
            };
        }
        if ((match = matchers.hex3.exec(color))) {
            return {
                r: parseIntFromHex(match[1] + '' + match[1]),
                g: parseIntFromHex(match[2] + '' + match[2]),
                b: parseIntFromHex(match[3] + '' + match[3]),
                format: named ? "name" : "hex"
            };
        }

        return false;
    }

    window.tinycolor = tinycolor;
    })();

    $(function () {
        if ($.fn.spectrum.load) {
            $.fn.spectrum.processNativeColorInputs();
        }
    });

});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/CS/VizModule/VizTool.js":[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * A component displayed by a VizEngine.
 *
 * @constructor
 * @param {string} container - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 * @param {Object} callbacks - Dictionary of the callbacks available
 *                  Contains : engine - the parent engine instance
 */

// Abstract class
var VizTool = function () {
    function VizTool(container, data, callbacks) {
        _classCallCheck(this, VizTool);

        // Avoid abstract instance
        if (new.target === VizTool) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        this.name = "";
        this.data = data;
        this.container = container;
        this.callbacks = callbacks;
    }

    /**
     * Init a VizTool : collect and format the data (if necessary) then render the VizTool
     */


    _createClass(VizTool, [{
        key: "display",
        value: function display() {
            throw new Error("display method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Wake up (Restore) the VizTool with minimal recalculation.
         */

    }, {
        key: "wakeUp",
        value: function wakeUp() {
            throw new Error("wakeUp method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Persist the VizTool for a quick restoration.
         */

    }, {
        key: "sleep",
        value: function sleep() {}
        // If actions must be performed before hiding a viztool, there shall be done here
        // Example: remove pending requests, kill timers, ...


        /**
         * Common callback to add a Viz to parent VizEngine
         * @param {string} name - the name of the VizTool to add
         * @param {*} data - the data used by the added VizTool
         */

    }, {
        key: "addViz",
        value: function addViz(name, data) {
            this.callbacks.engine.addViz(name, data, false);
        }
    }]);

    return VizTool;
}();

module.exports = VizTool;

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CanvasCurve.js":[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*globals require, module*/
/*exported D3CanvasCurve*/

//less/style integration

require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/curve.less");
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less");
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/zoom-controls.less");

//dependencies
var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    LIGViz = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js"),
    DataProvider = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js"),
    scaleHelper = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js"),
    CanvasCurveDrawer = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Drawer/CanvasCurveDrawer.js"),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js");

//const TsPointsCache = require('../Tools/TsPointsCache');

var _formatTime = function _formatTime(d) {
    'use strict';

    return new Date(d).toISOString().substr(0, 23);
};

var computeHover = function computeHover(point, ts) {
    'use strict';

    var computed = Object.assign({}, point),
        patterns = [];
    Object.assign(computed, {
        tsuid: ts.tsuid,
        key: ts.key,
        color: ts.color,
        variable: ts.md.variable,
        idx: point.idx
    });
    var locations = void 0,
        pattern = void 0;
    for (var k = 0; k < (ts.patterns || []).length; k++) {
        /*jshint loopfunc: true */
        pattern = ts.patterns[k];
        locations = pattern.locations.filter(function (l) {
            return point.idx >= l && point.idx <= l + pattern.length;
        });
        if (locations.length > 0 && pattern.activated) {
            patterns.push(Object.assign({}, pattern, { locations: locations }));
        }
    }
    computed.patterns = patterns;
    return computed;
};

var getText = function getText(newhover) {
    'use strict';

    var txt = '<div><b>TS: ' + newhover.tsuid + '</b></div><div>idx: ' + newhover.idx + '</div><div>value: ' + newhover.value + '</div><div>time: ' + _formatTime(newhover.timestamp) + '</div>';

    if (newhover.size > 1) {
        txt += '<div>size : ' + newhover.size + ' </div><div>minmax : [' + [newhover.min, newhover.max] + ']</div>';
    }
    txt += '<div>variable : ' + newhover.variable + ' </div>';

    var ptxt = '<div>patterns : ' + newhover.patterns.map(function (p) {
        return p.regex + ' <br> ' + p.locations;
    }).join(', ') + '</div>';

    if (newhover.patterns.length > 0) {
        txt += ptxt;
    }
    return txt;
};

/**
 * Visualization showing timeseries using canvas
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 */

var CanvasCurve = function (_LIGViz) {
    _inherits(CanvasCurve, _LIGViz);

    function CanvasCurve(containerId, data) {
        _classCallCheck(this, CanvasCurve);

        var _this = _possibleConstructorReturn(this, (CanvasCurve.__proto__ || Object.getPrototypeOf(CanvasCurve)).call(this, containerId, data));
        // Call super-class constructor


        _this.containerId = containerId;
        _this.md = {};
        _this.name = 'CanvasCurve';
        _this.container = d3.select('#' + containerId).html('');
        _this.graph = null;
        _this.logger = new LigLogger('lig:curve');
        _this.dispatch = d3.dispatch('zoomed', 'reset', 'syncChange', 'removed', 'tsRemoved', 'change', 'pointmouseenter', 'pointmouseleave');
        _this.context = {
            start: null,
            stop: null,
            duration: 700,
            hover: null,
            x: null,
            scale: 1,
            indrawing: false,
            xInterval: [0, 0],
            xMinMax: [0, 0],
            timestampMinMax: [0, 0],
            indexMinMax: [0, 0],
            variables: [],
            yMinMax1: [undefined, undefined],
            yMinMax2: null
        };
        _this.scales = {};
        _this._options = {
            mode: 'timestamp', // {index|timestamp}
            margin: {
                left: 40,
                right: 40,
                top: 20,
                bottom: 0
            },
            minimap: {
                height: 15,
                show: true,
                paddingBottom: 5
            },
            weights: {
                height: 15,
                show: false,
                minColor: 'rgb(231, 228, 228)',
                maxColor: 'darkgrey'
            },
            dots: {
                radius: 1,
                hoverRadius: 5,
                highlightRadius: 2,
                mousePrecision: 2
            },
            lines: {
                width: 1,
                hoverWidth: 3
            },
            tooltip: {
                show: true, // per default
                position: 'top' // {auto|top|bottom}
            },
            minHTickDist: 10,
            minTickDist: 20, // use to know if we show tick as following event or not
            once: false, // used to know if we use cache or not
            autosize: true, // used to know if we recompute height and width dynamically
            height: 500,
            width: 1200,
            xMinMax: null,
            yMinMax: null,
            showAxis: true, // option to know if we show/hide axis
            showVariable: true, // option to know if we show/hide legends related to variables
            transform: null, // zoom object
            maxCurves: 100, // number max of curves
            dataProvider: new DataProvider(),
            debugMode: true
        };
        _this._inputs = {};
        _this.dom = {
            canvas: null,
            canvasMouse: null,
            canvasHidden: null,
            container: null,
            focus: null,
            minimap: null,
            zoom: null,
            brush: null
        };
        _this.obj = {
            dispatchZoom: true, // flag to know if we dispath the zoom Event
            canvasContext: null,
            canvasMouseContext: null,
            zoom: null,
            canvasDrawer: new CanvasCurveDrawer(),
            colors: d3.scaleOrdinal(d3.schemeCategory10),
            prevDomain: null,
            currentPointsRequest: null, // current promise that is the points requesting
            cacheMetadata: null, // property used to store async data (metadata) and use as cache
            cachePoints: null, // property used to store async data (points) and use as cache
            cache: {
                metadata: {},
                start: null,
                end: null,
                points: {}
            }
        };
        return _this;
    }

    _createClass(CanvasCurve, [{
        key: '_computeContext',
        value: function _computeContext() {
            var self = this;
            self.context = Object.assign(self.context || {}, {
                xMinMax: [0, d3.max(self.data.map(function (d) {
                    return +d.md.size - 1;
                }))],
                timestampMinMax: [d3.min(self.data.map(function (d) {
                    return +d.md.start_date;
                })), d3.max(self.data.map(function (d) {
                    return +d.md.end_date;
                }))],
                indexMinMax: [0, d3.max(self.data.map(function (d) {
                    return +d.md.end;
                }))],
                variables: Array.from(new Set(self.data.map(function (d) {
                    return d.md.variable;
                }))),
                yMinMax1: [undefined, undefined],
                yMinMax2: null,
                yTicks1: [],
                yTicks2: []
            });

            self.context.variables.forEach(function (variable, i) {
                // filter ts with same variable
                var ts_list = self.data.filter(function (d) {
                    return d.md.variable === variable;
                });
                // compute min/max
                var min = void 0,
                    max = void 0;
                if (self._options.yMinMax && self._options.yMinMax[variable]) {
                    min = self._options.yMinMax[variable][0];
                    max = self._options.yMinMax[variable][1];
                }
                if (_.isUndefined(min) || _.isUndefined(max)) {
                    min = d3.min(ts_list.map(function (d) {
                        return +d.md.min;
                    }));
                    max = d3.max(ts_list.map(function (d) {
                        return +d.md.max;
                    }));
                }
                if (_.isUndefined(min) || _.isUndefined(max)) {
                    self.logger.error('min (' + min + ') and (' + max + ') must be defined');
                    return;
                }
                // If there is bps, use it to adjust min / max
                if (self._inputs.breakpoints && self._inputs.breakpoints.findIndex(function (t) {
                    return t.key === variable;
                }) > -1) {
                    self.logger.log('has bps, use it to define minmax', self._inputs.breakpoints);
                    var bps = self._inputs.breakpoints[self._inputs.breakpoints.findIndex(function (t) {
                        return t.key === variable;
                    })].bps;
                    if (bps) {
                        var minBps = bps[0],
                            maxBps = bps[bps.length - 1],
                            tickValues = [];
                        bps.forEach(function (m) {
                            tickValues.push(m.min + (m.max - m.min) / 2);
                            if (maxBps.key !== m.key) {
                                tickValues.push(m.max);
                            }
                        });

                        self.context['yMinMax' + (i + 1)] = [minBps.min, maxBps.max];
                        self.context['yTicks' + (i + 1)] = tickValues;
                    }
                }
                // else apply diff of 10 pourcents to make the graph breath
                else {
                        self.logger.log('no bps, use values to define minmax', self._inputs.breakpoints, self._inputs);
                        // add 10% (y)
                        var diff = (max - min) * (10 / 100);
                        // apply it
                        self.context['yMinMax' + (i + 1)] = [min - diff, max + diff];
                    }
            });
            self.logger.log('context', self.context);
        }

        /**
         * Display widtget
         */

    }, {
        key: 'display',
        value: function display() {
            var self = this;
            self.logger.log('display \'' + self.containerId + '\'');
            // assign d3 container element
            this.container = d3.select('#' + this.containerId);

            // rseet content
            this.container.html('').classed('in-loading', true);

            this.init();
            this.update();

            return Promise.resolve(self.obj.cache.metadata).then(function (cachedMetadata) {
                var isInCache = self.data.findIndex(function (d) {
                    return !cachedMetadata[d.tsuid];
                }) === -1;
                if (isInCache) {
                    return cachedMetadata;
                }
                return self._options.dataProvider.getMetaData(self.data, self._inputs).then(function (metadata) {
                    return (self.obj.cache.metadata = metadata) && metadata;
                });
            }).then(function (metadata) {
                self.md = metadata;
                self.data.forEach(function (d) {
                    d.filtered = [];
                    d.scope = {};
                    d.concerned = null;
                    d.md = self.md[d.tsuid] || self.md[d.tsuid.split('#')[0]];
                });
                self.container.classed('in-loading', false);
                return self.render();
            });
        }
    }, {
        key: 'wakeUp',
        value: function wakeUp() {
            this.display();
        }

        /**
         * getComputedPoints with current context (scale, range)
         * @return {Promise} Promise with points as result
         */

    }, {
        key: 'getComputedPoints',
        value: function getComputedPoints() {
            var self = this;
            // beginning
            var start = self.scales.x.domain()[0],

            // ending
            end = self.scales.x.domain()[1];
            // get the diff in time between the beginning and the end
            var diff = Math.abs(end - start),

            // get the diff in px between the beginning and the end
            width = self.scales.x.range()[1] - self.scales.x.range()[0],
                height = self.scales.y.range()[1] - self.scales.y.range()[0],
                concerned = self.data.filter(function (d) {
                return d.filtered && (!d.scope || d.scope.start !== start || d.scope.width !== width || d.scope.height !== height);
            });

            // if there is no concerned ts, return an empty array
            if (concerned.length === 0) {
                return Promise.resolve([]);
            }

            var fn = function fn() {
                //console.log('fetching points', self.containerId, concerned.map(d =>d.tsuid), start, end, width, diff, self._options.mode);
                // loader on
                self.dom.subContainer.classed('in-loading', true);

                // get async data
                promise = Promise.resolve(self.obj.cache.points).then(function (data) {
                    var isInCache = self.data.findIndex(function (d) {
                        return !data[d.tsuid];
                    }) === -1;
                    // if cache is enough
                    if (isInCache && (self.obj.cache.start === start && self.obj.cache.end === end || self._options.once)) {
                        //console.log('cache is enough', self.data.map(d=>d.tsuid), self.obj.cache.points);
                        return self.data.map(function (d) {
                            return data[d.tsuid];
                        });
                    }
                    //else {
                    //console.log('cache is not enough', self.data.map(d=>d.tsuid), self.obj.cache.points);
                    //}
                    // reset old promise if it exists and cencellable
                    if (self.obj.promise && self.obj.promise.cancel) {
                        self.obj.promise.cancel();
                    }
                    // initialize new async promise
                    self.obj.promise = self._options.dataProvider.getPoints(concerned, start, end, width, diff, self._options.mode, self._inputs);
                    // keep result in cache
                    return self.obj.promise.then(function (results) {
                        self.obj.cache.end = end;
                        self.obj.cache.start = start;
                        self.obj.cache.points = {};
                        concerned.forEach(function (c, i) {
                            self.obj.cache.points[c.tsuid] = results[i];
                        });
                        return results;
                    });
                });
                // compute data
                return promise.then(function (results) {
                    results.forEach(function (result_tmp, i) {
                        // assign result
                        concerned[i].filtered = result_tmp.map(function (result) {
                            return Object.assign({}, result);
                        });
                        // keep trace of scope in
                        concerned[i].scope = {
                            start: start,
                            end: end,
                            width: width,
                            height: height
                        };
                        // reset cached points
                        concerned[i].concerned = null;
                    });
                    // store in context the min / max gathered
                    self.context.xInterval = [start - diff, end + diff];
                    // loader off
                    self.dom.subContainer.classed('in-loading', false);

                    return results;
                }).catch(function () {
                    // loader off
                    self.dom.subContainer.classed('in-loading', false);
                });
            };

            // create a new promise for optimization
            var promise = new Promise(function (resolve) {
                // abort previous request, if its not aleady processed
                clearTimeout(self.obj.currentPointsRequest);
                // ask points with a delay ( in order to be able to abort it)
                self.obj.currentPointsRequest = setTimeout(function () {
                    // propagate answer
                    resolve(fn());
                }, 50);
            });

            return promise;
        }
    }, {
        key: '_renderToolTip',
        value: function _renderToolTip(newhover) {
            var self = this;
            if (newhover && self._options.tooltip.show) {
                self.dom.tooltip.classed('hidden', false).style('opacity', 1).html(getText(newhover));
                var coords = scaleHelper.getUnscaledMousePosition(document.body);
                //var containerSize = self.dom.canvas.node().getBoundingClientRect();
                //var tooltipSize = self.dom.tooltip.node().getBoundingClientRect();
                //var rightWidth = Math.abs(containerSize.width - coords[0]);
                //var leftPosition = (rightWidth < tooltipSize.width + 50) ? coords[0] - tooltipSize.width : coords[0] + 50;
                //var topPosition = (coords[1] > containerSize.height / 2) ? coords[1] - tooltipSize.height + 40 : coords[1] - 5;
                self.dom.tooltip.style('position', 'fixed').style('left', coords[0] + 'px').style('top', coords[1] - 20 + 'px');
            } else if (newhover !== self.context.hover) {
                self.dom.tooltip.transition().duration(100).on('end', function () {
                    d3.select(this).classed('hidden', true);
                });
            }
        }

        /**
         * Highlight specific curve
         * @param  {String} tsuid * ts which will be highlighted
         * @return {ProtoCanvasCurve} current object
         */

    }, {
        key: 'highlightCurve',
        value: function highlightCurve(tsuid) {
            var self = this;
            // if tsuid equals to null, dont highlight a curve (reset)
            var sequence = self.data[self.data.findIndex(function (d) {
                return d.tsuid === tsuid;
            })];
            if (sequence) {
                self.context.hover = {
                    tsuid: sequence.tsuid,
                    variable: sequence.variable
                };
            } else {
                self.context.hover = null;
            }

            self.redrawCanvas();
            return self;
        }

        /**
         * Main function to initialize and create all needed elements
         */

    }, {
        key: 'init',
        value: function init() {
            var _this2 = this;

            var self = this;

            if (self._options.autosize) {
                self._options.width = $(self.container.node()).width();
                var h = $(self.container.node()).height();
                if (h !== 0) {
                    self._options.height = h;
                }
            }
            //self.container.style('position', 'relative');
            var innerChartWidth = self._options.width - self._options.margin.left - self._options.margin.right,
                minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0,
                weightsHeight = self._options.weights.show ? self._options.weights.height : 0,
                innerChartHeight = self._options.height - self._options.margin.top - self._options.margin.bottom - minimapHeight - weightsHeight,
                mouseDiff = self._options.dots.mousePrecision;

            var getCurrentHover = function getCurrentHover() {
                var coords = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
                var tsArray = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _this2.data;
                var hover = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _this2.context.hover;

                var minY = coords[1] + mouseDiff,
                    maxY = coords[1] - mouseDiff;
                // sort data to have the ts in hover in last
                var data = tsArray.slice(0);
                if (hover) {
                    data.sort(function (d) {
                        return hover && hover.tsuid === d.tsuid;
                    });
                }
                for (var i = data.length - 1; i >= 0; i--) {
                    var ts = data[i];
                    if (ts.closest && ts.closest.y >= maxY && ts.closest.y <= minY) {
                        return computeHover(ts.closest, ts);
                    }
                }
            };

            var getClosest = function getClosest(x, points) {
                // TODO -> clean this block (too dirty)
                if (x && points && points.length > 0) {
                    var afterIdx = points.findIndex(function (p) {
                        return x < p.x;
                    });
                    if (afterIdx < 0) {
                        afterIdx = 0;
                    }
                    var beforeIdx = afterIdx > 0 ? afterIdx - 1 : 0;

                    if (Math.abs(points[beforeIdx].x - x) < Math.abs(points[afterIdx].x - x)) {
                        return points[beforeIdx];
                    } else {
                        return points[afterIdx];
                    }
                }
            };

            self.container = d3.select('#' + self.containerId);

            self.dom.subContainer = self.container.append('div').attr('class', 'curve-subcontainer').style("width", self._options.width + 'px').style("height", self._options.height + 'px').style('position', 'relative');

            self.dom.loader = self.dom.subContainer.append('div').attr('class', 'loader small');

            self.dom.canvas = self.dom.subContainer.append('canvas').attr('class', 'curve-canvas').style('top', weightsHeight + 'px').style('left', self._options.margin.left + 'px');

            self.dom.canvasMouse = self.dom.subContainer.append('canvas').attr('class', 'curve-canvas-mouse').attr("width", innerChartWidth).style('top', weightsHeight + 'px').attr("height", innerChartHeight).style('left', self._options.margin.left + 'px');

            Object.assign(self.obj, {
                canvasContext: self.dom.canvas.node().getContext('2d'),
                canvasMouseContext: self.dom.canvasMouse.node().getContext('2d')
            });

            self.dom.canvasHidden = document.createElement('canvas');
            Object.assign(self.dom.canvasHidden, { width: innerChartWidth, height: innerChartHeight });

            self.graph = self.dom.subContainer.append('svg').attr("class", "curve").attr("width", self._options.width).attr("height", self._options.height).on('mouseleave', function () {
                self.obj.mouseCoords = null;
                if (self.context.hover) {
                    self.context.hover = null;
                    self.dom.tooltip.transition().duration(100).style("opacity", 0);
                }
                self.data.forEach(function (d) {
                    return d.closest = null;
                });
                self.container.selectAll('.yaxis, .yaxis2, .xaxis').classed('invisible', !self._options.showAxis);
                self.dom.canvasMouse.classed('hidden', true);
                self.redrawCanvas();
                self.renderWeightsBand();
            }).on('mousemove mouseenter', function () {
                self.dom.canvasMouse.classed('hidden', false);
                self.obj.mouseCoords = self.obj.canvasDrawer.drawMouseCross(self.dom.canvasMouse.node(), self.scales.x, self.scales.y, self.scales.y2);

                // hide zoom area if ctrl key is pressed (in order to handle brush)
                self.dom.zoom.style('display', d3.event.ctrlKey || d3.event.shiftKey ? 'none' : 'initial');

                var coords = scaleHelper.getUnscaledMousePosition(self.dom.canvas.node()),
                    newhover = null,
                    insideChart = innerChartWidth >= coords[0] && coords[0] >= 0 && coords[1] <= innerChartHeight;

                if (insideChart) {
                    self.data.forEach(function (d) {
                        d.closest = getClosest(coords[0], d.concerned);
                    });
                    newhover = getCurrentHover(coords);
                } else {
                    self.data.forEach(function (d) {
                        return d.closest = null;
                    });
                }

                self._renderToolTip(newhover);

                if (!newhover && self.context.hover) {
                    self.dispatch.call('pointmouseleave', self, Object.assign({}, self.context.hover));
                } else if (newhover && newhover !== self.context.hover) {
                    self.dispatch.call('pointmouseenter', self, Object.assign({}, newhover));
                }

                if ((newhover || self.context.hover) && newhover !== self.context.hover) {
                    self.context.hover = newhover;
                }

                if (newhover) {
                    if (self.context.variables.indexOf(newhover.variable) === 1) {
                        self.container.selectAll('.yaxis').classed('invisible', true);
                        self.container.selectAll('.yaxis2').classed('invisible', false);
                    } else {
                        self.container.selectAll('.yaxis').classed('invisible', false);
                        self.container.selectAll('.yaxis2').classed('invisible', true);
                    }
                } else {
                    self.container.selectAll('.yaxis, .yaxis2').classed('invisible', !self._options.showAxis);
                }
                self.redrawCanvas();
                self.renderWeightsBand();

                self.graph.classed("pointing", newhover !== null);
            });
            d3.select('body').selectAll('.lig-tooltip.main-tooltip').data([0]).enter().append('div').attr("class", "lig-tooltip main-tooltip").style("opacity", 0);

            self.dom.tooltip = d3.select('body>.lig-tooltip');

            //self.graph.attr("viewBox", `0 0 ${self.options.width} ${self.options.height}`);
            self.dom.focus = self.graph.append("g").attr("class", "focus").attr('transform', 'translate(' + self._options.margin.left + ')');

            self.dom.xaxis = self.dom.focus.append("g").attr("class", "xaxis").attr("transform", 'translate(0,' + (self._options.height - self._options.margin.top - minimapHeight - weightsHeight) + ')');

            self.dom.yaxis = self.dom.focus.append("g").attr("class", "yaxis");

            self.dom.yaxis2 = self.dom.focus.append("g").attr("class", "yaxis2");

            self.container.selectAll('.yaxis, .yaxis2, .xaxis').classed('invisible', !self._options.showAxis);
            if (self._options.weights.show) {
                self.dom.weights = self.graph.append('g').attr('class', 'weights').attr('transform', 'translate(' + self._options.margin.left + ', 0)');
                // redraw minimap
            }
            if (self._options.minimap.show) {
                self.dom.minimap = self.graph.append('g').attr('class', 'minimap').attr('transform', 'translate(' + self._options.margin.left + ', ' + (self._options.height - minimapHeight) + ')');
                // redraw minimap
            }

            self.dom.brush = self.graph.append('g').attr('class', 'brush').attr('transform', 'translate(' + self._options.margin.left + ', ' + weightsHeight + ')');

            self.dom.zoom = self.graph.append("rect").attr('class', 'zoom').attr('width', innerChartWidth).attr('height', innerChartHeight).attr('transform', 'translate(' + self._options.margin.left + ', ' + weightsHeight + ')').style('fill', 'none');

            //var prevScale;
            self.zoomed = function () {
                var transformObject = d3.event.transform;
                if (!self.context.scale || !self.context.x || self.context.scale !== transformObject.k || Math.abs((self.context.x || 0) - transformObject.x) > 1) {
                    self.logger.log('zoom event', self.containerId, d3.event.transform);
                    self.context.scale = d3.event.transform.k;
                    self.context.x = d3.event.transform.x;
                    // apply transformation to scaleX
                    self.scales.x.domain(d3.event.transform.rescaleX(self.scales.x2).domain());
                    self.data.forEach(function (d) {
                        d.concerned = null;
                        d.closest = null;
                        d.scope = null;
                    });
                    // redraw xaxis
                    self.renderHorizontalAxis();

                    window.requestAnimationFrame(function () {
                        self.redrawCanvas(null, true);
                        self.renderWeightsBand();
                    });

                    if (self._options.minimap.show && self.scales.x) {
                        // redraw minimap
                        self.dom.minimap.call(self.brushMinimap.move, self.scales.x.range().map(transformObject.invertX, transformObject));
                    }
                }
            };
        }

        /**
         * Init shared d3 scales
         * @param {*} width
         * @param {*} height
         */

    }, {
        key: 'initScales',
        value: function initScales(width, height) {
            var self = this;

            // Sets up a D3 scales
            self.scales.x = d3.scaleLinear().range([0, width]).domain(self._options.mode === 'timestamp' ? self.context.timestampMinMax : self.context.indexMinMax);
            self.scales.x2 = d3.scaleLinear().range([0, width]).domain(self.scales.x.domain());
            self.scales.y = d3.scaleLinear().range([height, 0]).domain(self.context.yMinMax1);

            self.scales.y2 = null;

            if (self.context.yMinMax2) {
                self.scales.y2 = d3.scaleLinear().range([height, 0]).domain(self.context.yMinMax2);
            }
        }

        /**
         * Alias to register handler for specific event
         * @param  {String} eventName [description]
         * @param  {Function} handler   [Tdescription]
         * @return {ProtoWithLegend}           [description]
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            this.dispatch.on(eventName, handler);
            return this;
        }

        /**
         * Get or set current options
         * @param  {Object} _ * new options if it's given
         * @return {ProtoCanvasCurve} current object
         */

    }, {
        key: 'options',
        value: function options(_) {
            if (!arguments.length) {
                return this._options;
            }
            Object.assign(this._options, _);
            return this;
        }

        /**
         * Get the current node
         * @return Object
         */

    }, {
        key: 'node',
        value: function node() {
            return this.container.node();
        }

        /**
         * Render horizontal axis + its vertical ticks
         */

    }, {
        key: 'renderHorizontalAxis',
        value: function renderHorizontalAxis() {
            var self = this;
            // redresh horizontal axis
            var width = self.scales.x.range()[1] - self.scales.x.range()[0];
            var ticks = [],
                r_ticks = [];

            var nbticks = parseInt(width / 200);
            // choose what vertical ticks will be shown
            var example = self.data[0];
            if (example && example.filtered && width / example.filtered.length >= self._options.minTickDist) {
                self.logger.log('use examples to render ticks');
                // if dist between events is enough use events as ticks
                ticks = example.filtered.map(function (d) {
                    return self._options.mode === 'timestamp' ? d.timestamp : d.idx;
                }).filter(function (d) {
                    return width >= self.scales.x(d);
                });
                var current = null;
                ticks.forEach(function (t) {
                    var v = self.scales.x(t);
                    if (!current || v - self.scales.x(current) >= 200) {
                        r_ticks.push(t);
                        current = t;
                    }
                });
                self.xAxis.tickValues(r_ticks);
                self.graph.select(".xaxis").call(self.xAxis);
            } else {
                // use normal ticks
                self.xAxis.tickValues(null).ticks(nbticks);
                ticks = self.xAxis.scale().ticks();
                self.graph.select(".xaxis").call(self.xAxis);
                self.logger.log('bticks =>', ticks);
            }
            self.graph.select(".xaxis").call(self.xAxis);
            // render vertical ticks
            var tickData = self.graph.select('.xaxis').selectAll('line.tick-v').data(ticks);

            tickData.exit().remove();

            var tickEnter = tickData.enter().append('line').attr('class', 'tick-v');

            tickEnter.merge(tickData).attr('transform', function (d) {
                return 'translate(' + self.scales.x(d) + ')';
            }).attr('y2', -self._options.height);
        }

        /**
         * Render band that will represent weights (interesting points have high weight)
         */

    }, {
        key: 'renderWeightsBand',
        value: function renderWeightsBand() {
            var self = this;
            if (self._options.weights.show) {
                // indicators in minimap
                var weightBand = self.dom.weights.selectAll('g.sequence-weights').data(self.data || [], function (d) {
                    return d.tsuid;
                });

                var weightBandEnter = weightBand.enter().append('g').attr('class', 'sequence-weights');

                weightBand.exit().remove();

                var weightBandUpdate = weightBandEnter.merge(weightBand);
                weightBandUpdate.each(function (sequence) {
                    var greyScale = d3.scaleLinear().domain(d3.extent((sequence.concerned || []).map(function (d) {
                        return d.weight;
                    }))).range([d3.color(self._options.weights.minColor), d3.color(self._options.weights.maxColor)]);
                    var weightOccurence = weightBandUpdate.selectAll('rect.sequence-weight').data(sequence.concerned || []);
                    var weightOccurenceEnter = weightOccurence.enter().append('rect').attr('y', 0).attr('fill', 'grey').attr('height', self._options.weights.height).attr('class', 'sequence-weight');
                    weightOccurenceEnter.merge(weightOccurence).attr('fill', function (d) {
                        return greyScale(d.weight);
                    }).each(function (point, i) {
                        var elem = d3.select(this);
                        var isFirst = i === 0,
                            isLast = i === sequence.concerned.length - 1,
                            widthBeforePoint = !isFirst ? (sequence.concerned[i].fullX - sequence.concerned[i - 1].fullX) / 2 : 0,
                            widthAfterPoint = !isLast ? (sequence.concerned[i + 1].fullX - sequence.concerned[i].fullX) / 2 : 0;
                        elem.attr('width', widthBeforePoint + widthAfterPoint + 1).attr('x', Math.floor(point.x - (isLast ? widthBeforePoint : isFirst ? 0 : (widthBeforePoint + widthAfterPoint) / 2)));
                    }).on('mousemove mouseenter', function () {
                        self.dom.canvasMouse.classed('hidden', false);
                        self.obj.mouseCoords = self.obj.canvasDrawer.drawMouseCross(self.dom.canvasMouse.node(), self.scales.x, self.scales.y, self.scales.y2);
                    });
                });
                // Manage the extension of the mouse cross to be displayed on the weights band
                var indicator = self.dom.weights.selectAll('rect.indicator').data(self.obj.mouseCoords ? [self.obj.mouseCoords] : []);
                var indicatorEnter = indicator.enter().append('rect').attr('class', 'indicator').attr('width', 1).attr('y', 0);
                indicator.exit().remove();
                indicatorEnter.merge(indicator).attr('x', function (d) {
                    return d[0];
                }).attr('height', self._options.weights.height);
            }
        }

        /**
         * Main function to render viz
         */

    }, {
        key: 'render',
        value: function render() {
            var self = this;

            self.logger.log('render \'' + self.containerId + '\'');

            var margin = self._options.margin;
            var width = +self._options.width - margin.left - margin.right,
                weightsHeight = self._options.weights.show ? self._options.weights.height : 0,
                minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0,
                height = +self._options.height - margin.top - margin.bottom - minimapHeight - weightsHeight;

            self._computeContext();

            self.initScales(width, height);

            self.xAxis = d3.axisBottom(self.scales.x).tickFormat(self._options.mode === 'timestamp' ? _formatTime : function (d) {
                return d;
            });

            var yAxis = d3.axisLeft(self.scales.y).tickValues(d3.min([height / 20, 10])).tickFormat(d3.format(".2"));

            self.obj.zoom = d3.zoom().duration(self.context.duration).scaleExtent([1, 50000]).translateExtent([[0, 0], [width, height]]).extent([[0, 0], [width, height]]).on("end", function () {
                var newDomain = self.scales.x.domain();
                // stop process if previous domain is same as new domain
                if (self.obj.prevDomain && self.obj.prevDomain[0] === newDomain[0] && self.obj.prevDomain[1] === newDomain[1]) {
                    //} || sourceEvent === 2 || sourceEvent.detail < 2 || sourceEvent.type === 'brush') {
                    return;
                }
                // store transform object
                self._options.transform = d3.event.transform;

                // dispatch event it its property dispatchZoom is equal to true
                if (self.obj.dispatchZoom) {
                    if (self._options.debugMode) {
                        self.logger.log('dispatch zoom event', self._options.transform);
                    }
                    self.dispatch.call('zoomed', d3.event.transform, self.containerId, self._options.transform);
                } else if (self._options.debugMode) {
                    self.logger.log('dispatch zoom disable', self._options.transform);
                }

                // store current domain as previous domain
                self.obj.prevDomain = self.scales.x.domain();

                // TODO : work on getting data (async)
                if (self.data && self.data.length > 0) {
                    self.getComputedPoints().then(function () {
                        // reset the flag to true
                        self.obj.dispatchZoom = true;
                        // redraw
                        window.requestAnimationFrame(function () {
                            self.redrawCanvas();
                            self.renderWeightsBand();
                            self.renderHorizontalAxis();
                        });
                    });
                }
            }).on("zoom", self.zoomed).filter(function () {
                return !d3.event.ctrlKey && !d3.event.shiftKey;
            });

            /**
             * Apply ticks on an d3 axis and check min distance
             * @param {Object} axis - d3 axis
             * @param {Object} scale - d3 scale
             * @param {Array} ticks - array of tick values
             * @param {Integer} min_distance - min distance
             */
            var _setTicks = function _setTicks(axis, scale, ticks, min_distance) {
                var values = null,
                    current = void 0;
                if (ticks && ticks.length > 0) {
                    values = [];
                    // raw ticks
                    if (scale(ticks[0]) - scale(ticks[1]) > min_distance) {
                        values = ticks;
                    }
                    // filter ticks in looking min distance
                    else {
                            ticks.forEach(function (d, i) {
                                if (i !== 0 && (!current || current - scale(d) > min_distance)) {
                                    current = scale(d);
                                    values.push(d);
                                }
                            });
                        }
                }
                axis.tickValues(values);
            };

            var brushedFromGraph = function brushedFromGraph() {
                if (d3.event.sourceEvent && (d3.event.sourceEvent.type === "zoom" || d3.event.sourceEvent.type === "end")) {
                    return;
                } // ignore brush-by-zoom
                var s = d3.event.selection || self.scales.x.range();
                self.scales.x.domain(s.map(self.scales.x.invert, self.scales.x));
                var currentDomain = self.scales.x.domain();
                var scaleValue = void 0;
                if (self._options.mode === 'timestamp') {
                    // timestamp mode
                    scaleValue = (self.context.timestampMinMax[1] - self.context.timestampMinMax[0]) / (currentDomain[1] - currentDomain[0]);
                } else {
                    // index mode
                    scaleValue = (self.context.indexMinMax[1] - self.context.indexMinMax[0]) / (currentDomain[1] - currentDomain[0]);
                }
                // call zoom
                self.graph.select('.zoom').transition().duration(700).call(self.obj.zoom.transform, d3.zoomIdentity.scale(scaleValue).translate(-self.scales.x2(currentDomain[0]), 0));
                // hide tooltip
                self.dom.tooltip.style("opacity", 0);
                // remove brush
                self.graph.select('.brush').call(self.brushGraph.move, null);
            };

            self.brushMinimap = d3.brushX().extent([[0, 0], [width, minimapHeight - self._options.minimap.paddingBottom]]).on("brush", function () {
                if (d3.event.sourceEvent && d3.event.sourceEvent.type === "mousemove") {
                    var s = d3.event.selection;
                    if (!s || isNaN(s[0] || isNaN(s[1]))) {
                        s = self.scales.x.range();
                    }
                    // call zoom
                    self.dom.zoom.call(self.obj.zoom.transform, d3.zoomIdentity.scale((self.scales.x.range()[1] - self.scales.x.range()[0]) / (s[1] - s[0])).translate(-s[0], 0));
                }
            }).on("end", function () {
                if (d3.event.selection === null) {
                    // keep same scale but move the bar
                    var coords = d3.mouse(self.dom.minimap.node()),
                        position_x = d3.min([coords[0], width - width / self._options.transform.k]);
                    // call zoom
                    self.dom.zoom.call(self.obj.zoom.transform, d3.zoomIdentity.scale(self._options.transform.k).translate(-position_x, 0));
                }
            });

            self.brushGraph = d3.brushX().extent([[0, 0], [width, height]]).on("end", brushedFromGraph);

            /**new ZoomControls()
               .on('zoomIn', () => self.scale(self.context.scale * 2))
               .on('zoomOut', () => self.scale(self.context.scale / 2))
               .init(self.container, self.graph);
            **/

            _setTicks(yAxis, self.scales.y, self.context.yTicks1, self._options.minHTickDist);

            if (self._options.minimap.show) {
                self.dom.minimap.call(self.brushMinimap);
                // indicators in minimap
                var indicatorsData = self.dom.minimap.selectAll('rect.indicator').data(self.context.patternLocations || []);

                indicatorsData.enter().append('rect').attr('class', 'indicator').attr('y', 2).attr('height', 6);

                indicatorsData.exit().remove();

                self.dom.minimap.selectAll('rect.indicator').attr('fill', function (d) {
                    return d.color;
                }).attr('transform', function (d) {
                    return 'translate(' + self.scales.x(self._options.mode === 'timestamp' ? d.startDate : d.startIndex) + ', 0)';
                }).attr('width', function (d) {
                    var start, end;
                    if (self._options.mode === 'timestamp') {
                        start = d.startDate;
                        end = d.endDate;
                    } else {
                        start = d.startIndex;
                        end = d.endIndex;
                    }
                    return self.scales.x(end) - self.scales.x(start);
                });
            }

            var range = self.scales.x.range();
            if (self._options.transform) {
                // Does it need it here?
                self.scales.x.domain(self._options.transform.rescaleX(self.scales.x2).domain());
                range = self.scales.x.range().map(self._options.transform.invertX, self._options.transform);
            }

            // call minimap with range
            self.dom.minimap.call(self.brushMinimap.move, range);
            // assign zoom to dom
            self.dom.zoom.call(self.obj.zoom);
            // assign brush to dom
            self.dom.brush.call(self.brushGraph);

            self.getComputedPoints().then(function () {
                if (self._options.transform) {
                    // call zoom with transform option
                    self.dom.zoom.call(self.obj.zoom.transform, self._options.transform);
                }
                self.redrawCanvas();
                self.renderWeightsBand();
                self.renderHorizontalAxis();
            });

            self.graph.select(".yaxis").call(yAxis).selectAll('text.variable').data([0]).enter().append("text").attr('class', 'variable').attr("fill", "#000").attr("transform", "rotate(-90)").attr("y", 6).attr("dy", "0.71em").attr("text-anchor", "end").text(function () {
                return self._options.showVariable ? self.context.variables[0] : '';
            });

            if (self.scales.y2) {
                var yAxis2 = d3.axisRight(self.scales.y2).ticks(d3.min([height / 20, 10])).tickFormat(d3.format('.2'));

                _setTicks(yAxis2, self.scales.y2, self.context.yTicks2, self._options.minHTickDist);

                self.graph.select(".yaxis2").attr('transform', 'translate(' + width + ', 0)').call(yAxis2).selectAll('text.variable').data([[]]).enter().append("text").attr('class', 'variable').attr("fill", "#000").attr("transform", "rotate(-90)").attr("y", "-1em").attr("dy", "0.71em").attr("text-anchor", "end").text(function () {
                    return self._options.showVariable ? self.context.variables[1] : '';
                });
            }
        }

        /**
         * get/set inputs
         * @param  {Object} _ inputs
         *      ex : { ts_list: [], patterns: {}}
         * @return {Object}
         */

    }, {
        key: 'inputs',
        value: function inputs(_) {
            if (!arguments.length) {
                return this.inputs;
            }
            this._inputs = _;
            return this;
        }

        /**
         * Redraw main canvas
         * @param  {Array} ds - array of ts
         */
        /**
         * Redraw main canvas
         * @param  {Array} ds - array of ts
         */

    }, {
        key: 'redrawCanvas',
        value: function redrawCanvas(ds) {
            var quickdraw = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var self = this;

            /**
             * Helper to get d3 scale (in looking on variable)
             * @param  {Object} ts - timeseries
             * @return {Object}      color
             */
            var _getYScale = function _getYScale(variable) {
                return self.context.variables.indexOf(variable) === 1 ? self.scales.y2 : self.scales.y;
            };

            self.obj.mouseCoords = self.obj.canvasDrawer.drawMouseCross(self.dom.canvasMouse.node(), self.scales.x, self.scales.y, self.scales.y2);

            if (self.context.indrawing) {
                console.warn('Already in drawing');
            }

            var canvasNode = self.dom.canvas.node(),
                tmp = self.dom.canvasHidden;

            self.context.indrawing = true;
            var minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0;
            var weightsHeight = self._options.weights.show ? self._options.weights.height : 0;
            canvasNode.width = tmp.width = self._options.width - self._options.margin.left - self._options.margin.right;
            canvasNode.height = tmp.height = +self._options.height - self._options.margin.top - self._options.margin.bottom - minimapHeight - weightsHeight;

            // sort data to have the ts in hover in last
            var data = (ds || self.data).slice(0);
            if (self.context.hover) {
                data.sort(function (d) {
                    return self.context.hover && self.context.hover.tsuid === d.tsuid;
                });
            }

            var preparedData = data.map(function (ts) {
                //Cut in all points only some displayed
                var yScale = _getYScale(ts.variable || ts.md.variable);
                if (!ts.concerned) {
                    var startCutIdx = -1; //ts.filtered.findIndex(d=>d.timestamp > start) - 1;
                    if (startCutIdx < 0) {
                        startCutIdx = 0;
                    }
                    var endCutIdx = -1; //ts.filtered.findIndex(d=>d.timestamp > end) + 1;
                    if (endCutIdx === 0) {
                        endCutIdx = ts.filtered.length;
                    }
                    ts.concerned = ts.filtered.map(function (v) {
                        return Object.assign(v, {
                            x: Math.round(self.scales.x(self._options.mode === 'timestamp' ? v.timestamp : v.idx)),
                            fullX: self.scales.x(self._options.mode === 'timestamp' ? v.timestamp : v.idx),
                            y: Math.round(yScale(v.value)),
                            y1: Math.round(yScale(v.min)),
                            y2: Math.round(yScale(v.max))
                        });
                    });
                }

                var result = {
                    color: ts.color,
                    coords: ts.concerned,
                    tsuid: ts.tsuid,
                    xScale: self.scales.x,
                    yScale: yScale,
                    style: ts.style,
                    bps: ts.bps,
                    closest: ts.closest,
                    patterns: ts.patterns,
                    radius: self.context.hover && self.context.hover.tsuid === ts.tsuid ? self._options.dots.highlightRadius : self._options.dots.radius,
                    underlined: self.context.hover && self.context.hover.tsuid === ts.tsuid,
                    visible: !self.context.hover || self.context.hover.tsuid === ts.tsuid,
                    variable: ts.variable || ts.md.variable
                };
                // TODO: complexity
                if (self._inputs) {
                    if (self._inputs.breakpoints) {
                        result.bps = self._inputs.breakpoints[self._inputs.breakpoints.findIndex(function (t) {
                            return t.key === (ts.variable || ts.md.variable);
                        })];
                    }
                    if (self._inputs.disc_break_points && self._inputs.variables) {
                        result.discretizedDomain = self._inputs.disc_break_points[self._inputs.variables.indexOf(ts.variable || ts.md.variable)];
                    }
                }

                return result;
            });

            var variables = Array.from(new Set(self.data.map(function (d) {
                return d.variable;
            })));
            var hover, bps, mainVariable;
            // if there is an highlighted point
            if (self.context.hover) {
                hover = Object.assign({}, self.context.hover);
                Object.assign(hover, {
                    x: self.scales.x(self._options.mode === 'timestamp' ? self.context.hover.timestamp : self.context.hover.idx),
                    y: _getYScale(self.context.hover.variable)(hover.value),
                    radius: self._options.dots.hoverRadius
                });
                mainVariable = self.context.hover.variable;
            } else if (variables.length === 1) {
                mainVariable = variables[0];
            }
            // compute displayed breakpoints
            // ikats case
            if (mainVariable && self._inputs && self._inputs.breakpoints) {
                bps = self._inputs.breakpoints[self._inputs.breakpoints.findIndex(function (t) {
                    return t.key === mainVariable;
                })];
            }
            // lig case
            else if (mainVariable && preparedData.findIndex(function (p) {
                    return p.variable === mainVariable;
                }) > -1) {
                    bps = preparedData[preparedData.findIndex(function (p) {
                        return p.variable === mainVariable;
                    })].bps;
                }
            if (bps) {
                bps.left = variables.indexOf(mainVariable) === 0;
                self.obj.canvasDrawer.drawBreakPoints(self.obj.canvasContext, bps, tmp.width, _getYScale(mainVariable));
            }
            self.obj.canvasDrawer.redraw(preparedData, self.obj.canvasContext, hover, [tmp.width, tmp.height], quickdraw, self._options.mode);

            self.context.indrawing = false;
        }

        /**
         * Transform graph
         * @param  {Object}  transform
         * @param  {Boolean} dispatchZoom
         */

    }, {
        key: 'transform',
        value: function transform(_transform) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            if (_transform && _transform !== this._options.transform) {
                this.obj.dispatchZoom = dispatchZoom;
                this._options.transform = _transform;
                this.graph.select('.zoom').call(this.obj.zoom.transform, _transform);
            }
        }

        /**
         * Scale to a specific scale
         * @param  {Number}  scale
         * @param  {Boolean} dispatchZoom
         */

    }, {
        key: 'scale',
        value: function scale(_scale) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            if ((_scale || _scale > -1) && _scale !== this.context.scale) {
                this.obj.dispatchZoom = dispatchZoom;
                var target = this.graph.select('.zoom').transition().duration(this.context.duration / 2);
                this.obj.zoom.scaleTo(target, _scale);
            }
        }
    }, {
        key: 'update',
        value: function update(ts_list, inputs) {
            var self = this;

            if (ts_list) {
                self.data = ts_list;
            }

            if (inputs) {
                self._inputs = inputs;
            }

            if (self.data.length > self._options.maxCurves) {
                console.warn('too many to ts to draw (' + self.data.length + '), take only ' + self._options.maxCurves + ' ts');
                self.data = self.data.slice(0, self._options.maxCurves);
            }

            if (self.data) {
                // FIXME: reduce complexity
                var locations = d3.merge(self.data.map(function (d) {
                    return d3.merge((d.patterns || []).map(function (p) {
                        return p.locations.map(function (l) {
                            var diff = l.duration / p.length / 2;
                            return { startDate: l.timestamp - diff, startIndex: l.pos, endIndex: l.pos + p.length, endDate: l.timestamp + l.duration + diff, color: p.color };
                        });
                    }));
                }));
                self.context.patternLocations = locations;
            }

            // DEFAULT properties
            self.data.forEach(function (d, i) {
                d.color = d.color || self.obj.colors(i);
                d.filtered = d.filtered || [];
            });

            return self;
        }
    }]);

    return CanvasCurve;
}(LIGViz);

module.exports = CanvasCurve;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Drawer/CanvasCurveDrawer.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Drawer/CanvasCurveDrawer.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/curve.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/curve.less","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/zoom-controls.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/zoom-controls.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithLegend.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

//less/style integration

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/ts-legend.less");
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/colorpicker.less");

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

var LIGViz = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js"),
    CurveWithPatternLegend = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithPatternLegend.js"),
    d3ContextMenu = require("/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js"),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js"),
    spectrum = require("/home/davisp/projects/ikats_lig/hmi/node_modules/spectrum-colorpicker/spectrum.js"); // jshint ignore:line

/**
 * Visualization showing ts with a legend
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 * @param {Array}  callbacks - list of the callbacks used by Viz
 */

var CurveWithLegend = function (_LIGViz) {
    _inherits(CurveWithLegend, _LIGViz);

    function CurveWithLegend(containerId, inputs, callbacks) {
        _classCallCheck(this, CurveWithLegend);

        var _this = _possibleConstructorReturn(this, (CurveWithLegend.__proto__ || Object.getPrototypeOf(CurveWithLegend)).call(this, containerId, inputs.raw || inputs, callbacks));
        // Call super-class constructor


        _this.name = 'CurveWithLegend';
        _this.dispatch = d3.dispatch('zoomed', 'reset', 'syncChange', 'removed', 'tsRemoved', 'change');
        _this.containerId = containerId;
        _this.container = d3.select('#' + containerId);
        _this.context = {
            hover: null
        };
        _this.md = inputs.md;
        _this._options = {
            margin: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            },
            proportion: {
                graph: 88,
                legend: 12
            },
            synced: true, // TODO: still used?
            reset: false,
            height: 500,
            width: 1200,
            synchronized: true, // TODO: still used ?
            transform: null,
            styles: [['normal', []], ['small', [2, 2]], ['medium', [4, 4]], ['big', [6, 6]], ['mix', [3, 2, 1]]],
            showSettings: false
        };
        _this.logger = new LigLogger('lig-legend');
        _this.graph = null;
        _this.patterns = [];
        _this.graph = new CurveWithPatternLegend(_this.containerId + '-legendCurve', _this.data);
        return _this;
    }

    /**
     * set/get graph activated patterns
     * @param {Object} linkedPatterns
     * @memberof CurveWithLegend
     * @return {CurveWithLegend}
     */


    _createClass(CurveWithLegend, [{
        key: 'activatedPatterns',
        value: function activatedPatterns(_) {
            if (this.graph.activatedPatterns) {
                return this.graph.activatedPatterns(_);
            }
        }

        /**
         * Function allowing to display viz
         * @memberof CurveWithLegend
         */

    }, {
        key: 'display',
        value: function display() {
            this.update();
            this.container = d3.select('#' + this.containerId);
            this.container.classed('hidden', false);
            return this.render();
        }

        /**
         * Get the node container
         * @return {HTMLElement} element
         * @memberof CurveWithLegend
         */

    }, {
        key: 'node',
        value: function node() {
            return this.container.node();
        }

        /**
         * Alias to register handler for specific event
         * @param  {String} eventName [description]
         * @param  {Function} handler   [description]
         * @return {CurveWithLegend}           [description]
         * @memberof CurveWithLegend
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            this.dispatch.on(eventName, handler);
            return this;
        }

        /**
         * Get or set current options
         * @param  {Object} _ * new options if it's given
         * @return {CurveWithLegend} current object
         * @memberof CurveWithLegend
         */

    }, {
        key: 'options',
        value: function options(_) {
            if (!arguments.length) {
                return this._options;
            }
            Object.assign(this._options, _);
            this.graph.options(_);
            return this;
        }

        /**
         * Apply new scale
         * @param  {Object} scale
         * @param  {Boolean} dispatchZoom
         * @memberof CurveWithLegend
         */

    }, {
        key: 'scale',
        value: function scale(_scale) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            this.graph.scale(_scale, dispatchZoom);
        }

        /**
         * Sleep widget
         * @memberof CurveWithLegend
         */

    }, {
        key: 'sleep',
        value: function sleep() {
            this.container.classed('hidden', true);
        }

        /**
         * Apply new transformation
         * @param  {Object} transformation
         * @param  {Boolean} dispatchZoom
         * @memberof CurveWithLegend
         */

    }, {
        key: 'transform',
        value: function transform(_transform) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            this._options.transform = _transform;
            this.graph.transform(_transform, dispatchZoom);
        }

        /**
         * Function to render the viz
         * @memberof CurveWithLegend
         */

    }, {
        key: 'render',
        value: function render() {
            var self = this;

            // reset content
            self.container = d3.select('#' + self.containerId).html('');

            var graphWidth = Math.floor(self._options.width * self._options.proportion.graph / 100),
                legendWidth = Math.floor(self._options.width * self._options.proportion.legend / 100);

            var subContainer = self.container.append('div').attr('class', 'graph-legend-container').style('height', self._options.height + 'px');

            subContainer.append('div').classed('graph-inner', true).attr('id', self.containerId + '-' + 'legendCurve').style('width', graphWidth + 'px');

            self.controls = subContainer.append('div').classed('ts-legend', true).style('height', self._options.height - 10 + 'px').style('width', legendWidth + 'px');
            self.settings = self.controls.append('div').classed('ts-legend-settings', true).classed('hidden', false);
            self.settings.append('button').attr('type', 'button').attr('class', 'close').append('span').html('&times');

            self.settings.append('label').text('appearance');
            var lineAppearanceContainer = self.settings.append('div').classed('line-appearances-widget', true).append('ul').attr('class', 'list-group');

            lineAppearanceContainer.selectAll('li').data(self._options.styles).enter().append('li').attr('class', 'list-group-item').each(function () {
                var elem = d3.select(this);
                elem.append('svg').attr('width', '100px').attr('height', '10px').append('line').attr('stroke-dasharray', function (d) {
                    return d[1].join(',');
                }).attr('stroke', 'black').attr('x1', 0).attr('x2', 100).attr('y1', 5).attr('y2', 5);
            });
            self.settings.append('label').text('color');
            var colorPickerContainer = self.settings.append('div').classed('color-picker-widget', true);
            colorPickerContainer.append('input').attr('class', 'colorPicker');

            self.settings.classed('hidden', !self._options.showSettings);

            var controlContainer = self.controls.append('div').attr('class', 'ts-legend-buttons');

            var syncButton = controlContainer.append('span').attr('class', 'btn btn-xs btn-default sync-container').on('click', function () {
                var elem = d3.select(this).select('.glyphicon');
                self._options.synced = !self._options.synced;
                elem.classed('glyphicon-unchecked', !self._options.synced).classed('glyphicon-check', self._options.synced);
                self.dispatch.call('syncChange', self, self._options.synced);
            });

            syncButton.append('span').attr('class', 'glyphicon').classed('glyphicon-unchecked', !self._options.synced).classed('glyphicon-check', self._options.synced);

            syncButton.append('span').attr('class', 'sync-label').text('Sync');

            controlContainer.append('span').attr('class', 'btn btn-xs btn-default reset-container').text('Clear').on('click', function () {
                var dataIds = self.data.map(function (d) {
                    return d.tsuid;
                });
                self.data = [];
                self.graph.update(self.data);
                self.dispatch.call('reset', self, dataIds);
                self.display();
            });

            controlContainer.append('span').attr('class', 'btn btn-xs btn-default remove-container').text('Delete').on('click', function () {
                self.dispatch.call('removed', self);
                self.sleep();
            });
            self.legends = self.controls.append('div').attr('class', 'ts-legend-sequences');

            self.graph.on('zoomed', function () {
                self.context.transform = this;
                self._options.transform = d3.event.transform;
                self.dispatch.call('zoomed', d3.event.transform, self.containerId);
            }).on('change', function () {
                self.dispatch.call('change', self);
            }).on('pointmouseenter', function (point) {
                self.context.hover = point.tsuid;
                self.legends.selectAll('.ts-legend-field').classed('highlighted', function (d) {
                    return d.tsuid === point.tsuid;
                });
            }).on('pointmouseleave', function () {
                self.context.hover = null;
                self.legends.selectAll('.ts-legend-field').classed('highlighted', false);
            }).update(self.data.filter(function (d) {
                return d.selected;
            })).options({ width: graphWidth, height: self._options.height, transform: self._options.transform }).display();

            d3.select(self.graph.node()).on('contextmenu', function () {
                self._renderContextMenu(self.context.hover, this);
            });

            self.renderLegend();
        }

        /**
         * Render settings for a specific sequence
         * @param {Object} ts
         * @memberof CurveWithLegend
         */

    }, {
        key: '_renderSettings',
        value: function _renderSettings(ts) {
            var self = this;
            var _callback = function _callback() {
                self.graph.update(self.data);
                self.graph.render();
                self.renderLegend();
                self.settings.classed('hidden', true);
                self.dispatch.call('change', self);
            };
            var size = self.controls.node().getBoundingClientRect(),
                fullsize = document.body.getBoundingClientRect(),
                width = d3.max([200, size.width]),
                height = d3.max([350, size.height]),
                top = fullsize.height > size.top + height ? size.top : fullsize.bottom - height;

            self.settings.classed('hidden', false).style('top', size.top + 'px').style('width', width + 'px').style('left', size.left - width + size.width + 'px').style('top', top + 'px');

            var $controls = $(self.controls.node()),
                curve = self.data[self.data.findIndex(function (d) {
                return d.tsuid === ts.tsuid;
            })];
            curve.style = curve.style || self._options.styles[0];

            self.settings.select('.close').on('click', function () {
                self._options.showSettings = false;
                self.settings.classed('hidden', true);
            });

            self.settings.selectAll('li').classed('active', function (d) {
                return ts.style && d[0] === ts.style[0];
            }).on('click', function (d) {
                ts.style = d;
                _callback();
            });

            $(".colorPicker", $controls).spectrum({
                showInitial: true,
                flat: true,
                color: ts.color,
                clickoutFiresChange: false
            });

            self.controls.select('.sp-choose').on('click', function () {
                var color = $(".colorPicker", $controls).spectrum('get');
                if ('#' + color.toHex() !== ts.color) {
                    ts.color = '#' + color.toHex();
                    _callback();
                }
            });
        }

        /**
         * Render a contextmenu for a specific tsuid
         * @param {String} tsuid
         * @param {HTMLElement} elem
         * @memberof CurveWithLegend
         */

    }, {
        key: '_renderContextMenu',
        value: function _renderContextMenu(tsuid, elem) {
            var self = this;

            if (!tsuid) {
                return d3ContextMenu('close');
            }

            var fn = function fn() {
                var ts = self.data[self.data.findIndex(function (d) {
                    return tsuid && d.tsuid === tsuid;
                })];
                console.log('!!!ts', ts);
                if (!ts) {
                    return [];
                }

                var menu = [{
                    title: ts.key + ' [' + ts.variable + ']'
                }, {
                    divider: true
                }, {
                    title: 'Delete sequence',
                    action: function action() {
                        var idx = self.data.findIndex(function (d) {
                            return d.tsuid === ts.tsuid;
                        });
                        self.data.splice(idx, 1);
                        self.graph.update();
                        self.dispatch.call('tsRemoved', self, [ts.tsuid]);
                        self.display();
                    },
                    disabled: false // optional, defaults to false
                }, {
                    title: ts.selected === false ? 'Show' : 'Hide',
                    action: function action() {
                        ts.selected = !ts.selected;
                        self.refreshGraph();
                        self.renderLegend();
                        self.graph.highlightCurve(ts.selected ? ts.tsuid : null, ts.variable);
                    },
                    disabled: false // optional, defaults to false
                }, {
                    title: 'Change appearance',
                    action: function action() {
                        self._renderSettings(ts);
                    },
                    disabled: false // optional, defaults to false
                }];
                return menu;
            };

            // render context menu
            d3ContextMenu(fn, {
                onClose: function onClose() {
                    self.highlightCurve();
                    self.context.hover = null;
                },
                theme: 'd3-context-menu-theme with-backdrop',
                position: function position() {
                    //var size = document.querySelector('.d3-context.menu').getBoundingClientRect();
                    var elm = this;
                    var bounds = elm.getBoundingClientRect();
                    // eg. align bottom-left
                    return {
                        top: bounds.top + bounds.height - 20,
                        left: bounds.left
                    };
                }
            }).bind(elem)();

            var contextmenu_width = document.querySelector('.d3-context-menu > ul').getBoundingClientRect().width,
                mouse_coords = d3.mouse(document.body);
            d3.select('.d3-context-menu').attr('style', '').on('click', function () {
                return d3ContextMenu('close');
            }) // manage clickoutside
            .select('ul').style('left', mouse_coords[0] - contextmenu_width / 2 + 'px').style('top', mouse_coords[1] - 20 + 'px');
            self.highlightCurve(tsuid);
            setTimeout(function () {
                self.highlightCurve(tsuid);
            }, 50);
        }

        /**
         * Refresh inner graph
         * @memberof CurveWithLegend
         */

    }, {
        key: 'refreshGraph',
        value: function refreshGraph() {
            var self = this;
            self.graph.update(self.data.filter(function (d) {
                return d.selected;
            }));
            self.graph.render();
        }

        /**
         * Render the legend (bullets)
         * @memberof CurveWithLegend
         */

    }, {
        key: 'renderLegend',
        value: function renderLegend() {
            var self = this;
            var tsLegendFieldData = self.legends.selectAll('.ts-legend-field').data(self.data, function (ts) {
                return ts.tsuid;
            });

            var tsLengendFieldDataEnter = tsLegendFieldData.enter().append('span').attr('class', 'ts-legend-field').attr('title', function (d) {
                return d.key + ' [' + d.variable + ']';
            }).on('contextmenu', function () {
                self._renderContextMenu(self.context.hover, this);
            }); // attach menu to element

            tsLengendFieldDataEnter.append('span').attr('draggable', true).attr('class', 'color-legend').style('background-color', function (d) {
                return d.selected ? d.color : 'none';
            });
            var visibilityLegendEnter = tsLengendFieldDataEnter.append('span').attr('class', 'visibility-legend').classed('selected', function (d) {
                return d.selected;
            }).on('click', function (d) {
                d.selected = !d.selected;
                d3.select(this).classed('selected', d.selected).select('.color-legend').style('background-color', function (d) {
                    return d.selected ? d.color : 'transparent';
                });
                self.refreshGraph();
                self.graph.highlightCurve(d.selected ? d.tsuid : null);
            });
            visibilityLegendEnter.append('i').attr('class', 'glyphicon glyphicon-eye-close');
            visibilityLegendEnter.append('i').attr('class', 'glyphicon glyphicon-eye-open');
            tsLengendFieldDataEnter.append('span').attr('class', 'text-legend').text(function (d) {
                return (d.label ? d.label : d.key) + ' [' + d.variable + ']';
            });
            tsLegendFieldData.exit().remove();
            self.controls.selectAll('.ts-legend-field').on('mouseenter', function (d) {
                self.context.hover = d.tsuid;
                self.graph.highlightCurve(self.context.hover);
            }).on('mouseleave', function () {
                var size = this.getBoundingClientRect(),
                    coords = d3.mouse(this);
                //FIXME: understand why mouseout at wrong time
                // if mouse coords are still in the element, dont remove highlight
                if (coords[0] < 0 || coords[0] > size.width || coords[1] > size.height || coords[1] < 0) {
                    self.context.hover = null;
                    self.graph.highlightCurve();
                }
            });
            self.controls.selectAll('.color-legend').style('background-color', function (d) {
                return d.selected ? d.color : 'none';
            }).on('click', function (ts) {
                self._renderSettings(ts);
            });
            self.controls.selectAll('.visibility-legend').classed('selected', function (d) {
                return d.selected;
            });
        }

        /**
         * Highlight specific curve
         * @param  {String} tsuid * ts which will be highlighted
         * @return {CurveWithLegend} current object
         * @memberof CurveWithLegend
         */

    }, {
        key: 'highlightCurve',
        value: function highlightCurve(tsuid) {
            this.legends.selectAll('.ts-legend-field').classed('highlighted', function (d) {
                return d.tsuid === tsuid;
            });
            return this.graph.highlightCurve(tsuid);
        }

        /**
         * Function allowing to set data
         * @param  {Object} data
         * @memberof CurveWithLegend
         */

    }, {
        key: 'update',
        value: function update(data, metadata) {
            var self = this;
            if (data) {
                self.data = data;
            }
            if (metadata) {
                self.md = metadata;
            }
            var colors = d3.scaleOrdinal(d3.schemeCategory10);
            self.data.forEach(function (current, i) {
                if (current.selected === null || typeof current.selected === 'undefined') {
                    current.selected = true;
                }
                current.color = current.color || colors(i);
            });
            if (self.graph) {
                self.graph.update(data, metadata);
            }
        }
    }]);

    return CurveWithLegend;
}(LIGViz);

module.exports = CurveWithLegend;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js","/home/davisp/projects/ikats_lig/hmi/node_modules/spectrum-colorpicker/spectrum.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/spectrum-colorpicker/spectrum.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithPatternLegend.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithPatternLegend.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/colorpicker.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/colorpicker.less","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/ts-legend.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/ts-legend.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithPatternLegend.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

//less/style integration

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/pattern-legend.less");

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

var LIGViz = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js"),
    CanvasCurve = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CanvasCurve.js"),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js");

/**
 * Visualization showing ts with a legend
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 * @param {Array}  callbacks - list of the callbacks used by Viz
 */

var CurveWithPatternLegend = function (_LIGViz) {
    _inherits(CurveWithPatternLegend, _LIGViz);

    function CurveWithPatternLegend(container, inputs, callbacks) {
        _classCallCheck(this, CurveWithPatternLegend);

        var _this = _possibleConstructorReturn(this, (CurveWithPatternLegend.__proto__ || Object.getPrototypeOf(CurveWithPatternLegend)).call(this, container, inputs.raw || inputs, callbacks));
        // Call super-class constructor


        _this.name = 'CurveWithPatternLegend';
        _this.dispatch = d3.dispatch('zoomed', 'change', 'syncChange', 'removed', 'reset', 'tsRemoved', 'pointmouseenter', 'pointmouseleave');
        _this.containerId = container;
        _this.container = d3.select('#' + container);
        _this.context = {};
        _this.logger = new LigLogger('lig-pattern');
        _this._options = {
            margin: {
                left: 0,
                right: 0,
                top: 5,
                bottom: 0
            },
            proportion: {
                graph: 95,
                legend: 5
            },
            height: 500,
            width: 1200
        };
        _this.patterns = [];
        _this.obj = {
            activatedPatterns: {},
            patterns: []
        };
        _this.graph = new CanvasCurve(_this.containerId + '-patternCurve', _this.data || []);

        return _this;
    }

    /**
     * Function to get/set activated patterns related to the graph
     * @param  {Object} _ activatedPatterns
     * @return {ProtoWithPatternLegend}
     */


    _createClass(CurveWithPatternLegend, [{
        key: 'activatedPatterns',
        value: function activatedPatterns(_) {
            if (!arguments.length) {
                return this.obj.activatedPatterns;
            }
            this.obj.activatedPatterns = _;
            return this;
        }

        /**
         * Function allowing to display viz
         */

    }, {
        key: 'display',
        value: function display() {
            this.update();
            return this.render();
        }

        /**
         * Highlight specific curve
         * @param  {String} tsuid * ts which will be highlighted
         * @return {ProtoWithPatternLegend} current object
         */

    }, {
        key: 'highlightCurve',
        value: function highlightCurve(tsuid) {
            return this.graph.highlightCurve(tsuid);
        }

        /**
         * Get the node container
         * @return {HTMLElement} element
         */

    }, {
        key: 'node',
        value: function node() {
            return this.container.node();
        }

        /**
         * Able to manage events from dispatch feature
         * @param  {String} eventName
         * @param  {function} handler
         * @return {ProtoWithPatternLegend}
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            var self = this;
            self.dispatch.on(eventName, handler);
            return self;
        }

        /**
         * Get or set current options
         * @param  {Object} _ * new options if it's given
         * @return {ProtoWithPatternLegend} current object
         */

    }, {
        key: 'options',
        value: function options(_) {
            if (!arguments.length) {
                return this._options;
            }
            Object.assign(this._options, _);
            this.graph.options(_);
            return this;
        }

        /**
         * Apply new scale object (d3)
         * @param  {[type]}  scale      - related scale object
         * @param  {Boolean} dispatchZoom - tricky option to able to dispatchZoom
         */

    }, {
        key: 'scale',
        value: function scale(_scale) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            this.graph.scale(_scale, dispatchZoom);
        }

        /**
         * Apply new transform object (d3)
         * @param  {Object} transform   - related transform object
         * @param  {Boolean} dispatchZoom - tricky option to able to dispatchZoom
         */

    }, {
        key: 'transform',
        value: function transform(_transform) {
            var dispatchZoom = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            this._options.transform = _transform;
            this.graph.transform(_transform, dispatchZoom);
        }

        /**
         * Function to render the viz
         */

    }, {
        key: 'render',
        value: function render() {
            var self = this;
            self.logger.log('render graph', self.containerId);
            var height = self._options.height - self._options.margin.top - self._options.margin.bottom;
            var graphWidth = Math.floor(self._options.width * self._options.proportion.graph / 100),
                legendWidth = Math.floor(self._options.width * self._options.proportion.legend / 100);

            // reset content
            self.container = d3.select('#' + self.containerId).html('');
            var subContainer = self.container.append('div').attr('class', 'graph-pattern-legend');

            self.controls = subContainer.append('div').classed('pattern-legend', true).style('margin-top', self._options.margin.top + 'px').style('height', height - 10 + 'px').style('width', legendWidth + 'px');

            subContainer.append('div').classed('graph-inner', true).style('margin-top', self._options.margin.top + 'px').attr('id', self.containerId + '-patternCurve').style('height', height - 10 + 'px').style('width', graphWidth + 'px');

            self.graph.dispatch.on('zoomed', function () {
                self._options.transform = d3.event.transform;
                self.dispatch.call('zoomed', d3.event.transform, self.containerId);
            }).on('pointmouseenter', function (point) {
                self.dispatch.call('pointmouseenter', self, point);
            }).on('pointmouseleave', function (point) {
                self.dispatch.call('pointmouseleave', self, point);
            });

            self.graph.options({ width: graphWidth, height: height, transform: self._options.transform });
            self.updateGraphPatterns();
            self.graph.display();
            self.renderPatternLegend();
        }

        /**
         * render pattern legends
         */

    }, {
        key: 'renderPatternLegend',
        value: function renderPatternLegend() {
            var self = this;
            var entries = d3.entries(self.patterns);
            var patternField = self.controls.selectAll('span').data(entries, function (d) {
                return d.key;
            });
            patternField.enter().append('span').text(function (d) {
                return d.key;
            }).style('background-color', function (d) {
                return d.value.color;
            }).style('cursor', 'pointer').style('opacity', function (d) {
                return self.obj.activatedPatterns[d.key] ? 1 : 0.5;
            }).attr('title', function (d) {
                return d.key + ' ( ' + d.value.nb + ' )';
            }).on('click', function (d) {
                self.obj.activatedPatterns[d.key] = !self.obj.activatedPatterns[d.key];
                self.updateGraphPatterns();
                self.dispatch.call('change', self);

                self.graph.render();
                self.renderPatternLegend();
            }).on('mouseenter', function (d) {
                if (self.obj.activatedPatterns[d.key]) {
                    self.updateGraphPatterns(d.key, true);
                    self.graph.render();
                }
            }).on('mouseout', function () {
                self.updateGraphPatterns();
                self.graph.render();
            }).style('opacity', function (d) {
                return self.obj.activatedPatterns[d.key] ? 1 : 0.5;
            });
            patternField.transition().style('opacity', function (d) {
                return self.obj.activatedPatterns[d.key] ? 1 : 0.5;
            });
            patternField.exit().remove();
        }

        /**
         * Update graph with current patterns
         * @param  {String} patternIdToActivate - param allowing to activate a specific pattern
         */

    }, {
        key: 'updateGraphPatterns',
        value: function updateGraphPatterns(patternIdToActivate) {
            var filterAlsoTS = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var self = this;
            self.data.forEach(function (d) {
                (d.patterns || []).forEach(function (p) {
                    p.activated = patternIdToActivate ? p.regex === patternIdToActivate : self.obj.activatedPatterns[p.regex];
                });
            });
            var data = void 0;
            if (filterAlsoTS) {
                self.obj.cacheTs = data = self.graph.data;
            } else if (self.obj.cacheTs) {
                data = self.obj.cacheTs;
                self.obj.cacheTs = null;
            } else {
                data = self.graph.data;
            }
            var newData = self.graph.data.map(function (d, i) {
                var d1 = Object.assign({}, d);
                d1.patterns = (self.data[i].patterns || []).
                /**.filter(p=>(patternIdToActivate) ? (p.regex === patternIdToActivate) : self.obj.activatedPatterns[p.regex])**/
                map(function (p) {
                    if (patternIdToActivate) {
                        p.activated = p.regex === patternIdToActivate;
                    } else {
                        p.activated = self.obj.activatedPatterns[p.regex];
                    }
                    return p;
                });
                return d1;
            });
            self.graph.update(newData);
        }

        /**
         * Function allowing to set data
         * @param  {Array} data -
         */

    }, {
        key: 'update',
        value: function update(data, metadata) {
            var self = this;
            var current = {};
            var patterns = {};
            if (data) {
                self.data = data;
            }
            var colors = d3.scaleOrdinal(d3.schemeCategory10);
            for (var i = 0; i < self.data.length; i++) {
                current = self.data[i];
                current.color = current.color || colors(i);
                current.patterns = current.patterns || [];
                for (var j = 0; j < current.patterns.length; j++) {
                    var p = current.patterns[j];
                    p.activated = self.obj.activatedPatterns[p.regex];
                    patterns[p.regex] = patterns[p.regex] || {
                        nb: 0,
                        key: p.key,
                        regex: p.regex,
                        color: p.color
                    };
                    patterns[p.regex].nb = +patterns[p.regex].nb + p.locations.length;
                }
            }
            self.patterns = patterns;
            d3.keys(self.patterns).forEach(function (d) {
                self.obj.activatedPatterns[d] = self.obj.activatedPatterns[d] || false;
            });
            if (self.graph) {
                self.graph.update(data, metadata);
            }
            return self;
        }
    }]);

    return CurveWithPatternLegend;
}(LIGViz);

module.exports = CurveWithPatternLegend;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CanvasCurve.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CanvasCurve.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/pattern-legend.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/pattern-legend.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Drawer/CanvasCurveDrawer.js":[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    scaleHelper = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js");

/*exported CanvasCurveDrawer */

/*
 * Class CanvasCurveDrawer.
 * Class that manage as the service to draw elements from curve (in canvas mode)
 * @constructor
 */

var CanvasCurveDrawer = function () {
  function CanvasCurveDrawer() {
    _classCallCheck(this, CanvasCurveDrawer);

    this.tmpCanvas = document.createElement('canvas');
    this.debug = false;
  }

  /**
   * Draw the algo breakpoints (horizontal)
   * @param  {Object} ctx       - canvas context
   * @param  {Object} bpsData   - Breakpoints object
   * ex: {
   *       left: true/false,
   *       bps: [],
   *       map: {}
   *     }
   * @param  {Number} width     - in px
   * @param  {Function} yScale  - function to scale y value
   */


  _createClass(CanvasCurveDrawer, [{
    key: 'drawBreakPoints',
    value: function drawBreakPoints(ctx, bpsData, width, yScale) {
      var pixelDiff = Math.abs(yScale(bpsData.bps[0].max) - yScale(bpsData.bps[0].min));

      ctx.beginPath();
      ctx.strokeStyle = 'rgba(0, 0, 0, 0.2)';
      ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
      ctx.lineWidth = 1;
      ctx.font = d3.min([pixelDiff * 2 / 3, 20]) + "px Arial";

      var y = void 0;
      bpsData.bps.forEach(function (bp, i) {
        y = Math.round(yScale(bp.min));
        // draw each horizontal breakpoint
        ctx.moveTo(0, y);
        ctx.lineTo(width, y);
        // if line is not the last one, draw labels
        // if variable is the first one, draw in left else draw in right
        ctx.fillText(bp.key, bpsData.left ? 5 : width - 30, y - pixelDiff / 3);
        // draw last horizontal breakpoint
        if (bpsData.bps.length - 1 === i) {
          y = Math.round(yScale(bp.max));
          ctx.moveTo(0, y);
          ctx.lineTo(width, y);
        }
      });
      ctx.stroke();
    }

    /**
     * Draw single dot
     * @param  {Object} coords
     * @param  {Number} radius
     * @param  {String} color
     * @param  {Object} context
     */

  }, {
    key: 'drawDot',
    value: function drawDot(coords, radius) {
      var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'grey';
      var context = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      context.beginPath();
      context.lineWidth = 1;
      context.strokeStyle = d3.color(color).darker(1.7);
      context.fillStyle = color;
      context.arc(coords.x, coords.y, radius, 0, 2 * Math.PI);
      context.stroke();
      context.fill();
    }

    /**
     * Draw several dots
     * @param  {Array} ds
     * @param  {Object} ctx
     */

  }, {
    key: 'drawDotsLine',
    value: function drawDotsLine() {
      var coords = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var radius = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var ctx = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      var self = this;
      self.tmpCanvas.width = radius * 2;
      self.tmpCanvas.height = radius * 2;
      var ctx2 = self.tmpCanvas.getContext('2d');
      ctx2.fillStyle = color;
      ctx2.arc(radius, radius, radius, 0, 2 * Math.PI);
      ctx2.fill();
      var coordsLength = coords.length;
      for (var i = 0; i < coordsLength; i++) {
        ctx.drawImage(self.tmpCanvas, coords[i].x - radius, coords[i].y - radius);
      }
    }

    /**
     * Draw canvas line
     * @param  {Array}  coords    - coords
     * @param  {Object} ctx       - canvas context
     * @param  {Number} lineWidth - width of line
     * @param  {Object} color     - colorization
     */

  }, {
    key: 'drawLine',
    value: function drawLine(coords, ctx, lineWidth, color) {
      var style = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'line';

      var valueLine = d3.line()
      //.curve(d3.curveCatmullRom)
      .x(function (d) {
        return d.x;
      }).y(function (d) {
        return d.y;
      }).context(ctx);
      if (style && _.isArray(style[1])) {
        ctx.setLineDash(style[1]);
      }
      ctx.lineWidth = lineWidth;
      ctx.strokeStyle = color;
      ctx.beginPath();
      valueLine(coords);
      ctx.stroke();
      ctx.setLineDash([]);
    }

    /**
     * Draw canvas area
     * @param  {Array}  coords    - coords
     * @param  {Object} ctx       - canvas context
     * @param  {Number} height - height of the canvas
     * @param  {Object} color     - colorization
     */

  }, {
    key: 'drawArea',
    value: function drawArea(coords, ctx, height, color) {
      var valueLine = d3.area().x(function (d) {
        return d.x;
      }).y1(function (d) {
        return d.y;
      }).y0(height).context(ctx);
      ctx.fillStyle = color;
      ctx.beginPath();
      valueLine(coords);
      ctx.fill();
    }

    /**
     * Draw variations for a specific point (min, max) (aggregation)
     * @param  {Array}  coords
     * @param  {String} color
     * @param  {Object} ctx
     */

  }, {
    key: 'drawMinMaxLines',
    value: function drawMinMaxLines() {
      var coords = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'rgba(0, 0, 0, 0.4)';
      var ctx = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      ctx.beginPath();
      ctx.lineWidth = 1;
      ctx.strokeStyle = color;

      var node = void 0;
      for (var i = coords.length - 1; i >= 0; i--) {
        node = coords[i];
        if (node.y1 !== node.y2) {
          ctx.moveTo(node.x, node.y1);
          ctx.lineTo(node.x, node.y2);
        }
      }

      ctx.stroke();
    }

    /**
     * Draw mouse cross representing the cursor target
     */

  }, {
    key: 'drawMouseCross',
    value: function drawMouseCross(canvasNode, xScale, yScaleLeft, yScaleRight) {
      var ctx = canvasNode.getContext('2d');
      var width = canvasNode.width;
      var height = canvasNode.height;

      ctx.fillStyle = 'transparent';
      ctx.clearRect(0, 0, width, height);
      try {
        var coords = scaleHelper.getUnscaledMousePosition(canvasNode);

        if (height >= coords[1] && width >= coords[0] && coords[0] >= 0) {

          ctx.beginPath();
          ctx.lineWidth = 1;
          ctx.strokeStyle = 'grey';

          // horizontal
          ctx.moveTo(0, coords[1]);
          ctx.lineTo(width, coords[1]);
          // vertical
          ctx.moveTo(coords[0], 0);
          ctx.lineTo(coords[0], height);

          ctx.stroke();

          // text
          ctx.font = "10px Arial";
          ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';

          // x scale
          var x_value = xScale.invert(coords[0]).toFixed();
          if (!isNaN(x_value)) {
            ctx.fillText(x_value, coords[0] + 2, 10);
            ctx.fillText(x_value, coords[0] + 2, height - 5);
          }

          // first y scale
          var y1_value = yScaleLeft.invert(coords[1]).toPrecision(2);
          if (!isNaN(y1_value)) {
            ctx.fillText(y1_value, 5, coords[1] - 2);
            ctx.fillText(y1_value, width - 3 - y1_value.length * 6, coords[1] - 2);
          }

          if (yScaleRight) {
            // second y scale
            var y2_value = yScaleRight.invert(coords[1]).toPrecision(2);
            if (!isNaN(y2_value)) {
              ctx.fillText(y2_value, width - 30, coords[1] - 2);
            }
          }
          return coords;
        }
      } catch (e) {
        //console.error(e);
        return;
      }
    }

    /**
     * Draw patterns
     * @param  {Array} ds      - timeseries
     * @param  {Object} ctx    - canvas context
     * @param  {Number} height - height
     */

  }, {
    key: 'drawPatterns',
    value: function drawPatterns(ds, ctx, height) {
      var diff = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
      var mode = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'timestamp';

      var pattern, patternsLength, locationsLength, lineWidth, hColor, bColor;
      var tsDone = {};
      ctx.strokeStyle = '#939393FF';

      ds.forEach(function (data) {
        // usecase for paired variable dont show patterns two times
        if (!data.visible || tsDone[data.tsuid.split('#')[0]] && !data.underlined) {
          return;
        } else {
          tsDone[data.tsuid.split('#')[0]] = true;
        }

        patternsLength = (data.patterns || []).length;
        lineWidth = data.underlined ? 2 : 1;

        ctx.lineWidth = lineWidth;
        var len = void 0;

        for (var i = 0; i < patternsLength; i++) {
          pattern = data.patterns[i];
          len = pattern.length;
          // if pattern is not activated, it doesnt have to be drawn
          if (!pattern.activated) {
            continue;
          }

          hColor = d3.color(pattern.color).brighter(1);
          hColor.opacity = 1;
          bColor = d3.color(hColor);
          if (bColor.opacity > 0.5) {
            bColor.opacity = 0.5;
          }

          locationsLength = pattern.locations.length;
          var xc1 = null,
              xc2 = null;
          for (var j = 0; j < locationsLength; j++) {
            var x1 = void 0,
                x2 = void 0;
            var location = pattern.locations[j];
            ctx.beginPath();
            if (mode === 'timestamp') {
              diff = location.duration / len;
              x1 = Math.round(data.xScale(pattern.locations[j].timestamp - diff / 2));
              x2 = Math.round(data.xScale(pattern.locations[j].timestamp + diff * (pattern.length - 1) + diff / 2));
            } else {
              x1 = Math.round(data.xScale(pattern.locations[j].pos) - diff / 2);
              x2 = Math.round(data.xScale(pattern.locations[j].pos) + diff * (pattern.length - 1) + diff / 2);
            }
            /** OBSOLETE
            ctx.fillStyle = (pattern.locations[j+1] && (pattern.locations[j+1].pos - pattern.locations[j].pos) <= pattern.length) ? bColor: hColor;
            ctx.strokeStyle = 'grey';
            //ctx.strokeStyle = 'red';
            ctx.fillRect(x1, 0, (x2 - x1), height);
            // TODO: find better way
            ctx.strokeRect(x1, 0, (x2 - x1), height);
            **/
            if (pattern.locations[j + 1] && pattern.locations[j + 1].pos - pattern.locations[j].pos <= pattern.length) {
              if (xc1 === null) {
                xc1 = +x1;
              }
              xc2 = d3.max([x2, xc2]);
            } else {
              ctx.fillStyle = hColor;
              ctx.fillRect(x1, 0, x2 - x1, height);
              if (xc1 !== null && xc1 !== null) {
                ctx.fillStyle = bColor;
                ctx.fillRect(xc1, 0, x1 - xc1, height);
              }
              xc1 = null;xc2 = null;
              ctx.strokeStyle = 'grey';
              ctx.strokeRect(x1, 0, x2 - x1, height);
            }
          }
          ctx.strokeStyle = '#939393FF';
          ctx.stroke();
        }
      });
    }

    /**
     * Draw the pattern areas representing discretized values.
     * @param  {Array} ds   - Array of timeseries
     * @param  {Object} ctx - Canvas context
     */

  }, {
    key: 'drawPatternAreas',
    value: function drawPatternAreas(ds, ctx) {
      var diff = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var mode = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'timestamp';


      var self = this;
      var variableSeparator = '-';
      //diff = 0;
      //console.time('drawPatternAreas ')

      if (diff < 3 && mode !== 'timestamp') {
        if (self.debug) {
          console.warn('too small to draw pattern areas');
        }
        return;
      }
      // tmp variable
      var ts, pattern, location, variable, variables, square;
      var tsSize, patternSize, locationSize, variablesSize, regexSize;
      // indexs
      var patternIndex, locationIndex, variableIndex, regexIndex;
      //
      var x1, y1, y2;

      ctx.strokeStyle = 'black';
      ctx.lineWidth = 1;
      tsSize = ds.length;
      // loop on each sequence
      for (var i = 0; i < tsSize; i++) {
        // TODO
        /*jshint maxdepth: 6 */

        ctx.beginPath();
        ts = ds[i];
        var minDomain = ts.xScale.domain()[0];
        if (!ts.visible) {
          continue;
        }
        patternSize = (ts.patterns || []).length;

        // loop on each pattern
        for (patternIndex = 0; patternIndex < patternSize; patternIndex++) {
          pattern = ts.patterns[patternIndex];
          variables = pattern.variable.split(variableSeparator);
          variablesSize = variables.length;
          locationSize = pattern.locations.length;

          // if the pattern is not activated, exit
          if (!pattern.activated) {
            continue;
          }
          // loop on splitted variables (in case we have paired variables)
          for (variableIndex = variablesSize - 1; variableIndex >= 0; variableIndex--) {
            variable = variables[variableIndex];
            regexSize = pattern.regexPlain[variableIndex].length;
            // if variable is not same as the one from timeseries, exit
            if (ts.variable !== variable) {
              continue;
            }

            //ctx.fillStyle = (variables.length  0) ? p.color : d3.color(p.color).brighter(0.5);
            // loop on locations
            for (locationIndex = 0; locationIndex < locationSize; locationIndex++) {
              location = pattern.locations[locationIndex];
              // if pattern that we want to draw has another location overlappped, exit
              if (mode === 'timestamp') {
                diff = ts.xScale(minDomain + location.duration / pattern.length);
              }
              if (diff < 3 || pattern.locations[locationIndex + 1] && pattern.locations[locationIndex + 1].pos - location.pos <= pattern.length) {
                continue;
              }

              // if its paired variable and k is different to 0 (not first), show it with brighter color
              var color;
              if (variableIndex === 0) {
                color = d3.color(pattern.color);
              } else {
                color = d3.color(pattern.color).brighter(0.5);
                color.opacity = 0.8;
              }
              var minmaxRegex = ts.bps.bps[0].key + ts.bps.bps[ts.bps.bps.length - 1].key;
              //ctx.fillStyle = color;
              // All conditions are ok, draw each squares from pattern representation
              // TODO : look on regexPlain (a bit tricky)
              for (regexIndex = regexSize - 1; regexIndex >= 0; regexIndex--) {
                var c = d3.color(color);
                square = pattern.regexPlain[variableIndex][regexIndex];
                if (square === minmaxRegex) {
                  c.opacity = 0.4;
                }
                ctx.fillStyle = c;
                // TODO: find better way
                x1 = ts.xScale(mode === 'timestamp' ? location.timestamp : location.pos) + regexIndex * diff - diff / 2;
                y1 = ts.yScale(ts.bps.map[square[0]].min);
                y2 = ts.yScale(square.length > 1 ? ts.bps.map[square[1]].max : ts.bps.map[square[0]].max);
                // draw square
                ctx.fillRect(x1, y2, diff, Math.abs(y2 - y1));

                if (diff > 6 && ts.underlined) {
                  ctx.strokeStyle = 'black';
                  ctx.lineWidth = 1;
                  // draw borders
                  ctx.strokeRect(x1, y2, diff, Math.abs(y2 - y1));
                }
              }
            }
          }
        }
      }
    }

    /**
     * Redraw main function
     * @param  {Array}   ds          timeseries
     * @param  {Object}  ctx         canvas context
     * @param  {Object}  hover       object in hover
     * @param  {Object}  size        width and height
     * @param  {Boolean} quickRedraw option
     */

  }, {
    key: 'redraw',
    value: function redraw() {
      var ds = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var ctx = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var hover = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var size = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      var quickRedraw = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
      var mode = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 'timestamp';

      var self = this;
      // default value
      var _lineWidth = 1,
          _highlightLineWidth = 2;

      if (ds.length === 0) {
        return;
      }
      // debug usecase
      if (self.debug) {
        console.time('redrawCanvas');
      }
      // diff between each point (estimation)
      var diff = 0;
      // we look on data (first ts as sample)
      if (ds[0] && ds[0].coords && ds[0].coords.length > 1) {
        //diff = d3.max([Math.abs(ds[0].coords[0].x - ds[0].coords[1].x), diff]);
        var coord = ds[0].coords[0];
        var coord2 = ds[0].coords[1];
        // TODO: find better way
        var key = mode === 'timestamp' ? 'timestamp' : 'idx';
        diff = Math.round(Math.abs(ds[0].xScale(coord[key]) - ds[0].xScale(coord2[key]))) * 2 / ((coord.size || 1) + (coord2.size || 1));
      }

      var ts = void 0,
          color = void 0,
          lineWidth = void 0,
          nbPoints = 0;
      // loop on timeseries
      for (var i = 0; i < ds.length; i++) {
        /*jshint loopfunc: true */
        ts = ds[i];

        // draw patterns (background)
        // tsuid use to look like on ts01_var1-var2#var2 => it's the sequence of the second variable
        // this flag is used to know if we skip or not background pattern rendering for this ts
        // if it is the second part of a pair variable, we skip
        // TODO: try to simplify it
        var toBeSkipped = false;

        var tsuidSplitted = ts.tsuid.split('#');
        if (tsuidSplitted.length > 1 && ts.visible && !hover) {
          var _variable = tsuidSplitted[1];
          var _items = tsuidSplitted[0].split('-');
          if (_items.indexOf(_variable) === _items.length - 1) {
            toBeSkipped = true;
          }
        }

        if (!toBeSkipped) {
          self.drawPatterns([ts], ctx, size[1], diff, mode);
        }
        // draw patterns areas (squares)
        self.drawPatternAreas([ts], ctx, diff, mode);
      }

      // loop on timeseries
      for (var _i = 0; _i < ds.length; _i++) {
        /*jshint loopfunc: true */
        ts = ds[_i];

        // draw patterns (background)
        // tsuid use to look like on ts01_var1-var2#var2 => it's the sequence of the second variable
        // this flag is used to know if we skip or not background pattern rendering for this ts
        // if it is the second part of a pair variable, we skip
        // TODO: try to simplify it

        // count points (debug)
        nbPoints += ts.coords.length;
        // get color
        color = d3.color(ts.color);
        // set linewidth using default value
        lineWidth = _lineWidth;

        if (hover) {
          if (hover.tsuid === ts.tsuid) {
            lineWidth = _highlightLineWidth;
            var white = d3.color('white');
            white.opacity = 0.5;
            self.drawLine(ts.coords, ctx, lineWidth * 5, white, ts.style);
          }
          //else {
          //color.opacity = 0.4;
          //}
        }

        if (!quickRedraw) {
          // draw variations
          //self.drawMinMaxLines(coords, undefined, ctx);
          // draw dots
          if (hover && hover.tsuid === ts.tsuid) {
            self.drawDotsLine(ts.coords, ts.radius, color, ctx);
          }
        }

        // draw line
        self.drawLine(ts.coords, ctx, lineWidth, color, ts.style);
        //self.drawArea(coords, ctx, size[1], ts.color);
      }
      // draw highlighted dot
      if (!quickRedraw) {
        ds.forEach(function (d) {
          if (d.closest && (!hover || d.variable === hover.variable)) {
            self.drawDot(d.closest, 3, d.color, ctx);
          }
        });
        if (hover) {
          self.drawDot(hover, hover.radius, hover.color, ctx);
        }
      }

      // debug usecase
      if (self.debug) {
        console.log(nbPoints + ' points drawed in canvas');
        console.timeEnd('redrawCanvas');
      }
    }
  }]);

  return CanvasCurveDrawer;
}();

module.exports = CanvasCurveDrawer;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGHeatMap.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null);

var d3ContextMenu = require("/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js"),
    LIGViz = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js"),
    getMaxTextWidth = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/SvgTextWidth.js"),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js");

// less
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/heatmap.less");

/*exported ProtoHeatMap*/

/**
 * A prototype to display heatmap between patterns/ts.
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 * @param {Array} callbacks - the list of the callbacks used by Viz
 */

var LIGHeatMap = function (_LIGViz) {
    _inherits(LIGHeatMap, _LIGViz);

    function LIGHeatMap(containerId, data, callbacks) {
        _classCallCheck(this, LIGHeatMap);

        var _this = _possibleConstructorReturn(this, (LIGHeatMap.__proto__ || Object.getPrototypeOf(LIGHeatMap)).call(this, containerId, data, callbacks));
        // Call super-class constructor


        _this.name = "LIGHeatMap";
        _this.containerId = containerId;
        _this.logger = new LigLogger('lig-heatmap');
        _this.dispatch = d3.dispatch('orderRemoveTs', 'orderAddTs', 'cellEnter', 'cellLeave');
        _this._options = {
            width: 500,
            height: 200,
            autoSize: true,
            duration: 700,
            margin: {
                left: 200,
                right: 0,
                top: 0,
                bottom: 200
            },
            rect: {
                width: 35,
                height: 35,
                lightWidth: 12,
                ligthHeight: 5,
                minWidth: 1,
                minHeight: 1,
                lineWidth: 0.5,
                lineHoverWidth: 3
            },
            pourcent: {
                details: 0,
                graph: 100
            },
            minDetailsWidth: 0,
            minimap: {
                height: 20,
                show: true,
                paddingBottom: 10
            },
            colors: d3.scaleOrdinal(d3.schemeCategory10).range()
        };
        _this.dom = {
            svg: null,
            canvasHidden: document.createElement('canvas')
        };
        _this.obj = {
            transform: null,
            ts: [],
            filtered: [],
            patterns: [],
            colorScale: d3.scaleLinear().range(["white", "red"]).domain([0, 100]),
            markedData: {},
            linkedContainers: []
        };
        return _this;
    }

    /**
     * Init the VizTool : collect and format the data (if necessary) then render the VizTool
     */


    _createClass(LIGHeatMap, [{
        key: 'display',
        value: function display() {
            var self = this;
            self.update();

            self.container = d3.select('#' + self.containerId).html('');

            self.obj.graphHeight = $(self.container.node()).height();
            self.obj.graphWidth = $(self.container.node()).width();

            if (self._options.autoSize) {
                self._options.height = self.obj.graphHeight;
                self._options.width = self.obj.graphWidth;
            }

            var subContainer = self.container.append('div').attr('class', 'heatmap').style('height', self._options.height + 'px').style('width', self._options.width + 'px').append('div').attr('class', 'heatmap-subcontainer');

            var graphContainer = subContainer.append('div').attr('class', 'heatmap-graph').on('mousemove', function () {
                // if ctrlkey or shiftkey is pressed, switch in brush mode
                d3.select(this).classed('in-brushing', d3.event.ctrlKey || d3.event.shiftKey);
            });

            subContainer.append('div').attr('class', 'heatmap-details');

            self.dom.svg = graphContainer.append('svg').attr('class', 'svg').append('g');

            self.dom.canvas = graphContainer.append('canvas').attr('class', 'canvas').on('mousemove', function () {
                var d3ContextMenuElem = d3.select('.d3-context-menu');
                if (d3ContextMenuElem.node() && d3ContextMenuElem.style('display') === 'block') {
                    return;
                }
                var hoverBefore = self.obj.hover;
                self.detectHover();
                if (self.obj.hover && (!hoverBefore || hoverBefore.ts !== self.obj.hover.ts || hoverBefore.pattern.key !== self.obj.hover.pattern.key)) {
                    self.renderDetails();
                    var fn = function fn() {
                        // refresh canvas
                        window.requestAnimationFrame(function () {
                            self.renderCanvas();
                        });
                        // update vertical 'class' highlight
                        self.dom.yaxis3.selectAll('text').classed('selected', function (d) {
                            return self.obj.hover && self.obj.hover.pattern && self.obj.hover.pattern.variable === d.variable && self.obj.hover.pattern.class === d.key;
                        });
                        // update vertical 'variable' highlight
                        self.dom.yaxis2.selectAll('text').classed('selected', function (d) {
                            return self.obj.hover && self.obj.hover.pattern && self.obj.hover.pattern.variable === d.key;
                        });
                        // update vertical 'pattern name' highlight
                        self.dom.yaxis.selectAll('text').classed('selected', function (d) {
                            return self.obj.hover && self.obj.hover.pattern && self.obj.hover.pattern.regex === d;
                        });
                        // update horizontal 'observation' highlight
                        self.dom.xaxis.selectAll('text').classed('selected', function (d) {
                            return self.obj.hover && self.obj.hover.ts === d;
                        });
                        // update horizontal 'class' highlight
                        self.dom.xaxis2.selectAll('text').classed('selected', function (d) {
                            return self.obj.hover && self.obj.hover.class === d.key;
                        });
                    };
                    clearTimeout(self.obj.drawCanvasAsync);
                    // ask points with a delay ( in order to be able to abort it)
                    self.obj.drawCanvasAsync = setTimeout(fn, 0);
                }
            }).on('mouseout', function () {
                var d3ContextMenuElem = d3.select('.d3-context-menu');
                if (!d3ContextMenuElem.node() || d3ContextMenuElem.style('display') !== 'block') {
                    self.obj.hover = null;
                    // make empty details container
                    self.renderDetails();
                    // reset all highlight legend
                    self.container.selectAll('.selected').classed('selected', false);
                    // refresh canvas
                    self.renderCanvas();
                }
            });

            self.dom.canvasBackground = graphContainer.append('canvas').attr('class', 'canvas-background canvas');

            // vertical axis to list patterns
            self.dom.yaxis = self.dom.svg.append("g").attr('class', 'yaxis');
            // vertical axis to list variables
            self.dom.yaxis2 = self.dom.svg.append("g").attr('class', 'yaxis2');
            // vertical axis to list class
            self.dom.yaxis3 = self.dom.svg.append("g").attr('class', 'yaxis3');
            // horizontal axis to list observations
            self.dom.xaxis = self.dom.svg.append("g").attr('class', 'xaxis');
            // horizontal axis to list classes
            self.dom.xaxis2 = self.dom.svg.append("g").attr('class', 'xaxis2');

            self.dom.brush = self.dom.svg.append('g').attr('class', 'brush');

            if (self._options.minimap.show) {
                self.dom.minimap = self.dom.svg.append('g').attr('class', 'minimap').attr('transform', 'translate(0, ' + (self._options.height - self._options.minimap.height) + ')');
            }
            self.render();
            // call zoom with specified transform
            self.dom.canvas.call(self.obj.zoom.transform, self.obj.transform);
        }

        /**
         * Get or set current linked containers
         * @param  {Object} _ * linked containers if it's given
         *    format : [{idx: 0, ts: []}, {idx: 1, ts: []}]
         * @return {ProtoHeatMap} current object
         */

    }, {
        key: 'linkedContainers',
        value: function linkedContainers(_) {
            if (!arguments.length) {
                return this.obj.linkedContainers;
            }
            this.obj.linkedContainers = _;
            return this;
        }

        /**
         * Get or set current markedData
         * @param  {Object} _ * markedData if it's given
         * format :
         * {
         *    'ts1': {
         *     'pattern1': 1, // occurences
         *     'pattern2': 2
         *     }
         *  }
         * @return {ProtoHeatMap} current object
         */

    }, {
        key: 'markedData',
        value: function markedData(_) {
            if (!arguments.length) {
                return this.obj.markedData;
            }
            this.obj.markedData = _;
            return this;
        }

        /**
         * Alias to register handler for specific event
         * @param  {String} eventName [description]
         * @param  {Function} handler   [description]
         * @return {ProtoHeatMap}           [description]
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            this.dispatch.on(eventName, handler);
            return this;
        }
        /**
         * Detect and set hover item
         */

    }, {
        key: 'detectHover',
        value: function detectHover() {
            var self = this;
            var coords = d3.mouse(self.dom.canvas.node());
            var x = Math.floor(coords[0] / self.obj.xScale.step()),
                y = Math.floor(coords[1] / self.obj.yScale.step());

            var sequence = self.obj.tsFiltered[x],
                observation = self.obj.filtered[x],
                pattern = self.obj.patterns[y];

            //self.logger.log('detectHover', sequence, observation, pattern);

            if (!sequence || !observation || !pattern) {
                console.log(sequence, observation, pattern, self.obj.tsFiltered);
                self.obj.hover = null;
            } else if (!self.obj.hover || self.obj.hover.key !== observation.key || self.obj.hover.pattern.regex !== pattern.regex) {
                self.obj.hover = {
                    coords: coords,
                    x: x,
                    y: y,
                    variable: pattern.variable,
                    class: observation.class,
                    key: observation.key,
                    pattern: pattern,
                    ts: sequence,
                    tsuids: observation.ts.filter(function (t) {
                        return t.variable === pattern.variable && t.key === observation.key;
                    }).map(function (t) {
                        return t.tsuid;
                    }),
                    label: observation.label
                };
            }

            return self.obj.hover;
        }

        /**
         * Get or set current options
         * @param {any} _ 
         * @returns {ProtoHeatMap} current object
         * @memberof ProtoHeatMap
         */

    }, {
        key: 'options',
        value: function options(_) {
            if (!arguments.length) {
                return this._options;
            }
            this.options = _;
            return this;
        }
    }, {
        key: 'resetHiddenCanvas',
        value: function resetHiddenCanvas() {
            this.dom.canvasHidden.width = 0;
        }

        /**
         * Render/Refresh the viztool
        **/

    }, {
        key: 'render',
        value: function render() {
            var self = this;
            /**
             * Get current hover timeseries
             * @return {Array} - One or two time series (depending on the variable)
             */
            var getHoverTs = function getHoverTs() {
                var idx = void 0;
                if (self.obj.hover) {
                    var result = [];
                    idx = self.data.ts.findIndex(function (t) {
                        return t.key === self.obj.hover.ts && t.variable === self.obj.hover.pattern.variable;
                    });
                    var ts = self.data.ts[idx];
                    if (ts) {
                        var variables = ts.variable.split('-');
                        // in case its pair variables
                        if (variables.length > 1) {
                            variables.forEach(function (variable) {
                                idx = self.data.ts.findIndex(function (t) {
                                    return t.key === ts.key && t.variable === variable;
                                });
                                var tmp = _.clone(self.data.ts[idx]);
                                if (tmp) {
                                    tmp.tsuid = ts.tsuid + '#' + variable;
                                    tmp.patterns = ts.patterns;
                                    result.push(tmp);
                                }
                            });
                        }
                        // in case its simple variable
                        else {
                                result = [ts];
                            }
                        return result;
                    }
                }
            };

            /**
             * @return  {}
             */
            var getContextMenu = function getContextMenu() {
                var result = getHoverTs() || [];
                var tsuids = result.map(function (d) {
                    return d.tsuid;
                });
                // Context Menu
                var contextmenu = [{
                    title: 'Add in a new container</i>',
                    action: function action() {
                        self.resetHiddenCanvas();
                        self.dispatch.call('orderAddTs', self, result, self.data.md);
                    }
                }];

                (self.obj.linkedContainers || []).forEach(function (c, i) {
                    var inside = (c.ts || []).findIndex(function (t) {
                        return tsuids.indexOf(t) > -1;
                    }) > -1;
                    if (!inside) {
                        contextmenu.push({
                            title: 'Add in container ' + i,
                            action: function action() {
                                self.resetHiddenCanvas();
                                self.dispatch.call('orderAddTs', self, result, self.data.md, i);
                            }
                        });
                    } else {
                        contextmenu.push({
                            title: 'Remove from container ' + i,
                            action: function action() {
                                self.resetHiddenCanvas();
                                self.dispatch.call('orderRemoveTs', self, result, self.data.md, i);
                            }
                        });
                    }
                });
                return contextmenu;
            };

            var brushedFromGraph = function brushedFromGraph() {
                if (d3.event.sourceEvent && d3.event.sourceEvent.type === "mouseup") {
                    var s = d3.event.selection;
                    if (!s || isNaN(s[0] || isNaN(s[1]))) {
                        s = self.obj.xScale.range();
                    }
                    var bandWidth = self.obj.xScale.step(),
                        xDomain = self.obj.xScale.domain();
                    // compute min max domain
                    var minmax = [Math.floor(s[0] / bandWidth), Math.ceil(s[1] / bandWidth)].map(function (d) {
                        return xDomain[d];
                    }).map(self.obj.xScale2);

                    // if one of minmax is undefined, use extrem values
                    minmax[0] = minmax[0] || self.obj.xScale2.range()[0];
                    minmax[1] = minmax[1] || self.obj.xScale2.range()[1];

                    // compute new transform object
                    self.obj.transform = d3.zoomIdentity.scale((self.obj.xScale2.range()[1] - self.obj.xScale2.range()[0]) / (minmax[1] - minmax[0])).translate(-minmax[0], 0);
                    // call zoom
                    self.dom.canvas.transition().duration(self._options.duration).call(self.obj.zoom.transform, self.obj.transform);
                    // reset brush
                    self.dom.brush.call(self.brushGraph.move, null);
                }
            };

            // PREPARE data
            self.obj.ts = self.obj.tsFiltered = self.obj.observations.map(function (d) {
                return d.key;
            });

            // Redefine margins in looking on text displayed
            var regexAreaWidth = getMaxTextWidth(self.dom.svg, _.uniq(self.obj.patterns.map(function (d) {
                return d.key;
            })), { 'font-size': 10 }) + 30,
                variableAreaWidth = getMaxTextWidth(self.dom.svg, _.uniq(self.obj.patterns.map(function (d) {
                return d.variable + 'e';
            }))) + 7,
                classAreaWidth = getMaxTextWidth(self.dom.svg, _.uniq(self.obj.patterns.map(function (d) {
                return d.class;
            }))) + 5;

            self._options.margin.left = regexAreaWidth + variableAreaWidth + classAreaWidth;

            // transform DOM to follow new dimension
            self.dom.svg.attr('transform', 'translate(' + self._options.margin.left + ', ' + self._options.margin.top + ')');

            var minimapHeight = self._options.minimap.show ? self._options.minimap.height : 0,
                detailsAreaWidth = d3.max([self._options.minDetailsWidth, self._options.width / 100 * self._options.pourcent.details]);

            // detect rect width
            var heatMapAreaWidth = self._options.width - detailsAreaWidth - self._options.margin.left - 10;
            self._options.rect.width = heatMapAreaWidth / self.obj.ts.length;
            var observationAreaWidth = getMaxTextWidth(self.dom.svg, self.obj.observations.map(function (t) {
                return t.key;
            }));
            self._options.margin.bottom = observationAreaWidth + 30;

            var height = self._options.height - self._options.margin.bottom - minimapHeight;

            // detect rect height
            var potentialRectHeight = Math.floor(height / self.obj.patterns.length);
            self._options.rect.height = potentialRectHeight < self._options.rect.minHeight ? self._options.rect.minHeight : potentialRectHeight;

            var graphDimensions = [self._options.rect.width * self.obj.ts.length, self._options.rect.height * self.obj.patterns.length];
            // take the min between the maximum possible width and the theorical width
            var graphAreaWidth = Math.min(self._options.width - detailsAreaWidth - 10, graphDimensions[0] + self._options.margin.left);

            self.brushMinimap = d3.brushX().extent([[0, 0], [graphDimensions[0], minimapHeight - self._options.minimap.paddingBottom]]).on("brush", function () {
                if (d3.event.sourceEvent && d3.event.sourceEvent.type === "mousemove") {
                    var s = d3.event.selection;
                    if (!s || isNaN(s[0] || isNaN(s[1]))) {
                        s = self.scales.x.range();
                    }
                    // compute new transform object
                    self.obj.transform = d3.zoomIdentity.scale((self.obj.xScale.range()[1] - self.obj.xScale.range()[0]) / (s[1] - s[0])).translate(-s[0], 0);
                    self.dom.canvas.call(self.obj.zoom.transform, self.obj.transform);
                }
            }).on("end", function () {
                if (d3.event.selection === null) {
                    // keep same scale but move the bar
                    var coords = d3.mouse(self.dom.minimap.node()),
                        position_x = d3.min([coords[0], graphAreaWidth - graphAreaWidth / self.obj.transform.k]);
                    // compute new transform object
                    self.obj.transform = d3.zoomIdentity.scale(self.obj.transform.k).translate(-position_x, 0);
                    // call zoom
                    self.dom.canvas.call(self.obj.zoom.transform, self.obj.transform);
                }
            });

            self.brushGraph = d3.brushX().extent([[0, 0], [graphDimensions[0], graphDimensions[1]]]).on("end", brushedFromGraph).filter(function () {
                return d3.event.ctrlKey || d3.event.shiftKey;
            });

            // prepare canvas (background + main)
            self.container.selectAll('canvas.canvas').attr('height', graphDimensions[1]).attr('width', graphDimensions[0]).style('top', self._options.margin.top + 'px').style('left', self._options.margin.left + 'px').on('contextmenu', d3ContextMenu(getContextMenu, {}, self.container)); // context menu

            var zoomed = function zoomed() {
                self.obj.transform = d3.event.transform;
                self.render();
            };

            self.obj.zoom = d3.zoom().duration(self._options.duration).scaleExtent([1, self.obj.ts.length]).on("zoom", zoomed).translateExtent([[0, 0], graphDimensions]).extent([[0, 0], graphDimensions]).filter(function () {
                return !d3.event.ctrlKey && !d3.event.shiftKey;
            });

            self.container.select('.heatmap-graph').style('min-height', self._options.height + 'px').style('width', graphAreaWidth + 'px');

            self.container.select('.heatmap-details').classed('hidden', self._options.pourcent.details === 0).style('min-height', self._options.margin.top + self._options.margin.bottom + graphDimensions[1] + minimapHeight - 5 + 'px').style('width', detailsAreaWidth + 'px');

            // redraw AXIS
            var yScale = self.obj.yScale = d3.scaleBand().domain(self.obj.patterns.map(function (d) {
                return d.regex;
            })).range([0, graphDimensions[1]]);

            // redraw AXIS
            var yScale2 = d3.scaleLinear().domain([0, 100]).range([0, graphDimensions[1]]);

            var xScale2 = d3.scaleLinear().domain([0, 100]).range([0, graphDimensions[0]]);

            var yAxis = d3.axisLeft(yScale);

            self.dom.yaxis.call(yAxis).selectAll('text').classed('selected', function (d) {
                return self.obj.hover && self.obj.hover.pattern.key === d;
            });

            self.obj.xScale = d3.scaleBand().domain(self.obj.observations.map(function (t) {
                return t.key;
            })).range([0, graphDimensions[0]]);

            self.obj.xScale2 = d3.scaleBand().domain(self.obj.observations.map(function (t) {
                return t.key;
            })).range([0, graphDimensions[0]]);

            var range = self.obj.xScale2.range();
            var tsClasses = self.obj.tsClasses;
            if (self.obj.transform) {
                // for each "tick" in our domain is it in selection
                var filteredDomain = [];
                range = self.obj.xScale2.range().map(self.obj.transform.invertX, self.obj.transform);
                self.obj.xScale2.domain().forEach(function (d) {
                    var pos = self.obj.xScale2(d) + self.obj.xScale2.bandwidth() / 2;
                    if (pos > range[0] && pos < range[1]) {
                        filteredDomain.push(d);
                    }
                });
                // set new domain
                self.obj.xScale.domain(filteredDomain);

                var idx = self.obj.xScale2.domain().indexOf(filteredDomain[0]),
                    endIdx = idx + filteredDomain.length;

                self.obj.filtered = self.obj.observations.slice(idx, endIdx);
                self.obj.tsFiltered = self.obj.ts.slice(idx, idx + endIdx);

                // PREPARE ts classes that we will use as horizontal legend
                var total = 0;
                tsClasses = _.uniq(self.obj.filtered.map(function (o) {
                    return o.class;
                })).map(function (classKey) {
                    var observations_tmp = self.obj.filtered.filter(function (d) {
                        return d.class === classKey;
                    });
                    var classs = {
                        key: classKey,
                        pourcent: observations_tmp.length * 100 / self.obj.filtered.length,
                        start: total
                    };
                    total += classs.pourcent;
                    return classs;
                });
            } else {
                self.obj.filtered = self.obj.observations;
            }

            var xAxis = d3.axisBottom(self.obj.xScale);

            if (self.obj.xScale.step() > self._options.rect.lightWidth) {
                self.dom.xaxis.attr('transform', 'translate(0, ' + graphDimensions[1] + ')').call(xAxis).selectAll('.tick').classed('hidden', false).selectAll('text').classed('selected', function (d) {
                    return self.obj.hover && self.obj.hover.ts === d;
                }).attr('y', 0).attr('x', 9).attr('dy', '.35em').attr('transform', 'rotate(90)').style('text-anchor', 'start');
            } else {
                self.dom.xaxis.selectAll('.tick').classed('hidden', true);
            }

            var tsClassPartsData = self.dom.xaxis2.attr('transform', 'translate(0, ' + (graphDimensions[1] + observationAreaWidth + 20) + ')').attr('text-anchor', 'middle').selectAll('.ts-class-part').data(tsClasses, function (d) {
                return d.key + d.pourcent + d.start;
            });
            // remove old legends for ts
            tsClassPartsData.exit().remove();
            // add class legend for ts
            var tsClassParts = tsClassPartsData.enter().append('g').attr('class', 'ts-class-part part').attr('transform', function (d) {
                return 'translate(' + xScale2(d.start) + ', 0)';
            });

            tsClassParts.each(function (d, i) {
                if (i > 0) {
                    d3.select(this).append('line').attr('x1', 0).attr('x2', 0).attr('y1', -observationAreaWidth - 20).attr('y2', 10);
                }
            });
            tsClassParts.append('text').attr('class', 'class-label').text(function (d) {
                return d.key;
            }).attr('x', 5).attr('transform', function (d) {
                return 'translate(' + xScale2(d.pourcent) / 2 + ', 0)';
            });

            var classParts = self.dom.yaxis3.attr('transform', 'translate(-' + regexAreaWidth + ', 0)').attr('text-anchor', 'end').selectAll('.part').data(self.obj.classes).enter().append('g').attr('class', 'class-part').attr('transform', function (d) {
                return 'translate(0, ' + yScale2(d.start) + ')';
            });

            classParts.each(function (d, i) {
                if (i > 0 && self.obj.classes[i - 1] && self.obj.classes[i - 1].variable === d.variable) {
                    d3.select(this).append('line').attr('x1', -classAreaWidth + 6).attr('x2', regexAreaWidth).attr('y1', 0).attr('y2', 0);
                }
            });

            classParts.append('text').text(function (d) {
                return d.key;
            }).attr('x', 5).attr('transform', function (d) {
                return 'translate(0, ' + yScale2(d.pourcent) / 2 + ')';
            });

            var parts = self.dom.yaxis2.attr('transform', 'translate(-' + (regexAreaWidth + classAreaWidth) + ', 0)').attr('text-anchor', 'end').selectAll('.part').data(self.obj.variables).enter().append('g').attr('class', 'part').attr('transform', function (d) {
                return 'translate(0, ' + yScale2(d.start) + ')';
            });

            parts.each(function (d, i) {
                if (i > 0) {
                    d3.select(this).append('line').attr('x1', 0).attr('x2', regexAreaWidth + classAreaWidth).attr('stroke', 'black').attr('y1', 0).attr('y2', 0);
                }
            });

            parts.append('rect').attr('width', 6).attr('height', function (d) {
                return yScale2(d.pourcent) - 1;
            }).attr('fill', function (d) {
                return self._options.colors[self.obj.variables.findIndex(function (v) {
                    return v.key === d.key;
                })];
            });

            parts.append('text').text(function (d) {
                return d.key;
            }).attr('x', -5).attr('transform', function (d) {
                return 'translate(0, ' + yScale2(d.pourcent) / 2 + ')';
            });

            self.dom.yaxis2.selectAll('text').classed('selected', function (d) {
                return self.obj.hover && self.obj.hover.pattern.variable === d.key;
            });

            // DRAW HEATMAP
            self.renderBackground();
            self.renderCanvas();

            // enable brush feature
            self.dom.brush.call(self.brushGraph);
            // enable zoom feature
            self.dom.canvas.call(self.obj.zoom);

            // enable minimap
            if (self._options.minimap.show) {
                self.dom.minimap.call(self.brushMinimap);
                self.dom.minimap.call(self.brushMinimap.move, range);
                self.dom.minimap.attr('transform', 'translate(0, ' + (self._options.height - self._options.minimap.height) + ')');
            }
        }

        /**
         * render the background of the heatmap :
         *  it represents the main grid, shouldnt change
         *
         * @memberof ProtoHeatMap
         **/

    }, {
        key: 'renderBackground',
        value: function renderBackground() {
            var self = this;
            // dimensions
            var rectWidth = self.obj.xScale.step(),
                rectHeight = self._options.rect.height;
            // HTML nodes
            var canvasHiddenNode = self.dom.canvasHidden,
                canvasBackgroundNode = self.dom.canvasBackground.node();
            // context
            var backgroundContext = canvasBackgroundNode.getContext('2d'),
                ctx = canvasHiddenNode.getContext('2d');
            // tmp variables
            var observation = void 0,
                pattern = void 0,
                nbLocations = void 0,
                prevPatternClass = void 0,
                prevPatternVariable = void 0,
                prevObsClass = void 0,
                i = void 0,
                j = void 0;

            var observations = self.obj.filtered;

            // if cache canvas is different than the current background canvas, recreate the cache
            //if (canvasHiddenNode.width !== canvasBackgroundNode.width || canvasHiddenNode.height !== canvasBackgroundNode.height) {
            ctx.beginPath();

            // set the good size
            canvasHiddenNode.width = canvasBackgroundNode.width;
            canvasHiddenNode.height = canvasBackgroundNode.height;

            // prepare the context
            ctx.strokeStyle = 'black';
            ctx.lineWidth = self._options.rect.lineWidth;

            // lines that we will show (class separator)
            var lines = [];
            // uncompliants data where the observation class is not the same as pattern class
            var uncompliants = [];

            // draw rectangles
            for (i = 0; i < observations.length; i++) {
                /*jshint loopfunc: true */
                observation = observations[i];
                for (j = 0; j < self.obj.patterns.length; j++) {
                    pattern = self.obj.patterns[j];
                    nbLocations = (pattern.locations[observation.key] || []).length;
                    ctx.fillStyle = pattern.scale(nbLocations);
                    if (i === 0) {
                        // pattern class separator
                        if (prevPatternClass && prevPatternVariable && (pattern.class !== prevPatternClass || pattern.variable !== prevPatternVariable)) {
                            lines.push({
                                x1: 0,
                                y1: j * rectHeight,
                                x2: self.obj.observations.length * rectWidth,
                                y2: j * rectHeight
                            });
                        }
                        prevPatternClass = pattern.class;
                        prevPatternVariable = pattern.variable;
                    }

                    // background color
                    ctx.fillRect(i * rectWidth, j * rectHeight, rectWidth, rectHeight);

                    if (observation.class + '' !== pattern.class + '' && nbLocations > 0) {
                        uncompliants.push({
                            x: i * rectWidth,
                            y: j * rectHeight
                        });
                    }

                    // borders
                    if (rectWidth > self._options.rect.lightWidth) {
                        ctx.strokeRect(i * rectWidth, j * rectHeight, rectWidth, rectHeight);
                    }
                }
                // observation class separator
                if (prevObsClass && observation.class !== prevObsClass) {
                    lines.push({
                        x1: i * rectWidth,
                        y1: 0,
                        x2: i * rectWidth,
                        y2: self.obj.patterns.length * rectHeight
                    });
                }
                prevObsClass = observation.class;
            }

            // Draw lines
            ctx.beginPath();
            ctx.lineWidth = 2;
            lines.forEach(function (line) {
                ctx.moveTo(line.x1, line.y1);
                ctx.lineTo(line.x2, line.y2);
            });
            ctx.stroke();

            // Draw marks that display error between the pattern class and the observation class
            ctx.beginPath();
            ctx.fillStyle = 'red';
            var widthErrorMarker = d3.min([rectWidth / 2, rectHeight / 2]);
            uncompliants.forEach(function (uncompliant) {
                ctx.moveTo(uncompliant.x, uncompliant.y);
                ctx.lineTo(uncompliant.x + widthErrorMarker, uncompliant.y);
                ctx.lineTo(uncompliant.x, uncompliant.y + widthErrorMarker);
                ctx.fill();
            });
            //}

            backgroundContext.drawImage(canvasHiddenNode, 0, 0);
        }

        /**
         * Draw heatmap additional informations in canvas:
         *      marked patterns, current square in hover
         *
         * @memberof ProtoHeatMap
         **/

    }, {
        key: 'renderCanvas',
        value: function renderCanvas() {
            var self = this;
            var rectWidth = self.obj.xScale.step();
            var rectHeight = self._options.rect.height;

            // prepare context
            var canvas = self.dom.canvas.node();
            var mainCtx = canvas.getContext('2d');
            // reset the canvas
            mainCtx.clearRect(0, 0, canvas.width, canvas.height);

            mainCtx.strokeStyle = 'black';
            mainCtx.lineWidth = 2;
            mainCtx.beginPath();
            mainCtx.setLineDash([5, 5]);

            var pattern = void 0,
                marked = void 0,
                observation = void 0,
                i = void 0,
                j = void 0;
            var observations = self.obj.filtered;
            for (i = 0; i < observations.length; i++) {
                /*jshint loopfunc: true */
                observation = observations[i];
                marked = self.obj.markedData[observation.key];
                if (marked) {
                    for (j = 0; j < self.obj.patterns.length; j++) {
                        pattern = self.obj.patterns[j];
                        if (marked[pattern.regex]) {
                            mainCtx.moveTo((i + 1) * rectWidth, j * rectHeight);
                            mainCtx.lineTo(i * rectWidth, (j + 1) * rectHeight);
                        }
                    }
                }
            }

            mainCtx.stroke();
            mainCtx.setLineDash([]);

            // if there is a square in hover, redraw its borders
            if (self.obj.hover) {
                mainCtx.lineWidth = self._options.rect.lineHoverWidth;
                var obs_idx = self.obj.xScale.domain().indexOf(self.obj.hover.key);
                mainCtx.strokeRect(obs_idx * rectWidth, self.obj.hover.y * rectHeight, rectWidth, rectHeight);
            }
        }

        /**
         * [renderDetails description]
         */

    }, {
        key: 'renderDetails',
        value: function renderDetails() {
            var hover = this.obj.hover,
                self = this,
                detailsContainer = this.container.select('.heatmap-details');

            if (hover) {
                var list = [];
                if (hover) {
                    list.push(['TS', hover.ts]);
                    list.push(['regex', hover.pattern.regex]);
                    list.push(['variable', hover.variable]);
                    list.push(['class', hover.pattern.class]);
                    list.push(['occurences', (hover.pattern.locations[hover.ts] || []).length]);
                    list.push(['total', hover.pattern.nb]);
                    list.push(['max', hover.pattern.max]);
                }

                var foundInContainers = [];
                (self.obj.linkedContainers || []).forEach(function (container, idx) {
                    // if container has curve, add it in the foundInContainers array
                    if ((container.ts || []).findIndex(function (tsuid) {
                        return hover.tsuids.indexOf(tsuid) > -1;
                    }) > -1) {
                        foundInContainers.push(idx);
                    }
                });
                if (foundInContainers.length > 0) {
                    list.push(['found in', foundInContainers.map(function (c) {
                        return 'container #' + c;
                    }).join(', ')]);
                }

                detailsContainer.html('<table class="table table-condensed table-striped"><thead></thead><tbody></tbody></table>');
                var detailsData = detailsContainer.select('tbody').selectAll('.detail-line').data(list);
                var detailsEnter = detailsData.enter().append('tr').attr('class', 'detail-line').each(function () {
                    var elem = d3.select(this);
                    elem.append('td').attr('class', 'label-cell');
                    elem.append('td').attr('class', 'text-cell');
                });
                var detailsUpdate = detailsEnter.merge(detailsData);
                detailsUpdate.select('.label-cell').text(function (d) {
                    return d[0];
                });
                detailsUpdate.select('.text-cell').text(function (d) {
                    return d[1];
                });
                self.dispatch.call('cellEnter', self, {
                    hover: hover,
                    details: list
                });
            } else {
                self.dispatch.call('cellLeave');
                detailsContainer.html('');
            }
        }

        /**
         * Persist the VizTool for a quick restoration.
         */

    }, {
        key: 'sleep',
        value: function sleep() {}
        //this.container.classed('hidden', true);


        /**
         * Update data with patterns and ts
         * @param  {Object} data
         */

    }, {
        key: 'update',
        value: function update(data) {
            var self = this;
            if (data) {
                self.data = data;
            }

            // PREPARE PATTERNS
            self.obj.patterns = _.orderBy(self.data.patterns || [], ['variable', 'class', function (p) {
                return d3.keys(p.locations).length;
            }], ['asc', 'asc', 'desc']);

            // PREPARE PATTERN VARIABLES && PATTERN CLASSES that we will use as vertical legends
            var total = 0,
                total2 = 0;
            self.obj.classes = [];
            self.obj.variables = _.uniq(self.obj.patterns.map(function (d) {
                return d.variable;
            })).map(function (variableKey) {
                var patterns = self.obj.patterns.filter(function (d) {
                    return d.variable === variableKey;
                });
                var variable = {
                    key: variableKey,
                    pourcent: patterns.length * 100 / self.obj.patterns.length,
                    start: total,
                    color: _.find(self.obj.patterns, function (d) {
                        return d.variable === variableKey;
                    })
                };
                var classes = _.uniq(patterns.map(function (d) {
                    return d.class;
                }));
                classes.forEach(function (classKey) {
                    var classs = {
                        key: classKey,
                        variable: variableKey,
                        pourcent: patterns.filter(function (d) {
                            return d.class === classKey;
                        }).length * 100 / self.obj.patterns.length,
                        start: total2
                    };
                    total2 += classs.pourcent;
                    self.obj.classes.push(classs);
                });
                total += variable.pourcent;
                return variable;
            });

            // LINK PATTERNS AND VARIABLES (scale)
            self.obj.patterns.forEach(function (p) {
                // max locations found in one ts
                p.max = d3.max(d3.entries(p.locations), function (l) {
                    return l.value.length;
                });
                // prepare scale with a color related to the variable
                p.scale = d3.scaleLinear().range(["white", self._options.colors[self.obj.variables.findIndex(function (v) {
                    return v.key === p.variable;
                })]]).domain([0, p.max]);
                return p;
            });

            // PREPARE data
            var raw = self.data.raw || self.data.ts || [];
            self.obj.observations = _.uniq(_.sortBy(raw.filter(function (d) {
                return d.patterns.length > 0;
            }), 'class').map(function (d) {
                return d.key;
            })).map(function (d) {
                var ts_list = raw.filter(function (c) {
                    return c.key === d;
                });
                var obsClass = ts_list[0].class;
                return {
                    key: d,
                    ts: ts_list,
                    class: obsClass,
                    label: d + '-' + obsClass
                };
            });

            self.obj.observations = _.orderBy(self.obj.observations || [], ['class', function (ts) {
                return parseInt(("" + ts.key).replace(/[^0-9]/gi, ""));
            }], ['asc', 'asc']);
            self.obj.filtered = self.obj.observations;

            total = 0;
            // PREPARE ts classes that we will use as horizontal legend
            self.obj.tsClasses = _.uniq(self.obj.observations.map(function (o) {
                return o.class;
            })).map(function (classKey) {
                var observations = self.obj.observations.filter(function (d) {
                    return d.class === classKey;
                });
                var classs = {
                    key: classKey,
                    pourcent: observations.length * 100 / self.obj.observations.length,
                    start: total
                };
                total += classs.pourcent;
                return classs;
            });
            return this;
        }

        /**
         * Reset all variables and elements
         */

    }, {
        key: 'reset',
        value: function reset() {
            this.obj.ts = [];
            this.obj.transform = null;
            this.obj.filtered = [];
            this.obj.patterns = [];
            this.obj.markedData = {};
            this.obj.linkedContainers = [];
            if (this.dom.canvasHidden) {
                this.dom.canvasHidden.width = 0;
            }
        }

        /**
         * Wake up (Restore) the VizTool.
         */

    }, {
        key: 'wakeUp',
        value: function wakeUp() {
            //this.container.classed('hidden', false);
            this.display();
        }
    }]);

    return LIGHeatMap;
}(LIGViz);

module.exports = LIGHeatMap;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/SvgTextWidth.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/SvgTextWidth.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/heatmap.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/heatmap.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js":[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * An interface to use to implement vizsss from LIG.
 *
 * @constructor
 * @param {string} container - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 *                  Contains : engine - the parent engine instance
 */

// Abstract class
var LIGViz = function () {
    function LIGViz(container, data) {
        _classCallCheck(this, LIGViz);

        // Avoid abstract instance
        if (new.target === LIGViz) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        this.name = "";
        this.data = data;
        this.container = container;
    }

    /**
     * Init a LIGViz : collect and format the data (if necessary) then render the VizTool
     */


    _createClass(LIGViz, [{
        key: "display",
        value: function display() {
            throw new Error("display method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Wake up (Restore) the LIGViz with minimal recalculation.
         */

    }, {
        key: "wakeUp",
        value: function wakeUp() {
            throw new Error("wakeUp method of VizTool " + this.name + " has to be overridden");
        }

        /**
         * Persist the LIGViz for a quick restoration.
         */

    }, {
        key: "sleep",
        value: function sleep() {
            // If actions must be performed before hiding a viztool, there shall be done here
            // Example: remove pending requests, kill timers, ...
        }
    }]);

    return LIGViz;
}();

module.exports = LIGViz;

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/MultiCurves.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

// d3

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

// lodash
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

// VizTools
var LIGViz = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js"),
    CurveWithLegend = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithLegend.js");

// style
require("/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/css/d3-context-menu.css");
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/multi-curves.less");

/**
 * Visualization showing many timeseries individually
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 */

var MultiCurves = function (_LIGViz) {
	_inherits(MultiCurves, _LIGViz);

	function MultiCurves(containerId, data, callbacks) {
		var graphConstructor = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : CurveWithLegend;

		_classCallCheck(this, MultiCurves);

		var _this = _possibleConstructorReturn(this, (MultiCurves.__proto__ || Object.getPrototypeOf(MultiCurves)).call(this, containerId, data, callbacks));
		// Call super-class constructor


		_this.name = 'MultiCurves';
		_this.containerId = containerId;
		_this.container = d3.select('#' + containerId).html('');
		_this.dispatch = d3.dispatch('graphRemoved', 'graphAdded', 'tsRemoved', 'change');
		_this._options = {
			margin: {
				left: 0,
				right: 0,
				top: 30,
				bottom: 0
			},
			height: 800,
			width: 1200,
			minCurveHeight: 100,
			synchronized: true, // use to know if all graph are sync or not
			transform: null,
			graphConstructor: graphConstructor
		};
		_this.dom = {};
		_this.syncItems = {};
		_this.graphs = [];
		// default first graph
		_this.incr = 1;
		_this.graphs.push(new _this._options.graphConstructor(_this.containerId + '-curve' + _this.incr + '-', [])); //, md: this.data.md }));
		return _this;
	}

	/**
  * Add a new container in the interface
  * @param {Array}  ds - data that we want to use on it
  * @memberof MultiCurves
  */


	_createClass(MultiCurves, [{
		key: 'addNewContainer',
		value: function addNewContainer() {
			var ds = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
			var md = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
			var throwEvent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

			this.incr += 1;
			// create new instance of graph
			var graph = new this._options.graphConstructor(this.containerId + '-curve' + this.incr, []);
			graph.update(ds, md);
			// add it in graphs set
			this.graphs.push(graph);
			// dispatch specific event
			if (throwEvent) {
				this.dispatch.call('graphAdded', graph, graph);
			}
			return graph;
		}

		/**
   * Get all containers found
   * @return {Array} containers
   * @memberof MultiCurves
   */

	}, {
		key: 'containers',
		value: function containers() {
			return this.graphs.map(function (g, i) {
				return {
					idx: i,
					ts: g.data.map(function (ts) {
						return ts.tsuid;
					})
				};
			});
		}

		/**
   * Function allowing to display viz
   * @memberof MultiCurves
   */

	}, {
		key: 'display',
		value: function display() {
			return this.render();
		}

		/**
   * Get initial drag behavior
   * @memberof MultiCurves
   */

	}, {
		key: 'dragBehavior',
		value: function dragBehavior() {
			var self = this;
			// drag behavior
			var oldGraph = null;
			var initCoords = {};
			var getHoverGraph = function getHoverGraph() {
				var graphs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

				var idx = (self.graphs || graphs).findIndex(function (graph) {
					var coords = d3.mouse(graph.node());
					return coords[0] >= 0 && coords[0] <= graph._options.width && coords[1] >= 0 && coords[1] <= graph._options.height;
				});
				return self.graphs[idx];
			};
			var drag = d3.drag().on('start', function () {
				initCoords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
				var test = d3.select(this);
				self.dom.draggedElement.html(test.html()).style('background-color', test.style('background-color'));
				self.container.classed('in-dragging', true);
				oldGraph = getHoverGraph();
			}).on('drag', function () {
				self.dom.draggedElement.classed('hidden', false).style('top', d3.event.sourceEvent.y - 10 + 'px').style('left', d3.event.sourceEvent.x + 'px');
				var graph = getHoverGraph();
				self.container.classed('in-draging', true);
				self.container.selectAll('div.curve').classed('in-draging', function (d) {
					return d.containerId !== oldGraph.containerId && d.containerId === (graph || {}).containerId;
				});
			}).on('end', function (ts) {
				var graph = getHoverGraph();
				self.container.classed('in-draging', false);
				self.container.selectAll('div.curve').classed('in-draging', false);
				if (graph) {
					var currentVariables = _.uniq(graph.data.map(function (d) {
						return d.variable;
					}));
					var newVariables = [ts.variable];
					// control number of variables in the graph (2 in same time at max)
					if (_.union(currentVariables, newVariables).length > 2) {
						console.error('A graph can manage at max 2 variables, process aborted');
						return;
					}
					var ddata = graph.data;
					// add data in specific graph
					if (ddata.findIndex(function (d) {
						return d.tsuid === ts.tsuid;
					}) === -1) {
						ddata.push(ts);
						graph.update(ddata, oldGraph.md);
						var idx = oldGraph.data.findIndex(function (d) {
							return d.tsuid === ts.tsuid;
						});
						oldGraph.data.splice(idx, 1);
						self.render();
					}
				}
				self.container.classed('in-dragging', false);
				self.dom.draggedElement.classed('hidden', true);
			});
			return drag;
		}

		/**
   * Get all data found in all graphs
   * @return {Array} array of ts
   * @memberof MultiCurves
   */

	}, {
		key: 'getAllTs',
		value: function getAllTs() {
			return d3.merge(this.graphs.map(function (d) {
				return d.data;
			}));
		}

		/**
   * Highlight specific curves
   * @param {Array} tsuids
   * @memberof MultiCurves
   */

	}, {
		key: 'hightlightCurves',
		value: function hightlightCurves() {
			var tsuids = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

			var self = this;
			self.graphs.forEach(function (graph) {
				var graphTsuids = graph.data.map(function (sequence) {
					return sequence.tsuid;
				}),
				    tsuidsFound = tsuids.filter(function (tsuid) {
					return graphTsuids.indexOf(tsuid) > -1;
				}),
				    tsuid = tsuidsFound.length > 0 ? tsuidsFound[0] : null;
				graph.highlightCurve(tsuid);
			});
		}

		/**
   * Alias to register handler for specific event
   * @param  {String} eventName - name of the event (have to be declated in dispatch property)
   * @param  {Function} handler - callback
   * @return {Multicurves} - internal object (this)
   * @memberof MultiCurves
   */

	}, {
		key: 'on',
		value: function on(eventName, handler) {
			this.dispatch.on(eventName, handler);
			return this;
		}

		/**
   * Get or set current options
   * @param  {Object} _ * new options if it's given
   * @memberof MultiCurves
   * @return {Multicurves} current object
   */

	}, {
		key: 'options',
		value: function options(_) {
			if (!arguments.length) {
				return this._options;
			}
			// override current options with from parameter
			Object.assign(this._options, _);
			return this;
		}

		/**
   * Helper to refresh dradrop behavior
   * @memberof MultiCurves
  */

	}, {
		key: 'refreshDragDropBinding',
		value: function refreshDragDropBinding() {
			var self = this;
			// feature to allow to drag a ts to another container
			self.container.selectAll('.color-legend').call(self.dragBehavior());
		}

		/**
   * Remove a specific container from interface
   * @param  {Number} idx
   * @memberof MultiCurves
   */

	}, {
		key: 'removeContainer',
		value: function removeContainer() {
			var idx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

			this.graphs.splice(idx, 1);
		}

		/**
   * Function to render the viz
   * @memberof MultiCurves
   */

	}, {
		key: 'render',
		value: function render() {
			var self = this;
			var margin = self._options.margin;
			// TODO: why additional minus five
			var height = +self._options.height - margin.top - margin.bottom - 5;
			var tmpHeight = Math.floor(d3.max([height / self.graphs.length, self._options.minCurveHeight]));

			// relink selector <-> dom
			// make empty the container
			// TODO : dont create every time the whole wheel
			self.container = d3.select('#' + self.containerId).html('');
			var subContainerEnter = self.container.selectAll('.multi-curve-container').data([0]).enter().append('div').classed('multi-curve-container', true);

			subContainerEnter.append('div').attr('class', 'multi-curve-controls').append('span').attr('class', 'btn').html('<i class="glyphicon glyphicon-plus"></i> Add new container').on('click', function () {
				self.addNewContainer();
				self.render();
			});

			subContainerEnter.append('div').attr('class', 'multi-curve-curves');
			subContainerEnter.append('span').attr('class', 'color-legend drag-element hidden');

			var curvesData = self.container.select('.multi-curve-curves').selectAll('div.curve-container').data(self.graphs, function (d) {
				return d.containerId;
			});

			var curvesDataEnter = curvesData.enter().append('div').attr('class', 'curve-container').each(function (d) {
				self.syncItems[d.containerId] = typeof self.syncItems[d.containerId] === 'undefined' || self.syncItems[d.containerId] === true;
			});
			var buttonsEnter = curvesDataEnter.append('div').attr('class', 'switch-buttons');
			buttonsEnter.append('div').attr('class', 'button btn btn-default arrow-up').append('span').attr('class', 'glyphicon glyphicon-arrow-up');

			buttonsEnter.append('div').attr('class', 'button btn btn-default arrow-down').append('span').attr('class', 'glyphicon glyphicon-arrow-down');

			curvesDataEnter.append('div').attr('class', 'curve child').attr('id', function (d) {
				return d.containerId;
			});

			curvesData.exit().remove();

			self.container.selectAll('.switch-buttons .arrow-up').classed('hidden', function (d, i) {
				return i === 0;
			}).on('click', function (d, i) {
				self.graphs.splice(i - 1, 0, self.graphs.splice(i, 1)[0]);
				self.render();
				self.dispatch.call('change', self);
			});
			self.container.selectAll('.switch-buttons .arrow-down').classed('hidden', function (d, i) {
				return i === self.graphs.length - 1;
			}).on('click', function (d, i) {
				self.graphs.splice(i + 1, 0, self.graphs.splice(i, 1)[0]);
				self.render();
				self.dispatch.call('change', self);
			});

			// dragged element
			self.dom.draggedElement = self.container.select('.drag-element');

			self.graphs.forEach(function (graph, idx) {
				var transform = void 0;
				if (self.syncItems[graph.containerId]) {
					transform = self._options.transform;
				}
				// update height, width, transform
				graph.options({ height: tmpHeight, width: self._options.width, transform: transform, synced: self.syncItems[graph.containerId] });
				// display graph
				graph.display();

				// at each zoomed event caught in one graph, rescale other graph
				graph.on('zoomed', function (a) {
					if (self._options.synchronized && self.syncItems[graph.containerId]) {
						self._options.transform = this;
						self.rescale(this, a);
					}
				}).on('syncChange', function (a) {
					self.syncItems[graph.containerId] = a;
					self.dispatch.call('change', self);
				}).on('removed', function () {
					// remove graph
					self.graphs.splice(idx, 1);
					self.dispatch.call('graphRemoved', graph, graph.data.map(function (d) {
						return d.tsuid;
					}));
					self.render();
				}).on('tsRemoved', function (a) {
					// remove sequence
					self.dispatch.call('tsRemoved', graph, a);
				}).on('change', function (a) {
					// changes on a graph
					self.dispatch.call('change', graph, a);
				});
			});
			self.refreshDragDropBinding();
		}

		/**
   * Rescale all related graphs using specific transormation
   * @param  {Object} transform - {x:, y:, k}
   * @param  {string} sourceId - source container id
   * @memberof MultiCurves
   */

	}, {
		key: 'rescale',
		value: function rescale(transform, sourceId) {
			var self = this;
			self.graphs.forEach(function (d) {
				if (d.containerId !== sourceId && self.syncItems[d.containerId] === true) {
					// TODO check if requestAnimationFrame is really useful here
					window.requestAnimationFrame(function () {
						return d.transform(transform, false);
					}, self.container.node());
				}
			});
		}

		/**
   * Reset viz as its initial state
   * @memberof MultiCurves
   */

	}, {
		key: 'reset',
		value: function reset() {
			// reset shared transform
			this._options.transform = null;
			// rest sync items as en empty dict
			this.syncItems = {};
			// reset graphs as an empty array
			this.graphs = [];
		}
	}]);

	return MultiCurves;
}(LIGViz);

module.exports = MultiCurves;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/css/d3-context-menu.css":"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/css/d3-context-menu.css","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithLegend.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/CurveWithLegend.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGViz.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/multi-curves.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/multi-curves.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/TSplorer.js":[function(require,module,exports){
(function (global){
'use strict';

// less integration

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/scopes.less");
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/tsplorer.less");
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less");

// external dependencies
var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    ResizeObserver = require("/home/davisp/projects/ikats_lig/hmi/node_modules/resize-observer-polyfill/dist/ResizeObserver.global.js");

// internal dependencies
var VizTool = require("/home/davisp/projects/ikats_lig/hmi/src/CS/VizModule/VizTool.js"),
    MultiCurves = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/MultiCurves.js"),
    LIGHeatMap = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGHeatMap.js");

var DataProvider = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js"),
    getNewName = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/NameHelper.js"),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js"),
    fullScreenApi = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js"),
    d3ContextMenu = require("/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js"),
    SideMenu = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/SideMenu.js");

/**
 * get patterns occurences as a map
 * @param  {Array}  tss - timeseries
 * @return {Object} mapping
 */
var getPatternsMapping = function getPatternsMapping() {
    var tss = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    var currentTsPatterns = void 0;
    var mapping = {};
    tss.forEach(function (ts) {
        currentTsPatterns = mapping[ts.key] || {};
        ts.patterns.forEach(function (p) {
            currentTsPatterns[p.regex] = p.locations.length || 1;
        });
        mapping[ts.key] = currentTsPatterns;
    });
    return mapping;
};

/**
 * Visualization showing many timeseries individually
 *
 * @constructor
 * @param {string} containerId - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 */

var TSplorer = function (_VizTool) {
    _inherits(TSplorer, _VizTool);

    function TSplorer(container, result, callbacks) {
        _classCallCheck(this, TSplorer);

        var _this = _possibleConstructorReturn(this, (TSplorer.__proto__ || Object.getPrototypeOf(TSplorer)).call(this, container, [], callbacks));
        // Call super-class constructor


        var self = _this;
        _this.name = 'TSplorer';
        _this.container = d3.select('#' + container);
        _this.containerId = container;
        _this._options = {
            margin: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            },
            waitBeforeDisplay: 1000,
            menuPourcentWidth: 15,
            width: 500,
            height: 200,
            scopeHeight: 40,
            autosize: true,
            graphId: 'tsplorerCurves', // identifier of the graph container
            mode: 'curve' // mode of the viz (curve|heatmap)
        };
        _this.dom = {
            graphScopes: null
        };
        _this.context = {
            type: 'curve',
            scope: 'current'
        };
        _this.logger = new LigLogger('lig-tsplorer');
        _this.obj = {
            menu: new SideMenu(), // menu to interact with the graph
            dataset: null, // dataset object
            viz: null, // default viz
            heatmap: null, // heatmap viz
            dataProvider: new DataProvider(), // dataprovider
            selectionControls: null, // selectors
            pendingResizeCb: null,
            prevSize: null,
            menuCriterias: {},
            resizeObserver: new ResizeObserver(function (entries, observer) {
                var $elem = $(self.container.node());
                if (!self._options.autosize || $elem.height() === self.obj.height && $elem.width() === self.obj.width) {
                    return;
                }
                clearTimeout(_this.obj.pendingResizeCb);
                _this.obj.pendingResizeCb = setTimeout(function () {
                    if (_this.container.select('.tsplorer-container').size() !== 0) {
                        _this.render();
                    } else {
                        observer.unobserve(_this.container.node());
                    }
                }, 30);
            }),
            cacheMenu: {}, // use to save async results in a cache
            width: null,
            height: null,
            scopes: null
        };
        _this.inputs = result;
        if (callbacks && callbacks.inputs && callbacks.inputs.rhm__i__learningset) {
            _this.inputs = Object.assign({
                ikats: true,
                table_name: callbacks.inputs.rhm__i__learningset
            }, result);
        }
        _this._options.manageScopes = _this.inputs && !_this.inputs.ikats;
        _this._options.managePatterns = _this.inputs.patterns && _this.inputs.patterns.length > 0;
        return _this;
    }

    /**
     * Render left menu
     */


    _createClass(TSplorer, [{
        key: '_renderMenu',
        value: function _renderMenu() {
            var self = this,
                controls = self.container.select('.tsplorer-controls');
            self.logger.log('render Menu');
            var onLoadDataset = function onLoadDataset() {
                self.container.select('.tsplorer-container').classed('in-loading', true);
                return self.obj.dataProvider.getDataset(self._options.dataset, self.inputs).then(function (d) {
                    // color scale
                    var colors = d3.scaleOrdinal(d3.schemeCategory20);
                    var data = {
                        ts: d.raw || [],
                        patterns: self.inputs.patterns || d.patterns || [],
                        md: d.md || {},
                        variables: d.variables || [],
                        classes: (d.class || []).map(function (d) {
                            return d + '';
                        })
                    };

                    // set all pattern as active
                    data.patterns.forEach(function (pattern) {
                        return pattern.active = true;
                    });

                    // integrate patterns result in ts
                    data.ts.forEach(function (ts) {
                        data.patterns.forEach(function (pattern, pidx) {
                            if (ts.variable === pattern.variable && ts.patterns.findIndex(function (p) {
                                return p.key === pattern.key;
                            }) === -1) {
                                if (pattern.locations[ts.key]) {
                                    // add locations and color
                                    var customPattern = Object.assign({}, pattern);
                                    Object.assign(customPattern, { locations: pattern.locations[ts.key], color: d3.color(colors(pidx)).darker(0.8) });
                                    ts.patterns.push(customPattern);
                                }
                            }
                        });
                    });
                    return data;
                }).then(function (d) {
                    self.logger.log('datasetchange : ' + self.context.dataset + ' -> ' + d.md.datasetid + ' ');
                    self.obj.viz.reset(); // reset viz as we changed the dataset
                    self.obj.dataset = d; // assign new dataset
                    self.context.scope = 'current'; // assign new scope
                    // TODO: self.obj.dataset and self.context.dataset are not same object, fix typo
                    self.context.dataset = d.md.datasetid; // assign new datasetid
                    self.context.md = d.md; // assign metadata
                    self.inputs = Object.assign(self.inputs, self.context.md); // mixin
                    self._options.managePatterns = self.inputs.patterns && self.inputs.patterns.length > 0 || d && d.patterns && d.patterns.length > 0;
                    self.obj.heatmap.reset(); // reset heatmap
                    return self.getDataAndRefresh(); // main fn that reload viz with new context
                });
            };

            var getHoverGraph = function getHoverGraph() {
                var graphs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

                return graphs[graphs.findIndex(function (graph) {
                    var coords = d3.mouse(graph.node());
                    return coords[0] >= 0 && coords[0] <= graph._options.width && coords[1] >= 0 && coords[1] <= graph._options.height;
                })];
            };

            var section1 = [],
                section2 = [],
                section3 = [],
                section4 = [];
            section1.push({
                label: 'types',
                help: 'Types',
                type: 'buttons',
                data: [{ text: 'Curve', value: 'curve', active: self._options.mode === 'curve' }, { text: 'Heatmap', value: 'heatmap', active: self._options.mode === 'heatmap' }],
                onClick: function onClick(data) {
                    self._options.mode = data.value;
                    self.getDataAndRefresh();
                }
            });

            if (!self.inputs.table_name) {
                section2.push({
                    label: 'datasets',
                    help: 'Choose the dataset to explore',
                    type: 'select',
                    inline: true,
                    value: self._options.dataset,
                    values: null,
                    provider: function provider() {
                        var labelId = this.label;
                        // get cache item
                        return Promise.resolve(self.obj.cacheMenu[labelId]).then(function (cachedValues) {
                            // return cache values or promise from dataprovider
                            return cachedValues ? cachedValues : self.obj.dataProvider.getDatasetIds(true);
                        }).then(function (values) {
                            // store values in cache
                            self.obj.cacheMenu[labelId] = values;
                            // in case we have no initial datasetid
                            // find it in the result from the provider
                            if (!self._options.dataset) {
                                self._options.dataset = values[0];
                                onLoadDataset();
                            }
                            return values;
                        });
                    },
                    onChange: function onChange(data, elem) {
                        self._options.dataset = elem.value;
                        self.obj.menuCriterias = {};
                        onLoadDataset();
                    }
                });
            }

            if (self.obj.dataset) {
                self.obj.menuCriterias.patternFiltered = self.obj.menuCriterias.patternFiltered || self.obj.dataset.patterns;
                self.obj.menuCriterias.tsFiltered = self.obj.menuCriterias.tsFiltered || self.obj.dataset.ts;
                self.obj.menuCriterias.label = self.obj.menuCriterias.label || 'all';
                self.obj.menuCriterias.variable = self.obj.menuCriterias.variable || self.obj.dataset.variables[0];
                var matchedPatterns = self.obj.dataset.patterns.filter(function (pattern) {
                    return pattern.variable === self.obj.menuCriterias.variable && (self.obj.menuCriterias.label === 'all' || pattern.class + '' === self.obj.menuCriterias.label + '');
                });

                if (self.obj.menuCriterias.joker === true) {
                    self.obj.menuCriterias.patterns = [];
                } else if (!self.obj.menuCriterias.patterns) {
                    self.obj.menuCriterias.patterns = matchedPatterns.map(function (d) {
                        return d.regex;
                    });
                }

                var joker = self.obj.menuCriterias.joker || self.obj.menuCriterias.patterns.length === 0;
                var matchedSequences = self.obj.dataset.ts.filter(function (sequence) {
                    // if at least one pattern is found in the sequence
                    return sequence.patterns.findIndex(function (pattern) {
                        return self.obj.menuCriterias.patterns.indexOf(pattern.regex) > -1;
                    }) > -1 ||
                    // if joker is selected (*) and variable and class is same as selection
                    joker && sequence.variable === self.obj.menuCriterias.variable && (self.obj.menuCriterias.label === 'all' || '' + sequence.class === '' + self.obj.menuCriterias.label);
                });

                section2.push({
                    label: 'class',
                    help: 'Choose the label criteria',
                    type: 'select',
                    inline: true,
                    value: self.obj.menuCriterias.label,
                    values: ['all'].concat(self.obj.dataset.classes),
                    onChange: function onChange(data, elem) {
                        self.obj.menuCriterias.label = elem.value;
                        self.obj.menuCriterias.patterns = null;
                        self._renderMenu();
                    }
                });

                if (self._options.mode === 'curve') {
                    section2.push({
                        label: 'variable',
                        help: 'Change the scale/zoom',
                        type: 'select',
                        inline: true,
                        value: self.obj.menuCriterias.variable,
                        values: self.obj.dataset.variables,
                        onChange: function onChange(data, elem) {
                            self.obj.menuCriterias.variable = elem.value;
                            self.obj.menuCriterias.patterns = null;
                            self._renderMenu();
                        }
                    });
                } else {
                    section2.push({
                        help: 'Patterns',
                        type: 'search',
                        showTable: false,
                        showCount: true,
                        label: 'patterns',
                        inline: true,
                        filterValue: self.obj.menuCriterias.txtPattern,
                        count: self.obj.menuCriterias.patternFiltered.length,
                        data: matchedPatterns.map(function (d) {
                            return [d.regex];
                        }),
                        onSearch: function onSearch(data) {
                            self.obj.menuCriterias.txtPattern = data.filterValue;
                            self.obj.menuCriterias.patternFiltered = self.obj.dataset.patterns.filter(function (e) {
                                return !self.obj.menuCriterias || (!self.obj.menuCriterias.label || self.obj.menuCriterias.label === 'all' || self.obj.menuCriterias.label === e.class + '') && (!self.obj.menuCriterias.txtPattern || self.obj.menuCriterias.txtPattern === '' || e.regex.indexOf(self.obj.menuCriterias.txtPattern) > -1 || e.key.indexOf(self.obj.menuCriterias.txtPattern) > -1);
                            });
                            self.obj.heatmap.update(Object.assign({}, self.obj.dataset, { patterns: self.obj.menuCriterias.patternFiltered })).display();
                            self._renderMenu();
                        }
                    }, {
                        help: 'Sequences',
                        type: 'search',
                        showTable: false,
                        showCount: true,
                        label: 'sequences',
                        inline: true,
                        count: self.obj.menuCriterias.tsFiltered.length,
                        filterValue: self.obj.menuCriterias.txtSequence,
                        data: matchedPatterns.map(function (d) {
                            return [d.regex];
                        }),
                        onSearch: function onSearch(data) {
                            var patternFiltered = self.obj.menuCriterias.patternFiltered || self.obj.dataset.patterns;
                            self.obj.menuCriterias.txtSequence = data.filterValue;
                            self.obj.menuCriterias.tsFiltered = self.obj.dataset.ts.filter(function (ts) {
                                return (!self.obj.menuCriterias || !self.obj.menuCriterias.txtSequence || self.obj.menuCriterias.txtSequence === '' || ts.key.indexOf(self.obj.menuCriterias.txtSequence) > -1) && patternFiltered.findIndex(function (t) {
                                    return t.class + '' === ts.class + '' || (t.locations[ts.key] || []).length > 0;
                                }) > -1;
                            });
                            self.obj.heatmap.update(Object.assign({}, self.obj.dataset, { patterns: patternFiltered, ts: self.obj.menuCriterias.tsFiltered })).display();
                            self._renderMenu();
                        }
                    });
                }

                if (self._options.managePatterns && self._options.mode === 'curve') {
                    section3.push({
                        help: 'Patterns',
                        type: 'search',
                        showHeader: false,
                        showTable: true,
                        selected: self.obj.menuCriterias.patterns,
                        data: matchedPatterns.map(function (d) {
                            return [d.regex];
                        }),
                        onClick: function onClick(pattern, elem, opts) {
                            self.obj.menuCriterias.patterns = opts.selected;
                            self.obj.menuCriterias.joker = false;
                            self._renderMenu();
                        }
                    });
                }

                if (self._options.mode === 'curve') {
                    section4.push({
                        label: 'Show all sequences',
                        active: joker,
                        help: 'Change the tree display to fit all space available',
                        type: 'button',
                        classed: [['selected', function () {
                            return self.obj.menuCriterias.joker;
                        }]],
                        onClick: function onClick() {
                            self.obj.menuCriterias.joker = true;
                            self.obj.menuCriterias.patterns = [];
                            self._renderMenu();
                        }
                    });

                    var tsuids = self.obj.viz.getAllTs().map(function (d) {
                        return d.tsuid.split('#')[0];
                    }),
                        tsuidsMap = {};
                    tsuids.forEach(function (tsuid) {
                        return tsuidsMap[tsuid] = true;
                    });
                    self.obj.menuCriterias.arrows = self.obj.menuCriterias.arrows || [{ id: 0, order: 'asc', class: 0 }, { id: 1, order: 'asc', class: 1 }, { active: true, id: 2, order: 'asc', class: 2 }, { id: 3, order: 'asc', class: 3 }];
                    section4.push({
                        help: 'Sequences',
                        type: 'search',
                        draggable: true,
                        showCount: true,
                        showSelectAll: true,
                        columns: [{ label: 'id', visible: false }, { label: 'Class' }, { label: 'Obs', onSort: function onSort(d) {
                                return parseInt(d.replace(/[A-Za-z$-]/g, ''));
                            } }, { label: 'Nb' }],
                        classed: [['in-graph', function (sequence) {
                            return tsuidsMap[sequence[0]] === true;
                        }], ['different-class', function (sequence) {
                            return self.obj.menuCriterias.label !== 'all' && '' + sequence[1] !== '' + self.obj.menuCriterias.label;
                        }]],
                        selected: self.obj.menuCriterias.sequences,
                        arrows: self.obj.menuCriterias.arrows,
                        data: matchedSequences.map(function (d) {
                            return [d.tsuid, d.class, d.key, '(' + d.patterns.length + ')'];
                        }),
                        onSortChange: function onSortChange(arrows) {
                            return self.obj.menuCriterias.arrows = arrows;
                        },
                        onMouseEnter: function onMouseEnter(sequence) {
                            return self.obj.viz.hightlightCurves([sequence[0]]);
                        },
                        onMouseLeave: function onMouseLeave() {
                            return self.obj.viz.hightlightCurves([]);
                        },
                        onDragStart: function onDragStart() {
                            return self.container.select('.tsplorer-container').classed('in-draging', true);
                        },
                        onClick: function onClick(pattern, elem, opts) {
                            self.obj.menuCriterias.sequences = opts.selected;
                            self._renderMenu();
                        },
                        onDragMove: function onDragMove() {
                            var graph = getHoverGraph(self.obj.viz.graphs ? self.obj.viz.graphs : [self.obj.viz]);
                            self.container.selectAll('.curve').classed('in-draging', function (d) {
                                return graph && d.containerId === graph.containerId;
                            });
                        },
                        onDragEnd: function onDragEnd(sequences) {
                            var ts = [];
                            sequences.forEach(function (selected) {
                                var mainSequence = _.clone(self.obj.dataset.ts[self.obj.dataset.ts.findIndex(function (ts) {
                                    return ts.tsuid === selected[0];
                                })]),
                                    variables = mainSequence.variable.split('-');
                                if (variables.length > 1) {
                                    // paired variable
                                    variables.forEach(function (variable, j) {
                                        var tmp = _.clone(self.obj.dataset.ts[self.obj.dataset.ts.findIndex(function (ts) {
                                            return ts.key === mainSequence.key && ts.variable === variable;
                                        })]);
                                        if (tmp) {
                                            tmp.tsuid = tmp.tsuid + '#' + mainSequence.variable;
                                            tmp.pairedIndex = j;
                                            tmp.patterns = mainSequence.patterns;
                                            ts.push(tmp);
                                        }
                                    });
                                } else {
                                    ts.push(mainSequence);
                                }
                            });

                            // get all colors linked to a tsuid
                            var _colors = {};
                            self.obj.viz.graphs.forEach(function (g) {
                                g.data.forEach(function (d) {
                                    _colors[d.tsuid] = d.color;
                                });
                            });

                            var graphs = self.obj.viz.graphs ? self.obj.viz.graphs : [self.obj.viz],
                                graph = getHoverGraph(graphs);
                            if (graph) {
                                var data = graph.data;
                                if (_.union(_.uniq(data.map(function (d) {
                                    return d.variable;
                                })), _.uniq(ts.map(function (d) {
                                    return d.variable;
                                }))).length > 2) {
                                    self.logger.error('A graph can manage at max 2 variables, process aborted');
                                    return;
                                }
                                for (var i = 0; i < ts.length; i++) {
                                    var t = Object.assign({}, ts[i]);
                                    /*jshint loopfunc: true */
                                    if (data.findIndex(function (d) {
                                        return d.tsuid === t.tsuid;
                                    }) === -1) {
                                        // if i > 0, that means we are in a pair variable
                                        if (t.pairedIndex === 1) {
                                            t.style = (graph.options().styles || [])[2];
                                        }
                                        t.color = _colors[t.tsuid] || t.color;
                                        data.push(t);
                                    }
                                }
                                graph.update(data, self.inputs);

                                var sharedMinMax = self.getSharedMinMax();
                                graphs.forEach(function (g) {
                                    if (!_.isEqual(g.options().yMinMax, sharedMinMax) || _.isEqual(graph, g)) {
                                        g.options({ yMinMax: sharedMinMax }).render();
                                    }
                                });

                                self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs())).linkedContainers(self.obj.viz.containers());

                                self.obj.viz.refreshDragDropBinding();
                            }
                            self._renderMenu();
                            self.saveScope();
                            self.container.selectAll('.curve').classed('in-draging', false);
                        }
                    });
                }
            } else if (self.inputs && self.inputs.table_name) {
                onLoadDataset();
                return false;
            }

            var blocs = [{
                collapsable: false,
                showTitle: false,
                title: 'types',
                type: 'controls',
                data: section1
            }, {
                collapsable: false,
                showTitle: false,
                title: 'criterias',
                type: 'controls',
                data: section2
            }];

            if (self._options.mode === 'curve') {
                blocs = blocs.concat([{
                    collapsable: false,
                    showTitle: true,
                    title: 'patterns',
                    type: 'controls',
                    data: section3
                }, {
                    collapsable: false,
                    showTitle: true,
                    title: 'sequences',
                    type: 'controls',
                    data: section4
                }]);
            }

            if (self._options.mode === 'heatmap') {
                blocs.push({
                    collapsable: true,
                    title: 'informations',
                    type: 'informations',
                    data: (self.obj.menuCriterias.heatmap || {}).details || []
                });
            }

            self.obj.menu.setContainer(controls).blocs(blocs).on('resize-x-move', function (newValue) {
                self.container.select('.tsplorer-graph-main').style('width', self.obj.width - self._options.margin.left - self._options.margin.right - newValue + 'px');
            })
            // handle event when user resize the panel
            .on('resize-x', function (newValue) {
                self._options.menuPourcentWidth = newValue * 100 / (self.obj.width - self._options.margin.left - self._options.margin.right);
                self.render();
            }).on('change', function () {
                return self.render();
            }).refresh();
        }

        /**
         * Function to render the viz
         */

    }, {
        key: 'render',
        value: function render() {
            var self = this;
            self.logger.log('render TSplorer');

            var height = self._options.height,
                width = self._options.width;

            // if autosize get real height/width container
            if (self._options.autosize) {
                height = $(self.container.node()).height();
                width = $(self.container.node()).width();
            }

            self.obj.width = width;
            self.obj.height = height;

            var graphHeight = height - self._options.margin.top - self._options.margin.bottom,
                graphWidth = width - self._options.margin.left - self._options.margin.right;

            // create container
            var tmp = self.container.selectAll('.tsplorer-container').data([self]);

            var containerEnter = tmp.enter().append('div').classed('tsplorer-container', true);
            //.classed('in-loading', true)

            containerEnter.append('div').classed('tsplorer-controls', true);

            var container = containerEnter.merge(tmp);
            var controls = container.select('.tsplorer-controls');

            var subContainer = self.container.select('.tsplorer-container');
            subContainer.style('margin-left', self._options.margin.left + 'px').style('margin-top', self._options.margin.top + 'px').style('width', graphWidth + 'px').style('height', graphHeight + 'px');

            var menuWidth = self.obj.menu.isCollapsed() ? 0 : graphWidth * (self._options.menuPourcentWidth / 100),
                curveWidth = self.obj.menu.isCollapsed() ? graphWidth : graphWidth * ((100 - self._options.menuPourcentWidth) / 100);

            var graphMainEnter = containerEnter.append('div').classed('tsplorer-graph-main', true);

            containerEnter.append('div').attr('class', 'loader');

            controls.style('width', menuWidth + 'px').style('height', graphHeight + 'px');
            subContainer.select('.tsplorer-graph-main').style('width', curveWidth + 'px') // TODO: check if its stable
            .style('height', graphHeight + 'px');

            self._renderMenu();
            self.initScopeContainer(graphMainEnter, subContainer);

            if (self._options.manageScopes) {
                self.refreshScopes();
            }

            var graphInner = graphMainEnter.append('div').attr('class', 'tsplorer-graph-inner').attr('id', self._options.graphId);

            // Init curve
            if (!self.obj.viz) {

                var _callback = function _callback() {
                    self.logger.log('_callback');
                    self._renderMenu();
                    self.saveScope();
                    self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs())).linkedContainers(self.obj.viz.containers());
                };

                self.obj.viz = new MultiCurves(self._options.graphId, [], null /**, ProtoWithPatternLegend**/);
                // manage event when a graph or a curve is removed
                self.obj.viz.on('graphRemoved tsRemoved', _callback).on('change graphAdded', function (graph) {
                    if (graph) {
                        var _c = {};
                        graph.data.forEach(function (c) {
                            return _c[c.tsuid] = c.color;
                        });
                        graph.on('reset', _callback);
                        self.obj.viz.graphs.forEach(function (g) {
                            g.data.forEach(function (d) {
                                return d.color = _c[d.tsuid] || d.color;
                            });
                        });
                    }
                    _callback();
                });
            }

            // Init heatmap
            if (!self.obj.heatmap) {
                self.obj.heatmap = new LIGHeatMap(self._options.graphId, {}, {});
                self.obj.heatmap.on('orderAddTs', function (ts, md, idx) {
                    if (typeof idx === 'undefined') {
                        var c = self.obj.viz.addNewContainer(ts, md, false);
                        c.update(ts, self.inputs);
                    } else {
                        self.obj.viz.graphs[idx].update(_.union(self.obj.viz.graphs[idx].data, ts), self.inputs);
                    }

                    self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs())).linkedContainers(self.obj.viz.containers()).render();

                    self._renderMenu();
                    self.saveScope();
                }).on('orderRemoveTs', function (ts, md, idx) {
                    var tsuids = ts.map(function (t) {
                        return t.tsuid;
                    });

                    if (typeof idx === 'undefined') {
                        self.logger.error('need to specify the container that the ts will be removed');
                    } else {
                        _.remove(self.obj.viz.graphs[idx].data, function (d) {
                            return tsuids.indexOf(d.tsuid) > -1;
                        });
                    }

                    self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs())).linkedContainers(self.obj.viz.containers()).render();

                    self._renderMenu();
                    self.saveScope();
                }).on('cellEnter', function (d) {
                    self.obj.menuCriterias.heatmap = d;
                    self.obj.menu.refreshPartial('informations', d.details);
                }).on('cellLeave', function () {
                    self.obj.menuCriterias.heatmap = null;
                    self.obj.menu.refreshPartial('informations', []);
                });
            }

            self.obj.viz.options({ width: curveWidth, height: graphHeight - self._options.scopeHeight });

            // mode curve
            if (self._options.mode !== 'heatmap') {
                self.obj.viz.container = graphInner;
                self.obj.viz.display();
            }
            // mode heatmap
            else {
                    self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs()));
                    self.obj.heatmap.linkedContainers(self.obj.viz.containers());
                    self.obj.heatmap.display();
                }
        }

        /**
         * Function allowing to display viz
         */

    }, {
        key: 'display',
        value: function display() {
            var self = this;
            setTimeout(function () {
                self.update();
                self.render();
                // activate or not loading depending if there is a cached dataset
                self.container.select('.tsplorer-container').classed('in-loading', !(self.obj && self.obj.dataset));
                // observe container size in and use callback
                self.obj.resizeObserver.observe(document.body);
            }, self._options.waitBeforeDisplay);
        }

        /**
         * Refresh containers
         */

    }, {
        key: 'getDataAndRefresh',
        value: function getDataAndRefresh() {
            var self = this;
            self.logger.log('start fetching data and refresh tsplorer');
            var scopes = void 0,
                allTsuids = [];

            if (self._options.manageScopes) {
                self.obj.scopes = self.obj.dataProvider.scopes.getScopes(self.context.dataset);
                if (d3.keys(self.obj.scopes).length === 0) {
                    var scope = {
                        key: self.context.scope,
                        containers: [],
                        visible: true
                    };
                    self.obj.scopes[self.context.scope] = scope;
                    self.obj.dataProvider.scopes.saveScope(self.context.dataset, self.context.scope, scope);
                }
                scopes = self.obj.dataProvider.scopes.getScope(self.context.dataset, self.context.scope, true);
                if (!scopes.visible) {
                    scopes = d3.values(self.obj.scopes).filter(function (s) {
                        return s.visible === true;
                    })[0];
                    self.context.scope = scopes.key;
                }
                var containers = scopes ? scopes.containers || [] : [];
                if (containers.length === 0) {
                    containers.push({ idx: 0, tsuids: [], patterns: [] });
                }
                // get linked data needs by the scopes
                allTsuids = _.uniq(d3.merge(containers.map(function (p) {
                    return p.tsuids;
                }))).map(function (d) {
                    return {
                        tsuid: d
                    };
                });
            }

            self.obj.dataProvider.getMetaData(allTsuids, self.inputs).then(function (data) {
                var graphs = self.obj.viz.graphs ? self.obj.viz.graphs : [self.obj.viz];
                // in case there is no first graph, add it
                if (self.obj.viz.graphs.length === 0) {
                    self.obj.viz.addNewContainer([], self.inputs, false);
                }

                // if scope is managed, sync display with it
                if (self._options.manageScopes) {
                    self.syncDisplayWithScope(graphs, scopes, data);
                }

                // update data in the heatmap
                self.obj.heatmap.update(Object.assign({}, self.obj.dataset, { patterns: self.obj.menuCriterias.patternFiltered || self.obj.dataset.patterns, ts: self.obj.menuCriterias.tsFiltered || self.obj.dataset.ts }));

                self.container.select('.tsplorer-container').classed('in-loading', false);
                // mode curve
                if (self._options.mode !== 'heatmap') {
                    self.obj.viz.display();
                }
                // mode heatmap
                else {
                        self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs()));
                        self.obj.heatmap.linkedContainers(self.obj.viz.containers());
                        self.obj.heatmap.display();
                    }
                self._renderMenu();
            });
        }

        /**
         * Get shared minmax in looking on given data or from viz
         * @param {*} data
         */

    }, {
        key: 'getSharedMinMax',
        value: function getSharedMinMax(data) {
            var self = this;
            var shared = {};

            if (!data || data === {}) {
                data = {};
                self.obj.viz.getAllTs().forEach(function (d) {
                    return data[d.tsuid] = d;
                });
            }

            // aggregate min / max from each ts
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    var item = data[key],
                        variable = item.variable;
                    if (_.isUndefined(item.min) || _.isUndefined(item.max)) {
                        self.logger.warn('min ' + item.min + ' and max ' + item.max + ' should be found in the TS ' + item.key);
                    } else {
                        var minmax = [+item.min, +item.max];
                        shared[variable] = shared[variable] ? d3.extent(shared[variable].concat(minmax)) : minmax;
                    }
                }
            }

            // use breakpoints from inputs
            (self.inputs.breakpoints || []).forEach(function (bps) {
                var minmax = shared[bps.key],
                    bps_array = bps.bps;
                if (minmax && bps_array) {
                    shared[bps.key] = [bps_array[0].min, bps_array[bps_array.length - 1].max];
                }
            });

            return shared === {} ? null : shared;
        }
    }, {
        key: 'initScopeContainer',
        value: function initScopeContainer(graphMainEnter, subContainer) {
            var self = this;
            var graphScopesEnter = graphMainEnter.append('div').classed('tsplorer-scopes wrap-tabs', true);

            var graphScopes = subContainer.select('.tsplorer-scopes');

            graphScopesEnter.append('div').classed('tsplorer-scopes-list tabs', true);
            self.dom.graphScopesList = graphScopes.select('.tsplorer-scopes-list');

            var menu = [{ title: 'Load:' }, { divider: true }];

            var _makeScopeVisible = function _makeScopeVisible(scope) {
                return function () {
                    scope.visible = true;
                    self.obj.dataProvider.scopes.saveScope(self.context.dataset, scope.key, scope);
                    self.context.scope = scope.key;
                    self.getDataAndRefresh();
                };
            };

            var keys = d3.keys(self.obj.scopes);
            for (var i = 0; i < keys.length; i++) {
                var scope = self.obj.scopes[keys[i]];
                if (!scope.visible) {
                    menu.push({
                        title: keys[i],
                        action: _makeScopeVisible(scope)
                    });
                }
            }

            graphScopesEnter.append('span').attr('class', 'new-scope plus btn btn-default btn-xs').classed('hidden', !self._options.manageScopes).on('click', function () {
                self.context.scope = getNewName(d3.keys(self.obj.scopes));
                self.obj.dataProvider.scopes.saveScope(self.context.dataset, self.context.scope, {
                    key: self.context.scope,
                    containers: [],
                    visible: true
                });
                self.getDataAndRefresh();
            }).on('contextmenu', d3ContextMenu(menu, {}, self.container)).append('i').attr('class', 'glyphicon glyphicon-plus');

            graphScopesEnter.append('input').attr('class', 'new-scope-name hidden');

            graphScopesEnter.append('span').attr('class', 'hidden new-scope-save btn btn-default btn-xs').text('save').on('click', function () {
                self.obj.dataProvider.scopes.deleteScope(self.context.dataset, self.context.scope);
                self.context.scope = graphScopes.select('input').property('value');
                self.saveScope();
                self.getDataAndRefresh();
            });

            graphScopesEnter.append('span').attr('class', 'hidden new-scope-cancel btn btn-default btn-xs').text('cancel').on('click', function () {
                graphScopes.selectAll('.new-scope-name, .new-scope-save, .new-scope-cancel').classed('hidden', true);
            });

            graphScopesEnter.append('div').attr('class', 'btn-default btn-xs').text('fullscreen').on('click', function () {
                fullScreenApi.toggle(self.container.node());
            });
        }
        /**
         * Refresh scopes display
         */

    }, {
        key: 'refreshScopes',
        value: function refreshScopes() {
            var self = this;
            var scopes = self.obj.scopes;

            var menu = [{
                title: function title(d) {
                    return d ? d.key || 'test' : 'toto';
                }
            }, {
                divider: true
            },
            // Action to remove the graph selected
            {
                title: 'Edit <i class="hide-icon glyphicon glyphicon-pencil"></i>',
                action: function action(elm, d) {
                    self.container.selectAll('.new-scope-name').property('value', d.key);
                    self.container.selectAll('.new-scope-name, .new-scope-save, .new-scope-cancel').classed('hidden', false);
                }
            },
            // Action to remove the graph selected
            {
                title: 'Hide <i class="hide-icon glyphicon glyphicon-eye-close"></i>',
                action: function action(elm, d) {
                    scopes[d.key].visible = false;
                    self.obj.dataProvider.scopes.saveScope(self.context.dataset, d.key, scopes[d.key]);
                    self.getDataAndRefresh();
                }
            },
            // Action to remove the graph selected
            {
                title: 'Destroy <i class="delete-icon glyphicon glyphicon-remove"></i>',
                action: function action(elm, d) {
                    self.obj.dataProvider.scopes.deleteScope(self.context.dataset, d.key);
                    self.context.scope = 'current';
                    self.getDataAndRefresh();
                }
            },
            // Action to remove the graph selected
            {
                title: 'Duplicate <i class="duplicate-icon glyphicon glyphicon-duplicate"></i>',
                action: function action() {
                    self.context.scope = self.context.scope + '-copy';
                    self.saveScope();
                    self.getDataAndRefresh();
                }
            }];

            var graphScopesData = self.dom.graphScopesList.selectAll('li.scope').data(d3.entries(scopes).filter(function (s) {
                return s.value.visible;
            }));

            var graphScopesDataEnter = graphScopesData.enter().append('li').attr('class', 'scope tab') // btn btn-default btn-xs')
            .on('click', function (d) {
                self.context.scope = d.key;
                self.getDataAndRefresh();
            }).on('dblclick', function (d) {
                self.container.selectAll('.new-scope-name').property('value', d.key);
                self.container.selectAll('.new-scope-name, .new-scope-save, .new-scope-cancel').classed('hidden', false);
            });

            graphScopesDataEnter.append('a').text(function (d) {
                return d.key;
            });
            graphScopesDataEnter.append('i').attr('class', 'glyphicon glyphicon-remove-circle').on('click', function (d) {
                scopes[d.key].visible = false;
                self.obj.dataProvider.scopes.saveScope(self.context.dataset, d.key, scopes[d.key]);
                self.getDataAndRefresh();
            });
            graphScopesData.exit().remove();

            self.dom.graphScopesList.selectAll('li.scope').classed('active', function (d) {
                return d.key === self.context.scope;
            }).on('contextmenu', d3ContextMenu(menu, {}, self.container)); // attach menu to element
            // TODO: find another way
            self.dom.graphScopesList.classed('hidden', !self._options.manageScopes);
            self.container.selectAll('.new-scope').classed('hidden', !self._options.manageScopes);
        }

        /**
         * Save current scope
         */

    }, {
        key: 'saveScope',
        value: function saveScope() {
            var self = this;

            if (!self._options.manageScopes) {
                return;
            }

            var colors = {};
            // prepare what we will save
            var containers = self.obj.viz.graphs.map(function (g) {
                g.data.forEach(function (d) {
                    colors[d.tsuid] = d.color;
                });
                return {
                    // store tsuids
                    tsuids: g.data.map(function (d) {
                        return d.tsuid;
                    }),
                    // store patterns
                    patterns: _.uniqBy(d3.merge(g.data.map(function (d) {
                        return d.patterns.map(function (p) {
                            return {
                                key: p.key,
                                activated: p.activated
                            };
                        });
                    })), 'key'),
                    // store if the graph is synced or not
                    synced: g._options.synced,
                    // store line styles
                    styles: g.data.map(function (d) {
                        return d.style;
                    }).filter(function (d) {
                        return !!d;
                    })
                };
            });
            // save scope using dataprovider
            self.obj.dataProvider.scopes.saveScope(self.context.dataset, self.context.scope, {
                containers: containers,
                key: self.context.scope,
                colors: colors,
                visible: true // visible by default
            });
        }

        /**
          * Sync graphs with current scope
          * @param  {Array}  graphs
          * @param  {Array} scopes
          * @param  {Object} result
          */

    }, {
        key: 'syncDisplayWithScope',
        value: function syncDisplayWithScope() {
            var graphs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var scope = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

            var self = this;
            var len = d3.max([(scope.containers || []).length, graphs.length]);
            var tmp = void 0,
                scopeContainer = void 0,
                graph = void 0,
                patterns = void 0,
                idxToRemove = [];
            var sharedMinMax = self.getSharedMinMax(data);
            for (var i = 0; i < len; i++) {
                graph = graphs[i];
                /*jshint loopfunc: true */
                scopeContainer = scope.containers[i];
                if (scopeContainer) {
                    tmp = ((scopeContainer || {}).tsuids || []).map(function (d) {
                        return Object.assign({}, data[d], {
                            patterns: _.cloneDeep(data[d].patterns)
                        });
                    });
                    tmp.forEach(function (d, i) {
                        // apply curve style from scope
                        d.style = (scopeContainer.styles || [])[i];
                        d.color = (scope.colors || {})[d.tsuid] || d.color;
                    });
                    patterns = {};
                    scopeContainer.patterns.forEach(function (p) {
                        patterns[p.key] = p.activated;
                    });
                    // if there is a scope but no graph / create graph
                    if (!graph) {
                        graph = self.obj.viz.addNewContainer([], self.inputs, false);
                    }
                    if (graph.activatedPatterns) {
                        graph.activatedPatterns(patterns);
                    }
                    graph.update(tmp, self.inputs);
                }
                // if there is no scope but a graph add index in queue to be removed
                else if (graph) {
                        idxToRemove.push(i);
                    }
                graph.options({ yMinMax: sharedMinMax }).on('reset', function () {
                    self.saveScope();
                    if (self.obj.heatmap) {
                        self.obj.heatmap.markedData(getPatternsMapping(self.obj.viz.getAllTs())).linkedContainers(self.obj.viz.containers());
                    }
                    self._renderMenu();
                });
            }
            // reverse idx to be sure to dont break next loop
            idxToRemove.reverse();
            // remove container in looking its position in all containers
            idxToRemove.forEach(function (r) {
                return self.obj.viz.removeContainer(r);
            });

            self.saveScope();
            self.refreshScopes();

            // TODO
            scope.containers.forEach(function (d, i) {
                self.obj.viz.syncItems[self.obj.viz.graphs[i].containerId] = d.synced;
            });
        }

        /**
         * Update viz data
         * @param  {Object} data - new data
         */

    }, {
        key: 'update',
        value: function update(data) {
            var self = this;
            var current = {},
                patterns = {};
            if (data) {
                self.data = data;
            }
            self.data = _.orderBy(self.data, function (d) {
                return parseInt(d.tsuid.replace(/[A-Za-z$-]/g, ''));
            });
            var colors = d3.scaleOrdinal(d3.schemeCategory20);
            for (var i = 0; i < self.data.length; i++) {
                current = self.data[i];
                current.patterns = current.patterns || [];
                var p = void 0;
                for (var j = 0; j < current.patterns.length; j++) {
                    p = current.patterns[j];
                    patterns[p.key] = patterns[p.key] || {
                        nb: 0,
                        key: p.key,
                        color: p.color
                    };
                    patterns[p.key].nb = +patterns[p.key].nb + p.locations.length;
                }
                // put a color if there is not already assigned
                current.color = current.color || colors(i);
            }
            self.patterns = _.orderBy(d3.values(patterns), 'key');
        }

        /**
         * Able to wakeup tsplorer
         * @memberof TSplorer
         */

    }, {
        key: 'wakeUp',
        value: function wakeUp() {
            this.logger.log('wakeup');
            this.display();
        }
    }]);

    return TSplorer;
}(VizTool);

module.exports = TSplorer;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/d3-context-menu/js/d3-context-menu.js","/home/davisp/projects/ikats_lig/hmi/node_modules/resize-observer-polyfill/dist/ResizeObserver.global.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/resize-observer-polyfill/dist/ResizeObserver.global.js","/home/davisp/projects/ikats_lig/hmi/src/CS/VizModule/VizTool.js":"/home/davisp/projects/ikats_lig/hmi/src/CS/VizModule/VizTool.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGHeatMap.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/LIGHeatMap.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/MultiCurves.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/MultiCurves.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/NameHelper.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/NameHelper.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/SideMenu.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/SideMenu.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/scopes.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/scopes.less","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/tsplorer.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/tsplorer.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/DataProvider.js":[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);
/* jshint unused: false */

/*
 * Interface DataProvider.
 * Tool to manage data requests for testing
 * @constructor
 */

var DataProvider = function () {
    function DataProvider(rootPath) {
        _classCallCheck(this, DataProvider);

        this.rootPath = rootPath;
    }

    ///
    /// DATASETS
    ///

    _createClass(DataProvider, [{
        key: 'getScopes',
        value: function getScopes() {
            throw new Error("getScopes method of DataProvider has to be overridden");
        }

        /**
         * Get specific dataset scopes
         * @param  {String}  dataset_id           - dataset identifier
         * @param  {String}  scope_id             - scope identifier
         * @param  {Boolean} generate_if_no_exist - option
         * @return {Object}                      [description]
         */

    }, {
        key: 'getScope',
        value: function getScope(dataset_id, scope_id, generate_if_no_exist) {
            throw new Error("getScope method of DataProvider has to be overridden");
        }

        /**
         * Save specific scope
         * @param  {String} datasetid - dataset identifier
         * @param  {String} scopeid   - scope identifier
         * @param  {Object} data      - data we want to save
         * @return {Object}           - data
         */

    }, {
        key: 'saveScope',
        value: function saveScope(scope_id) {
            throw new Error("saveScope method of DataProvider has to be overridden");
        }

        ///
        /// SCOPES
        ///

        /**
         * Get all dataset ids from database
         * @return {Promise} - ds_names
         */

    }, {
        key: 'getDatasetIds',
        value: function getDatasetIds() {
            throw new Error("getDatasetIds method of DataProvider has to be overridden");
        }

        /**
         * get specific dataset representation
         * @param  {String} ds_name - dataset name
         * @return {Promise} - dataset representation
         */

    }, {
        key: 'getDataset',
        value: function getDataset(ds_name) {
            throw new Error("getDataset method of DataProvider has to be overridden");
        }

        /**
        * Get metadata for a list of ts
        * @param  {Array} ts_list - list of ts
        * @return {Promise} dictionnary - {ts1: md, ts2: md}
        */

    }, {
        key: 'getMetaData',
        value: function getMetaData(ts_list) {
            throw new Error("getMetaData method of DataProvider has to be overridden");
        }

        /**
         * Get points
         * @param  {Array} ts_list  - list of ts
         * @param  {Integer} start  - start (date / index)
         * @param  {Integer} end    - end (date / index)
         * @param  {Integer} nbmax  - number of pixels
         * @return {Promise}          - array of points
         */

    }, {
        key: 'getPoints',
        value: function getPoints(ts_list, start, end, nbmax) {
            throw new Error("getPoints method of DataProvider has to be overridden");
        }
    }, {
        key: 'parseRegex',
        value: function parseRegex(regex) {
            var t = [];
            for (var i = 0; i < regex.length; i++) {
                var letter = regex[i];
                if (letter === '[') {
                    var idx = regex.indexOf(']', i);
                    letter = regex.substring(i + 1, idx);
                    i = idx;
                }
                t.push(letter);
            }
            return t;
        }
    }, {
        key: '_parseTableToLS',
        value: function _parseTableToLS(table) {
            var ls = [];
            table.headers.row.data.forEach(function (obs_id, i) {
                if (obs_id !== null) {
                    var idx = i - 1;
                    var target = table.content.cells[idx][table.content.cells[idx].length - 1];
                    var obj = {
                        obs_id: obs_id,
                        key: obs_id,
                        ts_list: [],
                        class: target
                    };
                    table.headers.col.data.forEach(function (headerKey, j) {
                        if (j !== 0) {
                            var link = table.content.links[idx][j - 1];
                            if (!link) {
                                obj[headerKey] = table.content.cells[idx][j - 1];
                            } else {
                                var ts = {
                                    funcId: link.val[0].funcId,
                                    tsuid: link.val[0].tsuid,
                                    key: obs_id,
                                    obs_id: obs_id,
                                    variable: headerKey,
                                    class: target
                                };
                                obj.ts_list.push(ts);
                            }
                        }
                    });
                    ls.push(obj);
                }
            });
            return ls;
        }
    }]);

    return DataProvider;
}();

module.exports = DataProvider;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/IkatsDataProvider.js":[function(require,module,exports){
(function (global){
'use strict';

// libs

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DataProvider = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/DataProvider.js"),
    d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    ikats = (typeof window !== "undefined" ? window['ikats'] : typeof global !== "undefined" ? global['ikats'] : null),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js"),
    treeHelper = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/TreeHelper.js");

// caches
var _cachedMetadatas = {},
    _cachedTables = {},
    _cachedPoints = {};

// constants
var VARIABLE_SEPARATOR = '-',
    REGEX_VARIABLE_SEPARATOR = '$',
    TSUID_VARIABLE_SEPARATOR = '#';

/*
 * Class DataProvider.
 * Tool to manage data requests in IKATS (CS) context
 * @constructor
 */

var IkatsDataProvider = function (_DataProvider) {
    _inherits(IkatsDataProvider, _DataProvider);

    function IkatsDataProvider(rootPath) {
        var _ret;

        _classCallCheck(this, IkatsDataProvider);

        var _this = _possibleConstructorReturn(this, (IkatsDataProvider.__proto__ || Object.getPrototypeOf(IkatsDataProvider)).call(this, rootPath));

        _this.colors = d3.scaleOrdinal(d3.schemeCategory10);
        _this.scopes = {
            getScopes: function getScopes() {
                return [];
            },
            getScope: function getScope() {
                return {};
            },
            saveScope: function saveScope() {},
            deleteScope: function deleteScope() {}
        };
        _this.metadatas = _cachedMetadatas;
        _this.tables = _cachedTables;
        _this.points = _cachedPoints;
        _this.logger = new LigLogger('ikats:data');
        return _ret = _this, _possibleConstructorReturn(_this, _ret);
    }

    /**
     * Helper to parse inputs from direct result / md / callbacks
     * @param {*} result - raw result as dict
     * @param {*} md - metadatas as dict
     * @param {*} callbacks - callbacks as dict
     */


    _createClass(IkatsDataProvider, [{
        key: '_parseInputs',
        value: function _parseInputs(result) {
            var md = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
            var callbacks = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

            var self = this;
            var inputs = {};
            if (result) {
                // ds_name
                if (Object.prototype.toString.call(result) === '[object String]') {
                    inputs = {
                        datasetId: result
                    };
                }
                // table
                else if (result.headers && result.content) {
                        var ls = self._parseTableToLS(result);
                        inputs = {
                            ts_list: d3.merge(ls.map(function (l) {
                                return l.ts_list;
                            })), // can be fit
                            patterns: [],
                            disc_break_points: [],
                            breakpoints: [],
                            variables: [], // can be fit
                            learning_set: ls
                        };
                    }
                    // rhm output
                    else if (callbacks && callbacks.inputs && callbacks.inputs.learningset) {
                            // parse table from inputs
                            var _ls = self._parseTableToLS(callbacks.inputs.learningset);
                            var breakpoints = result.variables.filter(function (d) {
                                return d.indexOf('-') === -1;
                            }).map(function (d, i) {
                                var t = result.break_points[i].map(function (r, j) {
                                    return {
                                        key: result.disc_break_points[i][j],
                                        min: r,
                                        max: result.break_points[i][j + 1] || r + Math.abs(r - result.break_points[i][j - 1])
                                    };
                                });
                                var map = {};
                                t.forEach(function (d) {
                                    return map[d.key] = d;
                                });
                                return {
                                    key: d,
                                    bps: t,
                                    map: map
                                };
                            });

                            var patterns = d3.values(result.patterns);
                            var minmax = result.disc_break_points.map(function (d) {
                                return '[' + d[0] + d[d.length - 1] + ']';
                            });
                            patterns.forEach(function (d) {
                                return d.regexPlain = d.regex.split(REGEX_VARIABLE_SEPARATOR).map(function (t, i) {
                                    return self.parseRegex(t.replace(/\./g, minmax[result.variables.indexOf(d.variable.split(VARIABLE_SEPARATOR)[i])]));
                                });
                            });

                            inputs = {
                                ts_list: d3.merge(_ls.map(function (l) {
                                    return l.ts_list;
                                })),
                                patterns: patterns,
                                disc_break_points: result.disc_break_points,
                                breakpoints: breakpoints,
                                variables: result.variables,
                                learning_set: _ls
                            };
                        }
            }
            return Object.assign({}, md, inputs);
        }
    }, {
        key: 'getScopes',
        value: function getScopes() /**datasetid**/{
            this.logger.warn('getScopes is not implmented for now in IkatsDataProvider');
            return [];
        }
    }, {
        key: 'getScope',
        value: function getScope() /**datasetid**/{
            this.logger.warn('getScopes is not implmented for now in IkatsDataProvider');
            return {};
        }
    }, {
        key: 'saveScope',
        value: function saveScope() {
            this.logger.warn('getScopes is not implmented for now in IkatsDataProvider');
        }

        /**
         * Get ts data
         * @param  {String} datasetid - dataset identifier
         * @param  {Object} params - scopes
         * @return {Object} dictionnary - {ts1: data, ts2: data}
         */

    }, {
        key: 'getDatasetIds',
        value: function getDatasetIds() {
            return new Promise(function (resolve, reject) {
                return ikats.api.ds.list({
                    async: true,
                    success: function success(result) {
                        resolve(result.data.map(function (d) {
                            return d.name;
                        }));
                    },
                    error: function error(e) {
                        reject("Could not get list of datasets.", e);
                    }
                });
            });
        }

        /**
         * Get dataset from a tree
         * @param  {String} ds_name
         * @param  {Object} _inputs
         * @return {Object} dataset
         */

    }, {
        key: 'getTreeDataset',
        value: function getTreeDataset(ds_name, _inputs) {
            return new Promise(function (resolve, reject) {
                if (_inputs && _inputs.header && _inputs.tree) {
                    resolve(treeHelper.parseTreeOutputAsDataset(_inputs, ds_name));
                } else {
                    reject('tree not found');
                }
            });
        }

        /**
         * Get tree result
         * @param  {String} ds_name
         * @param  {Object} _inputs
         * @return {Object} tree
         */

    }, {
        key: 'getTreeResult',
        value: function getTreeResult(ds_name, _inputs) {
            var self = this;
            return new Promise(function (resolve, reject) {
                if (_inputs && _inputs.header && _inputs.tree) {
                    return self.getTreeDataset(ds_name, _inputs).then(function (dataset) {
                        resolve(treeHelper.parseTreeOutput(_inputs, {}, dataset.colors, dataset.variableColors, dataset.raw));
                    }).catch(function (d) {
                        reject(d);
                    });
                } else {
                    reject('tree not found');
                }
            });
        }

        /**
         * Get dataset (base of the process)
         * @param  {String} ds_name - dataset name
         * @param  {Object} _inputs - additional data (touchy)
         * @return {Object}  dataset
         */

    }, {
        key: 'getDataset',
        value: function getDataset(ds_name, _inputs) {
            var self = this;
            var _metadata = {};
            if (_inputs && _inputs.header && _inputs.tree) {
                // tree case (TMP solution)
                return self.getTreeDataset(ds_name, _inputs);
            }
            return new Promise(function (resolve, reject) {
                // preprocessing inputs (table) if neeeded
                if (_inputs && _inputs.table_name) {
                    // if in inputs we have table_name, fetch it
                    var table_name = _inputs.table_name;
                    var table = self.tables[table_name];
                    if (!table) {
                        ikats.api.table.read({
                            table_name: table_name,
                            async: true,
                            success: function success(response) {
                                self.tables[table_name] = response.data;
                                resolve(response.data);
                            },
                            error: function error(e) {
                                reject("Could not get dataset." + ds_name, e);
                            }
                        });
                    } else {
                        resolve(table);
                    }
                } else {
                    resolve({});
                }
            }).then(function (table) {
                if (!table) {
                    return;
                }
                // parse data from table
                _metadata = self._parseInputs(_inputs, _inputs, { inputs: { learningset: table } });
                // apply changes on _inputs
                Object.assign(_inputs, _metadata);
            }).then(function () {
                // in case we have a ts_list in the inputs, dont need to get data by the common dataset request
                if (_metadata && _metadata.ts_list) {
                    return { ts_list: _metadata.ts_list };
                } else {
                    // else, ask the dataset by the api
                    return new Promise(function (resolve, reject) {
                        return ikats.api.ds.read({
                            ds_name: ds_name,
                            async: true,
                            success: function success(response) {
                                resolve(response.data);
                            },
                            error: function error(e) {
                                reject("Could not get dataset." + ds_name, e);
                            }
                        });
                    });
                }
            }).then(function (dataset) {
                // ask the metadata
                return self.getMetaData(dataset.ts_list, _inputs).then(function (metas) {
                    var classes = [],
                        variables = [],
                        mapping = {},
                        paired_ts = [];

                    // prepare ts_list
                    var ts_list = dataset.ts_list.map(function (d) {
                        var ts = Object.assign({}, { patterns: [], key: metas[d.tsuid].obs_id }, metas[d.tsuid], d);
                        if (variables.indexOf(ts.variable) === -1) {
                            variables.push(ts.variable);
                        }
                        if (classes.indexOf(ts.class) === -1) {
                            classes.push(ts.class);
                        }
                        if (mapping[ts.variable]) {
                            mapping[ts.variable].push(ts);
                        } else {
                            mapping[ts.variable] = [ts];
                        }
                        return ts;
                    });

                    var paired_patterns = _metadata.patterns.filter(function (pattern) {
                        return pattern.variable.indexOf(VARIABLE_SEPARATOR) > -1;
                    });

                    paired_patterns.forEach(function (pattern) {
                        d3.keys(pattern.locations).forEach(function (obs_id) {
                            paired_ts.push({
                                key: obs_id,
                                funcId: obs_id + '_' + pattern.variable,
                                tsuid: obs_id + '_' + pattern.variable,
                                class: pattern.class,
                                patterns: [],
                                variable: pattern.variable
                            });
                        });
                    });

                    return {
                        raw: ts_list.concat(paired_ts),
                        md: Object.assign({ datasetid: ds_name }, _metadata),
                        class: classes,
                        variables: _inputs.variables || variables,
                        patterns: _metadata.patterns || []
                    };
                });
            });
        }

        /**
         * Get ts data
         * @param  {String} datasetid - dataset identifier
         * @param  {Object} params - scopes
         * @return {Object} dictionnary - {ts1: data, ts2: data}
         */

    }, {
        key: 'getMetaData',
        value: function getMetaData(ts_list, _inputs) {
            var self = this;

            if (ts_list.length === 0) {
                self.logger.log('no item in ts_list, resolve empty array');
                return Promise.resolve([]);
            }

            var all_tsuids = ts_list.map(function (ts) {
                return ts.tsuid.split(TSUID_VARIABLE_SEPARATOR)[0];
            }),
                missed_tsuids = all_tsuids.filter(function (tsuid) {
                return !self.metadatas[tsuid];
            }),
                gmd0 = performance.now();

            return new Promise(function (resolve, reject) {

                if (missed_tsuids.length === 0) {
                    self.logger.log('no ajax request (md) needed, all md is already in cache');
                    resolve([]);
                }
                // look on metadaa in lookings the tree dataset
                else if (_inputs && _inputs.tree) {
                        return self.getTreeDataset(undefined, _inputs).then(function (d) {
                            d.raw.forEach(function (r) {
                                self.metadatas[r.tsuid] = {
                                    variable: r.variable,
                                    size: r.size,
                                    start: 0,
                                    end: r.size - 1,
                                    start_date: r.raw[0].timestamp,
                                    end_date: r.raw[r.size - 1].timestamp,
                                    min: r.meta_data.min_vals[0],
                                    max: r.meta_data.max_vals[0],
                                    obs_id: r.key,
                                    tsuid: r.tsuid,
                                    funcId: r.key,
                                    class: r.class,
                                    datasetId: _inputs.header.learningSet || _inputs.header.LearningSet // TODO -> check if this property is really needed and used ( seems not )
                                };
                            });
                            resolve(self.metadatas);
                        });
                    } else {
                        var aj0 = performance.now();
                        self.logger.log('ajax request (md) start for ' + missed_tsuids.length + ' sequences');

                        ikats.api.md.read({
                            ts_list: missed_tsuids,
                            async: true,
                            success: function success(metas) {
                                // When api call results in a success, resolve promise
                                var partial_result = {};

                                var classValue = void 0,
                                    identifierValue = void 0,
                                    newTmp = void 0;

                                metas.data.forEach(function (d) {
                                    var tmp = partial_result[d.tsuid] || {};
                                    tmp[d.name] = d.value;
                                    partial_result[d.tsuid] = tmp;
                                });

                                self.logger.log('ajax request (md) success (' + (performance.now() - aj0).toFixed(2) + ' ms)', partial_result);
                                var detected_errors = { start_date: [], end_date: [], min: [], max: [], metric: [], nb: [], diff: [] };
                                for (var key in partial_result) {
                                    if (partial_result.hasOwnProperty(key)) {
                                        var tmp = partial_result[key];
                                        var tmp2 = {};
                                        if (_inputs && _inputs.ts_list) {
                                            tmp2 = _inputs.ts_list[_inputs.ts_list.findIndex(function (d) {
                                                return d.tsuid === key;
                                            })]; // jshint ignore:line
                                        }
                                        classValue = tmp.class || 'c0';
                                        identifierValue = 'obs_id';

                                        if (_inputs && _inputs.population && _inputs.population.data) {
                                            identifierValue = _inputs.population.identifier || identifierValue;
                                            classValue = _inputs.population.data[tmp[identifierValue]] || classValue;
                                        }

                                        // CONTROL IF METADATAS ARE HERE
                                        if (!tmp.metric) {
                                            detected_errors.metric.push(key);
                                        }
                                        if (_.isUndefined(tmp.ikats_start_date)) {
                                            detected_errors.start_date.push(key);
                                        }
                                        if (_.isUndefined(tmp.ikats_end_date)) {
                                            detected_errors.end_date.push(key);
                                        }
                                        if (_.isUndefined(tmp.qual_min_value)) {
                                            detected_errors.min.push(key);
                                        }
                                        if (_.isUndefined(tmp.qual_max_value)) {
                                            detected_errors.max.push(key);
                                        }
                                        if (_.isUndefined(tmp.qual_nb_points)) {
                                            detected_errors.nb.push(key);
                                        }
                                        if (tmp.ikats_end_date === tmp.ikats_start_date) {
                                            detected_errors.diff.push([key, tmp.ikats_start_date, tmp.ikats_end_date]);
                                        }

                                        newTmp = {
                                            variable: tmp.metric,
                                            size: tmp.qual_nb_points, // quality stats
                                            start: 0,
                                            end: tmp.qual_nb_points - 1,
                                            start_date: tmp.ikats_start_date,
                                            end_date: tmp.ikats_end_date,
                                            min: tmp.qual_min_value, // quality stats
                                            max: tmp.qual_max_value, // quality stats
                                            obs_id: tmp[identifierValue] || tmp2[identifierValue],
                                            tsuid: key,
                                            key: tmp2.key,
                                            funcId: tmp.funcId || tmp2.funcId,
                                            class: classValue,
                                            datasetId: tmp.datasetId // TODO -> check if this property is really needed and used ( seems not )
                                        };
                                        self.metadatas[key] = newTmp;
                                    }
                                }

                                // ERRORS
                                var nbTs = d3.keys(self.metadatas).length;
                                if (detected_errors.metric.length > 0) {
                                    self.logger.error('metadata \'metric\' must be defined for ' + detected_errors.metrics.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.start_date.length > 0) {
                                    self.logger.error('metadata \'ikats_start_date\' must be defined for ' + detected_errors.start_date.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.end_date.length > 0) {
                                    self.logger.error('metadata \'ikats_end_date\' must be defined for ' + detected_errors.end_date.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.min.length > 0) {
                                    self.logger.error('metadata \'qual_min_value\' must be defined for ' + detected_errors.min.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.max.length > 0) {
                                    self.logger.error('metadata \'qual_max_value\' must be defined for ' + detected_errors.max.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.nb.length > 0) {
                                    self.logger.error('metadata \'qual_nb_points\' must be defined for ' + detected_errors.diff.length + ' TS on ' + nbTs);
                                }
                                if (detected_errors.diff.length > 0) {
                                    self.logger.error('metadata \'ikats_start_date\' and \'ikats_end_date\' shouldnt be the same for ' + detected_errors.diff.length + ' TS on ' + nbTs);
                                }

                                resolve(partial_result);
                            },
                            error: function error(e) {
                                self.logger.warn("Could not get metadata for given TS list.", missed_tsuids);
                                self.logger.error(e);
                                reject("Could not get metadata for given TS list.", missed_tsuids);
                            } });
                    }
            }).then(function () {
                var full_result = {};
                all_tsuids.forEach(function (tsuid) {
                    return full_result[tsuid] = self.metadatas[tsuid];
                });
                self.logger.log('get metadata done (' + (performance.now() - gmd0).toFixed(2) + ' ms)', full_result);
                return full_result;
            });
        }

        /**
         * Get sample aggreagted points for curves to represent them in specific amount of points
         * @param  {String}  datasetid - dataset identifier
         * @param  {Array}   tsuids    - array of ts identifiers
         * @param  {Integer} start     - first index that we start the cut
         * @param  {Integer} end       - last index that we stop the cut
         * @param  {Integer} nbmax     - number of aggregated points
         * @return {Array}             - set of points
         */

    }, {
        key: 'getPoints',
        value: function getPoints(ts_list, start, end, nbmax, diff, mode, inputs) {

            var self = this,
                ajaxRq = [],
                gp0 = performance.now();
            diff = Math.abs(end - start);

            if (_.isUndefined(start)) {
                self.logger.error('start must be defined');
            }
            if (_.isUndefined(end)) {
                self.logger.error('end must be defined');
            }
            if (_.isUndefined(nbmax)) {
                self.logger.error('nbmax must be defined');
            }

            self.logger.log('get points start', start, end, nbmax, ts_list);
            var mainPromise = new Promise(function (resolve, reject) {
                if (inputs && inputs.tree) {
                    return self.getTreeDataset('', inputs).then(function (dataset) {
                        dataset.raw.forEach(function (r) {
                            return self.points[r.tsuid] = r.raw;
                        });
                        resolve(ts_list.map(function (ts) {
                            return self.points[ts.tsuid];
                        }));
                    });
                }
                // Keep track of downsampled timeseries for information display
                var promises = [];
                var total_period = Math.abs(end - start);
                // Calculate dp considering data + out of range and the current number of TS
                // TODO ->if Improve it / Understand it
                var dp = total_period / nbmax;
                //total_period / (10000 / (ts_list.length * 2));
                ts_list.forEach(function (datum) {

                    var promise = new Promise(function (resolve2) {
                        var tsuid = datum.tsuid.split(TSUID_VARIABLE_SEPARATOR)[0];
                        self.logger.log('ajax request (points) start', tsuid, start, end);
                        var aj0 = performance.now();
                        ajaxRq.push(ikats.api.ts.read({
                            tsuid: tsuid,
                            async: true,
                            da: dp && 'avg' || null,
                            dp: dp,
                            sd: start,
                            ed: end,
                            md: [{
                                tsuid: tsuid,
                                name: 'ikats_start_date',
                                value: parseInt(start - diff / 2) //datum.md.start_date,
                            }, {
                                tsuid: tsuid,
                                name: 'ikats_end_date',
                                value: parseInt(end + diff / 2) //datum.md.end_date,
                            }],
                            success: function success(ts_points) {
                                self.logger.log('ajax request (points) success (' + (performance.now() - aj0).toFixed(2) + ' ms)', datum.tsuid);
                                var tmp = d3.entries(ts_points.data).map(function (d) {
                                    return {
                                        value: d.value,
                                        timestamp: +d.key
                                    };
                                });
                                resolve2(tmp);
                            },
                            error: function error(e) {
                                self.logger.error('ajax request (points) with error', e);
                                reject(e);
                            }
                        }));
                    });
                    promises.push(promise);
                });

                Promise.all(promises).then(function (values) {
                    self.logger.log('get ' + values.length + ' points done (' + (performance.now() - gp0).toFixed(2) + ' ms)');
                    resolve(values);
                });
            });
            mainPromise.cancel = function () {
                ajaxRq.forEach(function (d) {
                    return d.abort();
                });
            };
            return mainPromise;
        }
    }]);

    return IkatsDataProvider;
}(DataProvider);

module.exports = IkatsDataProvider;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/DataProvider.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/DataProvider.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/TreeHelper.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/TreeHelper.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/Data/TreeHelper.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

var initTimeStamp = +new Date().getTime(),
    d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    variableSeparator = '$',
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js"),
    logger = new LigLogger('lig-tree'),
    colorManager = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/colorManager.js"),
    patternColorsDomain = d3.schemeCategory10;

/**
 * Generate a fake tree using a set of examples
 * @param  {Array} examples
 * @return {Object} Tree - Root node
 */
var _generateFakeTree = function _generateFakeTree(examples) {
    var idFn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
    var sil = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'toto';
    var colors = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];

    // randomize order
    examples = d3.shuffle(examples);

    var clss = Array.from(new Set(examples.map(function (d) {
        return d.class;
    })));
    clss.sort();

    var repartition = clss.map(function (name) {
        var nb = examples.filter(function (d) {
            return d.class === name;
        }).length;
        return {
            nb: nb,
            pourcent: nb / examples.length * 100,
            class: name,
            color: colors(name)
        };
    });

    if (examples.length === 1 || clss.length === 1) {
        return { type: 'leaf', examples_list: examples, case: sil, repartition: repartition, class: examples[0].class, id: idFn(), color: colors(examples[0].class) };
    }

    var nb = Math.floor(d3.randomUniform(0, 3)()),
        pattern;

    if (nb === 0) {
        var example = examples[examples.findIndex(function (e) {
            return e.ts_list.findIndex(function (ts) {
                return ts.patterns.length > 0;
            }) > -1;
        })];
        if (example) {
            var ts = example.ts_list[example.ts_list.findIndex(function (ts) {
                return ts.patterns.length > 0;
            })];
            if (ts) {
                pattern = ts.patterns[0];
            } else {
                nb = 1;
            }
        } else {
            nb = 1;
        }
    }

    var idx = Math.floor(examples.length / 2);
    var sets = [examples.slice(0, idx), examples.slice(idx, examples.length + 1)];

    if (pattern) {
        var hasPattern = examples.filter(function (e) {
            return e.ts_list.findIndex(function (t) {
                return t.patterns.findIndex(function (p) {
                    return p.regex === pattern.regex;
                }) > -1;
            }) > -1;
        });
        var hasNotPattern = examples.filter(function (e) {
            return e.ts_list.findIndex(function (t) {
                return t.patterns.findIndex(function (p) {
                    return p.regex === pattern.regex;
                }) > -1;
            }) === -1;
        });

        if (hasNotPattern.length > 0) {
            sets = [hasNotPattern, hasPattern];
        }
    }

    var idxs = [],
        charts = [],
        keys = [];

    while (nb !== charts.length) {
        idx = Math.floor(d3.randomUniform(0, examples.length)());
        if (idxs.indexOf(idx) === -1) {
            idxs.push(idx);
            keys.push(examples[idx].key);
            // for showed charts copy only ts without pair variables
            charts.push(examples[idx].ts_list.filter(function (d) {
                return d.variable.split('-').length === 1;
            }).map(_.clone));
        }
    }

    var nodeType = keys.length > 1 ? 'comparison' : 'similarity',
        subject = keys.length > 1 ? 'comparison (' + keys[0] + '/' + keys[1] + ')' : 'similarity with ' + keys[0];

    if (pattern) {
        nodeType = 'pattern';
        subject = 'pattern ' + pattern.regex;
    }

    return {
        type: 'node',
        id: idFn(),
        nodeType: nodeType,
        pattern: pattern,
        subject: subject,
        examples: examples,
        repartition: repartition,
        case: sil,
        children: sets.map(function (d, i) {
            var txt = nodeType === 'comparison' && examples.length > 1 ? examples[i].key + ' (' + examples[i].class + ')' : Math.random();
            return _generateFakeTree(d, idFn, txt, colors);
        }),
        viz: charts
    };
};

/**
 * Convert regex to an array of discretized intervals
 * @param  {String} regex
 * examples  G.G   -> ['G', '.', 'G']
 *          [A-D]G -> ['AD', 'G']
 * @return {Array}       [description]
 */
var parseRegex = function parseRegex(regex) {
    var t = [];
    for (var i = 0; i < regex.length; i++) {
        var letter = regex[i];
        if (letter === '[') {
            var idx = regex.indexOf(']', i);
            letter = regex.substring(i + 1, idx).replace('-', '');
            i = idx;
        }
        t.push(letter);
    }
    return t;
};

/**
 * Parse recursively a node and its children, and convert it as a common tree node model
 *
 * @param {*} node
 * @param {*} node_case_mapping
 * @param {*} colors
 * @param {*} patternColors
 * @param {*} [examples=[]]
 * @param {*} [definitions=[]]
 * @param {string} [sil='not found']
 * @returns {object} computed _ode
 */
var _parseNode = function _parseNode(node, node_case_mapping, colors, patternColors, examples, definitions, sil) {
    if (!examples) {
        examples = [];
    }
    if (!definitions) {
        definitions = [];
    }
    if (!sil) {
        sil = 'not found';
    }
    // init node
    var computedNode = {
        id: node.node,
        type: node.type !== 'leaf' ? 'node' : 'leaf',
        nodeType: node.type,
        subject: node.name,
        case: sil,
        evaluation: node.evaluation,
        distance: node.distance,
        viz: [],
        raw: node
    };
    if (node_case_mapping[computedNode.id]) {
        computedNode.case = node_case_mapping[computedNode.id];
    }
    // in case of 'similarity' and 'comparison' node
    if (node.type !== 'leaf') {
        // assign children in looking definitions
        computedNode.children = node.description.criteria.map(function (c) {
            var def = definitions[definitions.findIndex(function (d) {
                return d.node === c.child;
            })];
            return _parseNode(def, node_case_mapping, colors, patternColors, examples, definitions);
        });
    } else {
        // assign class
        computedNode.class = node.label;
        computedNode.color = colors(computedNode.class);
    }

    var chartIds = [];

    var _getSequenceId = function _getSequenceId(v) {
        if (!_.isArray(v)) {
            logger.warn('sequence should folloow new format (node.description.sequence)');
            return v;
        } else {
            return v[0];
        }
    };

    if (node.type === 'similarity') {
        chartIds.push(_getSequenceId(node.description.sequence));
    } else if (node.type === 'comparison') {
        node.description.criteria.forEach(function (c) {
            return chartIds.push(_getSequenceId(c.sequence));
        });
    } else if (node.type === 'pattern') {
        var pattern = node.description.pattern;

        var vocabulary = void 0;
        if (pattern.domain) {
            // find direct vocabulary
            vocabulary = pattern.domain;
            //logger.log('find domain', domain);
        } else {
            // compute vocabulary
            vocabulary = pattern.variables.map(function (variable) {
                return variable.breakpoints.map(function (d, i) {
                    return 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[i];
                });
            });
            //logger.log('compute domain in looking breakpoints', pattern.variables, domain);
        }

        var regexPlain = pattern.definition.split(variableSeparator).map(function (d, i) {
            return parseRegex(d.replace(/\./g, '[' + vocabulary[i][0] + '-' + vocabulary[i][vocabulary[i].length - 1] + ']'));
        });

        computedNode.pattern = {
            regex: pattern.definition.split(variableSeparator),
            bps: vocabulary.map(function (a) {
                return a.map(function (d) {
                    return {
                        key: d,
                        value: d
                    };
                });
            }),
            color: pattern.variables.map(function (v) {
                return patternColors(v.name);
            }),
            regexPlain: regexPlain,
            length: pattern.length || regexPlain[0].length,
            variables: pattern.variables.map(function (v) {
                return v.name;
            })
        };
        //logger.log('pattern description', pattern);
        //logger.log('computedNode pattern', computedNode.pattern, node.description.pattern);
    }

    var total = d3.sum(node.statistics.map(function (d) {
        return d[1];
    }));
    node.statistics.forEach(function (s) {
        return s[2] = s[2] || s[1] / total;
    });
    // assign repartition
    computedNode.repartition = node.statistics.map(function (s) {
        return {
            class: s[0],
            nb: s[1],
            pourcent: s[2] * 100,
            color: colors(s[0])
        };
    });
    if (node.statistics.length === 1) {
        computedNode.class = node.statistics[0][0];
    }
    //logger.log('chartIds', chartIds, examples, node);
    // find all concerned examples data
    computedNode.viz = chartIds.map(function (c) {
        return [examples[examples.findIndex(function (e) {
            return e.key === c;
        })]];
    });
    //logger.log('computedNode viz', computedNode.viz);
    return computedNode;
};

/**
 * Parse tree input and convert it as a tree managed by the hmi
 *
 * @param {*} output
 * @param {*} nodeCaseMapping
 * @param {*} colors
 * @param {*} patternColors
 * @param {*} sequences
 * @returns
 */
var _parseTreeOutput = function _parseTreeOutput(output, nodeCaseMapping, colors, patternColors, sequences) {
    output.tree.forEach(function (d) {
        if (d.type === 'similarity' || d.type === 'variable' || d.type === 'pattern') {
            d.description.criteria.forEach(function (c) {
                // ex : '> 100'
                var value = c.value;
                if (_.isUndefined(value)) {
                    value = c.threshold;
                }
                //logger.log('description criteria', c, v);
                nodeCaseMapping[c.child] = c.test + ' ' + value;
            });
        } else if (d.type === 'comparison') {
            d.description.criteria.forEach(function (c) {
                // sequence for now, ex: 'TS_01'
                nodeCaseMapping[c.child] = c.sequence;
            });
        }
    });
    return _parseNode(output.tree[0], nodeCaseMapping, colors, patternColors, sequences, output.tree);
};

/**
 * Parse Tree Output to the dataset that use to be managed by the hmi
 *
 * @param {*} output
 * @param {*} fileName
 * @returns
 */
var _parseTreeOutputAsDataset = function _parseTreeOutputAsDataset(output, fileName) {
    var classMapping = {},
        variableMapping = {},
        classes = output.tree[0].statistics.map(function (d) {
        return d[0];
    }); // get classes in looking statistics from the root node

    output.data = output.data || [];
    output.validation = output.validation || {};

    classes.sort(); // sort classes by its value

    output.tree.forEach(function (node) {
        node.observations = node.observations || [];
        if (node.type === 'leaf') {
            // compute label if there is no label property, (in looking statistics)
            if (!node.label && node.statistics.length === 1) {
                node.label = node.statistics[0][0];
            }
            // populate classMapping if there is a label
            if (node.label && node.observations) {
                node.observations.forEach(function (obsKey) {
                    return classMapping[obsKey] = node.label;
                });
            }
        } else if (node.type === 'similarity') {
            // in case of similarity populate variableMapping with variable and sequence from description
            variableMapping[node.description.sequence] = node.description.variable;
        } else if (node.type === 'comparison') {
            // in case of comparison populate variableMapping with variable and criteria (n sequences) from description
            node.description.criteria.forEach(function (c) {
                return variableMapping[c.sequence] = node.description.variable;
            });
        }
    });

    // use variableMapping values to define variables and classes from the dataset
    var variables = Array.from(new Set(d3.values(variableMapping)));

    var colors = d3.scaleOrdinal().range(classes.map(function (d) {
        return colorManager.getColor(d, output.header.learningSet || output.header.LearningSet || 'common');
    })).domain(classes);

    var variableColors = d3.scaleOrdinal().range(patternColorsDomain).domain(variables);

    output.data.forEach(function (d) {
        d.class = d.class || d.label || classMapping[d.sequence];
        d.meta_data = {
            max_vals: [d3.max(d.value)],
            min_vals: [d3.min(d.value)]
        };
        d.variable = variableMapping[d.sequence];
        d.key = d.sequence;
        d.tsuid = d.sequence + '-' + d.variable;
        d.raw = (d.value || []).map(function (v, i) {
            return {
                pos: i,
                idx: i,
                timestamp: i * 10000 + initTimeStamp,
                value: v,
                weight: d.weight ? (d.weight[1] || d.weight[0] || [])[i] : null // FIXME: wrong concept
            };
        });
        d.funcId = 'tdt_' + fileName + '-' + d.key, d.color = colors(d.class); // jshint ignore:line
        d.size = d.raw.length;
        d.patterns = [];
    });

    output.validation = output.validation || {};
    // loop on traces
    output.validation.trace = (output.validation.trace || []).map(function (trace_entry) {
        // get the last node
        var trace_nodes = trace_entry[2];
        var last_node = output.tree[output.tree.findIndex(function (d) {
            return d.node === trace_nodes[trace_nodes.length - 1];
        })];
        if (last_node) {
            // add at the end of the trace entry an array of labels
            trace_entry.push(_.sortBy(last_node.statistics, function (s) {
                return s[1];
            }).map(function (s) {
                return s[0];
            }).reverse());
        }
        return trace_entry;
    });

    return {
        raw: output.data,
        validation: output.validation,
        md: {
            variables: variables,
            hasWeights: (output.data[0] || {}).weight && (output.data[0] || {}).weight.length > 0,
            date: output.header.date,
            learningSet: output.header.learningSet,
            version: output.header.TDTFormatVersion
        },
        class: classes,
        colors: colors,
        variableColors: variableColors,
        variables: variables,
        patterns: []
    };
};

module.exports = {
    generateFakeTree: _generateFakeTree,
    parseTreeOutput: _parseTreeOutput,
    parseTreeOutputAsDataset: _parseTreeOutputAsDataset
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/colorManager.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/colorManager.js"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js":[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// less integration
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/fullscreen.less");

var FullScreenApiClass = function () {
    function FullScreenApiClass() {
        _classCallCheck(this, FullScreenApiClass);
    }

    /**
     * Get into full screen
     *
     * @param {HTMLElement} element
     * @memberof FullScreenApiClass
     */


    _createClass(FullScreenApiClass, [{
        key: 'in',
        value: function _in(element) {
            var _this = this;

            element.classList.add('fullscreen');

            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }

            var callback = function callback() {
                if (!_this.isInFullScreen(element)) {
                    document.removeEventListener("fullscreenchange", callback);
                    document.removeEventListener("mozfullscreenchange", callback);
                    document.removeEventListener("webkitfullscreenchange", callback);
                    element.classList.remove('fullscreen');
                }
            };

            document.addEventListener("fullscreenchange", callback);
            document.addEventListener("mozfullscreenchange", callback);
            document.addEventListener("webkitfullscreenchange", callback);
        }

        /**
         * Get out of full screen
         *
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'out',
        value: function out() {
            document.querySelectorAll('.fullscreen').forEach(function (htmlElement) {
                htmlElement.classList.remove('fullscreen');
            });
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

        /**
         * Get the node of the current fullscreen element
         *
         * @returns {HTMLElement}
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'getFullScreenElement',
        value: function getFullScreenElement() {
            // Returns the DOM Node of the fullscreen element
            return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        }

        /**
         * Check if there is already a fullscreen element
         *
         * @param {HTMLElement} element - if there is no element, check the whole document
         * @returns {Boolean}
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'isInFullScreen',
        value: function isInFullScreen(element) {
            if (element) {
                // check if the current full screen element is same as 'element'
                return this.getFullScreenElement() === element;
            }
            // check the whole document
            return document.fullscreen || document.webkitIsFullScreen || document.mozFullScreen;
        }

        /**
         * Toggle fullscreen state
         *
         * @param {any} element
         * @memberof FullScreenApiClass
         */

    }, {
        key: 'toggle',
        value: function toggle(element) {
            var self = this;
            if (self.isInFullScreen(element)) {
                self.out();
            } else {
                self.in(element);
            }
        }
    }]);

    return FullScreenApiClass;
}();

// entry point


var fullScreenApi = new FullScreenApiClass();
module.exports = fullScreenApi;

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/fullscreen.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/fullscreen.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);
// constant used to manage shared debug mode
window.LIG_DEBUG = false;

var _namespaces = new Set();

/**
 * LigLogger.
 * Class to manager logging with a specific context
 * @constructor
 */

var LigLogger = function () {
    function LigLogger(name) {
        var activate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        _classCallCheck(this, LigLogger);

        this.name = name;

        // keep name in namespaces
        _namespaces.add(name);

        // use name to know wich color to use
        this.namespace_color = d3.schemeCategory20b[Array.from(_namespaces).indexOf(name)];
        this.title_style = 'color: ' + this.namespace_color + '; font-weight: bold';
        // default behavior
        this.unactivate();

        // if activate && LIG_DEBUG is True activate
        if (activate && window.LIG_DEBUG) {
            this.activate();
        }

        // generic error managmeent
        this.error = Function.prototype.bind.call(console.error, console);
    }

    /**
     * Activate logging in attaching log / error / warn / debug function
     * @memberOf LigLogger
     */


    _createClass(LigLogger, [{
        key: 'activate',
        value: function activate() {
            // custom log title
            this.log = Function.prototype.bind.call(console.log, console, '%c' + this.name, this.title_style);
            this.error = Function.prototype.bind.call(console.error, console);
            this.warn = Function.prototype.bind.call(console.warn, console);
            this.debug = Function.prototype.bind.call(console.debug, console);
        }

        /**
         * Unactivate logging for log / warn / debug function
         * @memberOf LigLogger
         */

    }, {
        key: 'unactivate',
        value: function unactivate() {
            this.log = this.warn = this.debug = function () {};
        }
    }]);

    return LigLogger;
}();

module.exports = LigLogger;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/NameHelper.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

/**
 * Helper to get max width
 * @param  {Object} d3Element - d3 element
 * @param  {Array} strings    - array of strings wich will be displayed
 * @return {Integer}          - the max width of the text (in pixels)
 */
function getNewName(strings) {
  var defaultRootName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'new';

  var idx = d3.max([1, d3.max(strings.filter(function (s) {
    return s.indexOf('new') > -1;
  }).map(function (s) {
    return parseInt(s.replace(/[A-Za-z$-]/g, ''));
  }))]) + 1;
  return defaultRootName + idx;
}

module.exports = getNewName;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/ScaleHelper.js":[function(require,module,exports){
(function (global){
'use strict';

/*jslint node: true*/

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

/**
 * Helper to get the scale transform
 * @param  {HTMLNode} elem - html node element
 * @return {Arrays}        - [scaleX, scaleY]
 */
function getScaleTransform(elem) {
    var boundingClientRect = elem.getBoundingClientRect();
    return [boundingClientRect.width / elem.offsetWidth, boundingClientRect.height / elem.offsetHeight];
}

/**
 * Helper to get the scale X
 * @param  {HTMLNode} elem - html node element
 * @return {Float}         - scaleX
 */
function getScaleTransformX(elem) {
    return getScaleTransform(elem)[0];
}

/**
 * Helper to get the scale Y
 * @param  {HTMLNode} elem - html node element
 * @return {Float}         - scaleY
 */
function getScaleTransformY(elem) {
    return getScaleTransform(elem)[1];
}

/**
 * Helper to get the scale Y
 * @param  {HTMLNode} elem - html node element that we will use as context
 * @return {Arrays}        - coords
 */
function getUnscaledMousePosition(elem) {
    var coords = d3.mouse(elem),
        scale = getScaleTransform(elem);
    return [coords[0] * (1 / scale[0]), coords[1] * (1 / scale[1])].map(Math.round);
}

module.exports = {
    getScaleTransform: getScaleTransform,
    getScaleTransformX: getScaleTransformX,
    getScaleTransformY: getScaleTransformY,
    getUnscaledMousePosition: getUnscaledMousePosition
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/SvgTextWidth.js":[function(require,module,exports){
(function (global){
'use strict';

var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
/*jslint node: true*/

/**
 * Helper to get max width
 * @param  {Object} d3Element - d3 element
 * @param  {Array} strings    - array of strings wich will be displayed
 * @return {Integer}          - the max width of the text (in pixels)
 */
function getMaxTextWidth(d3Element, strings) {
	// generate a text tag for the experiment
	var txtSvg = d3Element.append('text');
	// get the biggest string
	var biggest = _.maxBy(strings, function (d) {
		return d.length;
	});
	// set text then look on the computed text length
	var max = txtSvg.text(biggest).node().getComputedTextLength();
	// remove the text tag
	txtSvg.remove();
	return max;
}

module.exports = getMaxTextWidth;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/colorManager.js":[function(require,module,exports){
(function (global){
'use strict';

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

var COLORS = ["#007CDB", "#FA8345", "#0AB45A", "#DB1A43", "#00B3FA", "#AF1AD8", "#F0E200", "#14D2DC", "#FA78FA", "#017C93", "#89FA76", "#FACCA5", "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2"],
    _cachedColors = {};

/**
 * Helper to centralize color getter
 * @param {String} label
 */

var getColor = function getColor(label) {
    var group = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "common";

    var colors = _cachedColors[group] || {};
    if (!colors[label]) {
        var new_id = d3.keys(colors).length;
        if (new_id > COLORS.length - 1) {
            new_id = new_id - Math.trunc(new_id / COLORS.length) * COLORS.length;
            console.warn('new id ' + d3.keys(colors).length + ' for label "' + label + '" inside group "' + group + '" is out of ' + (COLORS.length - 1) + 'cached colors, new id choosen ' + new_id);
        }
        colors[label] = COLORS[new_id];
        _cachedColors[group] = colors;
    }
    return colors[label];
};

module.exports = {
    getColor: getColor
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/SideMenu.js":[function(require,module,exports){
(function (global){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null),
    resizable = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/resizable.js"),
    LigLogger = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js"),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
    _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null),
    fullScreenApi = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js"),
    keyMaster = require("/home/davisp/projects/ikats_lig/hmi/node_modules/keymaster/keymaster.js"),
    select2 = require("/home/davisp/projects/ikats_lig/hmi/node_modules/select2/dist/js/select2.js");

// bind select2 plugin with jquery
select2(window, $);

// less integration
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/menu.less");

/**
 * FixedMenu.
 * Widget showing different items that user can interact with it
 *
 * @constructor
 * @param {Object} selection - d3 element
 */

var SideMenu = function () {
    function SideMenu(selection) {
        _classCallCheck(this, SideMenu);

        this.name = "FixedMenu";
        this.dispatch = d3.dispatch('change', 'resize-x', 'resize-x-move', 'layoutchange');
        this.container = selection;
        this._options = {
            collasped: false,
            width: 200,
            minResizeHeight: 50, // used by resizable
            fitBottom: true // used to know if we fit the last resizable element
        };
        this.logger = new LigLogger('lig-sidemenu');
        this.dom = {
            menuContainer: null,
            container: selection,
            button: null,
            list: null
        };
        this.obj = {
            // items that we will display
            items: [],
            // list will be display only if this property is equal to true
            isCollasped: false,
            // additional informations
            infos: [],
            blocs: [],
            collaspedBlocs: {},
            cache: {}
        };
    }
    /**
     * Update the d3 container node
     *
     * @param {any} containerNode - d3 container node
     * @memberof FixedMenu
     */


    _createClass(SideMenu, [{
        key: 'setContainer',
        value: function setContainer(d3ContainerNode) {
            var self = this;
            self.container = d3ContainerNode;
            self.dom.container = d3ContainerNode;
            return self;
        }

        /**
         * Update/Get blocs
         *
         * @param {Arrays} _ - blocs
         * @memberof FixedMenu
         */

    }, {
        key: 'blocs',
        value: function blocs(_) {
            if (!arguments.length) {
                return this.obj.blocs;
            }
            this.obj.blocs = _;
            return this;
        }

        /**
         * Refresh the menu
         *
         * @memberof FixedMenu
         */

    }, {
        key: 'refresh',
        value: function refresh() {
            var self = this;
            self.logger.log('refresh');

            var refreshSelect2 = function refreshSelect2($elem, d) {
                // TODO: find a way to get a good rendering and reset hacky style (see menu.less)
                $elem.select2({ theme: "bootstrap", dropdownAutoWidth: true }) // select 2 with bootstrap theme
                .off('select2:select') // reset previous event handler
                .on('select2:select', function (e) {
                    // bind event handler
                    this.value = e.params.data.id; // copy value to dom
                    return d.onChange ? d.onChange(d, this) : null; // call callback
                }).off('select2:open') // reset previous event handler
                .on('select2:open', function () {
                    // bind event handler
                    setTimeout(function () {
                        // set val with the one from the cache then trigger input in order to let widget redrawing
                        $('.select2-search--dropdown input').val(self.obj.cache[d.label] || '').trigger('input');
                    });
                }).off('select2:closing') // reset previous event handler
                .on('select2:closing', function () {
                    // bind event handler
                    self.obj.cache[d.label] = $('.select2-search--dropdown input').val();
                });
            };

            var refreshOptions = function refreshOptions(elem, values, value) {
                var options = elem.selectAll('option').data(values);
                var optionsEnter = options.enter().append('option');
                options.merge(optionsEnter).attr('value', function (t) {
                    return t;
                }).text(function (t) {
                    return t;
                });
                options.exit().remove();
                elem.property('value', value);
            };

            var menuContainer = self.dom.container.selectAll('.slide.menu').data([[]]).enter().append('div').attr('nochilddrag', 'nochilddrag').classed('menu', true).classed('slide', true);

            menuContainer.append('span').attr('class', 'btn-default menu__button btn btn-xs').on('click', function () {
                self.obj.isCollasped = !self.obj.isCollasped;
                self.dispatch.call('change', self);
                self.refresh();
            }).append('i').attr('class', 'glyphicon glyphicon-align-justify');

            self.dom.menuContainer = self.dom.container.select('.slide.menu');
            var bloc = self.dom.menuContainer.selectAll('.bloc').data(self.obj.blocs, function (d) {
                return d.title;
            });

            bloc.enter().append('div').attr('class', 'bloc').each(function (d) {
                self.logger.log('create bloc');

                var collapser = d3.select(this).append('span').attr('class', 'collapser').on('click', function (d) {
                    if (d.collapsable) {
                        self.obj.collaspedBlocs[d.title] = !self.obj.collaspedBlocs[d.title];
                        d3.select(this.parentNode).classed('collapsed', self.obj.collaspedBlocs[d.title]);
                        setTimeout(function () {
                            self.dispatch.call('layoutchange', self);
                        }, 500);
                    }
                }).classed('collapsable', function (d) {
                    return d.collapsable === true;
                });

                collapser.append('span').attr('class', 'bloc-title').text(d.title).classed('hidden', function (d) {
                    return d.showTitle === false;
                });

                collapser.append('i').attr('class', 'glyphicon glyphicon-chevron-up');
                collapser.append('i').attr('class', 'glyphicon glyphicon-chevron-down');

                var content = d3.select(this).append('div').attr('class', 'bloc-content');
                if (d.type === 'informations') {
                    content.append('table').attr('class', 'table table-condensed table-striped bloc-informations').append('tbody');
                } else {
                    content.append('ul').attr('class', 'list-group bloc-controls');
                }
            });
            bloc.exit().remove();
            self.dom.menuContainer.selectAll('.bloc').classed('collapsed', function (d) {
                return self.obj.collaspedBlocs[d.title];
            }).classed('hide-title', function (d) {
                return d.showTitle === false;
            }).classed('collapsable', function (d) {
                return d.collapsable;
            });

            // quickly helper to get block data from a blockname
            var _getData = function _getData(blocName) {
                return self.obj.blocs[self.obj.blocs.findIndex(function (d) {
                    return d.title === blocName;
                })].data;
            };

            self.dom.menuContainer.selectAll('.bloc-informations>tbody').each(function (d) {
                var infos = d3.select(this).selectAll('tr.information').data(_getData(d.title) || []);
                var infosEnter = infos.enter().append('tr').attr('class', 'information');

                infosEnter.append('td').attr('class', 'title');
                infosEnter.append('td').attr('class', 'text');

                var infosUpdate = infosEnter.merge(infos);
                infosUpdate.select('.title').classed('fullscreenable', function (d) {
                    return (d[2] || {}).fullscreenable === true;
                }).html(function (d) {
                    return d[0];
                }).on('click', function (d) {
                    if ((d[2] || {}).fullscreenable) {
                        fullScreenApi.toggle($(this).parent().find('td.text')[0]);
                    }
                });

                infosUpdate.select('.text').each(function (d) {
                    if ((d[2] || {}).compute) {
                        // if there is a compute function in options use it
                        d[2].compute(this, d[1]);
                    } else {
                        // if there is not compute function, fill html with the value
                        d3.select(this).html(function (d) {
                            return d[1];
                        });
                    }
                });
                infos.exit().remove();
            });

            self.dom.menuContainer.selectAll('.bloc-controls').attr('bloc-id', function (d) {
                return d.title;
            }).each(function (d) {
                // TODO: need to check why we are not able to get data directly
                var items = d3.select(this).selectAll('li').data(_getData(d.title), function (a) {
                    return a.help + a.label + a.type;
                });

                // ENTER
                var labelEnter = void 0;
                var itemsEnter = items.enter().append('li').attr('type', function (t) {
                    return t.type;
                }).classed('list-group-item', true).classed('inline', function (d) {
                    return d.inline;
                });

                // BUTTON enter
                itemsEnter.filter(function (d) {
                    return d.type === 'button';
                }).append('span').attr('class', 'btn-default btn btn-xs form-control');

                // BUTTONS enter
                itemsEnter.filter(function (d) {
                    return d.type === 'buttons';
                }).append('span').attr('class', 'btn-group form-control');

                // RANGE enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'range';
                }).append('label');
                labelEnter.append('span').text(function (d) {
                    return d.label;
                }).attr('class', 'bloc-control-label').classed('hidden', function (d) {
                    return d.hideLabel;
                });
                labelEnter.append('input').attr('type', 'range');

                // NUMBER enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'number';
                }).append('label');
                labelEnter.append('span').attr('class', 'bloc-control-label').text(function (d) {
                    return d.label;
                }).classed('hidden', function (d) {
                    return d.hideLabel;
                });

                labelEnter.append('input').attr('type', 'number').attr('class', 'form-control');

                // SELECT enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'select';
                }).append('label');
                labelEnter.append('span').text(function (d) {
                    return d.label;
                }).attr('class', 'bloc-control-label').classed('hidden', function (d) {
                    return d.hideLabel;
                });

                labelEnter.append('select').attr('class', 'form-control');

                // BOOLEAN enter
                var checkbox = itemsEnter.filter(function (d) {
                    return d.type === 'boolean';
                }).append('div').attr('class', 'checkbox');

                checkbox.append('label').append('input').attr('type', 'checkbox');

                checkbox.select('label').append('span').text(function (d) {
                    return d.label;
                });

                // SEARCH enter
                labelEnter = itemsEnter.filter(function (d) {
                    return d.type === 'search';
                }).append('label');
                labelEnter.append('span').text(function (d) {
                    return d.label;
                }).attr('class', 'bloc-control-label').classed('hidden', function (d) {
                    return d.hideLabel;
                });
                var searchFieldEnter = labelEnter.append('div').attr('class', 'search-field input-group');

                searchFieldEnter.filter(function (d) {
                    return d.showSelectAll;
                }).append('span').attr('class', 'input-group-addon addon-left').append('i').attr('class', 'glyphicon select-all btn btn-default btn-xs').on('click', function () {
                    d3.select($(this).parents('li')[0]).selectAll('tr.row-table:not(.hidden)').classed('selected', true);
                }).classed('glyphicon-check', true);
                searchFieldEnter.append('input').attr('type', 'search').attr('placeholder', function (c) {
                    return c.placeholder;
                }).property('value', function (d) {
                    return d.filterValue;
                }).attr('class', 'search-input form-control');
                searchFieldEnter.append('span').attr('class', 'input-group-addon').each(function (d) {
                    if (d.showCount) {
                        d3.select(this).append('span').attr('class', 'badge result-count').text(0);
                    } else {
                        d3.select(this).append('i').attr('class', 'glyphicon glyphicon-search');
                    }
                });

                var tableEnter = labelEnter.append('div').attr('class', 'search-table-container').append('table').attr('class', 'search-table table').append('tbody');
                tableEnter.append('tr').attr('class', 'header-arrows');
                tableEnter.append('tr').attr('class', 'drag-element hidden');

                // UPDATE
                var itemsUpdate = itemsEnter.merge(items);

                // BUTTON update
                itemsUpdate.filter(function (d) {
                    return d.type === 'button';
                }).select('span').text(function (d) {
                    return d.label;
                }).on('click', function (d) {
                    return d.onClick(d, this);
                }).classed('active', function (d) {
                    return d.active;
                }).each(function (d) {
                    var elem = d3.select(this);
                    (d.classed || []).forEach(function (classed) {
                        elem.classed(classed[0], function (sequence) {
                            return classed[1](sequence);
                        });
                    });
                });

                // BUTTONS update
                itemsUpdate.filter(function (d) {
                    return d.type === 'buttons';
                }).select('span').each(function (group) {
                    var d3Elem = d3.select(this);
                    var button = d3Elem.selectAll('button').data(group.data);
                    var buttonEnter = button.enter().append('button').attr('class', 'btn btn-default').text(function (b) {
                        return b.text;
                    });
                    button.merge(buttonEnter).classed('active', function (b) {
                        return b.active;
                    }).on('click', function (b) {
                        return group.onClick(b, this);
                    });
                });

                itemsUpdate.filter(function (d) {
                    return d.type === 'boolean';
                }).select('input').property('checked', function (d) {
                    return d.value;
                }).on('change', function (d) {
                    return d.onChange(d, this);
                });

                // SELECT update
                itemsUpdate.filter(function (d) {
                    return d.type === 'select';
                }).select('select').each(function (d) {
                    var $elem = $(this);
                    var elem = d3.select(this);
                    if (d.provider) {
                        d.provider().then(function (datasets) {
                            refreshOptions(elem, datasets, d.value);refreshSelect2($elem, d);
                        });
                    } else {
                        refreshOptions(elem, d.values, d.value);
                        refreshSelect2($elem, d);
                    }
                }).on('change', function (d) {
                    self.logger.log('change select');
                    return d.onChange ? d.onChange(d, this) : null;
                });

                // RANGE update
                itemsUpdate.filter(function (d) {
                    return d.type === 'range';
                }).select('input').attr('min', function (d) {
                    return d.interval[0];
                }).attr('max', function (d) {
                    return d.interval[1];
                }).attr('step', function (d) {
                    return d.step ? d.step : 'any';
                }).property('value', function (d) {
                    return d.value;
                }).attr('title', function (d) {
                    return d.value;
                }).on('change', function (d) {
                    return d.onChange ? d.onChange(d, this) : null;
                });

                // RANGE update
                itemsUpdate.filter(function (d) {
                    return d.type === 'number';
                }).select('input').attr('min', function (d) {
                    return d.interval[0];
                }).attr('max', function (d) {
                    return d.interval[1];
                }).attr('step', function (d) {
                    return d.step;
                }).property('value', function (d) {
                    return d.value;
                }).attr('title', function (d) {
                    return d.value;
                }).on('input', function (d) {
                    self.logger.log('input !!');
                    return d.onChange ? d.onChange(d, this) : null;
                });

                /**
                 * Helper to refresh search table widget
                 *  => search input with keyup event
                 *  => list of value rows with click event
                 *  => arrows row with data sorting
                 */
                var refreshSearchTable = function refreshSearchTable(elem, itemData) {
                    // prepare arrows data
                    var arrows_row_data = itemData.arrows || (itemData.data[0] || []).map(function (d, i) {
                        return {
                            active: false,
                            class: i,
                            order: 'asc',
                            id: i
                        };
                    });

                    // prepare value data
                    var data = void 0;
                    var sortData = arrows_row_data[arrows_row_data.findIndex(function (d) {
                        return d.active === true;
                    })];
                    if (sortData) {
                        data = _.orderBy(itemData.data, [function (ts) {
                            var value = ts[sortData.id],
                                columnDefinition = (itemData.columns || [])[sortData.id] || {};
                            return columnDefinition.onSort ? columnDefinition.onSort(value) : value;
                        }], [sortData.order]);
                    } else {
                        data = itemData.data;
                    }

                    // drag behaviour
                    //self.logger.log('refreshTable', elem, itemData);
                    var initCoords = {};
                    var drag = d3.drag().on('start', function () {
                        initCoords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
                        d3.select(this).classed('list-item-hover', true);
                        elem.select('.drag-element').html(d3.select(this).html()).classed('hidden', true).style('top', initCoords.y - 10 + 'px').style('left', initCoords.x - 10 + 'px');
                        if (itemData.onDragStart) {
                            itemData.onDragStart();
                        }
                    }).on('drag', function () {
                        var coords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
                        if (itemData.draggable && (Math.abs(coords.x - initCoords.x) > 10 || Math.abs(coords.y - initCoords.y) > 10)) {
                            elem.select('.drag-element').classed('hidden', false).style('top', d3.event.sourceEvent.y - 10 + 'px').style('left', d3.event.sourceEvent.x + 'px');
                            if (itemData.onDragMove) {
                                itemData.onDragMove();
                            }
                        }
                    }).on('end', function (d) {
                        var coords = { x: d3.event.sourceEvent.x, y: d3.event.sourceEvent.y };
                        d3.select(this).classed('list-item-hover', false);
                        elem.select('.drag-element').classed('hidden', true);
                        // if current mouse coords are different from the initial mouse coords, real drag end
                        if (itemData.draggable && coords.x !== initCoords.x && coords.y !== initCoords.y) {
                            var _data = elem.selectAll('tr.row-table.selected').data();
                            if (_data.length === 0 || _data.findIndex(function (t) {
                                return t[0] === d[0];
                            }) === -1) {
                                _data = [d];
                            }
                            if (itemData.onDragEnd) {
                                itemData.onDragEnd(_data, this);
                            }
                        }
                        // equivalents to click
                        else {
                                // FIXME: if we select then we search/ then select other one/ then undo search, the old selection is still here
                                // FIXME: how we manage the all select button ? should we lock as now or change behavior
                                // if ctrl or shift combined with click, it modifies pattern activation in current patternList
                                itemData.selected = itemData.selected || [];
                                //itemData.selected = (itemData.selected === d[0]) ? null : d[0];
                                if (keyMaster.command || keyMaster.control) {
                                    var idx = itemData.selected.indexOf(d[0]);
                                    if (idx > -1) {
                                        itemData.selected.splice(idx, 1);
                                    } else {
                                        itemData.selected.push(d[0]);
                                    }
                                } else if (keyMaster.shift) {
                                    var _idx = data.findIndex(function (de) {
                                        return de[0] === d[0];
                                    });
                                    var nearest = -1;
                                    itemData.selected.forEach(function (s) {
                                        var a = data.findIndex(function (de) {
                                            return de[0] === s;
                                        });
                                        if (nearest === -1 || Math.abs(_idx - nearest) > Math.abs(_idx - a)) {
                                            nearest = a;
                                        }
                                    });
                                    if (nearest !== -1) {
                                        var begin = _idx,
                                            end = nearest;
                                        if (end < begin) {
                                            end = _idx;
                                            begin = nearest;
                                        }
                                        for (var index = nearest > _idx ? _idx : nearest; index <= end; index++) {
                                            itemData.selected.push(data[index][0]);
                                        }
                                    }
                                }
                                // otherwise just modify list to activate only the wanted pattern
                                else if (itemData.selected.length > 1 || itemData.selected[0] !== d[0]) {
                                        itemData.selected = [d[0]];
                                    } else {
                                        itemData.selected = [];
                                    }
                                elem.selectAll('tr.row-table').classed('selected', function (d) {
                                    return itemData.selected.indexOf(d[0]) > -1;
                                });
                                if (itemData.onClick) {
                                    itemData.onClick(d, this, { selected: itemData.selected });
                                }
                                if (d3.event.stopPropagation) {
                                    d3.event.stopPropagation();
                                }
                            }
                    });

                    // store
                    itemData.arrows = arrows_row_data;
                    if (itemData.showHeader !== false) {
                        // arrows
                        var arrows_cell = elem.select('.header-arrows').selectAll('td').data(itemData.arrows);
                        var arrows_cell_enter = arrows_cell.enter().append('td');
                        var header_sub_container = arrows_cell_enter.append('span').classed('header-sub-container', true);

                        header_sub_container.append('span').attr('class', 'header-title').classed('hidden', function (d) {
                            return !d.label;
                        }).text(function (d) {
                            return d.label;
                        });
                        var arrows_cell_enter_icons = header_sub_container.append('span').attr('class', 'header-icons');
                        arrows_cell_enter_icons.append('i').attr('class', 'glyphicon glyphicon-chevron-up');
                        arrows_cell_enter_icons.append('i').attr('class', 'glyphicon glyphicon-chevron-down');

                        var arrows_cell_update = arrows_cell_enter.merge(arrows_cell);
                        arrows_cell_update.classed('hidden', function (d, i) {
                            return itemData.columns && itemData.columns[i] && itemData.columns[i].visible === false;
                        }).classed('active', function (d) {
                            return d.active;
                        }).on('click', function (cell) {
                            // change cell value
                            if (!cell.active) {
                                console.log('not active, active it');
                                arrows_row_data.forEach(function (d) {
                                    return d.active = false;
                                });
                                cell.active = true;
                                cell.order = 'asc';
                            } else {
                                console.log('active, desc');
                                cell.order = cell.order === 'asc' ? 'desc' : 'asc';
                            }
                            if (itemData.onSortChange) {
                                itemData.onSortChange(itemData.arrows);
                            }
                            d3.event.stopPropagation(); // stop propagation
                            return refreshSearchTable(elem, itemData); // refresh table
                        }).each(function (d) {
                            var node = d3.select(this);
                            node.select('.glyphicon-chevron-up').classed('invisible', d.order === 'asc' && d.active === true);
                            node.select('.glyphicon-chevron-down').classed('invisible', d.order === 'desc' && d.active === true);
                        });

                        arrows_cell_update.select('span.header-title').classed('hidden', function (d, i) {
                            return !itemData.columns || !itemData.columns[i] || !itemData.columns[i].label;
                        }).text(function (d, i) {
                            return itemData.columns && itemData.columns[i] && itemData.columns[i].label;
                        });
                    }
                    // value rows
                    var row = elem.select('table tbody').selectAll('tr.row-table').data(data, function (t, i) {
                        return t[0] + '-' + i;
                    });

                    row.enter().append('tr').attr('class', 'row-table').attr('draggable', 'true').each(function (rowData) {
                        d3.select(this).selectAll('td').data(rowData).enter().append('td').classed('hidden', function (d, i) {
                            return itemData.columns && itemData.columns[i] && itemData.columns[i].visible === false;
                        }).html(function (d) {
                            return d;
                        });
                    });

                    row.exit().remove();

                    elem.selectAll('tr.row-table').classed('selected', function (d) {
                        return itemData.selected && itemData.selected.indexOf(d[0]) > -1;
                    }).classed('active', function (d) {
                        return itemData.selected && itemData.selected.indexOf(d[0]) > -1;
                    }).classed('text-center', true).call(drag).on('mouseenter', function (d) {
                        if (itemData.onMouseEnter) {
                            itemData.onMouseEnter(d, this);
                        }
                    }).on('mouseleave', function (d) {
                        if (itemData.onMouseLeave) {
                            itemData.onMouseLeave(d, this);
                        }
                    }).each(function () {
                        var elem = d3.select(this);
                        (itemData.classed || []).forEach(function (classed) {
                            elem.classed(classed[0], function (sequence) {
                                return classed[1](sequence);
                            });
                        });
                    });
                    // keyup event handler
                    elem.select('input').property('value', function (d) {
                        return d.filterValue;
                    }).on('keyup', function () {
                        itemData.filterValue = this.value;
                        elem.selectAll('tr.row-table').classed('hidden', function (d) {
                            return d.findIndex(function (a) {
                                return a.indexOf(itemData.filterValue) > -1;
                            }) === -1;
                        });
                        elem.select('.result-count').text(elem.selectAll('.row-table:not(.hidden)').size());
                        if (itemData.onSearch) {
                            return itemData.onSearch(itemData, this);
                        }
                    });
                    elem.select('.result-count').text(elem.selectAll('.row-table:not(.hidden)').size());
                };

                // SEARCH update
                itemsUpdate.filter(function (itemData) {
                    return itemData.type === 'search';
                }).each(function (itemData) {
                    var elem = d3.select(this);
                    if (itemData.showTable !== false) {
                        resizable(elem.select('label').node(), elem.select('.search-table-container').node(), self._options.minResizeHeight, undefined, false, true);
                        return refreshSearchTable(elem, itemData);
                    } else {
                        elem.select('input').on('keyup', function () {
                            itemData.filterValue = this.value;
                            if (itemData.onSearch) {
                                return itemData.onSearch(itemData, this);
                            }
                        });
                    }
                    elem.select('.search-table-container').classed('hidden', itemData.showTable === false);
                    elem.select('.result-count').text(itemData.count);
                });

                // update the help description showed by the title tag
                itemsUpdate.attr('title', function (d) {
                    return d.help;
                });

                // EXIT
                items.exit().remove();
            });
            // resizable is a widget to allow user to resize specified element
            resizable(self.dom.menuContainer.node(), self.dom.container.node(), self._options.minResizeHeight, self.dispatch, true, false);
            var size = self.dom.container.node().getBoundingClientRect();
            self.container.classed('collapsed', self.isCollapsed());
            self.container.select('.menu__button').style('left', function () {
                return size.left + 'px';
            }).style('top', size.top + 'px');
            self.fitBottom();
        }
        /**
         * Function to fit last bloc inside its menu
         *
         * @memberof FixedMenu
         */

    }, {
        key: 'fitBottom',
        value: function fitBottom() {
            var self = this;
            var containerNode = self.container.node(),
                all_blocs = containerNode.querySelectorAll(".menu>div.bloc"),
                lastBloc = all_blocs[all_blocs.length - 1],
                table_dom = lastBloc.querySelector('.resizable');
            // update bloc to assign 'last' class to the last bloc
            d3.selectAll(all_blocs).classed('last', function (d, i) {
                return i === all_blocs.length - 1;
            });
            // if fitBottom is on and there is a resizable element inside, recalculate height
            if (table_dom && self._options.fitBottom) {
                var newHeight = d3.max([$(containerNode).height() - $(lastBloc).position().top - $(lastBloc).outerHeight(false) + $(table_dom).outerHeight(false) - 3, self._options.minResizeHeight]);
                d3.select(table_dom).style('height', newHeight + 'px').style('max-height', newHeight + 'px');
            }
        }

        /**
         * Function to refresh partialy menu (a specific bloc)
         *
         * @param {any} bloc_title - the identifier to get the bloc
         * @param {any} data - data of the bloc
         * @memberof SideMenu
         */

    }, {
        key: 'refreshPartial',
        value: function refreshPartial(bloc_title, data) {
            var self = this;
            var bloc = self.obj.blocs[self.obj.blocs.findIndex(function (b) {
                return b.title === bloc_title;
            })];
            bloc.data = data;
            self.dom.menuContainer.selectAll('.bloc').filter(function (d) {
                return d.title === bloc_title;
            }).each(function () {
                var infos = d3.select(this).select('tbody').selectAll('tr.information').data(bloc.data);
                var infosEnter = infos.enter().append('tr').attr('class', 'information');

                infosEnter.append('td').attr('class', 'title');
                infosEnter.append('td').attr('class', 'text');

                var infosUpdate = infosEnter.merge(infos);
                infosUpdate.select('.title').classed('fullscreenable', function (d) {
                    return (d[2] || {}).fullscreenable === true;
                }).html(function (d) {
                    return d[0];
                }).on('click', function (d) {
                    if ((d[2] || {}).fullscreenable) {
                        fullScreenApi.toggle($(this).parent().find('td.text')[0]);
                    }
                });

                infosUpdate.select('.text').each(function (d) {
                    if ((d[2] || {}).compute) {
                        // if there is a compute function in options use it
                        d[2].compute(this, d[1]);
                    } else {
                        // if there is not compute function, fill html with the value
                        d3.select(this).html(function (d) {
                            return d[1];
                        });
                    }
                });
                infos.exit().remove();
            });
            self.fitBottom();
        }

        /**
         * Getter to know if the menu is collapsed
         * @returns {boolean}
         * @memberof SideMenu
         */

    }, {
        key: 'isCollapsed',
        value: function isCollapsed() {
            return this.obj.isCollasped;
        }

        /**
         * Event generic handler setter
         *
         * @param {any} eventName - name of the event from dispatch property
         * @param {any} handler - function that will be used as handler
         * @memberof SideMenu
         */

    }, {
        key: 'on',
        value: function on(eventName, handler) {
            this.dispatch.on(eventName, handler);
            return this;
        }
    }]);

    return SideMenu;
}();

module.exports = SideMenu;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/node_modules/keymaster/keymaster.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/keymaster/keymaster.js","/home/davisp/projects/ikats_lig/hmi/node_modules/select2/dist/js/select2.js":"/home/davisp/projects/ikats_lig/hmi/node_modules/select2/dist/js/select2.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/FullScreenApi.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Tools/LigLogger.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/resizable.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/resizable.js","/home/davisp/projects/ikats_lig/hmi/src/LIG/less/menu.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/menu.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/Widgets/resizable.js":[function(require,module,exports){
(function (global){
'use strict';

// external lib
var d3 = (typeof window !== "undefined" ? window['d3'] : typeof global !== "undefined" ? global['d3'] : null);

// less
require("/home/davisp/projects/ikats_lig/hmi/src/LIG/less/resizable.less");

/**
 * Allow to manage resize behaviors
 *
 * @param {*} location : where the resizers have to be included
 * @param {*} target - element that will be impacted
 * @param {*} minWidth - minimum width allowed
 * @param {*} dispatcher - external dispatcher
 */
var resizable = function () {
    "use strict";

    return function () {
        var location = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        var target = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
        var minWidth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 50;
        var dispatcher = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
        var horizontal = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
        var vertical = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : true;

        // Drag behavior (based on d3 drag)
        var drag = d3.drag().on('start', function () {
            // Apply new width on the target
            var d3Element = d3.select(this);
            d3.select(location).classed('x-resizing', d3Element.classed('x-resizing-right')).classed('y-resizing', d3Element.classed('y-resizing-bottom'));
        }).on('drag', function () {
            // Determine resizer position relative to resizable (target or parent)
            var coords = d3.mouse(target || this.parentNode);
            // Avoid negative or really small widths/heights
            var x = Math.max(minWidth, coords[0]),
                y = Math.max(minWidth, coords[1]);
            if (dispatcher) {
                dispatcher.call('resize-x-move', this, x);
            }
            // Apply new width on the target
            if (d3.select(this).classed('x-resizing-right') && horizontal) {
                d3.select(target || this.parentNode).style('width', x + 'px');
            }
            if (d3.select(this).classed('y-resizing-bottom') && vertical) {
                d3.select(target || this.parentNode).style('height', y + 'px').style('max-height', y + 'px');
            }
        }).on('end', function () {
            var x = d3.mouse(target || this.parentNode)[0];
            // Avoid negative or really small widths
            x = Math.max(minWidth, x);
            // TODO: hacky
            // use delegated dispatcher to dispatch resizable events
            if (dispatcher) {
                dispatcher.call('resize-x', this, x);
            }
            d3.select(location).classed('x-resizing', false).classed('y-resizing', false);
        });

        if (horizontal) {
            //  Add horizontal resizer (right side)
            d3.select(location).selectAll('.x-resizing-right').data([0]).enter().append('div').attr('class', 'x-resizing-right').call(drag);
        }

        if (vertical) {
            //  Add horizontal resizer (right side)
            d3.select(location).selectAll('.y-resizing-bottom').data([0]).enter().append('div').attr('class', 'y-resizing-bottom').call(drag);
        }

        d3.select(target).classed('resizable', true);
    };
}();

module.exports = resizable;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/resizable.less":"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/resizable.less"}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/colorpicker.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".sp-container{position:absolute;top:0;left:0;display:inline-block;*display:inline;*zoom:1;z-index:9999994;overflow:hidden}.sp-container.sp-flat{position:relative}.sp-container,.sp-container *{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}.sp-top{position:relative;width:100%;display:inline-block}.sp-top-inner{position:absolute;top:0;left:0;bottom:0;right:0}.sp-color{position:absolute;top:0;left:0;bottom:0;right:20%}.sp-hue{position:absolute;top:0;right:0;bottom:0;left:84%;height:100%}.sp-clear-enabled .sp-hue{top:33px;height:77.5%}.sp-fill{padding-top:80%}.sp-sat,.sp-val{position:absolute;top:0;left:0;right:0;bottom:0}.sp-alpha-enabled .sp-top{margin-bottom:18px}.sp-alpha-enabled .sp-alpha{display:block}.sp-alpha-handle{position:absolute;top:-4px;bottom:-4px;width:6px;left:50%;cursor:pointer;border:1px solid black;background:white;opacity:.8}.sp-alpha{display:none;position:absolute;bottom:-14px;right:0;left:0;height:8px}.sp-alpha-inner{border:solid 1px #333}.sp-clear{display:none}.sp-clear.sp-clear-display{background-position:center}.sp-clear-enabled .sp-clear{display:block;position:absolute;top:0;right:0;bottom:0;left:84%;height:28px}.sp-container,.sp-replacer,.sp-preview,.sp-dragger,.sp-slider,.sp-alpha,.sp-clear,.sp-alpha-handle,.sp-container.sp-dragging .sp-input,.sp-container button{-webkit-user-select:none;-moz-user-select:-moz-none;-o-user-select:none;user-select:none}.sp-container.sp-input-disabled .sp-input-container{display:none}.sp-container.sp-buttons-disabled .sp-button-container{display:none}.sp-container.sp-palette-buttons-disabled .sp-palette-button-container{display:none}.sp-palette-only .sp-picker-container{display:none}.sp-palette-disabled .sp-palette-container{display:none}.sp-initial-disabled .sp-initial{display:none}.sp-sat{background-image:-webkit-gradient(linear, 0 0, 100% 0, from(#FFF), to(rgba(204,154,129,0)));background-image:-webkit-linear-gradient(left, #FFF, rgba(204,154,129,0));background-image:-moz-linear-gradient(left, #fff, rgba(204,154,129,0));background-image:-o-linear-gradient(left, #fff, rgba(204,154,129,0));background-image:-ms-linear-gradient(left, #fff, rgba(204,154,129,0));background-image:linear-gradient(to right, #fff, rgba(204,154,129,0));-ms-filter:\"progid:DXImageTransform.Microsoft.gradient(GradientType = 1, startColorstr=#FFFFFFFF, endColorstr=#00CC9A81)\";filter:progid:DXImageTransform.Microsoft.gradient(GradientType=1, startColorstr='#FFFFFFFF', endColorstr='#00CC9A81')}.sp-val{background-image:-webkit-gradient(linear, 0 100%, 0 0, from(#000000), to(rgba(204,154,129,0)));background-image:-webkit-linear-gradient(bottom, #000000, rgba(204,154,129,0));background-image:-moz-linear-gradient(bottom, #000, rgba(204,154,129,0));background-image:-o-linear-gradient(bottom, #000, rgba(204,154,129,0));background-image:-ms-linear-gradient(bottom, #000, rgba(204,154,129,0));background-image:linear-gradient(to top, #000, rgba(204,154,129,0));-ms-filter:\"progid:DXImageTransform.Microsoft.gradient(startColorstr=#00CC9A81, endColorstr=#FF000000)\";filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00CC9A81', endColorstr='#FF000000')}.sp-hue{background:-moz-linear-gradient(top, #ff0000 0, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);background:-ms-linear-gradient(top, #ff0000 0, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);background:-o-linear-gradient(top, #ff0000 0, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);background:-webkit-gradient(linear, left top, left bottom, from(#ff0000), color-stop(.17, #ffff00), color-stop(.33, #00ff00), color-stop(.5, #00ffff), color-stop(.67, #0000ff), color-stop(.83, #ff00ff), to(#ff0000));background:-webkit-linear-gradient(top, #ff0000 0, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);background:linear-gradient(to bottom, #ff0000 0, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%)}.sp-1{height:17%;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0000', endColorstr='#ffff00')}.sp-2{height:16%;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffff00', endColorstr='#00ff00')}.sp-3{height:17%;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00ff00', endColorstr='#00ffff')}.sp-4{height:17%;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00ffff', endColorstr='#0000ff')}.sp-5{height:16%;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0000ff', endColorstr='#ff00ff')}.sp-6{height:17%;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff00ff', endColorstr='#ff0000')}.sp-hidden{display:none !important}.sp-cf:before,.sp-cf:after{content:\"\";display:table}.sp-cf:after{clear:both}.sp-cf{*zoom:1}@media (max-device-width:480px){.sp-color{right:40%}.sp-hue{left:63%}.sp-fill{padding-top:60%}}.sp-dragger{border-radius:5px;height:5px;width:5px;border:1px solid #fff;background:#000;cursor:pointer;position:absolute;top:0;left:0}.sp-slider{position:absolute;top:0;cursor:pointer;height:3px;left:-1px;right:-1px;border:1px solid #000;background:white;opacity:.8}.sp-container{border-radius:0;background-color:#ECECEC;border:solid 1px #f0c49B;padding:0}.sp-container,.sp-container button,.sp-container input,.sp-color,.sp-hue,.sp-clear{font:normal 12px \"Lucida Grande\",\"Lucida Sans Unicode\",\"Lucida Sans\",Geneva,Verdana,sans-serif;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box}.sp-top{margin-bottom:3px}.sp-color,.sp-hue,.sp-clear{border:solid 1px #666}.sp-input-container{float:right;width:100px;margin-bottom:4px}.sp-initial-disabled .sp-input-container{width:100%}.sp-input{font-size:12px !important;border:1px inset;padding:4px 5px;margin:0;width:100%;background:transparent;border-radius:3px;color:#222}.sp-input:focus{border:1px solid orange}.sp-input.sp-validation-error{border:1px solid red;background:#fdd}.sp-picker-container,.sp-palette-container{float:left;position:relative;padding:10px;padding-bottom:300px;margin-bottom:-290px}.sp-picker-container{width:172px;border-left:solid 1px #fff}.sp-palette-container{border-right:solid 1px #ccc}.sp-palette-only .sp-palette-container{border:0}.sp-palette .sp-thumb-el{display:block;position:relative;float:left;width:24px;height:15px;margin:3px;cursor:pointer;border:solid 2px transparent}.sp-palette .sp-thumb-el:hover,.sp-palette .sp-thumb-el.sp-thumb-active{border-color:orange}.sp-thumb-el{position:relative}.sp-initial{float:left;border:solid 1px #333}.sp-initial span{width:30px;height:25px;border:none;display:block;float:left;margin:0}.sp-initial .sp-clear-display{background-position:center}.sp-palette-button-container,.sp-button-container{float:right}.sp-replacer{margin:0;overflow:hidden;cursor:pointer;padding:4px;display:inline-block;*zoom:1;*display:inline;border:solid 1px #91765d;background:#eee;color:#333;vertical-align:middle}.sp-replacer:hover,.sp-replacer.sp-active{border-color:#F0C49B;color:#111}.sp-replacer.sp-disabled{cursor:default;border-color:silver;color:silver}.sp-dd{padding:2px 0;height:16px;line-height:16px;float:left;font-size:10px}.sp-preview{position:relative;width:25px;height:20px;border:solid 1px #222;margin-right:5px;float:left;z-index:0}.sp-palette{*width:220px;max-width:220px}.sp-palette .sp-thumb-el{width:16px;height:16px;margin:2px 1px;border:solid 1px #d0d0d0}.sp-container{padding-bottom:0}.sp-container button{background-color:#eeeeee;background-image:-webkit-linear-gradient(top, #eeeeee, #cccccc);background-image:-moz-linear-gradient(top, #eeeeee, #cccccc);background-image:-ms-linear-gradient(top, #eeeeee, #cccccc);background-image:-o-linear-gradient(top, #eeeeee, #cccccc);background-image:linear-gradient(to bottom, #eeeeee, #cccccc);border:1px solid #ccc;border-bottom:1px solid #bbb;border-radius:3px;color:#333;font-size:14px;line-height:1;padding:5px 4px;text-align:center;text-shadow:0 1px 0 #eee;vertical-align:middle}.sp-container button:hover{background-color:#dddddd;background-image:-webkit-linear-gradient(top, #dddddd, #bbbbbb);background-image:-moz-linear-gradient(top, #dddddd, #bbbbbb);background-image:-ms-linear-gradient(top, #dddddd, #bbbbbb);background-image:-o-linear-gradient(top, #dddddd, #bbbbbb);background-image:linear-gradient(to bottom, #dddddd, #bbbbbb);border:1px solid #bbb;border-bottom:1px solid #999;cursor:pointer;text-shadow:0 1px 0 #ddd}.sp-container button:active{border:1px solid #aaa;border-bottom:1px solid #888;-webkit-box-shadow:inset 0 0 5px 2px #aaaaaa,0 1px 0 0 #eeeeee;-moz-box-shadow:inset 0 0 5px 2px #aaaaaa,0 1px 0 0 #eeeeee;-ms-box-shadow:inset 0 0 5px 2px #aaaaaa,0 1px 0 0 #eeeeee;-o-box-shadow:inset 0 0 5px 2px #aaaaaa,0 1px 0 0 #eeeeee;box-shadow:inset 0 0 5px 2px #aaaaaa,0 1px 0 0 #eeeeee}.sp-cancel{font-size:11px;color:#d93f3f !important;margin:0;padding:2px;margin-right:5px;vertical-align:middle;text-decoration:none}.sp-cancel:hover{color:#d93f3f !important;text-decoration:underline}.sp-palette span:hover,.sp-palette span.sp-thumb-active{border-color:#000}.sp-preview,.sp-alpha,.sp-thumb-el{position:relative;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAIAAADZF8uwAAAAGUlEQVQYV2M4gwH+YwCGIasIUwhT25BVBADtzYNYrHvv4gAAAABJRU5ErkJggg==)}.sp-preview-inner,.sp-alpha-inner,.sp-thumb-inner{display:block;position:absolute;top:0;left:0;bottom:0;right:0}.sp-palette .sp-thumb-inner{background-position:50% 50%;background-repeat:no-repeat}.sp-palette .sp-thumb-light.sp-thumb-active .sp-thumb-inner{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAIVJREFUeNpiYBhsgJFMffxAXABlN5JruT4Q3wfi/0DsT64h8UD8HmpIPCWG/KemIfOJCUB+Aoacx6EGBZyHBqI+WsDCwuQ9mhxeg2A210Ntfo8klk9sOMijaURm7yc1UP2RNCMbKE9ODK1HM6iegYLkfx8pligC9lCD7KmRof0ZhjQACDAAceovrtpVBRkAAAAASUVORK5CYII=)}.sp-palette .sp-thumb-dark.sp-thumb-active .sp-thumb-inner{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAMdJREFUOE+tkgsNwzAMRMugEAahEAahEAZhEAqlEAZhEAohEAYh81X2dIm8fKpEspLGvudPOsUYpxE2BIJCroJmEW9qJ+MKaBFhEMNabSy9oIcIPwrB+afvAUFoK4H0tMaQ3XtlrggDhOVVMuT4E5MMG0FBbCEYzjYT7OxLEvIHQLY2zWwQ3D+9luyOQTfKDiFD3iUIfPk8VqrKjgAiSfGFPecrg6HN6m/iBcwiDAo7WiBeawa+Kwh7tZoSCGLMqwlSAzVDhoK+6vH4G0P5wdkAAAAASUVORK5CYII=)}.sp-clear-display{background-repeat:no-repeat;background-position:center;background-image:url(data:image/gif;base64,R0lGODlhFAAUAPcAAAAAAJmZmZ2dnZ6enqKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq/Hx8fLy8vT09PX19ff39/j4+Pn5+fr6+vv7+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAP8ALAAAAAAUABQAAAihAP9FoPCvoMGDBy08+EdhQAIJCCMybCDAAYUEARBAlFiQQoMABQhKUJBxY0SPICEYHBnggEmDKAuoPMjS5cGYMxHW3IiT478JJA8M/CjTZ0GgLRekNGpwAsYABHIypcAgQMsITDtWJYBR6NSqMico9cqR6tKfY7GeBCuVwlipDNmefAtTrkSzB1RaIAoXodsABiZAEFB06gIBWC1mLVgBa0AAOw==)}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/curve.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".lig-tooltip{position:fixed;display:block;text-align:center;min-width:60px;min-height:28px;padding:5px;margin-left:15px;margin-top:-10px;line-height:28px;font:12px sans-serif;background:lightsteelblue;border:0;border-radius:8px;pointer-events:none;z-index:5000}.curve-subcontainer .curve-canvas,.curve-subcontainer .curve-canvas-mouse{position:absolute}.curve-subcontainer .pointing{cursor:pointer}.curve-subcontainer .xaxis line.tick-v{stroke:darkgrey;stroke-opacity:.5;stroke-width:.8}.curve-subcontainer .zoom{fill:none;pointer-events:all}.curve-subcontainer .minimap{stroke:grey}.curve-subcontainer .minimap .indicator{pointer-events:none}.curve-subcontainer .curve-canvas{z-index:500;display:block}.curve-subcontainer .curve-canvas-mouse{z-index:1000;display:block}.curve-subcontainer .curve{position:relative}.curve-subcontainer svg{z-index:1200;display:block}.curve-subcontainer text.variable{fill:\"#000\";transform:\"rotate(-90)\"}.curve-subcontainer .weights{fill:grey}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/fullscreen.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".fullscreen{width:100% !important;height:100% !important;margin:0;background-color:white;position:fixed;top:0;left:0}.fullscreen:-webkit-full-screen,.fullscreen:-moz-full-screen,.fullscreen:-ms-fullscreenn,.fullscreen:fullscreen{width:100%;height:100%;margin:0;background-color:white;position:fixed;top:0;left:0;z-index:10000}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/heatmap.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".heatmap{width:100%;position:relative;overflow:auto}.heatmap .canvas{position:absolute;z-index:1000}.heatmap .canvas.canvas-background{position:absolute;z-index:900}.heatmap .svg{z-index:1;width:100%;height:100%;position:absolute;left:0;top:0;overflow:initial}.heatmap .svg .minimap{stroke:grey}.heatmap .in-brushing .brush{cursor:crosshair}.heatmap .in-brushing .svg{z-index:2000}.heatmap .selected{font-weight:bold}.heatmap .heatmap-details,.heatmap .heatmap-graph{display:inline-block;vertical-align:top}.heatmap .heatmap-details{border:1px solid grey;margin:auto;font-size:12px;margin-left:10px;overflow:auto}.heatmap .heatmap-details table .label-cell{text-align:left;width:50%;padding-left:10%;word-break:break-all;font-weight:bold;font-size:14px}.heatmap .heatmap-details table .text-cell{font-size:13px;width:50%;word-break:break-all}.heatmap .part line{stroke:black;stroke-dasharray:5.5;stroke-width:2}.heatmap .class-part line{stroke:grey;stroke-dasharray:5.5;stroke-width:2}.heatmap .heatmap-subcontainer{position:absolute;left:0;right:0;top:0;margin-left:auto;margin-right:auto}.heatmap .heatmap-subcontainer>div{position:relative}.heatmap .heatmap-subcontainer .yaxis .domain,.heatmap .heatmap-subcontainer .xaxis .domain{stroke:none}.heatmap .yaxis2{text-anchor:'end'}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/loader.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".in-loading{position:relative;pointer-events:none}.in-loading>.loader{position:absolute;left:calc(50% - 60px);top:calc(50% - 60px);border:16px solid #f3f3f3;border-top:16px solid #3498db;border-radius:50%;width:120px;height:120px;opacity:1;z-index:10000;animation:spin 2s linear infinite}.in-loading>.loader.small{left:calc(50% -  25px);top:calc(50% -  25px);border:6px solid #f3f3f3;border-top:6px solid #3498db;width:50px;height:50px}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/menu.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".select2-container{box-sizing:border-box;display:inline-block;margin:0;position:relative;vertical-align:middle}.select2-container .select2-selection--single{box-sizing:border-box;cursor:pointer;display:block;height:28px;user-select:none;-webkit-user-select:none}.select2-container .select2-selection--single .select2-selection__rendered{display:block;padding-left:8px;padding-right:20px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}.select2-container .select2-selection--single .select2-selection__clear{position:relative}.select2-container[dir=\"rtl\"] .select2-selection--single .select2-selection__rendered{padding-right:8px;padding-left:20px}.select2-container .select2-selection--multiple{box-sizing:border-box;cursor:pointer;display:block;min-height:32px;user-select:none;-webkit-user-select:none}.select2-container .select2-selection--multiple .select2-selection__rendered{display:inline-block;overflow:hidden;padding-left:8px;text-overflow:ellipsis;white-space:nowrap}.select2-container .select2-search--inline{float:left}.select2-container .select2-search--inline .select2-search__field{box-sizing:border-box;border:none;font-size:100%;margin-top:5px;padding:0}.select2-container .select2-search--inline .select2-search__field::-webkit-search-cancel-button{-webkit-appearance:none}.select2-dropdown{background-color:white;border:1px solid #aaa;border-radius:4px;box-sizing:border-box;display:block;position:absolute;left:-100000px;width:100%;z-index:1051}.select2-results{display:block}.select2-results__options{list-style:none;margin:0;padding:0}.select2-results__option{padding:6px;user-select:none;-webkit-user-select:none}.select2-results__option[aria-selected]{cursor:pointer}.select2-container--open .select2-dropdown{left:0}.select2-container--open .select2-dropdown--above{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--open .select2-dropdown--below{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.select2-search--dropdown{display:block;padding:4px}.select2-search--dropdown .select2-search__field{padding:4px;width:100%;box-sizing:border-box}.select2-search--dropdown .select2-search__field::-webkit-search-cancel-button{-webkit-appearance:none}.select2-search--dropdown.select2-search--hide{display:none}.select2-close-mask{border:0;margin:0;padding:0;display:block;position:fixed;left:0;top:0;min-height:100%;min-width:100%;height:auto;width:auto;opacity:0;z-index:99;background-color:#fff;filter:alpha(opacity=0)}.select2-hidden-accessible{border:0 !important;clip:rect(0 0 0 0) !important;-webkit-clip-path:inset(50%) !important;clip-path:inset(50%) !important;height:1px !important;overflow:hidden !important;padding:0 !important;position:absolute !important;width:1px !important;white-space:nowrap !important}.select2-container--default .select2-selection--single{background-color:#fff;border:1px solid #aaa;border-radius:4px}.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:28px}.select2-container--default .select2-selection--single .select2-selection__clear{cursor:pointer;float:right;font-weight:bold}.select2-container--default .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--default .select2-selection--single .select2-selection__arrow{height:26px;position:absolute;top:1px;right:1px;width:20px}.select2-container--default .select2-selection--single .select2-selection__arrow b{border-color:#888 transparent transparent transparent;border-style:solid;border-width:5px 4px 0 4px;height:0;left:50%;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--default[dir=\"rtl\"] .select2-selection--single .select2-selection__clear{float:left}.select2-container--default[dir=\"rtl\"] .select2-selection--single .select2-selection__arrow{left:1px;right:auto}.select2-container--default.select2-container--disabled .select2-selection--single{background-color:#eee;cursor:default}.select2-container--default.select2-container--disabled .select2-selection--single .select2-selection__clear{display:none}.select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #888 transparent;border-width:0 4px 5px 4px}.select2-container--default .select2-selection--multiple{background-color:white;border:1px solid #aaa;border-radius:4px;cursor:text}.select2-container--default .select2-selection--multiple .select2-selection__rendered{box-sizing:border-box;list-style:none;margin:0;padding:0 5px;width:100%}.select2-container--default .select2-selection--multiple .select2-selection__rendered li{list-style:none}.select2-container--default .select2-selection--multiple .select2-selection__placeholder{color:#999;margin-top:5px;float:left}.select2-container--default .select2-selection--multiple .select2-selection__clear{cursor:pointer;float:right;font-weight:bold;margin-top:5px;margin-right:10px}.select2-container--default .select2-selection--multiple .select2-selection__choice{background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px;cursor:default;float:left;margin-right:5px;margin-top:5px;padding:0 5px}.select2-container--default .select2-selection--multiple .select2-selection__choice__remove{color:#999;cursor:pointer;display:inline-block;font-weight:bold;margin-right:2px}.select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover{color:#333}.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice,.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__placeholder,.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-search--inline{float:right}.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice{margin-left:5px;margin-right:auto}.select2-container--default[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.select2-container--default.select2-container--focus .select2-selection--multiple{border:solid black 1px;outline:0}.select2-container--default.select2-container--disabled .select2-selection--multiple{background-color:#eee;cursor:default}.select2-container--default.select2-container--disabled .select2-selection__choice__remove{display:none}.select2-container--default.select2-container--open.select2-container--above .select2-selection--single,.select2-container--default.select2-container--open.select2-container--above .select2-selection--multiple{border-top-left-radius:0;border-top-right-radius:0}.select2-container--default.select2-container--open.select2-container--below .select2-selection--single,.select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple{border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--default .select2-search--dropdown .select2-search__field{border:1px solid #aaa}.select2-container--default .select2-search--inline .select2-search__field{background:transparent;border:none;outline:0;box-shadow:none;-webkit-appearance:textfield}.select2-container--default .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--default .select2-results__option[role=group]{padding:0}.select2-container--default .select2-results__option[aria-disabled=true]{color:#999}.select2-container--default .select2-results__option[aria-selected=true]{background-color:#ddd}.select2-container--default .select2-results__option .select2-results__option{padding-left:1em}.select2-container--default .select2-results__option .select2-results__option .select2-results__group{padding-left:0}.select2-container--default .select2-results__option .select2-results__option .select2-results__option{margin-left:-1em;padding-left:2em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-2em;padding-left:3em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-3em;padding-left:4em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-4em;padding-left:5em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-5em;padding-left:6em}.select2-container--default .select2-results__option--highlighted[aria-selected]{background-color:#5897fb;color:white}.select2-container--default .select2-results__group{cursor:default;display:block;padding:6px}.select2-container--classic .select2-selection--single{background-color:#f7f7f7;border:1px solid #aaa;border-radius:4px;outline:0;background-image:-webkit-linear-gradient(top, white 50%, #eeeeee 100%);background-image:-o-linear-gradient(top, white 50%, #eeeeee 100%);background-image:linear-gradient(to bottom, white 50%, #eeeeee 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFFFF', endColorstr='#FFEEEEEE', GradientType=0)}.select2-container--classic .select2-selection--single:focus{border:1px solid #5897fb}.select2-container--classic .select2-selection--single .select2-selection__rendered{color:#444;line-height:28px}.select2-container--classic .select2-selection--single .select2-selection__clear{cursor:pointer;float:right;font-weight:bold;margin-right:10px}.select2-container--classic .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--classic .select2-selection--single .select2-selection__arrow{background-color:#ddd;border:none;border-left:1px solid #aaa;border-top-right-radius:4px;border-bottom-right-radius:4px;height:26px;position:absolute;top:1px;right:1px;width:20px;background-image:-webkit-linear-gradient(top, #eeeeee 50%, #cccccc 100%);background-image:-o-linear-gradient(top, #eeeeee 50%, #cccccc 100%);background-image:linear-gradient(to bottom, #eeeeee 50%, #cccccc 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFEEEEEE', endColorstr='#FFCCCCCC', GradientType=0)}.select2-container--classic .select2-selection--single .select2-selection__arrow b{border-color:#888 transparent transparent transparent;border-style:solid;border-width:5px 4px 0 4px;height:0;left:50%;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--classic[dir=\"rtl\"] .select2-selection--single .select2-selection__clear{float:left}.select2-container--classic[dir=\"rtl\"] .select2-selection--single .select2-selection__arrow{border:none;border-right:1px solid #aaa;border-radius:0;border-top-left-radius:4px;border-bottom-left-radius:4px;left:1px;right:auto}.select2-container--classic.select2-container--open .select2-selection--single{border:1px solid #5897fb}.select2-container--classic.select2-container--open .select2-selection--single .select2-selection__arrow{background:transparent;border:none}.select2-container--classic.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #888 transparent;border-width:0 4px 5px 4px}.select2-container--classic.select2-container--open.select2-container--above .select2-selection--single{border-top:none;border-top-left-radius:0;border-top-right-radius:0;background-image:-webkit-linear-gradient(top, white 0, #eeeeee 50%);background-image:-o-linear-gradient(top, white 0, #eeeeee 50%);background-image:linear-gradient(to bottom, white 0, #eeeeee 50%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFFFF', endColorstr='#FFEEEEEE', GradientType=0)}.select2-container--classic.select2-container--open.select2-container--below .select2-selection--single{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0;background-image:-webkit-linear-gradient(top, #eeeeee 50%, white 100%);background-image:-o-linear-gradient(top, #eeeeee 50%, white 100%);background-image:linear-gradient(to bottom, #eeeeee 50%, white 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFEEEEEE', endColorstr='#FFFFFFFF', GradientType=0)}.select2-container--classic .select2-selection--multiple{background-color:white;border:1px solid #aaa;border-radius:4px;cursor:text;outline:0}.select2-container--classic .select2-selection--multiple:focus{border:1px solid #5897fb}.select2-container--classic .select2-selection--multiple .select2-selection__rendered{list-style:none;margin:0;padding:0 5px}.select2-container--classic .select2-selection--multiple .select2-selection__clear{display:none}.select2-container--classic .select2-selection--multiple .select2-selection__choice{background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px;cursor:default;float:left;margin-right:5px;margin-top:5px;padding:0 5px}.select2-container--classic .select2-selection--multiple .select2-selection__choice__remove{color:#888;cursor:pointer;display:inline-block;font-weight:bold;margin-right:2px}.select2-container--classic .select2-selection--multiple .select2-selection__choice__remove:hover{color:#555}.select2-container--classic[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice{float:right;margin-left:5px;margin-right:auto}.select2-container--classic[dir=\"rtl\"] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.select2-container--classic.select2-container--open .select2-selection--multiple{border:1px solid #5897fb}.select2-container--classic.select2-container--open.select2-container--above .select2-selection--multiple{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.select2-container--classic.select2-container--open.select2-container--below .select2-selection--multiple{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--classic .select2-search--dropdown .select2-search__field{border:1px solid #aaa;outline:0}.select2-container--classic .select2-search--inline .select2-search__field{outline:0;box-shadow:none}.select2-container--classic .select2-dropdown{background-color:white;border:1px solid transparent}.select2-container--classic .select2-dropdown--above{border-bottom:none}.select2-container--classic .select2-dropdown--below{border-top:none}.select2-container--classic .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--classic .select2-results__option[role=group]{padding:0}.select2-container--classic .select2-results__option[aria-disabled=true]{color:grey}.select2-container--classic .select2-results__option--highlighted[aria-selected]{background-color:#3875d7;color:white}.select2-container--classic .select2-results__group{cursor:default;display:block;padding:6px}.select2-container--classic.select2-container--open .select2-dropdown{border-color:#5897fb}/*!\n * Select2 Bootstrap Theme v0.1.0-beta.10 (https://select2.github.io/select2-bootstrap-theme)\n * Copyright 2015-2017 Florian Kissling and contributors (https://github.com/select2/select2-bootstrap-theme/graphs/contributors)\n * Licensed under MIT (https://github.com/select2/select2-bootstrap-theme/blob/master/LICENSE)\n */.select2-container--bootstrap{display:block}.select2-container--bootstrap .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);background-color:#fff;border:1px solid #ccc;border-radius:4px;color:#555;font-size:14px;outline:0}.select2-container--bootstrap .select2-selection.form-control{border-radius:4px}.select2-container--bootstrap .select2-search--dropdown .select2-search__field{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);background-color:#fff;border:1px solid #ccc;border-radius:4px;color:#555;font-size:14px}.select2-container--bootstrap .select2-search__field{outline:0}.select2-container--bootstrap .select2-search__field::-webkit-input-placeholder{color:#999}.select2-container--bootstrap .select2-search__field:-moz-placeholder{color:#999}.select2-container--bootstrap .select2-search__field::-moz-placeholder{color:#999;opacity:1}.select2-container--bootstrap .select2-search__field:-ms-input-placeholder{color:#999}.select2-container--bootstrap .select2-results__option{padding:6px 12px}.select2-container--bootstrap .select2-results__option[role=group]{padding:0}.select2-container--bootstrap .select2-results__option[aria-disabled=true]{color:#777;cursor:not-allowed}.select2-container--bootstrap .select2-results__option[aria-selected=true]{background-color:#f5f5f5;color:#262626}.select2-container--bootstrap .select2-results__option--highlighted[aria-selected]{background-color:#337ab7;color:#fff}.select2-container--bootstrap .select2-results__option .select2-results__option{padding:6px 12px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__group{padding-left:0}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option{margin-left:-12px;padding-left:24px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-24px;padding-left:36px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-36px;padding-left:48px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-48px;padding-left:60px}.select2-container--bootstrap .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-60px;padding-left:72px}.select2-container--bootstrap .select2-results__group{color:#777;display:block;padding:6px 12px;font-size:12px;line-height:1.42857143;white-space:nowrap}.select2-container--bootstrap.select2-container--focus .select2-selection,.select2-container--bootstrap.select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(102,175,233,0.6);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(102,175,233,0.6);-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;border-color:#66afe9}.select2-container--bootstrap.select2-container--open .select2-selection .select2-selection__arrow b{border-color:transparent transparent #999;border-width:0 4px 4px}.select2-container--bootstrap.select2-container--open.select2-container--below .select2-selection{border-bottom-right-radius:0;border-bottom-left-radius:0;border-bottom-color:transparent}.select2-container--bootstrap.select2-container--open.select2-container--above .select2-selection{border-top-right-radius:0;border-top-left-radius:0;border-top-color:transparent}.select2-container--bootstrap .select2-selection__clear{color:#999;cursor:pointer;float:right;font-weight:700;margin-right:10px}.select2-container--bootstrap .select2-selection__clear:hover{color:#333}.select2-container--bootstrap.select2-container--disabled .select2-selection{border-color:#ccc;-webkit-box-shadow:none;box-shadow:none}.select2-container--bootstrap.select2-container--disabled .select2-search__field,.select2-container--bootstrap.select2-container--disabled .select2-selection{cursor:not-allowed}.select2-container--bootstrap.select2-container--disabled .select2-selection,.select2-container--bootstrap.select2-container--disabled .select2-selection--multiple .select2-selection__choice{background-color:#eee}.select2-container--bootstrap.select2-container--disabled .select2-selection--multiple .select2-selection__choice__remove,.select2-container--bootstrap.select2-container--disabled .select2-selection__clear{display:none}.select2-container--bootstrap .select2-dropdown{-webkit-box-shadow:0 6px 12px rgba(0,0,0,0.175);box-shadow:0 6px 12px rgba(0,0,0,0.175);border-color:#66afe9;overflow-x:hidden;margin-top:-1px}.select2-container--bootstrap .select2-dropdown--above{-webkit-box-shadow:0 -6px 12px rgba(0,0,0,0.175);box-shadow:0 -6px 12px rgba(0,0,0,0.175);margin-top:1px}.select2-container--bootstrap .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--bootstrap .select2-selection--single{height:34px;line-height:1.42857143;padding:6px 24px 6px 12px}.select2-container--bootstrap .select2-selection--single .select2-selection__arrow{position:absolute;bottom:0;right:12px;top:0;width:4px}.select2-container--bootstrap .select2-selection--single .select2-selection__arrow b{border-color:#999 transparent transparent;border-style:solid;border-width:4px 4px 0;height:0;left:0;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--bootstrap .select2-selection--single .select2-selection__rendered{color:#555;padding:0}.select2-container--bootstrap .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--bootstrap .select2-selection--multiple{min-height:34px;padding:0;height:auto}.select2-container--bootstrap .select2-selection--multiple .select2-selection__rendered{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;line-height:1.42857143;list-style:none;margin:0;overflow:hidden;padding:0;width:100%;text-overflow:ellipsis;white-space:nowrap}.select2-container--bootstrap .select2-selection--multiple .select2-selection__placeholder{color:#999;float:left;margin-top:5px}.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice{color:#555;background:#fff;border:1px solid #ccc;border-radius:4px;cursor:default;float:left;margin:5px 0 0 6px;padding:0 6px}.select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field{background:0 0;padding:0 12px;height:32px;line-height:1.42857143;margin-top:0;min-width:5em}.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove{color:#999;cursor:pointer;display:inline-block;font-weight:700;margin-right:3px}.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove:hover{color:#333}.select2-container--bootstrap .select2-selection--multiple .select2-selection__clear{margin-top:6px}.form-group-sm .select2-container--bootstrap .select2-selection--single,.input-group-sm .select2-container--bootstrap .select2-selection--single,.select2-container--bootstrap .select2-selection--single.input-sm{border-radius:3px;font-size:12px;height:30px;line-height:1.5;padding:5px 22px 5px 10px}.form-group-sm .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.input-group-sm .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.select2-container--bootstrap .select2-selection--single.input-sm .select2-selection__arrow b{margin-left:-5px}.form-group-sm .select2-container--bootstrap .select2-selection--multiple,.input-group-sm .select2-container--bootstrap .select2-selection--multiple,.select2-container--bootstrap .select2-selection--multiple.input-sm{min-height:30px;border-radius:3px}.form-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.input-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.select2-container--bootstrap .select2-selection--multiple.input-sm .select2-selection__choice{font-size:12px;line-height:1.5;margin:4px 0 0 5px;padding:0 5px}.form-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.input-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.select2-container--bootstrap .select2-selection--multiple.input-sm .select2-search--inline .select2-search__field{padding:0 10px;font-size:12px;height:28px;line-height:1.5}.form-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.input-group-sm .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.select2-container--bootstrap .select2-selection--multiple.input-sm .select2-selection__clear{margin-top:5px}.form-group-lg .select2-container--bootstrap .select2-selection--single,.input-group-lg .select2-container--bootstrap .select2-selection--single,.select2-container--bootstrap .select2-selection--single.input-lg{border-radius:6px;font-size:18px;height:46px;line-height:1.3333333;padding:10px 31px 10px 16px}.form-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow,.input-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow,.select2-container--bootstrap .select2-selection--single.input-lg .select2-selection__arrow{width:5px}.form-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.input-group-lg .select2-container--bootstrap .select2-selection--single .select2-selection__arrow b,.select2-container--bootstrap .select2-selection--single.input-lg .select2-selection__arrow b{border-width:5px 5px 0;margin-left:-10px;margin-top:-2.5px}.form-group-lg .select2-container--bootstrap .select2-selection--multiple,.input-group-lg .select2-container--bootstrap .select2-selection--multiple,.select2-container--bootstrap .select2-selection--multiple.input-lg{min-height:46px;border-radius:6px}.form-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.input-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice,.select2-container--bootstrap .select2-selection--multiple.input-lg .select2-selection__choice{font-size:18px;line-height:1.3333333;border-radius:4px;margin:9px 0 0 8px;padding:0 10px}.form-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.input-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field,.select2-container--bootstrap .select2-selection--multiple.input-lg .select2-search--inline .select2-search__field{padding:0 16px;font-size:18px;height:44px;line-height:1.3333333}.form-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.input-group-lg .select2-container--bootstrap .select2-selection--multiple .select2-selection__clear,.select2-container--bootstrap .select2-selection--multiple.input-lg .select2-selection__clear{margin-top:10px}.input-group-lg .select2-container--bootstrap .select2-selection.select2-container--open .select2-selection--single .select2-selection__arrow b,.select2-container--bootstrap .select2-selection.input-lg.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #999;border-width:0 5px 5px}.select2-container--bootstrap[dir=rtl] .select2-selection--single{padding-left:24px;padding-right:12px}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__rendered{padding-right:0;padding-left:0;text-align:right}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__clear{float:left}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__arrow{left:12px;right:auto}.select2-container--bootstrap[dir=rtl] .select2-selection--single .select2-selection__arrow b{margin-left:0}.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-search--inline,.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__choice,.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__placeholder{float:right}.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__choice{margin-left:0;margin-right:6px}.select2-container--bootstrap[dir=rtl] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.has-warning .select2-dropdown,.has-warning .select2-selection{border-color:#8a6d3b}.has-warning .select2-container--focus .select2-selection,.has-warning .select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #c0a16b;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #c0a16b;border-color:#66512c}.has-warning.select2-drop-active{border-color:#66512c}.has-warning.select2-drop-active.select2-drop.select2-drop-above{border-top-color:#66512c}.has-error .select2-dropdown,.has-error .select2-selection{border-color:#a94442}.has-error .select2-container--focus .select2-selection,.has-error .select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #ce8483;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #ce8483;border-color:#843534}.has-error.select2-drop-active{border-color:#843534}.has-error.select2-drop-active.select2-drop.select2-drop-above{border-top-color:#843534}.has-success .select2-dropdown,.has-success .select2-selection{border-color:#3c763d}.has-success .select2-container--focus .select2-selection,.has-success .select2-container--open .select2-selection{-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #67b168;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075),0 0 6px #67b168;border-color:#2b542c}.has-success.select2-drop-active{border-color:#2b542c}.has-success.select2-drop-active.select2-drop.select2-drop-above{border-top-color:#2b542c}.input-group>.select2-hidden-accessible:first-child+.select2-container--bootstrap>.selection>.select2-selection,.input-group>.select2-hidden-accessible:first-child+.select2-container--bootstrap>.selection>.select2-selection.form-control{border-bottom-right-radius:0;border-top-right-radius:0}.input-group>.select2-hidden-accessible:not(:first-child)+.select2-container--bootstrap:not(:last-child)>.selection>.select2-selection,.input-group>.select2-hidden-accessible:not(:first-child)+.select2-container--bootstrap:not(:last-child)>.selection>.select2-selection.form-control{border-radius:0}.input-group>.select2-hidden-accessible:not(:first-child):not(:last-child)+.select2-container--bootstrap:last-child>.selection>.select2-selection,.input-group>.select2-hidden-accessible:not(:first-child):not(:last-child)+.select2-container--bootstrap:last-child>.selection>.select2-selection.form-control{border-bottom-left-radius:0;border-top-left-radius:0}.input-group>.select2-container--bootstrap{display:table;table-layout:fixed;position:relative;z-index:2;width:100%;margin-bottom:0}.input-group>.select2-container--bootstrap>.selection>.select2-selection.form-control{float:none}.input-group>.select2-container--bootstrap.select2-container--focus,.input-group>.select2-container--bootstrap.select2-container--open{z-index:3}.input-group>.select2-container--bootstrap,.input-group>.select2-container--bootstrap .input-group-btn,.input-group>.select2-container--bootstrap .input-group-btn .btn{vertical-align:top}.form-control.select2-hidden-accessible{position:absolute !important;width:1px !important}@media (min-width:768px){.form-inline .select2-container--bootstrap{display:inline-block}}.fixed{position:absolute;left:100px;top:0}.collapsed{padding-right:0;border-width:0;margin-right:1px}.collapsed .bloc{display:none}.menu__button{position:fixed;z-index:1000;border:0}.select2-container{z-index:5000}.menu{min-height:100%;overflow:hidden}.menu,.infos{color:#000;position:relative;min-width:100px;width:99%;z-index:1000}.menu .table,.infos .table{color:black;font-size:12px}.menu .table .title,.infos .table .title{font-size:12px;font-weight:bold}.menu .list-group,.infos .list-group{padding:0;margin:0}.menu .list-group .list-group-item,.infos .list-group .list-group-item{border:1px solid #c7c5c5;padding:1px}.menu .list-group .list-group-item>*,.infos .list-group .list-group-item>*{border-width:0;height:auto;width:100%}.menu .list-group .list-group-item select,.infos .list-group .list-group-item select{font-weight:normal;max-width:100%}.menu .list-group .list-group-item .checkbox,.infos .list-group .list-group-item .checkbox{margin:0}.menu .list-group .list-group-item .checkbox label,.infos .list-group .list-group-item .checkbox label{display:flex;width:100%;justify-content:space-around;align-content:center}.menu .list-group .list-group-item .checkbox label input,.infos .list-group .list-group-item .checkbox label input{display:block;margin-left:-55px}.menu .list-group .list-group-item .checkbox label span,.infos .list-group .list-group-item .checkbox label span{margin:0;display:block}.menu .list-group .list-group-item:first-child,.infos .list-group .list-group-item:first-child{border-radius:0}.menu .list-group .list-group-item input[type=\"number\"],.infos .list-group .list-group-item input[type=\"number\"]{font-weight:normal;padding-left:15px}.menu .list-group .list-group-item.inline>label,.infos .list-group .list-group-item.inline>label{display:flex !important;margin-top:2px !important;margin-bottom:2px !important}.menu label,.infos label{font-size:12px}.menu .list-group.actions.collapsed,.infos .list-group.actions.collapsed{opacity:0;transition:opacity .4s ease-in;-ms-transition:opacity .4s ease-in;-moz-transition:opacity .4s ease-in;-webkit-transition:opacity .4s ease-in;display:none}.menu .list-group.actions,.infos .list-group.actions{opacity:1;transition:opacity .4s ease-in;-ms-transition:opacity .4s ease-in;-moz-transition:opacity .4s ease-in;-webkit-transition:opacity .4s ease-in}.menu .drag-element,.infos .drag-element{position:fixed;z-index:100000;opacity:.5}.menu .panel-body{padding:0}.menu .panel-body .infos{margin-top:40px;text-align:left}.menu .panel-body .infos li{border-color:white}.menu .bloc{margin-bottom:20px;padding:0;color:black}.menu .bloc.last{margin-bottom:2px;padding-bottom:0}.menu .bloc .bloc-content{max-height:1500px;transition:max-height .2s;overflow:initial}.menu .bloc table{text-align:left;outline:.5px solid lightgray}.menu .bloc table td{padding-left:10px}.menu .bloc.collapsed{margin:0;padding:0;margin-bottom:30px}.menu .bloc.collapsed .bloc-content{max-height:0 !important;overflow:hidden}.menu .bloc.collapsed .collapser.collapsable{border-bottom:.5px solid lightgray}.menu .bloc.collapsed .collapser.collapsable .glyphicon-chevron-down{display:none}.menu .bloc.collapsed .collapser.collapsable .glyphicon-chevron-up{display:block}.menu .bloc.hide-title:not(.collapsable) .collapser{display:none}.menu .bloc .collapser{background-color:rgba(228,226,226,0.589);border-top:.5px solid lightgray;border-right:.5px solid lightgray;border-left:.5px solid lightgray;width:100%;padding:5px;height:30px;display:block;text-align:right}.menu .bloc .collapser.collapsable{cursor:pointer}.menu .bloc .collapser.collapsable .glyphicon-chevron-up{display:none}.menu .bloc .collapser.collapsable .bloc-title:hover{color:black;text-decoration:underline}.menu .bloc .collapser:not(.collapsable) .glyphicon-chevron-up,.menu .bloc .collapser:not(.collapsable) .glyphicon-chevron-down{display:none}.menu .bloc .collapser .bloc-title{float:left;color:grey;margin-left:5px;font-family:\"Helvetica\";text-transform:uppercase}.menu .bloc-informations{font-size:12px}.menu .bloc-informations td.title{max-width:30%;width:10%;color:black}.menu .bloc-informations td.text{width:90%;word-break:break-word}.menu .bloc-informations td.text td{padding:2px;word-break:break-all}.menu .bloc-informations .table{margin-bottom:0;font-size:12px}.menu .bloc-informations .table-bordered td{word-break:normal !important}.menu .bloc-informations .table-bordered tr:first-child td{font-weight:bold}.menu .fullscreenable{cursor:pointer;color:#337ab7;text-decoration:none;background-color:transparent}.menu .fullscreenable:hover{color:#23527c;text-decoration:underline}.menu .bloc-controls .list-group-item.inline label{display:flex;align-items:center;justify-content:center;margin-top:2px}.menu .bloc-controls .list-group-item.inline label>select,.menu .bloc-controls .list-group-item.inline label>input,.menu .bloc-controls .list-group-item.inline label>.select2{width:75% !important;overflow:hidden;padding-right:3px}.menu .bloc-controls .list-group-item.inline label>.bloc-control-label{display:block;padding-left:15px;text-align:left;width:25%;font-weight:bold;text-overflow:ellipsis;overflow:hidden}.menu .bloc-controls .list-group-item.inline label .search-field{width:75%;padding-right:3px}.menu .bloc-controls .btn-group{display:flex;justify-content:center}.menu .bloc-controls .list-group-item[type=\"search\"] label{display:inline-block;font-weight:normal;padding:0;margin:0}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>*{padding-top:0;padding-bottom:0;height:30px}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field .addon-left{border:0;padding:0 5px;font-size:10px}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field .addon-left i.select-all{font-size:10px;padding:1px 2px}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>.input-group-addon{background-color:white;border-left:0}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>input{border-right:0}.menu .bloc-controls .list-group-item[type=\"search\"] label>.search-field>input:focus{box-shadow:none;border-color:#ccc}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table-container{max-height:300px;overflow:auto;position:relative}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table{table-layout:fixed;padding:0;margin:0;overflow:hidden !important}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table td{overflow:hidden;text-overflow:ellipsis}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table>tbody>tr>td{padding-top:4px;padding-bottom:1px}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td{font-size:60%;text-align:center;padding:1px;cursor:pointer;color:rgba(128,128,128,0.54)}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-icons{padding-left:10px;display:inline-block}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-title{font-size:11px;font-weight:bold;display:inline-block}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-sub-container{display:flex;justify-content:center;align-items:center}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows>td .header-sub-container:hover{padding:0;outline:1px solid grey}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows .disabled{color:rgba(128,128,128,0.54)}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows .active{background-color:white;color:black}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.header-arrows i{display:block}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.row-table{cursor:pointer;height:25px}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.row-table:hover{outline:1px solid lightgrey}.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.selected,.menu .bloc-controls .list-group-item[type=\"search\"] label .search-table.table tr.selected:hover{outline:1px solid black;background-color:lightgrey}.menu .bloc-controls .btn-default{text-overflow:ellipsis;overflow:hidden}.select2-container--bootstrap .select2-results>.select2-results__options{max-height:260px;overflow-y:auto}.select2-container--bootstrap .select2-results>.select2-results__options .select2-results__option{padding:4px 6px;font-size:13px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/multi-curves.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".multi-curve-container{position:relative;height:100%}.multi-curve-container .multi-curve-controls{height:35px}.multi-curve-container .multi-curve-curves{overflow-y:auto;overflow-x:visible;position:relative;height:calc(100% - 35px)}.multi-curve-container .graph-inner{height:100%}.curve-container{position:relative}.curve-container .switch-buttons{position:absolute;display:flex;left:10px;bottom:20px;flex-direction:column;z-index:1000}.curve-container .switch-buttons .button{color:#777;display:inline-block;padding:2px;border:0;font-size:12px;height:16px;width:16px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/pattern-legend.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".graph-pattern-legend{width:100%;padding:0;display:flex}.graph-pattern-legend>.row{margin:0}.graph-pattern-legend .graph-inner{padding:0}.graph-pattern-legend .pattern-legend{font-size:10px;padding:0;overflow:hidden;overflow-y:auto;padding-top:5px;padding-bottom:10px}.graph-pattern-legend .pattern-legend span{text-overflow:ellipsis;white-space:nowrap;overflow:hidden;display:inline-block;max-width:100%;color:white;opacity:.9;border:1px solid black;margin-right:2px;padding:1px 4px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/resizable.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".resizable{position:relative}.resizable .x-resizing-right{position:absolute;top:0;right:0;width:5px;z-index:1100;background-color:transparent;bottom:0;cursor:ew-resize}.resizable .x-resizing-right:hover{border-right:2px solid black}.resizable .y-resizing-bottom{position:absolute;bottom:0;right:0;height:10px;background-color:transparent;left:0;cursor:ns-resize}.resizable .y-resizing-bottom:hover{border-bottom:2px solid black}.resizable .x-resizing{cursor:ew-resize}.resizable .x-resizing .x-resizing-right{border-right:2px solid black}.resizable .y-resizing{cursor:ns-resize}.resizable .y-resizing .y-resizing-bottom{border-bottom:2px solid black}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/scopes.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".tsplorer-scopes{display:flex;justify-content:center;align-items:center;width:100%;margin-bottom:10px;border-bottom:1px solid rgba(128,128,128,0.8)}.tsplorer-scopes .tabs{z-index:1000;justify-content:center;margin:0;padding-left:10px;list-style-type:none;line-height:28px;max-height:28px;overflow:hidden;font-size:14px;display:inline-block}.tsplorer-scopes .tabs .glyphicon{visibility:hidden}.tsplorer-scopes .tabs .tab{float:left;cursor:pointer;max-height:28px}.tsplorer-scopes .tabs .tab a{position:relative;top:0;left:0;float:left;height:170px;min-width:100px;margin:5px -10px 0;border-top-right-radius:20px 170px;border-top-left-radius:20px 90px;padding:0 30px 0 30px;background:#e6e6e6;color:#1a1a1a;box-shadow:0 10px 20px rgba(0,0,0,0.5);text-decoration:none;text-overflow:ellipsis;white-space:nowrap;overflow:hidden}.tsplorer-scopes .tabs .tab a:hover{background:#ddd}.tsplorer-scopes .tabs .tab.active i{visibility:visible;font-size:90%;margin-left:-2px}.tsplorer-scopes .tabs .tab.active i:hover{color:black;font-size:95%}.tsplorer-scopes .tabs .tab.active a{background:#fff;z-index:888;border-top:.5px solid grey !important}.tsplorer-scopes .tabs .plus{position:relative !important;left:5px !important;min-width:30px !important;max-width:30px !important;height:16px !important;padding:0 !important;border-top-right-radius:0 0 !important;border-top-left-radius:0 0 !important;-webkit-transform:skew(20deg, 0deg) !important;border-radius:3px !important;margin-top:5px;margin-left:-8px;font-size:70%;display:inline-block;z-index:1}.tsplorer-scopes .tabs .plus a:hover{background:#ddd}.tsplorer-scopes .tabs .ion-close-circled{position:absolute;top:-2px;right:22px;color:#9a9a9a;font-size:15px;font-weight:bold;border-radius:100%;clear:both}.tsplorer-scopes .tabs .ion-close-circled:hover{color:#cc3333}.tsplorer-scopes .new-scope-name{margin-left:20px}.d3-context-menu hr{padding:0;margin:0}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/ts-legend.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".d3-context-menu-theme.with-backdrop{position:fixed;right:0;top:0;bottom:0;left:0;background-color:transparent;border:0}.d3-context-menu-theme.with-backdrop ul{border:1px solid #d4d4d4;display:block;position:fixed;background-color:#f2f2f2}.color-legend{min-width:15px;height:15px;border-radius:50%;margin-top:2px;border:1px solid black;background:none;cursor:pointer}.sync-container .sync-label{margin-left:5px}.sync-container .glyphicon{font-size:12px}.line-appearances-widget{width:120px;margin:auto}.line-appearances-widget ul{margin-bottom:0}.line-appearances-widget li{padding:0 10px;border-color:#f0efef0e 0e !important;border-width:1px}.line-appearances-widget li:hover{z-index:100000;border:1px solid blue;cursor:pointer}.visibility-legend{display:inline-block;padding-left:3px;cursor:pointer}.visibility-legend .glyphicon-eye-close{display:initial;color:grey}.visibility-legend .glyphicon-eye-open{display:none}.visibility-legend.selected .glyphicon-eye-open{display:initial}.visibility-legend.selected .glyphicon-eye-close{display:none}.color-picker-widget{text-align:center}.color-picker-widget .colorPicker{display:none !important}.color-picker-widget .sp-container.sp-flat{background:none}.ts-legend-settings{position:absolute;top:0;left:0;right:0;border:1px solid lightgrey;background-color:white;position:fixed;min-width:200px;z-index:100000}.ts-legend-settings .close{margin-right:10px}.ts-legend-settings label{display:inline-block;padding-left:5px}.graph-legend-container{position:relative;display:flex}.graph-legend-container .ts-legend{position:relative;padding:0 0 0 10px;text-align:left;overflow-y:auto;flex-direction:column}.graph-legend-container .ts-legend .ts-legend-field{margin:2px;display:flex;overflow:hidden}.graph-legend-container .ts-legend .ts-legend-field:hover,.graph-legend-container .ts-legend .ts-legend-field.highlighted{font-weight:bold}.graph-legend-container .ts-legend .ts-legend-field:hover .color-legend,.graph-legend-container .ts-legend .ts-legend-field.highlighted .color-legend{border:3px solid black}.graph-legend-container .ts-legend .ts-legend-field:hover:hover,.graph-legend-container .ts-legend .ts-legend-field.highlighted:hover{background-color:rgba(211,211,211,0.568)}.graph-legend-container .ts-legend .ts-legend-sequences{height:calc(100% - 30px);overflow-y:auto}.graph-legend-container .ts-legend .sp-cancel{display:none}.graph-legend-container .ts-legend .sync-container,.graph-legend-container .ts-legend .reset-container,.graph-legend-container .ts-legend .remove-container{text-overflow:ellipsis;overflow:hidden;padding-right:3px;white-space:nowrap}.graph-legend-container .ts-legend .text-legend{white-space:nowrap;margin-left:4px;text-overflow:ellipsis;overflow:hidden}.graph-legend-container .ts-legend .ts-legend-buttons{display:flex}.graph-legend-container .graph-inner{display:inline-block;padding:0}.graph-legend-container .is-sync{margin-right:5px;padding-top:5px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/tsplorer.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = "html .d3-context-menu{position:fixed;z-index:10000}.tsplorer-container{display:flex;height:100%;text-align:center;overflow-x:hidden}.tsplorer-container.in-loading{cursor:wait !important;pointer-events:all !important}.tsplorer-container.in-loading *{pointer-events:none !important;cursor:wait !important}.tsplorer-container>.tsplorer-controls{padding:5px;height:100%;overflow:auto}.tsplorer-container>.tsplorer-controls .different-class{color:red !important}.tsplorer-container>.tsplorer-controls .bloc-controls[bloc-id=\"patterns\"] .search-table-container{height:125px}.tsplorer-container>.tsplorer-controls .btn.selected{border-color:#337ab7 !important;outline-color:#337ab7 !important;color:white !important;background-color:rgba(51,122,183,0.73) !important;outline:none !important;border-radius:0;margin:0}.tsplorer-container>.tsplorer-controls [title~=Patterns] tr.selected,.tsplorer-container>.tsplorer-controls [title~=Sequences] tr.selected,.tsplorer-container>.tsplorer-controls [title~=Patterns] tr.selected>td,.tsplorer-container>.tsplorer-controls [title~=Sequences] tr.selected>td{border-color:#337ab7 !important;outline-color:#337ab7 !important;color:white !important;background-color:rgba(51,122,183,0.5) !important;outline:none !important}.tsplorer-container>.tsplorer-controls [title~=Patterns] tr:hover,.tsplorer-container>.tsplorer-controls [title~=Sequences] tr:hover{color:#555;text-decoration:none;background-color:#f5f5f5 !important}.tsplorer-container>.tsplorer-controls [title~=Patterns] tr.in-graph,.tsplorer-container>.tsplorer-controls [title~=Sequences] tr.in-graph{background-color:rgba(128,128,128,0.3) !important}.tsplorer-container>.tsplorer-controls [title~=Patterns] tr .different-class,.tsplorer-container>.tsplorer-controls [title~=Sequences] tr .different-class{color:red !important}.tsplorer-container .curve{overflow:hidden}.tsplorer-container .delete-icon{color:red}.tsplorer-container .tsplorer-controls,.tsplorer-container .tsplorer-graph,.tsplorer-container .tsplorer-switch-graph,.tsplorer-container .tsplorer-graph-main,.tsplorer-container .tsplorer-scopes-list{display:inline-block;position:relative}.tsplorer-container .tsplorer-graph-main{overflow-x:hidden}.tsplorer-container .tsplorer-graph-inner{height:calc(100% - 40px)}.tsplorer-container .in-draging{pointer-events:none}.tsplorer-container .in-draging .curve.child.in-draging{outline:1px solid black;transform:scale(.99);margin:0;pointer-events:none}.tsplorer-container .in-draging *{pointer-events:none}.tsplorer-container .tsplorer-heatmap{width:100%;height:100%}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/LIG/less/zoom-controls.less":[function(require,module,exports){
(function() { var head = document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css';var css = ".zoom-controls{position:absolute;right:5%;top:20px;z-index:2000;padding-top:2px;padding-right:3px;padding-left:3px;background-color:white;border:1px solid grey;border-radius:3px}";if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style);}())
},{}],"/home/davisp/projects/ikats_lig/hmi/src/bundle/TSplorer.bundle.js":[function(require,module,exports){
'use strict';

window.TSplorer = require("/home/davisp/projects/ikats_lig/hmi/src/LIG/js/TSplorer.js");

},{"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/TSplorer.js":"/home/davisp/projects/ikats_lig/hmi/src/LIG/js/TSplorer.js"}]},{},["/home/davisp/projects/ikats_lig/hmi/src/bundle/TSplorer.bundle.js"])

//# sourceMappingURL=TSplorer.bundle.js.map
