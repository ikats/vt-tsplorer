# IKATS Viztool vt-tsplorer

## Definition

TSplorer is made to be wrapped by Ikats framework.
It accepts 'tsporer' input format.

## How to use

It has to be used with "rhm" type ("RHM" operator output).

## User interface

- fullscreen

### Curve Mode

#### SideMenu

- Class
- Variable
- Patterns
- Sequences

#### VizTool

##### Container management

- add container : clicking add container
- remove container : mouse left (context menu) then click remove container, click on the button delete
- clear container : mouse left (context menu) then click clear container, click on the button clear
- move container : clicking arrows (up or down) on left of each container
- sync / unsync container: click on the button sync / unsync

##### Zooming / panning

- zoom in : using dble click, mouse wheel, ctrl + mouse, modify minimap
- zoom out : using mouse wheel, modify minimap
- pan : using click + move mouse

##### Display manageement

- activate/unactivate pattern: click on pattern on the left side of the container
- sequence visibility: click on the eye on the right container
- draw settings

### Heatmap Mode

#### SideMenu

- Class
- Patterns
- Sequences

#### VizTool

- zoom in
- zoom out
- add in a container
- remove from a container
- create a new container and add it

## Internal dependencies
- ...

## External depencies
- jquery
- d3
- lodash