# Changelog

## [3.62] - 2018-10-24

### Changed

- fix colorManager to improve color management

## [3.61] - 2018-10-15

### Changed

- introduce fitBottom option inside SideMenu

## [3.6] - 2018-10-01

### Changed

#### IkatsDataProvider

- manage paired variable inside ikats

#### TSplorer

- Able to disable scope management using an option called manageScopes ( default true, false in ikats case )

## [3.51] - 2018-08-29

### Changed

#### D3CanvasCurve

- set timestamp as default mode
- show tooltip as default
- add an option to wait before displaying the tool

## [3.5] - 2018-08-03

### Added

- Sidemenu (left)

### Changed

- Rename internal components
- Disable tooltip
- Improve keybindings
- Be able to change curve color
- Be able to change curve layout

## [3.4] - 2018-01-19

### Changed

- Make delivery as a bundle (browserify)

## [3.3] - 2018-01-17

### Added

- LIGViz abstract class for subviz
- Changelog file
- LigLogger to manage logging, able to enable it using global variable LIG_DEBUG

### Changed

- Change folder organization in order to follow delivery needs.

#### ProtoWithLegend

- Change sync button layout (not anymore a checkbox)
- Remove public method enableThrowEvent()
- Change typos
- Propagate options assign to child graph

#### ProtoWithPatternLegend

- Remove public method enableThrowEvent()
- Change typos
- Propagate options assign to child graph

#### ProtoHeatMap

- Add reset method to reset internal context to initial state
- Improve details rendering from the hover square
- Hide ts names from horizontal axis if there are too many ts
- Be able to zoom with double click, mouse wheel, double taps
- Add minimap to improve user experience (area selection, resizing)
- Be able to zoom using ctrl + select area (brush)
- Add public method detectHover to know which ts / patterns is on hover

#### D3CanvasCurve

- Add long ticks from horizontal axis to help user to estimate the number of event, to be refreshed using the public method renderHorizontalAxis.
- Harmonize ticks from y axis to follow breakpoints (if there is)
- Add logging using an LigLogger instance, enabled using debugMode option
- Add a dispatch zoom flag to know if we dispatch or not the zoom event
- Refactor context computation
- Add a spinner display during loading data

#### ProtoMultiCurves

- Add public method reset() to reset internal context to initial state
- Add public method on() to manage viz events binding
- Add an option graphConstructor, which be used to use a custom graph constructor

#### FullSelectionControls

- Rename events selectionChange, datasetLoaded, datasetChange, tsDragStart, tsDrag, tsDragEnd to selectionchange, datasetload, datasetchange, sequencedragstart, sequencedragmove, sequencedragend
- Remove obsolete events patternDragStart, patternDrag, patternDragEnd
- Remove public method getInitialDragBehaviorFor()
- Add public method container() to set/ get widget container
- Merge public methods setSelection() and getSelection() to public selection()
- Improve layout rendering in adjusting height with the height available
- Add new option to be able to hide/show class field

#### IkatsDataProvider

- Add logging using an LigLogger instance
- Add a cache for the metadata fetching
- Get table data async
- Add controls on metadata (metric, ikats_start_date, ikats_end_date, qual_nb_points, qual_min_value, qual_max_value) and log an error if one is missed

#### FullScreenApi

- Bind fullscreenchange event with a callback that removes fullscreen class and unbind event, if state is not in fullscreen.
- Fix css to force fullscreen to fit 100% width and 100% height of the screen

#### TSplorer

- Use resize obsever lib to detect size change and redraw viz
- Harmonize y axis using public method getSharedMinMax()
- Improve scope management
- Hide scopes in ikats mode
- Improve rendering
- Add a spinner display during loading data
